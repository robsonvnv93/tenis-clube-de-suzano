import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { WebServiceProvider } from '../../providers/web-service/web-service';


@IonicPage()
@Component({

	selector: 'page-noticia',
	templateUrl: 'noticia.html',

})


export class NoticiaPage {

	responseData: any;
	noticia		= { "imagem": "", "titulo": "", "texto": "" };
	carregado	= 0;

	constructor(

		public navCtrl: NavController,
		public wsprovider: WebServiceProvider,
		public navParams: NavParams
	
	) {

		this.wsprovider.ShowNoticia(this.navParams.get('id')).then((response) => {

			this.responseData = response;
			if(this.responseData.result == true) {

				this.noticia	= { "imagem": this.responseData.imagem, "titulo": this.responseData.titulo, "texto": this.responseData.texto };
				this.carregado	= 1;

			} else {
				
				this.carregado = -1;

			}

		});

	}



}
