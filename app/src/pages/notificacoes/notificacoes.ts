import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


// Inclusão do arquivo de consulta na API do app.
import { WebServiceProvider } from '../../providers/web-service/web-service';


// Inclusão das páginas que esta página utiliza.
import { SemInternetPage } from '../sem-internet/sem-internet';
import { NoticiaPage } from '../noticia/noticia';
import { EventoPage } from '../evento/evento';
import { NotificacaoPage } from '../notificacao/notificacao';


@IonicPage()
@Component({

	selector: 'page-notificacoes',
	templateUrl: 'notificacoes.html',

})
export class NotificacoesPage {

  // Variaveis que a classe utiliza com valores iniciais definidos.
	NotificacoesLimite	= 5;
	NotificacoesTotal	= -1;
	NotificacoesCount	= 0;
	NotificacoesPage	= 1;
	totalPage			= 0;
	Notificacoes		= [];

	
	// Variaveis que a classe utiliza com definição do tipo de dados que serão definidos.
	responseDataNotificacoes:	any;
	responseDataStatus:			any;
	NotificacoesItens:			any;

	
	// Função construtora da classe.
	constructor (

		public navCtrl: NavController,
		public navParams: NavParams,
		public wsprovider: WebServiceProvider,

	) {

		// Função para carregar as primeiras Notificacoes quando a página é aberta.
		this.wsprovider.GetNotificacoes(this.NotificacoesPage, this.NotificacoesLimite).then((response) => {

			this.responseDataNotificacoes = response;
			if(this.responseDataNotificacoes.result == true) {

				if(this.responseDataNotificacoes.todas >= 1) {

					this.NotificacoesTotal	= this.responseDataNotificacoes.todas;
					
					this.NotificacoesCount	= this.responseDataNotificacoes.total;
					this.NotificacoesItens	= this.responseDataNotificacoes.itens;
					for (let i = 0; i < this.NotificacoesCount; i++) {

						let n = { "log_id": this.NotificacoesItens[i].log_id, "notificacao_id": this.NotificacoesItens[i].notificacao_id, "tipo": this.NotificacoesItens[i].tipo, "item_id": this.NotificacoesItens[i].item_id, "data_notificacao": this.NotificacoesItens[i].data_notificacao, "titulo": this.NotificacoesItens[i].titulo, "conteudo": this.NotificacoesItens[i].conteudo, "aberto": this.NotificacoesItens[i].aberto };
						this.Notificacoes.push(n);

					}


					this.totalPage = this.NotificacoesCount;

				} else {

					this.NotificacoesTotal = 0;

				}

			} else {

				this.NotificacoesTotal = -2;

			}

		});

	}


	// Função para carregar mais Notificacoes ao rolar a pagina até o final.
	doInfinite(infiniteScroll) {

		if(this.wsprovider.CheckInternet() == '1') {

			this.NotificacoesPage	= (this.NotificacoesPage + 1);
			setTimeout(() => {

				this.wsprovider.GetNotificacoes(this.NotificacoesPage, this.NotificacoesLimite).then((response) => {

					this.responseDataNotificacoes = response;
					if(this.responseDataNotificacoes.result == true) {

						if(this.responseDataNotificacoes.total >= 1) {

							this.NotificacoesCount	= this.responseDataNotificacoes.total;
							this.NotificacoesItens	= this.responseDataNotificacoes.itens;

							for (let i = 0; i < this.NotificacoesCount; i++) {

								let n = { "log_id": this.NotificacoesItens[i]['log_id'], "notificacao_id": this.NotificacoesItens[i]['notificacao_id'], "tipo": this.NotificacoesItens[i]['tipo'], "item_id": this.NotificacoesItens[i]['item_id'], "data_notificacao": this.NotificacoesItens[i]['data_notificacao'], "titulo": this.NotificacoesItens[i]['titulo'], "conteudo": this.NotificacoesItens[i]['conteudo'], "aberto": this.NotificacoesItens[i]['aberto'] };

								this.Notificacoes.push(n);

							}


							this.totalPage = (this.totalPage + this.NotificacoesCount);

						}

					} else {

						this.NotificacoesTotal = -2;

					}

					infiniteScroll.complete();

				});

			}, 2000);

		} else {

			this.navCtrl.setRoot(SemInternetPage);

		}

	}


	// Função para abrir a página com exibição de mais detalhes do item selecionado dentro da página.
	ShowItem(indice, log_id, tipo, notificacao_id, item_id) {

		if(this.Notificacoes[indice].aberto == 0) {

			this.wsprovider.ChangeNotificacaoStatus(log_id).then((response) => {
				
				this.responseDataStatus = response;
				if(this.responseDataStatus.result == true) {
					
					this.Notificacoes[indice].aberto = 1;
				
				}

				if(tipo == 2) {

					this.navCtrl.push(NoticiaPage, { id: item_id });

				} else if(tipo == 3) {

					this.navCtrl.push(EventoPage, { id: item_id });

				} else {

					this.navCtrl.push(NotificacaoPage, { id: notificacao_id });
					
				}

			});

		} else {

			if(tipo == 2) {

				this.navCtrl.push(NoticiaPage, { id: item_id });

			} else if(tipo == 3) {

				this.navCtrl.push(EventoPage, { id: item_id });

			} else {

				this.navCtrl.push(NotificacaoPage, { id: notificacao_id });
				
			}

		}


	}

}
