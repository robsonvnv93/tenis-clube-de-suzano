import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// Inclusão do arquivo de consulta na API do app.
import { WebServiceProvider } from '../../providers/web-service/web-service';


@IonicPage()
@Component({

	selector: 'page-cupons',
	templateUrl: 'cupons.html',

})



export class CuponsPage {

	carregado	= 0;
	pagina		= { "titulo": "", "texto": "" };
	responseData:	any;

	constructor(

		public navCtrl: NavController,
		public wsprovider: WebServiceProvider,
		public navParams: NavParams

	) {

		this.wsprovider.ShowPagina(1).then((response) => {

			this.responseData = response;
			if(this.responseData.result == true) {

				this.pagina = { "titulo": this.responseData.titulo, "texto": this.responseData.texto };
				this.carregado = 1;

			} else {

				this.carregado	= -1;

			}

		});
	
	}


}
