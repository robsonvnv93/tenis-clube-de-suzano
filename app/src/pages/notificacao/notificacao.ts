import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { WebServiceProvider } from '../../providers/web-service/web-service';

@IonicPage()
@Component({
	
	selector: 'page-notificacao',
	templateUrl: 'notificacao.html',

})


export class NotificacaoPage {

	responseData: any;
	notificacao		= { "data_notificacao": "", "titulo": "", "conteudo": "" };
	carregado	= 0;

	constructor(

		public navCtrl: NavController,
		public wsprovider: WebServiceProvider,
		public navParams: NavParams
	
	) {

		this.wsprovider.ShowNotificacao(this.navParams.get('id')).then((response) => {

			this.responseData = response;
			if(this.responseData.result == true) {

				this.notificacao	= { "data_notificacao": this.responseData.data_notificacao, "titulo": this.responseData.titulo, "conteudo": this.responseData.conteudo };
				this.carregado	= 1;

			} else {
				
				this.carregado = -1;

			}

		});

	}

}