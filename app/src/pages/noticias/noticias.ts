import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


// Inclusão do arquivo de consulta na API do app.
import { WebServiceProvider } from '../../providers/web-service/web-service';


// Inclusão das páginas que esta página utiliza.
import { SemInternetPage } from '../sem-internet/sem-internet';
import { NoticiaPage } from '../noticia/noticia';


@IonicPage()
@Component({

	selector: 'page-noticias',
	templateUrl: 'noticias.html',

})


// Inicio da classe que a página utiliza.
export class NoticiasPage {


	// Variaveis que a classe utiliza com valores iniciais definidos.
	NoticiasLimite	= 5;
	NoticiasTotal	= -1;
	NoticiasCount	= 0;
	NoticiasPage	= 1;
	totalPage		= 0;
	Noticias		= [];

	
	// Variaveis que a classe utiliza com definição do tipo de dados que serão definidos.
	responseDataNoticias:	any;
	NoticiasItens:			any;

	
	// Função construtora da classe.
	constructor (

		public navCtrl: NavController,
		public navParams: NavParams,
		public wsprovider: WebServiceProvider,

	) {

		// Função para carregar as primeiras noticias quando a página é aberta.
		this.wsprovider.GetNoticias(this.NoticiasPage, this.NoticiasLimite).then((response) => {

			this.responseDataNoticias = response;
			if(this.responseDataNoticias.result == true) {

				if(this.responseDataNoticias.todas >= 1) {

					this.NoticiasTotal	= this.responseDataNoticias.todas;
					
					this.NoticiasCount	= this.responseDataNoticias.total;
					this.NoticiasItens	= this.responseDataNoticias.itens;
					for (let i = 0; i < this.NoticiasCount; i++) {

						let n = { "noticia_id": this.NoticiasItens[i].noticia_id, "imagem": this.NoticiasItens[i].imagem, "data_noticia": this.NoticiasItens[i].data_noticia, "titulo": this.NoticiasItens[i].titulo, "resumo": this.NoticiasItens[i].resumo };
						this.Noticias.push(n);

					}


					this.totalPage = this.NoticiasCount;

				} else {

					this.NoticiasTotal = 0;

				}

			} else {

				this.NoticiasTotal = -2;

			}

		});

	}


	// Função para carregar mais noticias ao rolar a pagina até o final.
	doInfinite(infiniteScroll) {

		if(this.wsprovider.CheckInternet() == '1') {

			this.NoticiasPage	= (this.NoticiasPage + 1);
			setTimeout(() => {

				this.wsprovider.GetNoticias(this.NoticiasPage, this.NoticiasLimite).then((response) => {

					this.responseDataNoticias = response;
					if(this.responseDataNoticias.result == true) {

						if(this.responseDataNoticias.total >= 1) {

							this.NoticiasCount	= this.responseDataNoticias.total;
							this.NoticiasItens	= this.responseDataNoticias.itens;

							for (let i = 0; i < this.NoticiasCount; i++) {

								let n = { "noticia_id": this.NoticiasItens[i]['noticia_id'], "imagem": this.NoticiasItens[i]['imagem'], "data_noticia": this.NoticiasItens[i]['data_noticia'], "titulo": this.NoticiasItens[i]['titulo'], "resumo": this.NoticiasItens[i]['resumo'] };

								this.Noticias.push(n);

							}


							this.totalPage = (this.totalPage + this.NoticiasCount);

						}

					} else {

						this.NoticiasTotal = -2;

					}

					infiniteScroll.complete();

				});

			}, 2000);

		} else {

			this.navCtrl.setRoot(SemInternetPage);

		}

	}


	// Função para abrir a página com exibição de mais detalhes da noticia selecionada dentro da página.
	ShowNoticia(noticia_id) {

		this.navCtrl.push(NoticiaPage, { id: noticia_id });

	}

}
