import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { WebServiceProvider } from '../../providers/web-service/web-service';

@IonicPage()
@Component({

	selector: 'page-grade',
	templateUrl: 'grade.html',

})


export class GradePage {

	responseData:	any;
	Horarios:		any;
	Atividade	= { "categoria": "", "nome": "", "descricao": "" };
	carregado	= 0;
	Total		= 0;

	constructor(

		public navCtrl: NavController,
		public wsprovider: WebServiceProvider,
		public navParams: NavParams
	
	) {

		this.wsprovider.ShowGrade(this.navParams.get('id')).then((response) => {

			this.responseData = response;
			console.log(this.responseData);
			if(this.responseData.result == true) {

				this.Atividade	= { "categoria": this.responseData.categoria, "nome": this.responseData.nome, "descricao": this.responseData.descricao };

				this.Total		= this.responseData.total;
				this.Horarios	= this.responseData.horarios;
				this.carregado	= 1;

			} else {
				
				this.carregado = -1;

			}

		});

	}

}
