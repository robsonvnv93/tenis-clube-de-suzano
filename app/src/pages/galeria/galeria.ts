import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { WebServiceProvider } from '../../providers/web-service/web-service';


@IonicPage()
@Component({

	selector: 'page-galeria',
	templateUrl: 'galeria.html',

})


export class GaleriaPage {

	responseData:	any;
	GaleriaItens:	any;

	carregado		= 0;
	Galeria			= { "capa": "", "titulo": "", "texto": "" };
	GaleriaTotal	= 0;


	constructor(

		public navCtrl: NavController,
		public wsprovider: WebServiceProvider,
		public navParams: NavParams
	
	) {

		this.wsprovider.ShowGaleria(this.navParams.get('id')).then((response) => {

			this.responseData = response;
			if(this.responseData.result == true) {

				this.Galeria 		= { "capa": this.responseData.capa, "titulo": this.responseData.titulo, "texto": this.responseData.texto };
				this.GaleriaTotal	= this.responseData.total;
				this.GaleriaItens	= this.responseData.itens;
				this.carregado		= 1;

			} else {
				
				this.carregado		= -1;

			}

		});

	}

}
