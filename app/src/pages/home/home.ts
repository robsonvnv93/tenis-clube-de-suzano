import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';


// Inclusão do arquivo de consulta na API do app.
import { WebServiceProvider } from '../../providers/web-service/web-service';


// Inclusão das páginas que esta página utiliza.
import { SemInternetPage } from '../sem-internet/sem-internet';

import { NoticiaPage } from '../noticia/noticia';

import { NoticiasPage } from '../noticias/noticias';
import { EventosPage } from '../eventos/eventos';
import { GradesPage } from '../grades/grades';
import { GaleriasPage } from '../galerias/galerias';
import { MapaPage } from '../mapa/mapa';
import { ContatoPage } from '../contato/contato';
import { CuponsPage } from '../cupons/cupons';


@Component({

	selector: 'page-home',
	templateUrl: 'home.html'

})


// Inicio da classe que a página utiliza.
export class HomePage {


	// Variaveis que a classe utiliza com valores iniciais definidos.
	BannersTotal	= 0;
	NoticiasTotal	= -1;
	

	// Variaveis que a classe utiliza com definição do tipo de dados que serão definidos.
	responseDataBanners:	any;
	Banners:				any;
	responseDataNoticias:	any;
	Noticias:				any;
	pages:					any;

	
	// Função construtora da classe.
	constructor (

		public navCtrl: NavController,
		public navParams: NavParams,
		public wsprovider: WebServiceProvider

	) {


		// Definição das paginas para navegação.
		this.pages = {

			noticias:	NoticiasPage,
			eventos:	EventosPage,
			grades:		GradesPage,
			galerias:	GaleriasPage,
			mapa:		MapaPage,
			contato:	ContatoPage,
			cupons:		CuponsPage

		};

		// Função para carregar os banners quando a página é aberta.
		this.wsprovider.GetBanners().then((response) => {

			this.responseDataBanners = response;
			if(this.responseDataBanners.result == true) {

				this.BannersTotal	= this.responseDataBanners.total;
				this.Banners		= this.responseDataBanners.itens;

			}

		});


		// Função para carregar as primeiras noticias quando a página é aberta.
		this.wsprovider.GetNoticias(1, 3).then((response) => {

			this.responseDataNoticias = response;
			if(this.responseDataNoticias.result == true) {

				this.NoticiasTotal	= this.responseDataNoticias.total;
				this.Noticias		= this.responseDataNoticias.itens;

			} else {

				this.NoticiasTotal = -2;

			}

		});

	}


	// Função para abrir a página com exibição de mais detalhes da noticia selecionada dentro da página.
	ShowNoticia(noticia_id) {

		this.navCtrl.push(NoticiaPage, { id: noticia_id });

	}


	// Função de ação de navegação dos botões da página.
	GoToPage(pagina) {

		if(this.wsprovider.CheckInternet() === '1') {

			this.navCtrl.setRoot(this.pages[pagina]);
		
		} else {
			
			this.navCtrl.setRoot(SemInternetPage);

		}

	}

}