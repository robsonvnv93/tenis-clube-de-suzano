import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


// Inclusão do arquivo de consulta na API do app.
import { WebServiceProvider } from '../../providers/web-service/web-service';


// Inclusão das páginas que esta página utiliza.
import { SemInternetPage } from '../sem-internet/sem-internet';
import { EventoPage } from '../evento/evento';


@IonicPage()
@Component({

	selector: 'page-eventos',
	templateUrl: 'eventos.html',

})


export class EventosPage {

	EventosTotal	= -1;
	EventosCount	= 0;
	EventosPage		= 1;
	totalPage		= 0;
	Eventos			= [];

	responseData:	any;
	EventosItens:	any;

	constructor(

		public navCtrl: NavController,
		public wsprovider: WebServiceProvider,
		public navParams: NavParams

	) {

		// Carregar Eventos
		this.wsprovider.GetEventos(this.EventosPage).then((response) => {

			this.responseData = response;
			if(this.responseData.result == true) {

				if(this.responseData.result == true) {

					if(this.responseData.todas >= 1) {

						this.EventosTotal	= this.responseData.todas;
						
						this.EventosCount	= this.responseData.total;
						this.EventosItens	= this.responseData.itens;
						for (let i = 0; i < this.EventosCount; i++) {

							let n = { "evento_id": this.EventosItens[i].evento_id, "imagem": this.EventosItens[i].imagem, "data_evento": this.EventosItens[i].data_evento, "titulo": this.EventosItens[i].titulo, "resumo": this.EventosItens[i].resumo };
							this.Eventos.push(n);

						}


						this.totalPage = this.EventosCount;

					} else {

						this.EventosTotal = 0;

					}

				} else {

					this.EventosTotal = -2;

				}

			} else {

				this.EventosTotal = -2;

			}

		});

	}


	// Função para carregar mais noticias ao rolar a pagina até o final.
	doInfinite(infiniteScroll) {

		if(this.wsprovider.CheckInternet() == '1') {

			this.EventosPage	= (this.EventosPage + 1);
			setTimeout(() => {

				this.wsprovider.GetEventos(this.EventosPage).then((response) => {

					this.responseData = response;
					if(this.responseData.result == true) {

						if(this.responseData.total >= 1) {

							this.EventosCount	= this.responseData.total;
							this.EventosItens	= this.responseData.itens;

							for (let i = 0; i < this.EventosCount; i++) {

								let n = { "evento_id": this.EventosItens[i]['evento_id'], "imagem": this.EventosItens[i]['imagem'], "data_evento": this.EventosItens[i]['data_evento'], "titulo": this.EventosItens[i]['titulo'], "resumo": this.EventosItens[i]['resumo'] };

								this.Eventos.push(n);

							}


							this.totalPage = (this.totalPage + this.EventosCount);

						}

					} else {

						this.EventosTotal = -2;

					}

					infiniteScroll.complete();

				});

			}, 2000);

		} else {

			this.navCtrl.setRoot(SemInternetPage);

		}

	}


	// Função para abrir a página com exibição de mais detalhes do evento selecionado dentro da página.
	ShowEvento(evento_id) {

		this.navCtrl.push(EventoPage, { id: evento_id });

	}

}
