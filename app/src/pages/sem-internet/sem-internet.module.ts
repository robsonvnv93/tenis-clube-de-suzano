import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SemInternetPage } from './sem-internet';

@NgModule({
  declarations: [
    SemInternetPage,
  ],
  imports: [
    IonicPageModule.forChild(SemInternetPage),
  ],
})
export class SemInternetPageModule {}
