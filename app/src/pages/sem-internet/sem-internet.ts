import { Component } from '@angular/core';
import { IonicPage, NavController, Platform } from 'ionic-angular';

import { WebServiceProvider } from '../../providers/web-service/web-service';


@IonicPage()
@Component({

	selector: 'page-sem-internet',
	templateUrl: 'sem-internet.html',

})

export class SemInternetPage {

	visibilidade:			any;
	
	constructor(

		public platform: Platform,
		public navCtrl: NavController,
		public wsprovider: WebServiceProvider
	
	) {

		this.visibilidade	= 1;

	}


	CheckInternet() {

		this.visibilidade	= 0;
		if(this.wsprovider.CheckInternet() == '1') {

			this.platform.exitApp();

		} else {

			this.visibilidade	= 1;

		}

	}

}
