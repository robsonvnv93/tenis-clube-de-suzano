import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { WebServiceProvider } from '../../providers/web-service/web-service';


@IonicPage()
@Component({

	selector: 'page-horarios',
	templateUrl: 'horarios.html',

})

export class HorariosPage {

	responseData:	any;
	Horarios:		any;
	Total			= -1;

	constructor(
		public navCtrl: NavController,
		public wsprovider: WebServiceProvider,
		public navParams: NavParams
	) {

		this.wsprovider.GetHorarios().then((response) => {

			this.responseData = response;
			if(this.responseData.result == true) {

				this.Total		= this.responseData.total;
				this.Horarios	= this.responseData.itens;

			} else {

				this.Total	= -2;

			}

		});

	}

}