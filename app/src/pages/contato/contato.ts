import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';


import { WebServiceProvider } from '../../providers/web-service/web-service';


import { SemInternetPage } from '../sem-internet/sem-internet';


@IonicPage()
@Component({

	selector: 'page-contato',
	templateUrl: 'contato.html',

})

export class ContatoPage {

	AssuntosTotal	= -1;
	formData		= {
		
		"nome":		"",
		"telefone":	"",
		"email":	"",
		"horario":	"",
		"assunto":	"",
		"mensagem":	""

	};

	responseDataAssuntos:	any;
	responseData:			any;
	Assuntos:				any;
	loading:				any;

	constructor(

		public navCtrl: NavController,
		public alertCtrl: AlertController,
		public wsprovider: WebServiceProvider,
		public loadingCtrl: LoadingController

	) {

		// Carregar os Assuntos
		this.wsprovider.GetContatoAssuntos().then((response) => {

			this.responseDataAssuntos = response;
			if(this.responseDataAssuntos.result == true) {

				this.AssuntosTotal	= this.responseDataAssuntos.total;
				this.Assuntos		= this.responseDataAssuntos.itens;

			}

		});

	}


	// Função para realizar o envio do formulário de contato.
	SubmitForm() {

		if(this.wsprovider.CheckInternet() === '1') {

			if(this.formData.nome == '') {

				this.ShowAlert('Atenção!', "Por favor digite seu Nome.");

			} else {

				if(this.formData.telefone == '') {

					this.ShowAlert('Atenção!', "Por favor digite seu Telefone.");

				} else {

					if(this.formData.email == '') {

						this.ShowAlert('Atenção!', "Por favor digite seu Email.");

					} else {

						if(this.formData.horario == '') {

							this.ShowAlert('Atenção!', "Por favor informe o melhor Horário para contato.");

						} else {

							if(this.formData.assunto == '') {

								this.ShowAlert('Atenção!', "Por favor informe o Assunto de seu contato.");

							} else {

								if(this.formData.mensagem == '') {

									this.ShowAlert('Atenção!', "Por favor digite sua Mensagem.");

								} else {

									this.showLoading();
									this.wsprovider.EnviarContato(this.formData).then((response) => {

										this.responseData = response;
										this.loading.dismiss();
										if(this.responseData.result === true) {

											this.ShowAlert('Sucesso!', "Mensagem enviada com sucesso!");
											this.formData = {

												"nome":		"",
												"telefone":	"",
												"email":	"",
												"horario":	"",
												"assunto":	"",
												"mensagem":	""

											};

										} else {

											this.ShowAlert('Erro!', this.responseData.message);
											
										}

									});

								}

							}

						}

					}

				}

			}

		} else {

			this.navCtrl.setRoot(SemInternetPage);

		}

	}


	// Mostrar mensagem de erro.
	ShowAlert(titulo, mensagem) {

		const alert = this.alertCtrl.create({

			subTitle:	mensagem,
			title:		titulo,
			buttons:	['OK']

		});
		
		alert.present();

	}


	showLoading() {

		this.loading = this.loadingCtrl.create({ content: 'Carregando...' });
		this.loading.present();

	}

}