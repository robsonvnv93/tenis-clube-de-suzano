import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { WebServiceProvider } from '../../providers/web-service/web-service';


import { GradePage } from '../grade/grade';


@IonicPage()
@Component({

	selector: 'page-grades',
	templateUrl: 'grades.html',

})


export class GradesPage {

	CategoriasTotal	= -1;

	responseData:	any;
	Categorias:		any;
	Categoria:		any;

	constructor (

		public navCtrl: NavController,
		public navParams: NavParams,
		public wsprovider: WebServiceProvider

	) {

		this.wsprovider.GetGrades().then((response) => {

			this.responseData = response;
			if(this.responseData.result == true) {

				this.CategoriasTotal	= this.responseData.total;
				this.Categorias			= this.responseData.itens;
				this.Categoria			= this.responseData.primeiro;

			} else {

				this.CategoriasTotal	= -2;

			}

		});

	}


	ShowCategoria(categoria_id) {

		this.Categoria = categoria_id;

	}


	ShowGrade(grade_id) {

		this.navCtrl.push(GradePage, { id: grade_id });

	}

}
