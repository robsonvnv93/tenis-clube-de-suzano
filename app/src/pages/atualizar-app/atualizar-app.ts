import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AlertController, LoadingController } from 'ionic-angular';


import { WebServiceProvider } from '../../providers/web-service/web-service';


import { SemInternetPage } from '../sem-internet/sem-internet';


@IonicPage()
@Component({

	selector: 'page-atualizar-app',
	templateUrl: 'atualizar-app.html',

})

export class AtualizarAppPage {


	responseData:	any;
	loading:		any;

	constructor(
		
		public navCtrl: NavController,
		public alertCtrl: AlertController,
		public wsprovider: WebServiceProvider,
		public loadingCtrl: LoadingController

	) {}


	AtualizarApp() {

		if(this.wsprovider.CheckInternet() === '1') {

			this.showLoading();
			this.wsprovider.MakeAppUpdate().then((response) => {
		
				this.responseData = response;
				this.loading.dismiss();
				if(this.responseData.result === true) {

					this.ShowAlert('Sucesso!', "Aplicativo atualizado com sucesso.");

				} else {

					this.ShowAlert('Erro!', 'O aplicativo não pode ser atualizado por favor tente novamento.');

				}

			});

		} else {

			this.navCtrl.setRoot(SemInternetPage);

		}

	}


	// Mostrar mensagem de erro.
	ShowAlert(titulo, mensagem) {

		const alert = this.alertCtrl.create({

			subTitle:	mensagem,
			title:		titulo,
			buttons:	['OK']

		});
		
		alert.present();

	}


	showLoading() {

		this.loading = this.loadingCtrl.create({ content: 'Atualizando...' });
		this.loading.present();

	}

}
