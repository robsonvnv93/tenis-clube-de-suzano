import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AtualizarAppPage } from './atualizar-app';

@NgModule({
  declarations: [
    AtualizarAppPage,
  ],
  imports: [
    IonicPageModule.forChild(AtualizarAppPage),
  ],
})
export class AtualizarAppPageModule {}
