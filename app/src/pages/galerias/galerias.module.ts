import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GaleriasPage } from './galerias';

@NgModule({
  declarations: [
    GaleriasPage,
  ],
  imports: [
    IonicPageModule.forChild(GaleriasPage),
  ],
})
export class GaleriasPageModule {}
