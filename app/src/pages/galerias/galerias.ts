import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { WebServiceProvider } from '../../providers/web-service/web-service';

import { GaleriaPage } from '../galeria/galeria';

@IonicPage()
@Component({

	selector: 'page-galerias',
	templateUrl: 'galerias.html',

})

export class GaleriasPage {

	responseData:	any;
	Galerias:		any;
	Total			= -1;

	constructor(

		public navCtrl: NavController,
		public wsprovider: WebServiceProvider,
		public navParams: NavParams

	) {

		this.wsprovider.GetGalerias().then((response) => {

			this.responseData = response;
			if(this.responseData.result == true) {

				this.Total		= this.responseData.total;
				this.Galerias	= this.responseData.itens;

			} else {

				this.Total	= -2;

			}

		});

	}


	// Exibir itens da galeria
	ShowGaleria(galeria_id) {

		this.navCtrl.push(GaleriaPage, { id: galeria_id });

	}

}