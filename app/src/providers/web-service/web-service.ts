import { Injectable } from '@angular/core';

import { Network } from '@ionic-native/network';
import { HTTP } from '@ionic-native/http';
import { Downloader } from '@ionic-native/downloader';

@Injectable()
export class WebServiceProvider {

	servidor	= 'http://app.tenisclubedesuzano.com.br/api/';
	appKey		= 'com.tenisclubedesuzano.app';
	versao		= '1.0';
	responseData:	any;
	postData:		any;

	constructor(
		
		public network: Network,
		public downloader: Downloader,
		public http: HTTP

	) {}


	
	// Verifica se o usuário possui uma conexão de internet.
	CheckInternet() {

		if (this.network.type === 'none') {

			return '0';

		} else {

			return '1';

		}

	}



	/* ========================================= */ 



	// Valida se o dispositivo possui a ultima versão do app.
	CheckVersion() {

		this.postData = {
			
			"_key":			this.appKey,
			"_version":		this.versao

		};
		

		return new Promise(resolve => {

			this.http.get(this.servidor + '_version/check/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('CheckVersion:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}



	/* ========================================= */ 



	// Faz o download do apk para nova versão do app.
	MakeAppUpdate() {

		var request = {

			uri: this.servidor + '_version/update/',
			title: 'tenis-clube-de-suzano.apk',
			visibleInDownloadsUi: true,
			destinationInExternalFilesDir: {

				dirType: 'Downloads',
				subPath: 'Downloads'

			}

		};


		return new Promise(resolve => {

			this.downloader.download(request).then(location => {

				this.responseData = JSON.parse(location);
				// console.log('MakeAppUpdate:');
				// console.log(this.responseData);
				resolve(this.responseData);
			
			});

		});

	}



	/* ========================================= */ 



	// Valida o FCMTokenID salvo na sessão encontrada no dispositivo.
	CheckToken(FCMTokenID, token) {

		this.postData = {
			
			"_FCMTokenID":	FCMTokenID,
			"_token":		token,
			"_key":			this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_token/check/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('CheckToken:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}



	/* ========================================= */ 



	// Cria um novo FCMTokenID para iniciar uma nova sessão no dispositivo.
	CreateToken(token) {

		this.postData = {
			
			"_token":	token,
			"_key":		this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_token/create/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('CreateToken:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}



	
	/* ========================================= */ 



	// Busca notificações cadastradas no sistema para o dispositivo fazendo paginação de resultados para exibição no app.
	GetNotificacoes(pagina, limite) {

		this.postData = {
			
			"_key":			this.appKey,
			'_FCMTokenID':	localStorage.getItem('FCMTokenID'),
			'_token':		localStorage.getItem('FCMToken'),
			'_pagina':		pagina,
			'_limite':		limite

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_get/notificacoes/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('GetNotificacoes:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}


	
	/* ========================================= */ 



	// Busca detalhes da notificação selecionada para exibir dentro do app.
	ShowNotificacao(envioID) {

		this.postData = {
			
			'envio_id':	envioID,
			"_key":		this.appKey,

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_show/notificacao/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('ShowNotificacao:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}
	


	/* ========================================= */ 



	// Altera o status de visualização do log de envio de uma notificação para visualizada.
	ChangeNotificacaoStatus(logID) {

		this.postData = {
			
			'log_id':	logID,
			"_key":		this.appKey,

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_change/notificacao/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('ChangeNotificacaoStatus:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}


	
	/* ========================================= */ 



	// Busca banners cadastrados no sistema para exibição no app.
	GetBanners() {

		this.postData = {
			
			"_key":		this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_get/banners/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('GetBanners:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}


	
	/* ========================================= */ 



	// Busca noticias cadastradas no sistema fazendo paginação de resultados para exibição no app.
	GetNoticias(pagina, limite) {

		this.postData = {
			
			"_key":		this.appKey,
			'_pagina':	pagina,
			'_limite':	limite

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_get/noticias/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('GetNoticias:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}


	
	/* ========================================= */ 



	// Busca detalhes da noticia selecionada para exibir dentro do app.
	ShowNoticia(noticiaID) {

		this.postData = {
			
			'noticia_id':	noticiaID,
			"_key":			this.appKey,

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_show/noticia/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('ShowNoticia:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}


	
	/* ========================================= */ 



	// Busca eventos cadastrados no sistema.
	GetEventos(pagina) {

		this.postData = {
			
			"_key":		this.appKey,
			'_pagina':	pagina

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_get/eventos/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('GetEventos:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}


	
	/* ========================================= */ 



	// Busca detalhes do evento selecionado no app.
	ShowEvento(eventoID) {

		this.postData = {
			
			'evento_id':	eventoID,
			"_key":			this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_show/evento/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('ShowEvento:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}



	/* ========================================= */ 



	// Busca Horários de funcionamento de atrações cadastrados no sistema.
	GetHorarios() {

		this.postData = {
			
			"_key":		this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_get/horarios/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('GetHorarios:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}


	
	/* ========================================= */ 



	// Busca grades cadastradas no sistema por categoria.
	GetGrades() {

		this.postData = {
			
			"_key":			this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_get/grades/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('GetGrades:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}


	
	/* ========================================= */ 



	// Busca grade da atividade selecionada.
	ShowGrade(atividadeID) {

		this.postData = {
			
			"atividade_id":	atividadeID,
			"_key":			this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_show/grade/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('ShowGrade:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}



	/* ========================================= */ 



	// Busca galerias cadastradas no sistema.
	GetGalerias() {

		this.postData = {
			
			"_key":		this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_get/galerias/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('GetGalerias:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}


	
	/* ========================================= */ 



	// Busca os itens da galeria selecionada dentro do app.
	ShowGaleria(galeriaID) {

		this.postData = {
			
			"galeria_id":	galeriaID,
			"_key":			this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_show/galeria/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('ShowGaleria:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}



	/* ========================================= */ 



	// Busca os assuntos cadastrados no sistema popular a página de contato.
	GetContatoAssuntos() {

		this.postData = {
			
			"_key":		this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_contato/getassuntos/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('GetContatoAssuntos:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}



	/* ========================================= */ 



	// Enviar as informações inseridas no formulário de contato e enviar para a API.
	EnviarContato(formulario) {

		this.postData = {
			
			"_formulario":		formulario,
			"_key":				this.appKey

		};


		return new Promise(resolve => {

			this.http.get(this.servidor + '_contato/enviar/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('EnviarContato:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}



	/* ========================================= */ 



	// Busca conteudo de uma página dentro do CMS para o app.
	ShowPagina(paginaID) {

		this.postData = {
			
			'pagina_id':	paginaID,
			"_key":			this.appKey

		};

		return new Promise(resolve => {

			this.http.get(this.servidor + '_show/pagina/', { "_POST": JSON.stringify(this.postData) }, { 'Content-Type': 'application/json' }).then(response => {

				this.responseData = JSON.parse(response.data);
				// console.log('ShowPagina:');
				// console.log(this.responseData);
				resolve(this.responseData);

			});

		});

	}


}
