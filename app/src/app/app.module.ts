import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';


import { WebServiceProvider } from '../providers/web-service/web-service';
import { Network } from '@ionic-native/network';
import { FCM } from '@ionic-native/fcm';
import { HTTP } from '@ionic-native/http';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { NoticiasPage } from '../pages/noticias/noticias';
import { NoticiaPage } from '../pages/noticia/noticia';
import { EventosPage } from '../pages/eventos/eventos';
import { EventoPage } from '../pages/evento/evento';
import { SobrePage } from '../pages/sobre/sobre';
import { ExpedientePage } from '../pages/expediente/expediente';
import { HorariosPage } from '../pages/horarios/horarios';
import { MapaPage } from '../pages/mapa/mapa';
import { GradesPage } from '../pages/grades/grades';
import { GradePage } from '../pages/grade/grade';
import { GaleriasPage } from '../pages/galerias/galerias';
import { GaleriaPage } from '../pages/galeria/galeria';
import { ContatoPage } from '../pages/contato/contato';
import { AjudaPage } from '../pages/ajuda/ajuda';
import { CuponsPage } from '../pages/cupons/cupons';
import { NotificacoesPage } from '../pages/notificacoes/notificacoes';
import { NotificacaoPage } from '../pages/notificacao/notificacao';


import { SemInternetPage } from '../pages/sem-internet/sem-internet';
import { AtualizarAppPage } from '../pages/atualizar-app/atualizar-app';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Downloader } from '@ionic-native/downloader';


@NgModule({
	
	declarations: [

		MyApp,
		HomePage,
		NoticiasPage,
		NoticiaPage,
		EventosPage,
		EventoPage,
		SobrePage,
		ExpedientePage,
		HorariosPage,
		MapaPage,
		GradesPage,
		GradePage,
		GaleriasPage,
		GaleriaPage,
		ContatoPage,
		AjudaPage,
		SemInternetPage,
		CuponsPage,
		NotificacoesPage,
		NotificacaoPage,
		AtualizarAppPage

	],
	imports: [

		BrowserModule,
		IonicModule.forRoot(MyApp),

	],
	bootstrap: [IonicApp],
	entryComponents: [

		MyApp,
		HomePage,
		NoticiasPage,
		NoticiaPage,
		EventosPage,
		EventoPage,
		SobrePage,
		ExpedientePage,
		HorariosPage,
		MapaPage,
		GradesPage,
		GradePage,
		GaleriasPage,
		GaleriaPage,
		ContatoPage,
		AjudaPage,
		SemInternetPage,
		CuponsPage,
		NotificacoesPage,
		NotificacaoPage,
		AtualizarAppPage

	],
	providers: [

		StatusBar,
		SplashScreen,
		Network,
		Downloader,
		FCM,
		HTTP,
		{provide: ErrorHandler, useClass: IonicErrorHandler},
		WebServiceProvider

	]

})
export class AppModule {}
