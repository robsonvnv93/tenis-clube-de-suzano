import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AlertController} from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { App } from 'ionic-angular';

import { FCM } from '@ionic-native/fcm';
import { WebServiceProvider } from '../providers/web-service/web-service';

import { HomePage } from '../pages/home/home';
import { NoticiasPage } from '../pages/noticias/noticias';
import { NoticiaPage } from '../pages/noticia/noticia';
import { EventosPage } from '../pages/eventos/eventos';
import { EventoPage } from '../pages/evento/evento';
import { SobrePage } from '../pages/sobre/sobre';
import { HorariosPage } from '../pages/horarios/horarios';
import { ExpedientePage } from '../pages/expediente/expediente';
import { MapaPage } from '../pages/mapa/mapa';
import { GradesPage } from '../pages/grades/grades';
import { GaleriasPage } from '../pages/galerias/galerias';
import { ContatoPage } from '../pages/contato/contato';
import { AjudaPage } from '../pages/ajuda/ajuda';
import { CuponsPage } from '../pages/cupons/cupons';
import { NotificacoesPage } from '../pages/notificacoes/notificacoes';
import { NotificacaoPage } from '../pages/notificacao/notificacao';


import { SemInternetPage } from '../pages/sem-internet/sem-internet';
import { AtualizarAppPage } from '../pages/atualizar-app/atualizar-app';

@Component({

	templateUrl: 'app.html'

})


export class MyApp {

	@ViewChild(Nav) nav: Nav;
	rootPage: any;
	pages: Array<{title: string, component: any}>;
	FCMTokenID:				any;
	responseDataFCM:		any;
	responseDataVersion:	any;

	constructor(

		public platform: Platform,
		public statusBar: StatusBar,
		public splashScreen: SplashScreen,
		public fcm: FCM,
		public wsprovider: WebServiceProvider,
		public alertCtrl: AlertController,
		public toastCtrl: ToastController,
		public app: App

	) {


		// used for an example of ngFor and navigation
		this.pages = [

			{ title: 'Home', component: HomePage },
			{ title: 'Cupons', component: CuponsPage },
			{ title: 'Sobre nós', component: SobrePage },
			{ title: 'Notícias', component: NoticiasPage },
			{ title: 'Eventos', component: EventosPage },
			{ title: 'Nossa estrutura', component: ExpedientePage },
			{ title: 'Expediente', component: HorariosPage },
			{ title: 'Grades', component: GradesPage },
			{ title: 'Galerias', component: GaleriasPage },
			{ title: 'Como Chegar', component: MapaPage },
			{ title: 'Contato', component: ContatoPage },
			{ title: 'Notificações', component: NotificacoesPage },
			{ title: 'Ajuda', component: AjudaPage }

		];

		this.initializeApp();


	}


	initializeApp() {

		this.platform.ready().then(() => {

			this.statusBar.styleDefault();
			
			if(this.wsprovider.CheckInternet() === '1') {

				this.rootPage	= HomePage;
				this.wsprovider.CheckVersion().then((response) => {

					this.responseDataVersion = response;
					if(this.responseDataVersion.result == true && this.responseDataVersion.updated == true) {

						this.FCMTokenID	= localStorage.getItem("FCMTokenID");
						let fcmCheck	= setInterval(() => {

							if (typeof FCM != 'undefined') {
								
								clearInterval(fcmCheck);

								this.fcm.onNotification().subscribe(data => {

									if(data.wasTapped){

										if(data.opcao == 'NOTIFICACAO') {

											this.nav.push(NotificacaoPage, { id: data.item_id });

										}


										if(data.opcao == 'EVENTO') {

											this.nav.push(EventoPage, { id: data.item_id });

										}


										if(data.opcao == 'NOTICIA') {

											this.nav.push(NoticiaPage, { id: data.item_id });

										}

									} else {

										this.exibirPush(data.title, data.body);

									};

								});


								this.fcm.getToken().then(token => {

									if(this.FCMTokenID != null && this.FCMTokenID != 'null') {

										this.wsprovider.CheckToken(this.FCMTokenID, token).then((response) => {

											this.responseDataFCM = response;
											if(this.responseDataFCM.result == false) {

												this.wsprovider.CreateToken(token).then((response) => {

													this.responseDataFCM = response;
													if(this.responseDataFCM.result === true) {

														localStorage.setItem('FCMTokenID',		this.responseDataFCM.FCMTokenID);
														localStorage.setItem('FCMToken',		token);
														this.splashScreen.hide();

													} else {

														localStorage.setItem('FCMTokenID',		null);
														localStorage.setItem('FCMToken',		null);

													}

												});

											} else {

												localStorage.setItem('FCMTokenID',		this.FCMTokenID);
												localStorage.setItem('FCMToken',		token);
												this.splashScreen.hide();

											}

										});

									} else {

										this.wsprovider.CreateToken(token).then((response) => {

											this.responseDataFCM = response;
											if(this.responseDataFCM.result === true) {

												localStorage.setItem('FCMTokenID',		this.responseDataFCM.FCMTokenID);
												localStorage.setItem('FCMToken',		token);
												this.splashScreen.hide();

											} else {

												localStorage.setItem('FCMTokenID',		null);
												localStorage.setItem('FCMToken',		null);

											}

										});

									}

								});

							}

						}, 1000);

					} else {

						this.rootPage = AtualizarAppPage;
						this.splashScreen.hide();

					}

				});

			} else {

				this.rootPage = SemInternetPage;
				this.splashScreen.hide();

			}
			

		});


		this.platform.registerBackButtonAction(() => {

			// Catches the active view
			let navs		= this.app.getActiveNavs()[0];
			let activeView	= navs.getActive();
			
			// Checks if can go back before show up the alert
			if(activeView.name === 'HomePage') {

				const alert = this.alertCtrl.create({

					title: 'Fechar o App',
					message: 'Você tem certeza?',
					buttons: [

						{

							text: 'Cancelar',
							role: 'cancel',
							handler: () => {}

						},
						{

							text: 'Fechar o App',
							handler: () => {

								alert.dismiss();
								this.platform.exitApp();

							}

						}

					]

				});

				alert.present();

			} else {

				if (navs.canGoBack()){

					navs.pop();

				} else {

					this.nav.setRoot(HomePage);

				}

			}
 
		});

	}


	// Ação de navegação do menu.
	openPage(page) {

		if(this.wsprovider.CheckInternet() === '1') {

			this.nav.setRoot(page.component);
		
		} else {
			
			this.nav.setRoot(SemInternetPage);

		}

	}


	exibirPush(titulo, mensagem) {

		const toast = this.toastCtrl.create({

			message: "" + titulo + "\n" + mensagem,
			position: 'top',
			duration: 10000

		});


		toast.present();

	}

}
