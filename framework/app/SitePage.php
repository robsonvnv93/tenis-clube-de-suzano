<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SitePage extends Model {

		protected $table	= 'site_pages';
		protected $fillable	= [
			
			'id',
			'lang_id',
			'title',
			'slug',
			'content',
			'parent',
			'visible',
			'status'

		];
	
	}
