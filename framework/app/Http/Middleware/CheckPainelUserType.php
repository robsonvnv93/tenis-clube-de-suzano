<?php

	namespace App\Http\Middleware;

	use Illuminate\Support\Facades\Auth;
	use App\Helpers\Helper;
	use Closure;
	use Route;

	class CheckPainelUserType {
		/**
		 * Handle an incoming request.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @param  \Closure  $next
		 * @return mixed
		 */
		public function handle($request, Closure $next, $guard = null) {
			
			if (Auth::guard($guard)->check()) {
				
				if(Helper::UserCanAccess(Auth::user()->id)) {

					return $next($request);

				} else {
					
					if(Route::currentRouteName() == 'admin-dashboard' || Route::currentRouteName() == 'admin-minha-conta') {

						return $next($request);

					} else {

						return redirect('/admin/access-denied');

					}


				}

			} else {

				return redirect('/admin/login');

			}

		}

	}
