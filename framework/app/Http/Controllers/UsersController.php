<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\User;

	class UsersController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Usuários',
				'subtitle'	=> 'Listar todos os usuários',
				'buttons'	=> [

					[
						
						'title'	=> 'Adicionar novo usuário',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-users-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-users-destroy',
				'search'	=> 'admin-users-index',
				'limit'		=> 15,
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Nome',
						'field'		=> 'name',
						'search'	=> true

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'E-mail',
						'field'		=> 'email',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar usuário',
						'route'	=> 'admin-users-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar usuário',
						'route'	=> 'admin-users-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir usuário',
						'route'	=> 'admin-users-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'users', $busca);


			return view('automator.paginator', $data);

		}


		public function view($user) {}
		public function create() {}
		public function store(Request $request) {}
		public function edit($user) {}
		public function update(Request $request) {}
		public function destroy(Request $request, $item = null) {}


		// Alterar status do usuário no sistema.
		public function status($user) {


			$user = User::where('id', $user)->first();
			if(count($user) >= 1) {

				if($user->status == 0) {

					$novo		= 1;
					$success	= 'Usuário ativado com sucesso!';
					$error		= 'Erro! O usuário não pode ser ativado!';

				} else {

					$novo		= 0;
					$success	= 'Usuário desativado com sucesso!';
					$error		= 'Erro! O usuário não pode ser desativado!';

				}


				$usuario			= User::find($user->id);
				$usuario->status	= $novo;
				if($usuario->save()) {

					echo Helper::AdminRedirectAlert($sucess, route('admin-users-index'));

				} else {

					echo Helper::AdminRedirectAlert($error, route('admin-users-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Usuário não encontrado!', route('admin-users-index'));

			}

		}

	}
