<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\SiteHorario;
	use Route;


	// NOMENCLATURA DESTE CONTROLADOR FOI ALTERADA PARA EVENTOS DENTRO DO SISTEMA.
	class AdminSiteHorariosController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Expediente',
				'subtitle'	=> 'Listar Atrações',
				'buttons'	=> [

					[
						
						'title'	=> 'Cadastrar Nova Atração',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-site-horarios-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-site-horarios-destroy',
				'search'	=> 'admin-site-horarios-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'order',
					'by'	=> 'ASC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Nome',
						'field'		=> 'name',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar Atração',
						'route'	=> 'admin-site-horarios-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar Atração',
						'route'	=> 'admin-site-horarios-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir Atração',
						'route'	=> 'admin-site-horarios-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'site_horarios', $busca);


			return view('automator.paginator', $data);

		}


		public function view($horario) {

			$horario	= SiteHorario::where('id', $horario)->first();
			if(count($horario) >= 1) {

				$header	= [

					'title'		=> 'Expediente',
					'subtitle'	=> 'Visualizar Atração',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Atrações',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-horarios-index'

						],
						[
						
							'title'	=> 'Editar Atração',
							'type'	=> 'link',
							'class'	=> 'btn-warning',
							'route'	=> 'admin-site-horarios-edit',
							'args'	=> $horario->id

						],
						[
						
							'title'	=> 'Cadastrar Nova Atração',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-horarios-create'

						]

					]

				];

				$body	= [

					'indice'	=> $horario->id,
					'fields'	=> [

						[

							'type'			=> 'file-image',
							'name'			=> 'Foto',
							'field'			=> 'image',
							'default'		=> asset('storage/horarios/' . $horario->image),

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'name',
							'default'		=> $horario->name,

						],
						[

							'type'			=> 'textarea',
							'name'			=> 'Descrição',
							'field'			=> 'texto',
							'default'		=> $horario->description,

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $horario->status,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::ViewerMaker($header, $body);

				return view('automator.viewer-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert("Atração não encontrada!", route('admin-site-horarios-index'));

			}

		}
		

		public function create() {

			$header	= [

				'title'		=> 'Expediente',
				'subtitle'	=> 'Cadastrar Nova Atração',
				'buttons'	=> [

					[
						
						'title'	=> 'Listar Atrações',
						'type'	=> 'link',
						'class'	=> 'btn-primary',
						'route'	=> 'admin-site-horarios-index'

					]

				]

			];

			$body	= [

				'action'	=> 'admin-site-horarios-store',
				'fields'	=> [

					[

						'type'			=> 'file-upload',
						'name'			=> 'Foto',
						'field'			=> 'imagem',
						'required'		=> false

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Nome',
						'field'			=> 'name',
						'required'		=> true

					],
					[

						'type'			=> 'editor',
						'name'			=> 'Descrição',
						'field'			=> 'description',
						'required'		=> true

					],
					[

						'type'		=> 'select',
						'name'		=> 'Status',
						'field'		=> 'status',
						'required'	=> true,
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',

							]

						]

					]

				]

			];

			$data	= Automator::FormMaker($header, $body);

			return view('automator.form-maker', $data);

		}


		public function store(Request $request) {

			$campos	= [

				'name'			=> 'O campo "Nome" é obrigatório',
				'description'	=> 'O campo "Descrição" é obrigatório',
				'status'		=> 'O campo "Status" é obrigatório'
			
			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$nameFile	= '';
				if($request->hasFile('imagem')) {

					$imagemname	= uniqid(date('HisYmd'));
					$extension	= $request->imagem->extension();
					$nameFile	= "{$imagemname}.{$extension}";

					$upload		= $request->imagem->storeAs('public/horarios/', $nameFile);
					if(!$upload) {

						$nameFile	= '';

					}

				}

				$nova				= new SiteHorario;
				$nova->image		= $nameFile;
				$nova->name			= $request->input('name');
				$nova->description	= $request->input('description');
				$nova->status		= $request->input('status');
				$nova->created_at	= now();
				if($nova->save()) {

					echo Helper::AdminRedirectAlert('Atração cadastrada com sucesso!', route('admin-site-horarios-index'));

				} else {

					echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o horario da atração não pode ser cadastrado.', route('admin-site-horarios-create'));

				}

			} else {

				echo Helper::AdminRedirectAlert($retorno, route('admin-site-horarios-index'));

			}

		}
		

		public function edit($horario) {

			$horario	= SiteHorario::where('id', $horario)->first();
			if(count($horario) >= 1) {

				$header	= [

					'title'		=> 'Expediente',
					'subtitle'	=> 'Editar Atração',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Atrações',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-horarios-index'

						],
						[
						
							'title'	=> 'Cadastrar Nova Atração',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-horarios-create'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-site-horarios-update',
					'indice'	=> $horario->id,
					'fields'	=> [

						[

							'type'			=> 'file-upload',
							'name'			=> 'Foto',
							'field'			=> 'imagem',
							'default'		=> asset('storage/horarios/' . $horario->image),
							'required'		=> false

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'name',
							'default'		=> $horario->name,
							'required'		=> true

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Descrição',
							'field'			=> 'description',
							'default'		=> $horario->description,
							'required'		=> true

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $horario->status,
							'required'	=> true,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Atração não encontrada!', route('admin-site-horarios-index'));

			}

		}


		public function update(Request $request) {

			$id			= $request->input('id');
			$horario	= SiteHorario::where('id', $id)->first();
			if(count($horario) >= 1) {

				$campos	= [

					'name'			=> 'O campo "Nome" é obrigatório',
					'description'	=> 'O campo "Descrição" é obrigatório',
					'status'		=> 'O campo "Status" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {

					$nameFile	= $horario->image;
					if($request->hasFile('imagem')) {

						$imagemname	= uniqid(date('HisYmd'));
						$extension	= $request->imagem->extension();
						$nameFile	= "{$imagemname}.{$extension}";

						$upload		= $request->imagem->storeAs('public/horarios/', $nameFile);
						if(!$upload) {

							$nameFile	= '';

						} else {

							$nameFile	= $horario->image;

						}

					}

					$atualizar				= SiteHorario::find($id);
					$atualizar->image		= $nameFile;
					$atualizar->name		= $request->input('name');
					$atualizar->description	= $request->input('description');
					$atualizar->status		= $request->input('status');
					$atualizar->updated_at	= now();
					if($atualizar->save()) {

						echo Helper::AdminRedirectAlert('Atração editada com sucesso!', route('admin-site-horarios-edit', $id));

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e a atração não pode ser editada.', route('admin-site-horarios-edit', $id));

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-site-horarios-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Atração não encontrada!', route('admin-site-horarios-index'));

			}


		}


		public function destroy(Request $request) {

			$itens	= $request->input('itens');
			return Automator::ExcluirRegistros('site_horarios', $itens, route('admin-site-horarios-index'), 'Atração/Atrações excluida(s) com sucesso!', 'Erro! A(s) Atração/Atrações não foi/foram excluida(s).');

		}

	}
