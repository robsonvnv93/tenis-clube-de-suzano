<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\User;

	class SitePagesController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Páginas',
				'subtitle'	=> 'Listar todas as páginas',
				'buttons'	=> [

					[
						
						'title'	=> 'Adicionar nova página',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-site-pages-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-site-pages-destroy',
				'search'	=> 'admin-site-pages-index',
				'limit'		=> 15,
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Titulo',
						'field'		=> 'title',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar página',
						'route'	=> 'admin-site-pages-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar página',
						'route'	=> 'admin-site-pages-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir página',
						'route'	=> 'admin-site-pages-status',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'site_pages', $busca);


			return view('automator.paginator', $data);

		}


		public function view($pagina) {}
		public function create() {}
		public function store(Request $request) {}
		public function edit($pagina) {}
		public function update(Request $request) {}
		public function destroy(Request $request, $item = null) {}


		// Alterar status do usuário no sistema.
		public function status($pagina) {


			$user = SitePage::where('id', $pagina)->first();
			if(count($user) >= 1) {

				if($user->status == 0) {

					$novo		= 1;
					$success	= 'Página ativada com sucesso!';
					$error		= 'Erro! A página não pode ser ativada!';

				} else {

					$novo		= 0;
					$success	= 'Página desativada com sucesso!';
					$error		= 'Erro! A página não pode ser desativada!';

				}


				$usuario			= SitePage::find($user->id);
				$usuario->status	= $novo;
				if($usuario->save()) {

					echo Helper::AdminRedirectAlert($sucess, route('admin-site-pages-index'));

				} else {

					echo Helper::AdminRedirectAlert($error, route('admin-site-pages-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Página não encontrada!', route('admin-site-pages-index'));

			}

		}

	}
