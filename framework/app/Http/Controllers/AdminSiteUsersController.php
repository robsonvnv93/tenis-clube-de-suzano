<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\SiteUserType;
	use App\SiteUser;

	class AdminSiteUsersController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Site',
				'subtitle'	=> 'Listar todos os usuários',
				'buttons'	=> [

					[
						
						'title'	=> 'Adicionar novo usuário',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-site-users-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-site-users-destroy',
				'search'	=> 'admin-site-users-index',
				'limit'		=> 15,
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Titulo',
						'field'		=> 'titulo',
						'search'	=> true

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Nome',
						'field'		=> 'name',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar usuário',
						'route'	=> 'admin-site-users-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar usuário',
						'route'	=> 'admin-site-users-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir usuário',
						'route'	=> 'admin-site-users-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'site_users', $busca);


			return view('automator.paginator', $data);

		}


		public function view($user) {}

		public function create() {

			$header	= [

				'title'		=> 'Site',
				'subtitle'	=> 'Adicionar novo usuário',
				'buttons'	=> [

					[
						
						'title'	=> 'Listar todos os usuários',
						'type'	=> 'link',
						'class'	=> 'btn-primary',
						'route'	=> 'admin-site-users-index'

					]

				]

			];

			$body	= [

				'action'	=> 'admin-site-users-store',
				'fields'	=> [

					// [

					// 	'type'			=> 'checkbox',
					// 	'name'			=> 'Nivel de acesso',
					// 	'field'			=> 'types',
					// 	'required'		=> true,
					// 	'options'		=> Automator::RelatedField('site_users_types', 'name', [['status', '=', 1]])

					// ],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Titulo',
						'field'			=> 'titulo',
						'required'		=> true

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Nome',
						'field'			=> 'name',
						'required'		=> true

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Senha',
						'field'			=> 'password',
						'required'		=> true

					],
					[

						'type'		=> 'select',
						'name'		=> 'Status',
						'field'		=> 'status',
						'required'	=> true,
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',

							]

						]

					]

				]

			];

			$data	= Automator::FormMaker($header, $body);

			return view('automator.form-maker', $data);

		}
		

		public function store(Request $request) {

			$campos	= [

				'titulo'		=> 'O campo "Titulo" é obrigatório',
				'name'			=> 'O campo "Nome" é obrigatório',
				'password'		=> 'O campo "Senha" é obrigatório',
				'status'		=> 'O campo "Status" é obrigatório'
			
			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$validaEmail		= SiteUser::where('titulo', $request->input('titulo'))->count();
				if($validaEmail >= 1) {

					echo Helper::AdminRedirectAlert("Este titulo já está em uso por outro usuário!", route('admin-users-create'));

				} else {

					$nova				= new SiteUser;
					$nova->titulo		= $request->input('titulo');
					$nova->name			= $request->input('name');
					// $nova->password		= bcrypt($request->input('password'));
					$nova->password		= ($request->input('password'));
					$nova->status		= $request->input('status');
					$nova->created_at	= now();
					if($nova->save()) {

						$types		= $request->input('types');
						$typesTotal	= count($types);
						if($typesTotal >= 1) {

							$user	= $nova->id;
							$ok		= 0;
							foreach ($types as $type) {
								
								$vincular				= '';
								$vincular				= new SiteUserType;
								$vincular->user_id		= $user;
								$vincular->user_type_id	= $type;
								if($vincular->save()) {

									$ok++;

								}

							}

							if($ok >= $typesTotal) {

								echo Helper::AdminRedirectAlert('Usuário cadastrado com sucesso!', route('admin-site-users-index'));

							} else {
								
								echo Helper::AdminRedirectAlert('Usuário cadastrado com sucesso! Porem houve algum problema ao vincular suas seus niveis de acesso.', route('admin-site-users-index'));

							}

						} else {

							echo Helper::AdminRedirectAlert('Usuário cadastrado com sucesso!', route('admin-site-users-index'));
							
						}

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o usuário não pode ser cadastrado.', route('admin-site-users-create'));

					}

				}

			} else {

				echo Helper::AdminRedirectAlert($retorno, route('admin-site-users-create'));

			}

		}


		public function edit($user) {

			$user	= SiteUser::where('id', $user)->first();
			if(count($user) >= 1) {

				$header	= [

					'title'		=> 'Site',
					'subtitle'	=> 'Editar usuário',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar todos os usuários',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-users-index'

						],
						[
						
							'title'	=> 'Adicionar novo usuário',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-users-create'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-site-users-update',
					'indice'	=> $user->id,
					'fields'	=> [

						// [

						// 	'type'			=> 'checkbox',
						// 	'name'			=> 'Nivel de acesso',
						// 	'field'			=> 'types',
						// 	'required'		=> false,
						// 	'options'		=> Automator::RelatedField('site_users_types', 'name', [['status', '=', 1]]),
						// 	'default'		=> Automator::RelatedValues('site_user_types', 'user_id', $user->id, 'user_type_id')

						// ],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo',
							'field'			=> 'titulo',
							'default'		=> $user->titulo,
							'required'		=> true

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'name',
							'default'		=> $user->name,
							'required'		=> true

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Senha',
							'field'			=> 'password',
							'default'		=> '',
							'required'		=> false

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $user->status,
							'required'	=> true,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Usuário não encontrado!', route('admin-site-users-index'));

			}

		}

		public function update(Request $request) {

			$id		= $request->input('id');
			$user	= SiteUser::where('id', $id)->first();
			if(count($user) >= 1) {

				$campos	= [

					'titulo'		=> 'O campo "Titulo" é obrigatório',
					'name'			=> 'O campo "Nome" é obrigatório',
					'status'		=> 'O campo "Status" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {

					$continuar	= true;
					$titulo		= $request->input('titulo');
					if($titulo != $user->titulo) {

						$continuar = false;
						$validaEmail = SiteUser::where('titulo', $titulo)->count();
						if($validaEmail == 0) { $continuar = true; }

					}

					if($continuar == true) {

						$senha					= $request->input('password');

						$atualizar				= SiteUser::find($id);
						$atualizar->titulo		= $request->input('titulo');
						$atualizar->name		= $request->input('name');
						if($senha != '') {

							// $atualizar->password	= bcrypt($senha);
							$atualizar->password	= ($senha);

						}

						$atualizar->status		= $request->input('status');
						$atualizar->updated_at	= now();
						if($atualizar->save()) {

							$user_types	= SiteUserType::where('user_id', $id)->count();
							if($user_types >= 1) {

								$limpa	= SiteUserType::where('user_id', $id)->delete();

							} else {

								$limpa	= true;

							}

							if($limpa) {

								$types		= $request->input('types');
								$typesTotal	= count($types);
								if($typesTotal >= 1) {

									$ok	= 0;
									foreach ($types as $type) {
										
										$vincular				= '';
										$vincular				= new SiteUserType;
										$vincular->user_id		= $id;
										$vincular->user_type_id	= $type;
										if($vincular->save()) {

											$ok++;

										}

									}

									if($ok >= $typesTotal) {

										echo Helper::AdminRedirectAlert('Usuário editado com sucesso!', route('admin-site-users-edit', $id));

									} else {

										echo Helper::AdminRedirectAlert('Usuário editado com sucesso! Porem não foi possivel atualizar seus niveis de acesso.', route('admin-site-users-edit', $id));

									}

								} else {
									
									echo Helper::AdminRedirectAlert('Usuário editado com sucesso!', route('admin-site-users-edit', $id));

								}
								
							} else {
								
								echo Helper::AdminRedirectAlert('Usuário editado com sucesso! Porem não foi possivel atualizar as seus niveis de acesso.', route('admin-site-users-edit', $id));

							}

						} else {

							echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o usuário não pode ser editado.', route('admin-site-users-edit', $id));

						}

					} else {

						echo Helper::AdminRedirectAlert("Este Titulo já está em uso por outro usuário!", route('admin-site-users-edit', $user->id));

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-site-users-edit', $user->id));

				}

			} else {

				echo Helper::AdminRedirectAlert('Uúario não encontrado!', route('admin-site-users-index'));

			}


		}


		public function destroy(Request $request, $item = null) {}


		// Alterar status do usuário no sistema.
		public function status($user) {


			$user = SiteUser::where('id', $user)->first();
			if(count($user) >= 1) {

				if($user->status == 0) {

					$novo		= 1;
					$success	= 'Usuário ativado com sucesso!';
					$error		= 'Erro! O usuário não pode ser ativado!';

				} else {

					$novo		= 0;
					$success	= 'Usuário desativado com sucesso!';
					$error		= 'Erro! O usuário não pode ser desativado!';

				}


				$usuario			= SiteUser::find($user->id);
				$usuario->status	= $novo;
				if($usuario->save()) {

					echo Helper::AdminRedirectAlert($sucess, route('admin-site-users-index'));

				} else {

					echo Helper::AdminRedirectAlert($error, route('admin-site-users-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Usuário não encontrado!', route('admin-site-users-index'));

			}

		}

	}
