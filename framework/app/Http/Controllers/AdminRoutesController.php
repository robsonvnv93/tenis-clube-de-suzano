<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\User;

	class AdminRoutesController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Administrador',
				'subtitle'	=> 'Listar todas as rotas',
				'buttons'	=> [

					[
						
						'title'	=> 'Adicionar nova rota',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-config-routes-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-config-routes-destroy',
				'search'	=> 'admin-config-routes-index',
				'limit'		=> 15,
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Name',
						'field'		=> 'name',
						'search'	=> true

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Controller',
						'field'		=> 'controller',
						'search'	=> true

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Function',
						'field'		=> 'function',
						'search'	=> true

					],

				],
				'actions'	=> [

					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar rota',
						'route'	=> 'admin-config-routes-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir rota',
						'route'	=> 'admin-config-routes-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'admin_routes', $busca);


			return view('automator.paginator', $data);

		}


		public function view($pagina) {}
		public function create() {}
		public function store(Request $request) {}
		public function edit($pagina) {}
		public function update(Request $request) {}
		public function destroy(Request $request, $item = null) {}


		// Alterar status do usuário no sistema.
		public function status($pagina) {


			$user = SitePage::where('id', $pagina)->first();
			if(count($user) >= 1) {

				if($user->status == 0) {

					$novo		= 1;
					$success	= 'Página ativada com sucesso!';
					$error		= 'Erro! A página não pode ser ativada!';

				} else {

					$novo		= 0;
					$success	= 'Página desativada com sucesso!';
					$error		= 'Erro! A página não pode ser desativada!';

				}


				$usuario			= SitePage::find($user->id);
				$usuario->status	= $novo;
				if($usuario->save()) {

					echo Helper::AdminRedirectAlert($sucess, route('admin-site-pages-index'));

				} else {

					echo Helper::AdminRedirectAlert($error, route('admin-site-pages-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Página não encontrada!', route('admin-site-pages-index'));

			}

		}

	}
