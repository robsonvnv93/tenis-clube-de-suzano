<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Helper;
	use View;

	class SiteController extends Controller {
		

		/**
		 * Create a new controller instance.
		 *
		 * @return void
		*/


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index() {

			return redirect('admin/');

		}

	}
