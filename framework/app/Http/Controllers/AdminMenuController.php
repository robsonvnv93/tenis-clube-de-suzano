<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\UserTypePermission;
	use App\AdminRoute;
	use App\AdminMenu;
	use App\UsersType;
	use Response;

	class AdminMenuController extends Controller {

		public function index(Request $request) {

			$itens		= [];
			$parents	= AdminMenu::where('parent', 0)->orderBy('order')->get();
			if(count($parents) >= 1) {

				foreach ($parents as $parent) {
					
					$submenus	= [];
					$childs		= AdminMenu::where('parent', $parent->id)->orderBy('order')->get();
					if(count($childs) >= 1) {

						foreach ($childs as $child) {
							
							$submenus[]	= [
								
								'id'		=> $child->id,
								'icon'		=> $child->icon,
								'name'		=> $child->name,
								'visible'	=> $child->visible,
								'status'	=> $child->status

							];
						
						}
					
					}

					$itens[]	= [

						'id'		=> $parent->id,
						'icon'		=> $parent->icon,
						'name'		=> $parent->name,
						'visible'	=> $parent->visible,
						'status'	=> $parent->status,
						'childs'	=> $submenus

					];

				}

			}


			$rotas		= [];
			$buscaRotas	= AdminRoute::get();
			if(count($buscaRotas) >= 1) {

				foreach ($buscaRotas as $rota) {
					
					$rotas[] = [
						
						'id'	=> $rota->id,
						'name'	=> $rota->name

					];

				}

			}

			$types	= UsersType::where('status', 1)->orderBy('name')->get();


			$data	= [

				'menu'	=> $itens,
				'types'	=> $types,
				'rotas'	=> $rotas

			];

			return view('admin.config.menu', $data);

		}


		public function store(Request $request) {

			$retorno	= [
				
				'result'	=> false,
				'message'	=> 'Requisição inválida!'

			];

			$dados		= $request->input('dados');
			if(isset($dados)) {

				if($dados != '') {

					$campos		= [

						'name'		=> 'Por favor preencha o "Nome" do menu.',
						'route'		=> 'Por favor selecione a "Rota" do menu.',
						'visible'	=> 'Por favor selecione a "Visibilidade" do menu.',
						'status'	=> 'Por favor selecione o "Status" do menu.'

					];


					$message	= '';
					foreach ($campos as $key => $value) {
						
						if($message == '') {

							if($dados[$key] == '') {

								$message = $value;

							}

						}

					}

					if($message == '') {

						$ultimo	= AdminMenu::orderBy('order', 'DESC')->first();
						if(count($ultimo) >= 1) {

							$order	= $ultimo->order;

						} else {

							$order	= 1;
						
						}

						$novo				= new AdminMenu;
						$novo->route_id		= $dados['route'];
						$novo->icon			= $dados['icon'];
						$novo->name			= $dados['name'];
						$novo->parent		= 0;
						$novo->visible		= $dados['visible'];
						$novo->status		= $dados['status'];
						$novo->order		= $order;
						$novo->created_at	= now();
						if($novo->save()) {

							$menu			= $novo->id;
							$permissions	= [];
							$permissoes		= $dados['permissoes'];
							if(count($permissoes) >= 1) {

								$total			= count($permissoes);
								$ok				= 0;
								foreach ($permissoes as $type) {
									
									$permissao					= '';
									$permissao					= new UserTypePermission;
									$permissao->menu_id			= $menu;
									$permissao->user_type_id	= $type;
									if($permissao->save()) {

										$permissions[]	= $type;
										$ok++;

									}

								}


								if($ok >= $total) {

									$message	= 'Menu cadastrado com sucesso!';	
									
								} else {
									
									$message	= 'Menu cadastrado com sucesso! Porem suas permissões de acesso não foram salvas corretamente.';
								
								}

							} else {

								$message	= 'Menu cadastrado com sucesso!';

							}

							$retorno	= [

								'result'		=> true,
								'message'		=> $message,
								'menu'			=> [

									'id'		=> $novo->id,
									'icon'		=> $dados['icon'],
									'name'		=> $dados['name'],
									'route'		=> $dados['route'],
									'visible'	=> $dados['visible'],
									'status'	=> $dados['status']

								],
								'permissions'	=> $permissions

							];

						} else {

							$retorno['message']	= 'Oops! Alguma coisa deu errado e o menu não pode ser criado.';

						}


					} else {

						$retorno['message']	= $message;

					}

				}

			}

			return response()->json($retorno);

		}


		public function edit(Request $request) {

			$retorno	= [
				
				'result'	=> false,
				'message'	=> 'Requisição inválida!'

			];

			$menu		= $request->input('menu');
			if(isset($menu)) {

				if($menu != '') {

					$menu	= AdminMenu::where('id', $menu)->first();
					if(count($menu) >= 1) {

						$permissions	= [];
						$avalible		= UserTypePermission::select('user_type_id')->where('menu_id', $menu->id)->distinct()->get();
						if(count($avalible) >= 1) {

							foreach ($avalible as $type) {
								
								$permissions[] = $type->user_type_id;

							}

						}

						$retorno		= [

							'result'	=> true,
							'menu'		=> [

								'id'		=> $menu->id,
								'route'		=> $menu->route_id,
								'icon'		=> $menu->icon,
								'name'		=> $menu->name,
								'parent'	=> $menu->parent,
								'visible'	=> $menu->visible,
								'status'	=> $menu->status
							
							],
							'permissions'	=> $permissions
						
						];

					} else {


						$retorno['message']	= 'Menu não encontrado!';

					}

				}

			}

			return response()->json($retorno);

		}


		public function update(Request $request) {

			$retorno	= [
				
				'result'	=> false,
				'message'	=> 'Requisição inválida!'

			];

			$dados		= $request->input('dados');
			if(isset($dados)) {

				if($dados != '') {

					$campos		= [

						'id'		=> 'Menu não encontrado.',
						'name'		=> 'Por favor preencha o "Nome" do menu.',
						'route'		=> 'Por favor selecione a "Rota" do menu.',
						'visible'	=> 'Por favor selecione a "Visibilidade" do menu.',
						'status'	=> 'Por favor selecione o "Status" do menu.'

					];


					$message	= '';
					foreach ($campos as $key => $value) {
						
						if($message == '') {

							if($dados[$key] == '') {

								$message = $value;

							}

						}

					}

					if($message == '') {

						$menu	= AdminMenu::where('id', $dados['id'])->first();
						if(count($menu) >= 1) {

							$editar				= AdminMenu::find($menu->id);
							$editar->route_id	= $dados['route'];
							$editar->icon		= $dados['icon'];
							$editar->name		= $dados['name'];
							$editar->visible	= $dados['visible'];
							$editar->status		= $dados['status'];
							$editar->updated_at	= now();
							if($editar->save()) {

								$permissoes		= $dados['permissoes'];
								if(count($permissoes) >= 1) {

									$id			= $menu->id;
									$busca		= UserTypePermission::where('menu_id', $id)->count();
									if($busca >= 1) {
										
										$limpa	= UserTypePermission::where('menu_id', $id)->delete();

									} else {
										
										$limpa	= true;

									}

									if($limpa) {

										$total			= count($permissoes);
										$ok				= 0;
										foreach ($permissoes as $type) {
											
											$permissao					= '';
											$permissao					= new UserTypePermission;
											$permissao->menu_id			= $id;
											$permissao->user_type_id	= $type;
											if($permissao->save()) {

												$ok++;

											}
										
										}

										if($ok >= $total) {

											$message	= 'Menu editado com sucesso!';	
											
										} else {
											
											$message	= 'Menu editado com sucesso! Porem suas permissões de acesso não foram salvas corretamente.';
										
										}

									} else {

										$message	= 'Menu editado com sucesso! Porem suas permissões de acesso não foram salvas corretamente.';

									}


									if($ok >= $total) {

										$message	= 'Menu editado com sucesso!';
										
									} else {

										$message	= 'Menu editado com sucesso! Porem suas permissões de acesso não foram salvas corretamente.';
									
									}

								} else {

									$message	= 'Menu editado com sucesso!';

								}

								$retorno		= [

									'result'	=> true,
									'message'	=> $message,
									'menu'		=> [

										'id'		=> $menu->id,
										'route'		=> $dados['route'],
										'icon'		=> $dados['icon'],
										'name'		=> $dados['name'],
										'parent'	=> $menu->parent,
										'visible'	=> $dados['visible'],
										'status'	=> $dados['status']
									
									]
								
								];

							} else {

								$retorno['message']	= 'Oops! Alguma coisa deu errado e o menu não pode ser editado.';

							}

						} else {

							$retorno['message']	= 'Menu não encontrado.';

						}

					} else {

						$retorno['message']	= $message;

					}

				}

			}

			return response()->json($retorno);

		}


		public function order(Request $request) {

			$retorno	= [

				'result'	=> false

			];

			$itens		= $request->input('itens');
			if(isset($itens)) {

				$itens	= json_decode($itens);
				if(count($itens) >= 1) {

					$pais	= 0;
					$filhos	= 0;
					$order	= 1;
					foreach ($itens as $parent) {

						$parentID			= $parent->id;
						$pai				= AdminMenu::find($parentID);
						$pai->parent		= 0;
						$pai->order			= $order;
						$pai->updated_at	= now();
						$pai->save();
						$order++;

						if(isset($parent->children)) {

							$childs	= $parent->children;
							if(count($childs) >= 1) {

								foreach ($childs as $child) {

									$filho				= AdminMenu::find($child->id);
									$filho->parent		= $parentID;
									$filho->order		= $order;
									$filho->updated_at	= now();
									$filho->save();
									$order++;
									$filhos++;

								}

							}

						}

						$pais++;

					}

					$total = $pais + $filhos;

					if($order >= $total) {

						$retorno['result']	= true;

					}

				}

			}

			return response()->json($retorno);

		}

		public function destroy(Request $request) {

			$retorno	= [

				'result'	=> false

			];

			$menu	= $request->input('menu');
			if(isset($menu)) {

				if($menu != '') {

					$menu = AdminMenu::where('id', $menu)->first();
					if(count($menu) >= 1) {

						$ok		= 0;
						$childs	= AdminMenu::where('parent', $menu->id)->get();
						if(count($childs) >= 1) {

							$total = (count($childs) + 1);
							foreach ($childs as $child) {

								$delChild = AdminMenu::where('id', $child->id)->delete();
								if($delChild == true) {

									$ok++;

								}

							}

							$del = AdminMenu::where('id', $menu->id)->delete();
							if($del == true) {

								$ok++;

							}

						} else {

							$total = 1;
							$del = AdminMenu::where('id', $menu->id)->delete();
							if($del == true) {

								$ok++;

							}

						}

						if($ok == $total) {

							$retorno['result']	= true;
						
						}

					}

				}

			}

			return response()->json($retorno);

		}

	}