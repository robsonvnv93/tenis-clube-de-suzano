<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\SiteCategoriaGrade;
	use App\SiteGradeItem;
	use App\SiteGrade;
	use Response;
	use Route;

	class AdminSiteGradesController extends Controller {


		public function ajax(Request $request) {

			$retorno	= [

				'result'	=> false,
				'redirect'	=> false,
				'message'	=> 'Solicitação inválida!'

			];

			$action	= $request->input('action');
			if(isset($action)) {

				// Buscar as atividades e os itens da grade.
				if($action == 'get-grade-itens') {

					$categoria	= $request->input('categoria');
					if(isset($categoria)) {

						if($categoria >= 1) {

							$categoria	= SiteCategoriaGrade::where('id', $categoria)->first();
							if(count($categoria) >= 1) {

								$itens		= SiteGrade::where('categoria_id', $categoria->id)->orderBy('order')->get();
								$total		= count($itens);
								$grades		= [];
								$data		= [];
								$_grades	= 0;

								if($total >= 1) {

									foreach ($itens as $item) {

										$busca	= SiteGradeItem::where('grade_id', $item->id)->where('status', 1)->orderBy('hora', 'ASC')->get();
										if(count($busca) >= 1) {

											foreach ($busca as $horario) {
												
												$grades[]	= [

													'id'		=> $horario->id,
													'atividade'	=> $item->name,
													'dia'		=> $horario->dia,
													'hora'		=> $horario->hora,
													'status'	=> $horario->status

												];

												$_grades++;

											}

										}

										$data[]	= [
											
											'id'			=> $item->id,
											'name'			=> $item->name,
											'description'	=> $item->description,
											'status'		=> $item->status
											
										];

									
									}

								}

								$retorno	= [
								
									'result'		=> true,
									'atividades'	=> [

										'total'		=> $total,
										'itens'		=> $data,
										'grades'	=> [

											'total'	=> $_grades,
											'itens'	=> $grades

										]

									]

								];

							}

						}

					}

				}



				// Buscar dados de uma atividade de uma categoria.
				if($action == 'get-grade-item') {

					$item	= $request->input('item');
					if(isset($item)) {

						if($item >= 1) {

							$item	= SiteGrade::where('id', $item)->first();
							if(count($item) >= 1) {

								$retorno['result']	= true;
								$retorno['item']	= [

									'id'		=> $item->id,
									'categoria'	=> $item->categoria_id,
									'name'		=> $item->name,
									'descricao'	=> $item->description,
									'status'	=> $item->status

								];

							} else {

								$retorno['message']	= 'Atividade não encontrada!';

							}

						}

					}

				}



				// Inserir atividade na grade de uma categoria.
				if($action == 'insert-grade-item') {

					$campos	= [

						'categoria'	=> 'Erro! Por favor atualize a página e repita o processo.',
						'item'		=> 'Erro! Por favor atualize a página e repita o processo.',
						'dia'		=> 'Por favor selecione o dia da semana que deseja inserir esta atividade.',
						'hora'		=> 'Por favor preencha a hora que deseja inserir esta atividade.'
					
					];

					$check	= '';
					foreach ($campos as $campo => $mensagem) {

						if($check == '') {

							$valor	= $request->input($campo);
							if(isset($valor)) {

								if($valor == '') {

									$check	= $mensagem;

								}

							} else {

								$check	= $mensagem;
							}

						}
						
					}

					if($check == '') {

						$inserir				= new SiteGradeItem;
						$inserir->grade_id		= $request->input('item');
						$inserir->dia			= $request->input('dia');
						$inserir->hora			= $request->input('hora') . ':00';
						$inserir->status		= 1;
						$inserir->created_at	= now();
						if($inserir->save()) {

							$retorno['result']		= true;
							$retorno['message']		= 'Atividade inserida na grade com sucesso!';
							$retorno['categoria']	= $request->input('categoria');

						} else {

							$retorno['message'] = 'Erro! A atividade não foi inserida na grade.';

						}

					} else {

						$retorno['message'] = $check;

					}

				}



				// Excluir uma atividade da grade de uma categoria.
				if($action == 'delete-grade-item') {

					$item	= $request->input('item');
					if(isset($item)) {

						if($item >= 1) {

							$item	= SiteGradeItem::where('id', $item)->first();
							if(count($item) >= 1) {

								$apagar = SiteGradeItem::where('id', $item->id)->delete();
								if($apagar == true) {

									$retorno	= [

										'result'	=> true,
										'message'	=> 'Atividade removida com sucesso!'

									];

								} else {

									$retorno['message']	= 'Erro! A atividade não pode ser removida da grade.';
								
								}

							} else {

								$retorno['message']	= 'Erro! A atividade não foi localizada dentro da grade.';

							}

						}

					}

				}



				// Cadastrar nova atividade em uma categoria.
				if($action == 'create-grade-item') {

					$campos	= [

						'categoria'	=> 'Erro! Por favor atualize a página e repita o processo.',
						'nome'		=> 'Por favor preencha o nome da atividade.',
						'status'	=> 'Por favor selecione o status da atividade.',
						'descricao'	=> 'Por favor preencha a descrição da atividade.'
					
					];

					$check	= '';
					foreach ($campos as $campo => $mensagem) {

						if($check == '') {

							$valor	= $request->input($campo);
							if(isset($valor)) {

								if($valor == '') {

									$check	= $mensagem;

								}

							} else {

								$check	= $mensagem;
							}

						}
						
					}

					if($check == '') {

						$inserir				= new SiteGrade;
						$inserir->categoria_id	= $request->input('categoria');
						$inserir->name			= $request->input('nome');
						$inserir->description	= $request->input('descricao');
						$inserir->status		= $request->input('status');
						$inserir->order			= 1;
						$inserir->created_at	= now();
						if($inserir->save()) {

							$retorno['result']		= true;
							$retorno['message']		= 'Atividade inserida na categoria com sucesso!';
							$retorno['categoria']	= $request->input('categoria');

						} else {

							$retorno['message'] = 'Erro! A atividade não foi inserida na categoria.';

						}


					} else {

						$retorno['message'] = $check;

					}

				}



				// Editar os dados de uma atividade dentro de uma categoria.
				if($action == 'edit-grade-item') {

					$campos	= [

						'item'		=> 'Erro! Por favor atualize a página e repita o processo.',
						'categoria'	=> 'Erro! Por favor atualize a página e repita o processo.',
						'nome'		=> 'Por favor preencha o nome da atividade.',
						'status'	=> 'Por favor selecione o status da atividade.',
						'descricao'	=> 'Por favor preencha a descrição da atividade.'
					
					];

					$check	= '';
					foreach ($campos as $campo => $mensagem) {

						if($check == '') {

							$valor	= $request->input($campo);
							if(isset($valor)) {

								if($valor == '') {

									$check	= $mensagem;

								}

							} else {

								$check	= $mensagem;
							}

						}
						
					}

					if($check == '') {

						$atualize				= SiteGrade::find($request->input('item'));
						$atualize->name			= $request->input('nome');
						$atualize->description	= $request->input('descricao');
						$atualize->status		= $request->input('status');
						$inserir->updated_at	= now();
						if($atualize->save()) {

							$retorno['result']		= true;
							$retorno['message']		= 'Dados da atividade atualizados com sucesso!';
							$retorno['categoria']	= $request->input('categoria');

						} else {

							$retorno['message'] = 'Erro! Os dados da atividade não foram atualizados.';

						}


					} else {

						$retorno['message'] = $check;

					}

				}

			}


			return response()->json($retorno);

		
		}

		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Grades',
				'subtitle'	=> 'Listar Categorias',
				'buttons'	=> []

			];

			$body	= [

				'search'	=> 'admin-site-grades-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'order',
					'by'	=> 'DESC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> '#ID',
						'class'		=> 'text-center paginator-w-8',
						'field'		=> 'id',
						'search'	=> false

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Nome',
						'field'		=> 'name',
						'class'		=> 'paginator-w-40',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center paginator-w-8',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar Grades da Categoria',
						'route'	=> 'admin-site-grades-view',
						'class'	=> 'btn-info'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'site_categorias_grade', $busca);


			return view('automator.paginator', $data);

		}


		public function view($categoria) {

			$categoria	= SiteCategoriaGrade::where('id', $categoria)->first();
			if(count($categoria) >= 1) {

				$header	= [

					'title'		=> 'Grades',
					'subtitle'	=> 'Visualizar Grades da Categoria',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Categorias',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-grades-index'

						]

					]

				];

				$body	= [

					'indice'	=> $categoria->id,
					'fields'	=> [

						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'name',
							'default'		=> $categoria->name,

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $categoria->status,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'ID',
							'field'			=> 'id',
							'default'		=> $categoria->id,

						]

					]

				];

				$data	= Automator::ViewerMaker($header, $body);

				return view('admin.grades', $data);

			} else {

				echo Helper::AdminRedirectAlert('Categoria não localizada!', route('admin-site-grades-index'));

			}

		}

	}
