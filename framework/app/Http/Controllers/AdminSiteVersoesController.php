<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\AppVersion;
	use Route;

	class AdminSiteVersoesController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Versões',
				'subtitle'	=> 'Listar Versões',
				'buttons'	=> [

					[
						
						'title'	=> 'Cadastrar Nova Versão',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-site-versoes-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-site-versoes-destroy',
				'search'	=> 'admin-site-versoes-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'id',
					'by'	=> 'DESC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Nome',
						'field'		=> 'nome',
						'class'		=> 'paginator-w-40',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar Versão',
						'route'	=> 'admin-site-versoes-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar Versão',
						'route'	=> 'admin-site-versoes-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir Versão',
						'route'	=> 'admin-site-versoes-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'app_versions', $busca);


			return view('automator.paginator', $data);

		}


		public function view($versao) {

			$versao	= AppVersion::where('id', $versao)->first();
			if(count($versao) >= 1) {

				$header	= [

					'title'		=> 'Versões',
					'subtitle'	=> 'Visualizar Versão',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Versões',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-versoes-index'

						],
						[
						
							'title'	=> 'Editar Versão',
							'type'	=> 'link',
							'class'	=> 'btn-warning',
							'route'	=> 'admin-site-versoes-edit',
							'args'	=> $versao->id

						],
						[
						
							'title'	=> 'Cadastrar Nova Versão',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-versoes-create'

						]

					]

				];

				$body	= [

					'indice'	=> $versao->id,
					'fields'	=> [

						[

							'type'			=> 'file-upload',
							'name'			=> 'Arquivo APK',
							'field'			=> 'arquivo',
							'default'		=> asset('storage/versoes/' . $versao->arquivo),

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'nome',
							'default'		=> $versao->nome,

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Comentários',
							'field'			=> 'comentarios',
							'default'		=> $versao->comentarios,

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $versao->status,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::ViewerMaker($header, $body);

				return view('automator.viewer-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Versão não localizada!', route('admin-site-versoes-index'));

			}

		}
		

		public function create() {

			$header	= [

				'title'		=> 'Versões',
				'subtitle'	=> 'Cadastrar Nova Versão',
				'buttons'	=> [

					[
						
						'title'	=> 'Listar Versões',
						'type'	=> 'link',
						'class'	=> 'btn-primary',
						'route'	=> 'admin-site-versoes-index'

					]

				]

			];

			$body	= [

				'action'	=> 'admin-site-versoes-store',
				'fields'	=> [

					[

						'type'			=> 'file-upload',
						'name'			=> 'APK da Versão',
						'field'			=> 'imagem',
						'required'		=> true

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Nome',
						'field'			=> 'nome',
						'required'		=> true

					],
					[

						'type'			=> 'editor',
						'name'			=> 'Comentários',
						'field'			=> 'comentarios',
						'required'		=> false

					],
					[

						'type'		=> 'select',
						'name'		=> 'Status',
						'field'		=> 'status',
						'required'	=> true,
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',

							]

						]

					]

				]

			];

			$data	= Automator::FormMaker($header, $body);

			return view('automator.form-maker', $data);

		}


		public function store(Request $request) {

			$campos	= [

				'nome'		=> 'O campo "Nome" é obrigatório',
				'status'	=> 'O campo "Status" é obrigatório'
			
			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$imagem	= $request->file('imagem');
				if($request->hasFile('imagem')) {

					$imagemname	= uniqid(date('HisYmd'));
					$extension	= $request->imagem->extension();
					$nameFile	= "{$imagemname}.{$extension}";

					$upload		= $request->imagem->storeAs('public/app_versions/', $nameFile);
					if(!$upload) {

						echo Helper::AdminRedirectAlert('Erro ao realizar upload do APK!', route('admin-site-versoes-index'));

					} else {

						$nova				= new AppVersion;
						$nova->arquivo		= $nameFile;
						$nova->nome			= $request->input('nome');
						$nova->comentarios	= $request->input('comentarios');
						$nova->status		= $request->input('status');
						$nova->created_at	= now();
						if($nova->save()) {

							echo Helper::AdminRedirectAlert('Versão cadastrada com sucesso!', route('admin-site-versoes-index'));

						} else {

							echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e a versão não pode ser cadastrada.', route('admin-site-versoes-create'));

						}

					}

				} else {

					echo Helper::AdminRedirectAlert('O campo "APK da Versão" é obrigatório!', route('admin-site-versoes-create'));

				}

			} else {

				echo Helper::AdminRedirectAlert($retorno, route('admin-site-versoes-create'));

			}

		}
		

		public function edit($versao) {

			$versao	= AppVersion::where('id', $versao)->first();
			if(count($versao) >= 1) {

				$header	= [

					'title'		=> 'Versões',
					'subtitle'	=> 'Editar Versão',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Versões',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-versoes-index'

						],
						[
						
							'title'	=> 'Cadastrar Nova Versão',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-versoes-create'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-site-versoes-update',
					'indice'	=> $versao->id,
					'fields'	=> [

						[

							'type'			=> 'file-upload',
							'name'			=> 'APK da Versão',
							'field'			=> 'imagem',
							'default'		=> asset('storage/app_versions/' . $versao->arquivo),
							'required'		=> false

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'nome',
							'default'		=> $versao->nome,
							'required'		=> true

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Comentários',
							'field'			=> 'comentarios',
							'default'		=> $versao->comentarios,
							'required'		=> false

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $versao->status,
							'required'	=> true,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Versão não encontrada!', route('admin-site-versoes-index'));

			}

		}


		public function update(Request $request) {

			$id		= $request->input('id');
			$versao	= AppVersion::where('id', $id)->first();
			if(count($versao) >= 1) {

				$campos	= [

					'nome'		=> 'O campo "Nome" é obrigatório',
					'status'	=> 'O campo "Status" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {

					$nameFile	= $versao->arquivo;
					if($request->hasFile('imagem')) {

						$imagemname	= uniqid(date('HisYmd'));
						$extension	= $request->imagem->extension();
						$nameFile	= "{$imagemname}.{$extension}";

						$upload		= $request->imagem->storeAs('public/app_versions/', $nameFile);
						if(!$upload) {

							$nameFile	= $versao->arquivo;

						}

					}

					$atualizar				= AppVersion::find($id);
					$atualizar->arquivo		= $nameFile;
					$atualizar->nome		= $request->input('nome');
					$atualizar->comentarios	= $request->input('comentarios');
					$atualizar->status		= $request->input('status');
					$atualizar->updated_at	= now();
					if($atualizar->save()) {

						echo Helper::AdminRedirectAlert('Versão editada com sucesso!', route('admin-site-versoes-edit', $id));

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e a versão não pode ser editada.', route('admin-site-versoes-edit', $id));

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-site-versoes-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Versão não encontrada!', route('admin-site-versoes-index'));

			}


		}


		public function destroy(Request $request) {

			$itens	= $request->input('itens');
			return Automator::ExcluirRegistros('app_versions', $itens, route('admin-site-versoes-index'), 'Versão/Versões excluida(s) com sucesso', 'Erro! A(s) Versão/Versões não foi/foram excluida(s).');

		}


		// Alterar status do versão.
		public function status($versao) {

			$versao = AppVersion::where('id', $versao)->first();
			if(count($versao) >= 1) {

				if($versao->status == 0) {

					$novo		= 1;
					$success	= 'Versão ativada com sucesso!';
					$error		= 'Erro! A versão não pode ser ativada!';

				} else {

					$novo		= 0;
					$success	= 'Versão desativada com sucesso!';
					$error		= 'Erro! A versão não pode ser desativada!';

				}


				$versao				= AppVersion::find($versao->id);
				$versao->status		= $novo;
				$versao->updated_at	= now();
				if($versao->save()) {

					echo Helper::AdminRedirectAlert($success, route('admin-site-versoes-index'));

				} else {

					echo Helper::AdminRedirectAlert($error, route('admin-site-versoes-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Versão não encontrada!', route('admin-site-versoes-index'));

			}

		}

	}
