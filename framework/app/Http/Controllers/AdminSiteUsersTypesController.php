<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\UsersType;

	class AdminSiteUsersTypesController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Site',
				'subtitle'	=> 'Listar todos os tipos de usuários',
				'buttons'	=> [

					[
						
						'title'	=> 'Adicionar novo tipo de usuário',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-site-users-types-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-site-users-types-destroy',
				'search'	=> 'admin-site-users-types',
				'limit'		=> 15,
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Nome',
						'field'		=> 'name',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar tipo de usuário',
						'route'	=> 'admin-site-users-types-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar tipo de usuário',
						'route'	=> 'admin-site-users-types-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir tipo de usuário',
						'route'	=> 'admin-site-users-types-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'site_users_types', $busca);


			return view('automator.paginator', $data);

		}


		public function view($user) {}
		public function create() {}
		public function store(Request $request) {}
		public function edit($user) {}
		public function update(Request $request) {}
		public function destroy(Request $request, $item = null) {}


		// Alterar status do tipo de usuário no sistema.
		public function status($user) {


			$usersType = UsersType::where('id', $user)->first();
			if(count($usersType) >= 1) {

				if($usersType->status == 0) {

					$novo		= 1;
					$success	= 'Tipo de usuário ativado com sucesso!';
					$error		= 'Erro! O tipo de usuário não pode ser ativado!';

				} else {

					$novo		= 0;
					$success	= 'Tipo de usuário desativado com sucesso!';
					$error		= 'Erro! O tipo de usuário não pode ser desativado!';

				}


				$tipo			= UsersType::find($usersType->id);
				$tipo->status	= $novo;
				if($tipo->save()) {

					echo Helper::AdminRedirectAlert($sucess, route('admin-users-types-index'));

				} else {

					echo Helper::AdminRedirectAlert($error, route('admin-users-types-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Tipo de usuário não encontrado!', route('admin-users-types-index'));

			}

		}

	}
