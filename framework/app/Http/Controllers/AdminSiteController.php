<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Helper;
	use App\SiteUsersType;
	use App\SiteLang;
	use App\SiteConfig;

	class AdminSiteController extends Controller {
		
		/**
		 * Create a new controller instance.
		 *
		 * @return void
		*/

		public function __construct() {}


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/
		

		public function configs() {

			$title			= SiteConfig::GetConfig('title');
			$description	= SiteConfig::GetConfig('description');
			$keywords		= SiteConfig::GetConfig('keywords');
			$langs			= SiteLang::where('status', 1)->orderBy('name')->get();
			$users_types	= SiteUsersType::where('status', 1)->get();

			$data	= [

				'title'			=> $title,
				'description'	=> $description,
				'keywords'		=> $keywords,
				'langs'			=> $langs,
				'users_types'	=> $users_types
			
			];

			return view('admin.site', $data);
			
		}


		public function custom() {

			echo 'lala123';
		}

	}
