<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\AppHelper;
	use App\AppContatoAssunto;
	use App\SiteGaleriaItens;
	use App\SiteGradeItem;
	use App\SiteCategoriaGrade;
	use App\AppNotificacao;
	use App\AppNotificacaoLog;
	use App\AppNotificacaoEnvio;
	use App\SiteUserToken;
	use App\SiteGaleria;
	use App\SiteNoticia;
	use App\SiteHorario;
	use App\SiteEvento;
	use App\SiteBanner;
	use App\SiteGrade;
	use App\AppVersion;
	use App\AppContato;
	use App\SitePagina;
	use Response;

	class AppController extends Controller {


		/*
			
			==========|> PADRAO DE ESTRUTURA DE FUNCOES DESTE CONTROLADOR. <|==========

			// Descrição da função
			public function nome_funcao(Request $request) {

				$_post	= $this->GetPostData($request->input('_POST'));

				$Key	= $_post['_key'];
				if(isset($Key)) {

					if($Key != '') {

						if($this->CheckRequest($Key) == true) {

						}

					}

				}


				return $this->SendReturn();

			}

		*/ 


		public $appKey		= 'com.tenisclubedesuzano.app';
		public $retorno		= [

			'result'	=> false,
			'message'	=> 'Requisição inválida'

		];



		// Valida se a requisição veio apartir do aplicativo para evitar ataques.
		private function CheckRequest($key) {

			if($key == $this->appKey) {

				return true;

			} else {

				return false;

			}

		}
		


		// Transforma os dados recebidos em post de objeto para array.
		private function GetPostData($_post) {
			
			$retorno	= [];
			$campos		= json_decode($_post);
			if(count($campos) >= 1) {

				foreach ($campos as $key => $value) {

					$retorno[$key] = $value;

				}

			}


			return $retorno;

		}
		


		// Retorna em formato json a resposta do web-service.
		private function SendReturn() {

			return response()->json($this->retorno);

		}



		// Verifica se o cliente possui a ultima versão do app instalada.
		public function CheckVersion(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$Version	= $_post['_version'];
						if(isset($Version)) {

							if($Version != '') {

								$AppVersion = AppVersion::where('status', 1)->orderBy('id', 'DESC')->first();
								if($Version == $AppVersion->nome) {

									$this->retorno	= [

										'result'	=> true,
										'updated'	=> true,

									];


								} else {

									$this->retorno	= [

										'result'	=> true,
										'updated'	=> false

									];

								}

							}

						}

					}

				}

			}

			
			return $this->SendReturn();

		}


		public function UpdateAppVersion(Request $request) {


			$AppVersion = AppVersion::where('status', 1)->last();
			$arquivo	= $AppVersion->arquivo;
			$file		= asset('storage/app_versions/' . $arquivo);
			
			return response()->download($file, "tenis-clube-de-suzano.apk");

		}



		// Valida o FCMToken enviado pelo dispositivo.
		public function CheckToken(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$Token	= $_post['_token'];
						if(isset($Token)) {

							if($Token != '') {

								$FCMTokenID	= $_post['_FCMTokenID'];
								if(isset($FCMTokenID)) {

									if($FCMTokenID != '') {

										$_getFCMTokenID = SiteUserToken::where('id', $FCMTokenID)->first();
										if(count($_getFCMTokenID) >= 1) {

											$FCMToken	= [

												'user'	=> $_getFCMTokenID->user_id,
												'hash'	=> $_getFCMTokenID->token

											];



											if($FCMToken['hash'] == $Token) {

												$this->retorno	= [

													'result'		=> true

												];

											}

										}

									}

								}

							}

						}

					}

				}

			}


			return $this->SendReturn();

		}



		// Salva o FCMToken enviado pelo dispositivo no banco de dados.
		public function CreateToken(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if($this->CheckRequest($Key) == true) {

				$Token	= $_post['_token'];
				if(isset($Token)) {

					if($Token != '') {

						$_checkFCMTokenHash = SiteUserToken::where('token', $Token)->first();
						if(count($_checkFCMTokenHash) >= 1) {

							$this->retorno	= [

								'result'		=> true,
								'FCMTokenID'	=> $_checkFCMTokenHash->id
							
							];

						} else {

							$novo				= new SiteUserToken;
							$novo->user_id		= 0;
							$novo->token		= $Token;
							$novo->created_at	= now();
							if($novo->save()) {

								$this->retorno	= [

									'result'		=> true,
									'FCMTokenID'	=> $novo->id
								
								];

							}

						}

					}

				}

			}


			return $this->SendReturn();

		}



		// Paginação de notificações para o app.
		public function GetNotificacoes(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$Token	= $_post['_token'];
						if(isset($Token)) {

							if($Token != '') {

								$FCMTokenID	= $_post['_FCMTokenID'];
								if(isset($FCMTokenID)) {

									if($FCMTokenID != '') {

										$_getFCMTokenID = SiteUserToken::where('id', $FCMTokenID)->first();
										if(count($_getFCMTokenID) >= 1) {

											if($_getFCMTokenID->token == $Token) {

												$todas		= 0;
												$total		= 0;
												$itens		= [];
												$senders	= AppNotificacaoLog::where('user_token_id', $FCMTokenID)->where('status', 1)->get();
												if(count($senders)) {

													$logs	= [];
													$envios	= [];
													foreach ($senders as $envio) {
														
														if(!in_array($envio->sender_id, $envios)) {

															$envios[]	= $envio->sender_id;

														}
														
														$logs[]		= $envio->id;

													}

													$limite		= 3;
													$_limite	= $_post['_limite'];
													if(isset($_limite)) {

														if($_limite != '') {

															$limite = $_limite;

														}

													}

													$pagina		= 1;
													$_pagina	= $_post['_pagina'];
													if(isset($_pagina)) {

														if($_pagina != '') {

															$pagina = $_pagina;

														}

													}

													$inicio			= $pagina - 1;
													$inicio			= $limite * $inicio;

													$todas			= AppNotificacaoEnvio::whereIn('id', $envios)->count();

													$notificacoes	= AppNotificacaoEnvio::whereIn('id', $envios)->offset($inicio)->limit($limite)->orderBy('created_at', 'DESC')->get();
													$total			= count($notificacoes);
													if($total >= 1) {

														$a = 0;
														foreach ($notificacoes as $envio) {

															$notificacao	= AppNotificacao::where('id', $envio->notificacao_id)->where('status', 1)->first();
															if(count($notificacao) >= 1) {

																$aberto		= 0;
																$open		= AppNotificacaoLog::where('sender_id', $envio->id)->where('user_token_id', $FCMTokenID)->where('status', 1)->first();
																if(isset($open->opened_at)) {

																	if($open->opened_at != '' && $open->opened_at != NULL && $open->opened_at != 'NULL') {
																		
																		$aberto = 1;

																	}

																}

																$titulo		= str_limit(strip_tags($notificacao->titulo), 46);
																$conteudo	= str_limit(strip_tags($notificacao->conteudo), 147);
																if($tipo == 1) {

																	$item_id = $envion->id;

																} else {

																	$item_id = $notificacao->item_id;

																}

																$itens[] = [

																	'log_id'			=> $open->id,
																	'notificacao_id'	=> $envio->id,
																	'data_notificacao'	=> AppHelper::formatDate('view-datetime', $envio->created_at),
																	'tipo'				=> $notificacao->tipo,
																	'item_id'			=> $item_id,
																	'titulo'			=> $titulo,
																	'conteudo'			=> $conteudo,
																	'aberto'			=> $aberto

																];

															}

															$a++;
															
														}
													
													}

												}


												$this->retorno	= [

													'result'	=> true,
													'todas'		=> $todas,
													'total'		=> $total,
													'itens'		=> $itens

												];

											}

										}

									}

								}

							}

						}

					}

				}

			}


			return $this->SendReturn();

		}



		// Exibição dos detalhes de uma notificação selecionada dentro do app.
		public function ShowNotificacao(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$envio	= $_post['envio_id'];
						if(isset($envio)) {

							if($envio != '') {

								$envio	= AppNotificacaoEnvio::where('id', $envio)->first();
								if(count($envio) >= 1) {

									$notificacao	= $envio->notificacao_id;
									$notificacao	= AppNotificacao::where('id', $notificacao)->where('status', 1)->first();
									if(count($notificacao) >= 1) {

										$this->retorno	= [

											'result'			=> true,
											'data_notificacao'	=> AppHelper::formatDate('view-datetime', $envio->created_at),
											'titulo'			=> $notificacao->titulo,
											'conteudo'			=> $notificacao->conteudo

										];

									} else {

										$this->retorno['message']	= 'Mensagem não encontrada!';

									}

								} else {

									$this->retorno['message']	= 'Mensagem não encontrada!';

								}

							}

						}

					}

				}

			}


			return $this->SendReturn();

		}



		// Alteração do status de visualização do log de uma notificação.
		public function ChangeNotificacaoStatus(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$log	= $_post['log_id'];
						if(isset($log)) {

							if($log != '') {

								$log	= AppNotificacaoLog::where('id', $log)->first();
								if(count($log) >= 1) {

									$atualizar				= AppNotificacaoLog::find($log->id);
									$atualizar->opened_at	= now();
									if($atualizar->save()) {

										$this->retorno['result']	= true;

									}

								}

							}

						}

					}

				}

			}


			return $this->SendReturn();

		}



		// Retorna os ultimos 3 banners cadastrados no sistema.
		public function GetBanners(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$itens		= [];
						$banners	= SiteBanner::where('status', 1)->orderBy('created_at', 'DESC')->limit(3)->get();
						if(count($banners) >= 1) {

							$conta = 1;
							foreach ($banners as $banner) {

								$imagem	= asset('images/logo.png');
								if(isset($banner->image)) {

									if($banner->image != '') {

										$imagem = asset('storage/banners/' . $banner->image);

									}

								}

								$link	= '';
								if(isset($banner->link)) {

									if($banner->link != '') {

										$link = $banner->link;

									}

								}

								$itens[] = [

									'id'		=> $banner->id,
									'numero'	=> $conta,
									'imagem'	=> $imagem,
									'link'		=> $link,
									'nome'		=> $banner->name

								];

								$conta++;
								
							}

						}
						
						$this->retorno	= [

							'result'	=> true,
							'total'		=> count($banners),
							'itens'		=> $itens

						];

					}

				}

			}


			return $this->SendReturn();

		}



		// Paginação de noticias para o app.
		public function GetNoticias(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$itens		= [];
						$limite		= 3;
						$_limite	= $_post['_limite'];
						if(isset($_limite)) {

							if($_limite != '') {

								$limite = $_limite;

							}

						}

						$pagina		= 1;
						$_pagina	= $_post['_pagina'];
						if(isset($_pagina)) {

							if($_pagina != '') {

								$pagina = $_pagina;

							}

						}

						$inicio		= $pagina - 1;
						$inicio		= $limite * $inicio;

						$todas		= SiteNoticia::where('status', 1)->count();

						$noticias	= SiteNoticia::where('status', 1)->orderBy('created_at', 'ASC')->offset($inicio)->limit($limite)->get();
						if(count($noticias) >= 1) {

							foreach ($noticias as $noticia) {

								$titulo	= str_limit(strip_tags($noticia->titulo), 46);
								$resumo	= str_limit(strip_tags($noticia->texto), 147);

								$imagem	= asset('images/logo.png');
								if(isset($noticia->image)) {

									if($noticia->image != '') {

										$arquivo = ('storage/noticias/' . $noticia->image);
										$imagem = asset($arquivo);

									}

								}

								$itens[] = [

									'noticia_id'	=> $noticia->id,
									'imagem'		=> $imagem,
									'data_noticia'	=> AppHelper::formatDate('view-datetime', $noticia->created_at),
									'titulo'		=> $titulo,
									'resumo'		=> $resumo

								];
								
							}

						}
						
						$this->retorno	= [

							'result'	=> true,
							'todas'		=> $todas,
							'total'		=> count($noticias),
							'itens'		=> $itens

						];

					}

				}

			}


			return $this->SendReturn();

		}



		// Exibição dos detalhes de uma noticia selecionada dentro do app.
		public function ShowNoticia(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$noticia	= $_post['noticia_id'];
						if(isset($noticia)) {

							if($noticia != '') {

								$noticia	= SiteNoticia::where('id', $noticia)->where('status', 1)->first();
								if(count($noticia) >= 1) {

									$imagem	= asset('images/logo.png');
									if(isset($noticia->image)) {
										
										if($noticia->image != '') {

											$arquivo = asset('storage/noticias/' . $noticia->image);
											// if(file_exists($arquivo)) {

												$imagem = $arquivo;

											// }

										}

									}

									$this->retorno	= [

										'result'		=> true,
										'data_noticia'	=> AppHelper::formatDate('view-datetime', $noticia->created_at),
										'imagem'		=> $imagem,
										'titulo'		=> $noticia->titulo,
										'texto'			=> $noticia->texto

									];

								} else {

									$this->retorno['message']	= 'Noticia não encontrada!';

								}

							}

						}

					}

				}

			}


			return $this->SendReturn();

		}



		// Paginação de eventos para o app.
		public function GetEventos(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$itens		= [];
						$limite		= 5;
						$pagina		= 1;
						$_pagina	= $_post['_pagina'];
						if(isset($_pagina)) {

							if($_pagina != '') {

								$pagina = $_pagina;

							}

						}

						$inicio		= $pagina - 1;
						$inicio		= $limite * $inicio;

						$todas		= SiteEvento::where('status', 1)->count();
						
						$eventos	= SiteEvento::where('status', 1)->orderBy('data_evento', 'ASC')->offset($inicio)->limit($limite)->get();
						if(count($eventos) >= 1) {

							foreach ($eventos as $evento) {

								$titulo	= str_limit(strip_tags($evento->titulo), 46);
								$resumo	= str_limit(strip_tags($evento->texto), 147);

								$imagem	= asset('images/logo.png');
								if(isset($evento->image)) {
								
									if($evento->image != '') {

										$arquivo = asset('storage/eventos/' . $evento->image);
										// if(file_exists($arquivo)) {

											$imagem = $arquivo;

										// }
									}

								}

								$itens[] = [

									'evento_id'		=> $evento->id,
									'data_evento'	=> AppHelper::formatDate('view-date', $evento->data_evento),
									'imagem'		=> $imagem,
									'titulo'		=> $titulo,
									'resumo'		=> $resumo

								];
								
							}

						}
						
						$this->retorno	= [

							'result'	=> true,
							'todas'		=> $todas,
							'total'		=> count($eventos),
							'itens'		=> $itens

						];

					}

				}

			}


			return $this->SendReturn();

		}



		// Exibição dos detalhes de um evento selecionado dentro do app.
		public function ShowEvento(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$evento	= $_post['evento_id'];
						if(isset($evento)) {

							if($evento != '') {

								$evento	= SiteEvento::where('id', $evento)->where('status', 1)->first();
								if(count($evento) >= 1) {

									$imagem	= asset('images/logo.png');
									if(isset($evento->image)) {
										
										if($evento->image != '') {


											$arquivo = asset('storage/eventos/' . $evento->image);
											// if(file_exists($arquivo)) {

												$imagem = $arquivo;

											// }

										}

									}

									$this->retorno	= [

										'result'		=> true,
										'imagem'		=> $imagem,
										'data_evento'	=> AppHelper::formatDate('view-date', $evento->data_evento),
										'titulo'		=> $evento->titulo,
										'texto'			=> $evento->texto

									];

								} else {

									$this->retorno['message']	= 'Evento não encontrado!';

								}

							}

						}

					}

				}

			}


			return $this->SendReturn();

		}



		// Listagem de horários de funcionamento de atrações do clube.
		public function GetHorarios(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$itens		= [];
						$horarios	= SiteHorario::where('status', 1)->orderBy('order')->get();
						if(count($horarios) >= 1) {

							foreach ($horarios as $horario) {

								$imagem	= asset('images/logo.png');
								if(isset($horario->image)) {

									$arquivo = asset('storage/horarios/' . $horario->image);
									// if(file_exists($arquivo)) {

										$imagem = $arquivo;

									// }

								}

								$itens[] = [

									'imagem'	=> $imagem,
									'nome'		=> $horario->name,
									'texto'		=> $horario->description

								];
								
							}

						}
						
						$this->retorno	= [

							'result'	=> true,
							'total'		=> count($horarios),
							'itens'		=> $itens

						];

					}

				}

			}


			return $this->SendReturn();

		}



		// Lista as atividades disponiveis para visualização das grades cadastradas no sistema.
		public function GetGrades(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$primeiro	= 1;
						$itens		= [];
						$categorias = SiteCategoriaGrade::where('status', 1)->orderBy('order')->get();
						$total		= count($categorias);
						if($total >= 1) {

							$a	= 0;
							foreach ($categorias as $categoria) {
								
								if($a == 0) { $primeiro = $categoria->id; }
								$grade		= [];
								$busca		= SiteGrade::where('categoria_id', $categoria->id)->where('status', 1)->orderBy('name')->get();

								foreach ($busca as $item) {
									
									$grade[]	= [

										'atividade_id'			=> $item->id,
										'atividade_nome'		=> $item->name,
										'atividade_descricao'	=> $item->description,
										'atividade_total'		=> SiteGradeItem::where('grade_id', $item->id)->where('status', 1)->count()

									];

								}

								$itens[]	= [

									'categoria_id'		=> $categoria->id,
									'categoria_nome'	=> $categoria->name,
									'categoria_itens'	=> $grade

								];

								$a++;

							}

						}

						$this->retorno	= [

							'result'	=> true,
							'total'		=> $total,
							'itens'		=> $itens,
							'primeiro'	=> $primeiro

						];

					}

				}

			}


			return $this->SendReturn();

		}



		// Mostra os detalhes da grade de uma atividade selecionada dentro do app.
		public function ShowGrade(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$Atividade = $_post['atividade_id'];
						if(isset($Atividade)) {

							if($Atividade != '') {

								$Atividade = SiteGrade::where('id', $Atividade)->first();
								if(count($Atividade) >= 1) {


									$nomeDia	= [

										1 => 'Segunda',
										2 => 'Terça',
										3 => 'Quarta',
										4 => 'Quinta',
										5 => 'Sexta',
										6 => 'Sabado',
										7 => 'Domingo'

									];

									$categoria		= SiteCategoriaGrade::where('id', $Atividade->categoria_id)->first();
									$categoria		= $categoria->name;

									$itens		= [];
									$total		= SiteGradeItem::where('grade_id', $Atividade->id)->count();
									if($total >= 1) {

										for ($i = 1; $i < 7; $i++) { 
											
											$horarios	= [];
											$dia		= SiteGradeItem::where('grade_id', $Atividade->id)->where('dia', $i)->orderBy('hora')->get();
											if(count($dia) >= 1) {

												foreach ($dia as $horario) {
													
													$horarios[] = [

														'item' => substr($horario->hora, 0, 5)

													];

												}

												$itens[]	= [

													'dia'	=> $nomeDia[$i],
													'itens' => $horarios

												];

											}

										}

									}

									$this->retorno	= [

										'result'	=> true,
										'categoria'	=> $categoria,
										'nome'		=> $Atividade->name,
										'descricao'	=> $Atividade->description,
										'total'		=> $total,
										'horarios'	=> $itens

									];

								}

							}

						}

					}

				}

			}


			return $this->SendReturn();

		}



		// Faz as listagem das galerias para o app.
		public function GetGalerias(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$itens		= [];
						$galerias	= SiteGaleria::where('status', 1)->orderBy('order')->get();
						if(count($galerias) >= 1) {

							foreach ($galerias as $galeria) {

								$titulo	= str_limit(strip_tags($galeria->titulo), 46);
								$resumo	= str_limit(strip_tags($galeria->texto), 147);

								$imagem	= asset('images/logo.png');
								if(isset($galeria->capa)) {

									$arquivo = asset('storage/galerias/' . $galeria->id . '/' . $galeria->capa);
									// if(file_exists($arquivo)) {

										$imagem = $arquivo;

									// }

								}

								$itens[] = [

									'id'		=> $galeria->id,
									'capa'		=> $imagem,
									'titulo'	=> $titulo,
									'resumo'	=> $resumo

								];
								
							}

						}
						
						$this->retorno	= [

							'result'	=> true,
							'total'		=> count($galerias),
							'itens'		=> $itens

						];

					}

				}

			}


			return $this->SendReturn();

		}



		// Mostra os itens da galeria selecionada.
		public function ShowGaleria(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$galeria	= $_post['galeria_id'];
						if(isset($galeria)) {

							if($galeria != '') {

								$galeria	= SiteGaleria::where('id', $galeria)->where('status', 1)->first();
								if(count($galeria) >= 1) {

									$itens	= [];
									$busca	= SiteGaleriaItens::where('galeria_id', $galeria->id)->where('status', 1)->orderBy('order')->get();
									$total	= count($busca);
									if($total >= 1) {

										foreach ($busca as $foto) {

											if($foto->tipo == 1) {

												$img = asset('images/logo.png');
												if(isset($foto->content)) {
													
													if($foto->content != '') {

														$file = asset('storage/galerias/' . $galeria->id . '/' . $foto->content);
														// if(file_exists($file)) {

															$img = $file;

														// }

													}

												}

												$conteudo = $img;

											} else {

												$conteudo	= '';

											}

											$itens[] = [
												
												'tipo'		=> $foto->tipo,
												'conteudo'	=> $conteudo

											];

										}	

									}



									$capa	= asset('images/logo.png');
									if(isset($galeria->capa)) {

										if($galeria->capa != '') {

											$arquivo = asset('storage/galerias/' . $galeria->id . '/' . $galeria->capa);
											// if(file_exists($arquivo)) {

												$capa = $arquivo;

											// }

										}

									}


									$this->retorno	= [

										'result'	=> true,
										'capa'		=> $capa,
										'titulo'	=> $galeria->titulo,
										'texto'		=> $galeria->texto,
										'total'		=> $total,
										'itens'		=> $itens

									];

								} else {

									$this->retorno['message']	= 'Galeria não encontrada!';

								}

							}

						}

					}

				}

			}


			return $this->SendReturn();

		}



		public function Contato(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$formulario	= $_post['_formulario'];
						if(isset($formulario)) {

							if($formulario != '') {

								$campos	= [

									'nome'		=> 'Por favor digite seu Nome.',
									'telefone'	=> 'Por favor digite seu Telefone.',
									'email'		=> 'Por favor digite seu Email',
									'horario'	=> 'Por favor informe o melhor Horário para contato.',
									'assunto'	=> 'Por favor selecione o Assunto.',
									'mensagem'	=> 'Por favor digite sua Mensagem.'
								
								];

								$check	= '';
								foreach ($campos as $campo => $mensagem) {

									if($check == '') {

										$valor	= $formulario->$campo;
										if(isset($valor)) {

											if($valor == '') {

												$check	= $mensagem;

											}

										} else {

											$check	= $mensagem;
										}

									}
									
								}

								if($check == '') {

									$enviar				= new AppContato;
									$enviar->nome		= $formulario->nome;
									$enviar->telefone	= $formulario->telefone;
									$enviar->email		= $formulario->email;
									$enviar->horario	= $formulario->horario;
									$enviar->assunto_id	= $formulario->assunto;
									$enviar->mensagem	= $formulario->mensagem;
									$enviar->created_at	= now();

									if($enviar->save()) {

										$this->retorno['result']	= true;

									} else {

										$this->retorno['message']	= 'Erro! Sua mensagem não foi enviada.';

									}

								} else {

									$this->retorno['message']	= $check;

								}

							}

						}

					}

				}

			}


			return $this->SendReturn();

		}




		// Salva no banco de dados informações enviadas pelo formulário de contato dentro do banco de dados do sistema.
		public function ContatoAssuntos(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$itens		= [];
						$assuntos	= AppContatoAssunto::where('status', 1)->orderBy('order')->get();
						if(count($assuntos) >= 1) {

							foreach ($assuntos as $assunto) {

								$itens[] = [

									'id'		=> $assunto->id,
									'nome'		=> $assunto->nome,
									'descricao'	=> $assunto->descricao

								];
								
							}

						}
						
						$this->retorno	= [

							'result'	=> true,
							'total'		=> count($assuntos),
							'itens'		=> $itens

						];

					}

				}

			}


			return $this->SendReturn();

		}
		


		public function ShowPagina(Request $request) {

			$_post	= $this->GetPostData($request->input('_POST'));

			$Key	= $_post['_key'];
			if(isset($Key)) {

				if($Key != '') {

					if($this->CheckRequest($Key) == true) {

						$pagina	= $_post['pagina_id'];
						if(isset($pagina)) {

							if($pagina != '') {

								$pagina	= SitePagina::where('id', $pagina)->where('status', 1)->first();
								if(count($pagina) >= 1) {

									$this->retorno	= [

										'result'	=> true,
										'titulo'	=> $pagina->titulo,
										'texto'		=> $pagina->conteudo

									];

								} else {

									$this->retorno['message'] = 'Página não encontrada!';

								}

							}

						}

					}

				}

			}


			return $this->SendReturn();


		}


	}