<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\AppPagina;
	use Route;


	class AdminAppPaginasController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'App',
				'subtitle'	=> 'Listar Paginas',
				'buttons'	=> []

			];

			$body	= [

				'destroy'	=> '',
				'search'	=> 'admin-app-paginas-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'created_at',
					'by'	=> 'ASC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Página',
						'field'		=> 'titulo',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar Página',
						'route'	=> 'admin-app-paginas-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar Página',
						'route'	=> 'admin-app-paginas-edit',
						'class'	=> 'btn-primary'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'app_paginas', $busca);


			return view('automator.paginator', $data);

		}


		public function view($pagina) {

			$pagina	= AppPagina::where('id', $pagina)->first();
			if(count($pagina) >= 1) {

				$header	= [

					'title'		=> 'App',
					'subtitle'	=> 'Visualizar Página',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Páginas',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-app-paginas-index'

						],
						[
						
							'title'	=> 'Editar Página',
							'type'	=> 'link',
							'class'	=> 'btn-warning',
							'route'	=> 'admin-app-paginas-edit',
							'args'	=> $pagina->id

						]

					]

				];

				$body	= [

					'indice'	=> $pagina->id,
					'fields'	=> [

						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo',
							'field'			=> 'titulo',
							'default'		=> $pagina->titulo,

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Conteudo',
							'field'			=> 'conteudo',
							'default'		=> $pagina->conteudo,

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $pagina->status,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::ViewerMaker($header, $body);

				return view('automator.viewer-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert("Página não encontrada!", route('admin-app-paginas-index'));

			}

		}

		

		public function edit($pagina) {

			$pagina	= AppPagina::where('id', $pagina)->first();
			if(count($pagina) >= 1) {

				$header	= [

					'title'		=> 'App',
					'subtitle'	=> 'Editar Página',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Páginas',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-app-paginas-index'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-app-paginas-update',
					'indice'	=> $pagina->id,
					'fields'	=> [

						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo',
							'field'			=> 'titulo',
							'default'		=> $pagina->titulo,
							'required'		=> true

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Conteudo',
							'field'			=> 'conteudo',
							'default'		=> $pagina->conteudo,
							'required'		=> true

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $pagina->status,
							'required'	=> true,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Página não encontrada!', route('admin-app-paginas-index'));

			}

		}


		public function update(Request $request) {

			$id		= $request->input('id');
			$pagina	= AppPagina::where('id', $id)->first();
			if(count($pagina) >= 1) {

				$campos	= [

					'titulo'		=> 'O campo "Titulo" é obrigatório',
					'conteudo'		=> 'O campo "Texto" é obrigatório',
					'status'		=> 'O campo "Status" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {


					$atualizar				= AppPagina::find($id);
					$atualizar->titulo		= $request->input('titulo');
					$atualizar->conteudo	= $request->input('conteudo');
					$atualizar->status		= $request->input('status');
					$atualizar->updated_at	= now();
					if($atualizar->save()) {

						echo Helper::AdminRedirectAlert('Página editada com sucesso!', route('admin-app-paginas-edit', $id));

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e a página não pode ser editada.', route('admin-app-paginas-edit', $id));

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-app-paginas-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Página não encontrada!', route('admin-app-paginas-index'));

			}


		}

	}
