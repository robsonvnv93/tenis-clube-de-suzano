<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\AppEmail;
	use Route;


	class AdminAppEmailsController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Contato',
				'subtitle'	=> 'Listar Destinatários',
				'buttons'	=> [

					[
						
						'title'	=> 'Novo Destinatário',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-app-emails-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-app-emails-destroy',
				'search'	=> 'admin-app-emails-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'nome',
					'by'	=> 'ASC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Nome',
						'field'		=> 'nome',
						'search'	=> true

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Email',
						'field'		=> 'email',
						'class'		=> 'paginator-w-40',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar Destinatário',
						'route'	=> 'admin-app-emails-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir Destinatário',
						'route'	=> 'admin-app-emails-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'app_emails', $busca);


			return view('automator.paginator', $data);

		}
		


		public function create() {

			$header	= [

				'title'		=> 'Contato',
				'subtitle'	=> 'Novo Destinatário',
				'buttons'	=> [

					[
						
						'title'	=> 'Listar Destinatários',
						'type'	=> 'link',
						'class'	=> 'btn-primary',
						'route'	=> 'admin-app-emails-index'

					]

				]

			];

			$body	= [

				'action'	=> 'admin-app-emails-store',
				'fields'	=> [

					[

						'type'			=> 'simple-text',
						'name'			=> 'Nome',
						'field'			=> 'nome',
						'required'		=> true

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Email',
						'field'			=> 'email',
						'required'		=> true

					],
					[

						'type'		=> 'select',
						'name'		=> 'Status',
						'field'		=> 'status',
						'required'	=> true,
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',

							]

						]

					]

				]

			];

			$data	= Automator::FormMaker($header, $body);

			return view('automator.form-maker', $data);

		}


		public function store(Request $request) {

			$campos	= [

				'nome'		=> 'O campo "Nome" é obrigatório',
				'email'		=> 'O campo "Email" é obrigatório',
				'status'	=> 'O campo "Status" é obrigatório'
			
			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$busca	= AppEmail::where('email', $request->input('email'))->count();
				if($busca >= 1) {

					echo Helper::AdminRedirectAlert("Este endereço de e-mail já está cadastrado na lista de destinatários.", route('admin-app-emails-create'));

				} else {

					$nova				= new AppEmail;
					$nova->nome			= $request->input('nome');
					$nova->email		= $request->input('email');
					$nova->status		= $request->input('status');
					$nova->created_at	= now();
					if($nova->save()) {

						echo Helper::AdminRedirectAlert('Destinatário cadastrado com sucesso!', route('admin-app-emails-index'));

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o destinatário não pode ser cadastrado.', route('admin-app-emails-create'));

					}

				}

			} else {

				echo Helper::AdminRedirectAlert($retorno, route('admin-app-emails-create'));

			}

		}
		

		public function edit($destinatario) {

			$destinatario	= AppEmail::where('id', $destinatario)->first();
			if(count($destinatario) >= 1) {

				$header	= [

					'title'		=> 'Contato',
					'subtitle'	=> 'Editar Destinatário',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Destinatários',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-app-emails-index'

						],
						[
						
							'title'	=> 'Novo Destinatário',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-app-emails-create'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-app-emails-update',
					'indice'	=> $destinatario->id,
					'fields'	=> [

						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'titulo',
							'default'		=> $destinatario->nome,
							'required'		=> true

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'E-mail',
							'field'			=> 'email',
							'default'		=> $destinatario->email,
							'required'		=> true

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $destinatario->status,
							'required'	=> true,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Destinatário não encontrado!', route('admin-app-emails-index'));

			}

		}


		public function update(Request $request) {

			$id				= $request->input('id');
			$destinatario	= AppEmail::where('id', $id)->first();
			if(count($destinatario) >= 1) {

				$campos	= [

					'nome'		=> 'O campo "Nome" é obrigatório',
					'email'		=> 'O campo "Email" é obrigatório',
					'status'	=> 'O campo "Status" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {

					$atual	= $destinatário->email;

					if($request->input('email') != $atual) {

						$busca	= AppEmail::where('email', $request->input('email'))->count();
						if($busca >= 1) {

							echo Helper::AdminRedirectAlert("Este endereço de e-mail já está cadastrado na lista de destinatários.", route('admin-app-emails-edit', $id));

						} else {

							$atualizar				= AppEmail::find($id);
							$atualizar->nome		= $request->input('nome');
							$atualizar->email		= $request->input('email');
							$atualizar->status		= $request->input('status');
							$atualizar->updated_at	= now();
							if($atualizar->save()) {

								echo Helper::AdminRedirectAlert('Destinatário editado com sucesso!', route('admin-app-emails-edit', $id));

							} else {

								echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o destinatário não pode ser editado.', route('admin-app-emails-edit', $id));

							}

						}

					} else {

						$atualizar				= AppEmail::find($id);
						$atualizar->nome		= $request->input('nome');
						$atualizar->status		= $request->input('status');
						$atualizar->updated_at	= now();
						if($atualizar->save()) {

							echo Helper::AdminRedirectAlert('Destinatário editado com sucesso!', route('admin-app-emails-edit', $id));

						} else {

							echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o destinatário não pode ser editado.', route('admin-app-emails-edit', $id));

						}

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-app-emails-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Destinatário não encontrado!', route('admin-app-emails-index'));

			}


		}


		public function destroy(Request $request) {

			$itens	= $request->input('itens');
			return Automator::ExcluirRegistros('app_emails', $itens, route('admin-app-emails-index'), 'Destinatário(s) excluido(s) com sucesso.', 'Erro! O(s) Destinatário(s) não foi/foram excluido(s).');

		}


		// Alterar status do destinatario.
		public function status($destinatario) {

			$destinatario = AppEmail::where('id', $destinatario)->first();
			if(count($destinatario) >= 1) {

				if($destinatario->status == 0) {

					$novo		= 1;
					$success	= 'Destinatário ativado com sucesso!';
					$error		= 'Erro! O destinatário não pode ser ativado!';

				} else {

					$novo		= 0;
					$success	= 'Destinatário desativado com sucesso!';
					$error		= 'Erro! O destinatário não pode ser desativado!';

				}


				$destinatario				= AppEmail::find($destinatario->id);
				$destinatario->status		= $novo;
				$destinatario->updated_at	= now();
				if($destinatario->save()) {

					echo Helper::AdminRedirectAlert($success, route('admin-app-emails-index'));

				} else {

					echo Helper::AdminRedirectAlert($error, route('admin-app-emails-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Destinatário não encontrado!', route('admin-app-emails-index'));

			}

		}

	}
