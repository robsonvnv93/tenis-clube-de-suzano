<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Helper;
	use App\SiteConfig;
	use Auth;
	use File;

	class SiteFrontController extends Controller {
		
		/**
		 * Create a new controller instance.
		 *
		 * @return void
		*/

		public function __construct() {}


		/**
		 * Show the themes cms application.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function themes() {

			$themes			= [];
			$path			= resource_path('views/themes/');
			$diretorio		= dir($path);
			while($pasta = $diretorio -> read()){
				
				if($pasta != '.') {

					if($pasta != '..') {

						$tema			= resource_path('views/themes/' . $pasta . '/');
						$temadir		= dir($tema);
						$index			= file_exists($tema . 'index.blade.php');
						$layout			= file_exists($tema . 'layout.blade.php');
						$css			= file_exists($tema . 'style.css');
						if($index == 1 && $layout == 1 && $css == 1) {

							while($arquivo = $temadir -> read()){
								
								if($arquivo == 'style.css') {

									$content		= File::get(resource_path('views/themes/' . $pasta . '/style.css'));
									$cabecalhoStart	= strpos($content, '/*');
									$cabecalhoEnd	= strpos($content, '*/');
									$cabecalho		= substr($content, ($cabecalhoStart + 2), ($cabecalhoEnd - 2));
									$quebrar		= explode(';', $cabecalho);
									if(file_exists(resource_path('views/themes/' . $pasta . '/screenshot.jpg'))) {

										$screenshot = '<img style="width: 100%; display: block;" src="' . resource_path('views/themes/' . $pasta . '/screenshot.jpg') . '" />';

									} else {

										$screenshot = '<img style="width: 100%; display: block;" src="https://via.placeholder.com/200x200?text=Sem+Imagem" />';

									}

									$basico			= [

										'screenshot'	=> $screenshot,
										'pasta'			=> $pasta,

									];

									$data			= [];
									foreach ($quebrar as $linha) {

										$linha				= trim($linha);
										$posicao			= strpos($linha, ':');
										$variavel			= strtolower(substr($linha, 0, $posicao));
										$valor				= substr($linha, ($posicao + 2));
										if($variavel != '') {

											$data[$variavel]	= $valor;

										}

									}

									$themes[]	= array_merge($basico, $data);

								}

							}
						}

					}

				}

			}

			$active	= 'default';
			$config	= SiteConfig::where('name', 'theme')->first();
			if(count($active) >= 1) {
				
				$active	= $config->value;

			}

			$data	=[
				
				'active'	=> $active,
				'themes'	=> $themes

			];
			
			return view('admin.themes', $data);

		}


		public function editor() {


			return view('admin.editor');

		}

	}
