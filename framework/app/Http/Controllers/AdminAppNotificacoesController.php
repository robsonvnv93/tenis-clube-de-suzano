<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\AppHelper;
	use App\Helpers\Helper;
	use App\AppNotificacaoLog;
	use App\AppNotificacaoEnvio;
	use App\AppNotificacao;
	use App\SiteUserToken;
	use App\SiteNoticia;
	use App\SiteEvento;
	use Response;
	use Route;
	use Auth;


	class AdminAppNotificacoesController extends Controller {



		public function ajax(Request $request) {

			$retorno	= [

				'result'	=> false,
				'redirect'	=> false,
				'message'	=> 'Solicitação inválida!'

			];

			$action	= $request->input('action');
			if(isset($action)) {

				// Enviar Notificação Simples.
				if($action == 'send-push') {

					$notificacao	= $request->input('notificacao');
					if(isset($notificacao)) {

						if($notificacao >= 1) {

							$notificacao	= AppNotificacao::where('id', $notificacao)->first();
							if(count($notificacao) >= 1) {

								$tokens		= SiteUserToken::where('status', 1)->get();
								if(count($tokens) >= 1) {

									$salvar					= new AppNotificacaoEnvio;
									$salvar->notificacao_id	= $notificacao->id;
									$salvar->user_id		= Auth::user()->id;
									$salvar->created_at		= now();
									if($salvar->save()) {

										$sender	= $salvar->id;
										$ids	= [];

										foreach ($tokens as $token) {
											
											$ids[]	= [
												
												'id'	=> $token->id,
												'FCM'	=> $token->token

											];

										}



										$enviar		= AppHelper::enviarPush($sender, $notificacao, $ids);
										$retorno	= [

											'result'	=> true,
											'success'	=> $enviar['success'],
											'fail'		=> $enviar['fail']

										];



										$atualizar				= AppNotificacao::find($notificacao->id);
										$atualizar->log			= AppNotificacaoEnvio::where('notificacao_id', $notificacao->id)->count();
										$atualizar->status		= 1;
										$atualizar->updated_at	= now();
										$atualizar->save();
										
									}

								} else {

									$retorno['message']	= 'Não existem dispositivos cadastrados para serem notificados.';
									
								}


							} else {

								$retorno['message']	= 'Notificação não encontrada!';

							}

						}

					}

				}



				// Buscar informações sobre uma notificação simples.
				if($action == 'get-notification-status') {

					$notificacao	= $request->input('notificacao');
					if(isset($notificacao)) {

						if($notificacao >= 1) {

							$notificacao	= AppNotificacao::where('id', $notificacao)->first();
							if(count($notificacao) >= 1) {

								$titulo	= 'Enviar Notificação';
								$botao	= 'btn-success';
								$icone	= 'comment';

								if($notificacao->status == 1) {

									$titulo	= 'Renviar Notificação';
									$botao	= 'btn-warning';
									$icone	= 'comments';

								}


								$retorno	= [

									'result'	=> true,
									'titulo'	=> $titulo,
									'classe'	=> $botao,
									'status'	=> $notificacao->status,
									'icone'		=> $icone

								];

							}

						}

					}

				}


				
				// Buscar informações sobre uma noticia.
				if($action == 'get-noticia-status') {

					$noticia	= $request->input('item');
					if(isset($noticia)) {

						if($noticia >= 1) {

							$noticia	= SiteNoticia::where('id', $noticia)->first();
							if(count($noticia) >= 1) {

								$titulo	= 'Enviar Notificação';
								$botao	= 'btn-success';
								$icone	= 'bell';

								if($noticia->status == 0) {

									$titulo	= 'Envio Indisponivel';
									$botao	= 'btn-warning';
									$icone	= 'bell-slash';

								}

								$notificacao = str_limit(strip_tags($noticia->titulo), 46);

								$retorno	= [

									'result'		=> true,
									'titulo'		=> $titulo,
									'notificacao'	=> $notificacao,
									'classe'		=> $botao,
									'tipo'			=> 2,
									'status'		=> $noticia->status,
									'icone'			=> $icone

								];

							}

						}

					}

				}


				
				// Buscar informações sobre um evento.
				if($action == 'get-evento-status') {

					$evento	= $request->input('item');
					if(isset($evento)) {

						if($evento >= 1) {

							$evento	= SiteEvento::where('id', $evento)->first();
							if(count($evento) >= 1) {

								$titulo	= 'Enviar Notificação';
								$botao	= 'btn-success';
								$icone	= 'bell';

								if($evento->status == 0) {

									$titulo	= 'Envio Indisponivel';
									$botao	= 'btn-warning';
									$icone	= 'bell-slash';

								}

								$notificacao = str_limit(strip_tags($evento->titulo), 131);

								$retorno	= [

									'result'		=> true,
									'titulo'		=> $titulo,
									'notificacao'	=> $notificacao,
									'classe'		=> $botao,
									'tipo'			=> 3,
									'status'		=> $evento->status,
									'icone'			=> $icone

								];

							}

						}

					}

				}


				// Enviar push de noticias ou eventos.
				if($action == 'create-push') {

					$tipo		= $request->input('tipo');
					$item		= $request->input('item');
					$titulo		= $request->input('titulo');
					$conteudo	= $request->input('conteudo');

					$campos	= [

						'tipo'		=> 'Solicitação inválida.',
						'item'		=> 'Solicitação inválida.',
						'titulo'	=> 'Por favor digite seu Titulo da notificação.',
						'conteudo'	=> 'Por favor digite o Conteudo da notificação.'
					
					];


					$check	= '';
					foreach ($campos as $campo => $mensagem) {

						if($check == '') {

							$valor	= $request->input($campo);
							if(isset($valor)) {

								if($valor == '') {

									$check	= $mensagem;

								}

							} else {

								$check	= $mensagem;
							}

						}
						
					}


					if($check == '') {

						$nova				= new AppNotificacao;
						$nova->tipo			= $request->input('tipo');
						$nova->item_id		= $request->input('item');
						$nova->titulo		= $request->input('titulo');
						$nova->conteudo		= $request->input('conteudo');
						$nova->status		= 0;
						$nova->created_at	= now();
						if($nova->save()) {

							$retorno	= [
								
								'result'		=> true,
								'message'		=> 'Notificação registrada com sucesso!',
								'notificacao'	=> $nova->id

							];

						} else {

							$retorno['message']	= 'Erro! A notificação não pode ser cadastrada.';

						}

					} else {

						$retorno['message']	= $check;

					}

				}


			}

			return response()->json($retorno);

		}

		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'App',
				'subtitle'	=> 'Listar Notificações',
				'buttons'	=> [

					[
						
						'title'	=> 'Nova Notificação',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-app-notificacoes-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-app-notificacoes-destroy',
				'search'	=> 'admin-app-notificacoes-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'created_at',
					'by'	=> 'DESC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center paginator-w-5',
						'name'		=> 'Tipo',
						'field'		=> 'tipo',
						'options'	=> [
							
							[
								
								'value'	=> 1,
								'text'	=> 'Notificação'

							],
							[
								
								'value'	=> 2,
								'text'	=> 'Noticia'

							],
							[
								
								'value'	=> 3,
								'text'	=> 'Evento'

							]

						]

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Data',
						'mask'		=> 'datetime2',
						'class'		=> 'text-center paginator-w-8',
						'field'		=> 'created_at',
						'search'	=> true

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Titulo',
						'field'		=> 'titulo',
						'class'		=> 'paginator-w-25',
						'search'	=> true

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'N. de Envios',
						'field'		=> 'log',
						'class'		=> 'text-center paginator-w-5',
						'search'	=> false

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center paginator-w-8',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> 0,
								'text'	=> 'Envio Pendente',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> 1,
								'text'	=> 'Envio Realizado',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar Notificação',
						'route'	=> 'admin-app-notificacoes-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar Notificação',
						'route'	=> 'admin-app-notificacoes-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'refresh fa-spin',
						'type'	=> 'link',
						'title'	=> 'Carregando ...',
						'route'	=> 'admin-app-notificacoes-ajax',
						'class'	=> 'btn-dark btn-send-push btn-send-push-cursor'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir Notificação',
						'route'	=> 'admin-app-notificacoes-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'app_notificacoes', $busca);


			return view('automator.paginator', $data);

		}



		public function view($notificacao) {

			$notificacao	= AppNotificacao::where('id', $notificacao)->first();
			if(count($notificacao) >= 1) {

				$header	= [

					'title'		=> 'App',
					'subtitle'	=> 'Visualizar Notificação',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Notificações',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-app-notificacoes-index'

						]

					]

				];


				$titulolog	= '&nbsp;';
				$dadoslog	= '&nbsp;';
				$envios		= AppNotificacaoEnvio::where('notificacao_id', $notificacao->id)->orderBy('created_at')->get();
				if(count($envios) >= 1) {

					$titulolog	= 'Log de Envios';
					$dadoslog	= '<div style="position: relative; display: table; max-width: 56%; background-color: #EFEFEF; padding-left: 20px; padding-right: 20px;">';
					foreach ($envios as $envio) {
						
						$sucesso	= AppNotificacaoLog::where('sender_id', $envio->id)->where('status', 1)->count();
						$falhas		= AppNotificacaoLog::where('sender_id', $envio->id)->where('status', 0)->count();
						$total		= ($sucesso + $falhas);

						$dadoslog .= '<div class="row" style="border-bottom: 3px solid #F7F7F7; padding-bottom: 20px; padding-top: 20px;">';

							$dadoslog .= '<div class="col-xs-12"><b>' . (Helper::GetUserData($envio->user_id, 'name')) . '</b> enviou esta notificação em <b>' . (Helper::formatDate('view-datetime', $envio->created_at)) . '</b> - <b id="detalhes-envio-botao-' . $envio->id . '" onclick="visualizarDetalhesEnvio(' . $envio->id . ');" class="text-primary" style="cursor: pointer;">Visualizar Detalhes</b></div>';
							$dadoslog .= '<div id="detalhes-envio-' . $envio->id . '" data-display="0" class="col-xs-12" style="display: none;">';
								
								$dadoslog .= '<div class="row">';

									$dadoslog .= '<div class="col-xs-12 col-sm-4 text-center text-primary" style="padding-top: 10px; padding-bottom: 10px;">';

										$dadoslog .= '<div style="font-size: 20px;">' . $total . '</div>';
										$dadoslog .= '<div style="font-size: 16px;">Total</div>';

									$dadoslog .= '</div>';
									$dadoslog .= '<div class="col-xs-12 col-sm-4 text-center text-success" style="padding-top: 10px; padding-bottom: 10px;">';

										$dadoslog .= '<div style="font-size: 20px;">' . $sucesso . '</div>';
										$dadoslog .= '<div style="font-size: 16px;">Enviadas</div>';

									$dadoslog .= '</div>';
									$dadoslog .= '<div class="col-xs-12 col-sm-4 text-center text-danger" style="padding-top: 10px; padding-bottom: 10px;">';
										
										$dadoslog .= '<div style="font-size: 20px;">' . $falhas . '</div>';
										$dadoslog .= '<div style="font-size: 16px;">Falharam</div>';

									$dadoslog .= '</div>';

								$dadoslog .= '</div>';

							$dadoslog .= '</div>';
						
						$dadoslog .= '</div>';

					}

					$dadoslog .= '</div>';

				}

				$noticia	= '';
				$evento		= '';

				if($notificacao->tipo == 2) {

					$buscaNoticia	= SiteNoticia::where('id', $notificacao->item_id)->first();
					if(count($buscaNoticia) >= 1) {

						$noticia	= '<u>' . $buscaNoticia->titulo . '</u>';

					} else {

						$noticia = '<u>Noticia não encontrada.</u>';

					}

				} elseif($notificacao->tipo == 3) {

					$buscaEvento	= SiteEvento::where('id', $notificacao->item_id)->first();
					if(count($buscaEvento) >= 1) {

						$evento	= '<u>' . $buscaEvento->titulo . '</u>';

					} else {

						$evento = '<u>Evento não encontrado.</u>';

					}

				}


				$body	= [

					'indice'	=> $notificacao->id,
					'fields'	=> [

						[

							'type'			=> 'simple-text',
							'name'			=> 'Criada em',
							'field'			=> 'created_at',
							'default'		=> Helper::formatDate('view-datetime', $notificacao->created_at),

						],
						[

							'type'		=> 'select',
							'name'		=> 'Tipo',
							'field'		=> 'tipo',
							'default'	=> $notificacao->tipo,
							'options'	=> [
								
								[
									
									'value'	=> '1',
									'text'	=> 'Notificação',

								],
								[
									
									'value'	=> '2',
									'text'	=> 'Noticia - ' . $noticia,

								],
								[
									
									'value'	=> '3',
									'text'	=> 'Evento - ' . $evento,

								]

							]

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo',
							'field'			=> 'titulo',
							'default'		=> $notificacao->titulo,

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Conteudo',
							'field'			=> 'conteudo',
							'default'		=> $notificacao->conteudo,

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $notificacao->status,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Envio Pendente',
									'class'	=> 'text-danger'

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Envio Realizado',
									'class'	=> 'text-success'

								]

							]

						],
						[

							'type'			=> 'simple-text',
							'name'			=> $titulolog,
							'field'			=> 'id',
							'default'		=> $dadoslog,

						]

					]

				];

				$data	= Automator::ViewerMaker($header, $body);

				return view('automator.viewer-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert("Notificação não encontrada!", route('admin-app-notificacoes-index'));

			}

		}
		


		public function create() {

			$header	= [

				'title'		=> 'App',
				'subtitle'	=> 'Nova Notificação',
				'buttons'	=> [

					[
						
						'title'	=> 'Listar Notificações',
						'type'	=> 'link',
						'class'	=> 'btn-primary',
						'route'	=> 'admin-app-notificacoes-index'

					]

				]

			];

			$body	= [

				'action'	=> 'admin-app-notificacoes-store',
				'fields'	=> [

					[

						'type'			=> 'simple-text',
						'name'			=> 'Titulo',
						'field'			=> 'titulo',
						'maxlength'		=> 191,
						'required'		=> true

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Conteudo',
						'field'			=> 'conteudo',
						'maxlength'		=> 191,
						'required'		=> true

					]

				]

			];

			$data	= Automator::FormMaker($header, $body);

			return view('automator.form-maker', $data);

		}


		public function store(Request $request) {

			$campos	= [

				'titulo'	=> 'O campo "Titulo" é obrigatório',
				'conteudo'	=> 'O campo "Conteudo" é obrigatório'
			
			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$nova				= new AppNotificacao;
				$nova->tipo			= 1;
				$nova->item_id		= 0;
				$nova->titulo		= $request->input('titulo');
				$nova->conteudo		= $request->input('conteudo');
				$nova->status		= 0;
				$nova->created_at	= now();
				if($nova->save()) {

					echo Helper::AdminRedirectAlert('Notificação cadastrada com sucesso!', route('admin-app-notificacoes-index'));

				} else {

					echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e a notificação não pode ser cadastrada.', route('admin-app-notificacoes-create'));

				}

			} else {

				echo Helper::AdminRedirectAlert($retorno, route('admin-app-notificacoes-create'));

			}

		}
		


		public function edit($notificacao) {

			$notificacao	= AppNotificacao::where('id', $notificacao)->first();
			if(count($notificacao) >= 1) {

				$header	= [

					'title'		=> 'App',
					'subtitle'	=> 'Editar Notificação',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Notificações',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-app-notificacoes-index'

						],
						[
						
							'title'	=> 'Nova Notificação',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-app-notificacoes-create'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-app-notificacoes-update',
					'indice'	=> $notificacao->id,
					'fields'	=> [

						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo',
							'field'			=> 'titulo',
							'default'		=> $notificacao->titulo,
							'required'		=> true

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Conteudo',
							'field'			=> 'conteudo',
							'default'		=> $notificacao->conteudo,
							'required'		=> true

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Notificação não encontrada!', route('admin-app-notificacoes-index'));

			}

		}


		public function update(Request $request) {

			$id				= $request->input('id');
			$notificacao	= AppNotificacao::where('id', $id)->first();
			if(count($notificacao) >= 1) {

				$campos	= [

					'titulo'	=> 'O campo "Nome" é obrigatório',
					'conteudo'	=> 'O campo "Email" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {

					$atualizar				= AppNotificacao::find($id);
					$atualizar->titulo		= $request->input('titulo');
					$atualizar->conteudo	= $request->input('conteudo');
					$atualizar->updated_at	= now();
					if($atualizar->save()) {

						echo Helper::AdminRedirectAlert('Notificação editada com sucesso!', route('admin-app-notificacoes-edit', $id));

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e a notificação não pode ser editada.', route('admin-app-notificacoes-edit', $id));

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-app-notificacoes-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Notificação não encontrada!', route('admin-app-notificacoes-index'));

			}


		}


		public function destroy(Request $request) {

			$itens	= $request->input('itens');
			return Automator::ExcluirRegistros('app_notificacoes', $itens, route('admin-app-notificacoes-index'), 'Notificação/Notificações excluida(s) com sucesso.', 'Erro! A(s) Notificação/Notificações não foi/foram excluida(s).');

		}

	}
