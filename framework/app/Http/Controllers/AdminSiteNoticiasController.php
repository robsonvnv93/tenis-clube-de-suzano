<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\SiteNoticia;
	use Route;


	class AdminSiteNoticiasController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Noticias',
				'subtitle'	=> 'Listar Noticias',
				'buttons'	=> [

					[
						
						'title'	=> 'Cadastrar Nova Noticia',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-site-noticias-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-site-noticias-destroy',
				'search'	=> 'admin-site-noticias-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'created_at',
					'by'	=> 'ASC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Titulo',
						'field'		=> 'titulo',
						'class'		=> 'paginator-w-40',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center paginator-w-8',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'refresh fa-spin',
						'type'	=> 'link',
						'title'	=> 'Carregando ...',
						'route'	=> 'admin-app-notificacoes-ajax',
						'class'	=> 'btn-dark btn-send-push-notificacoes btn-send-push-noticia btn-send-push-cursor'

					],
					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar Noticia',
						'route'	=> 'admin-site-noticias-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar Noticia',
						'route'	=> 'admin-site-noticias-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir Noticia',
						'route'	=> 'admin-site-noticias-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'site_noticias', $busca);


			return view('automator.paginator', $data);

		}


		public function view($noticia) {

			$noticia	= SiteNoticia::where('id', $noticia)->first();
			if(count($noticia) >= 1) {

				$header	= [

					'title'		=> 'Noticias',
					'subtitle'	=> 'Visualizar Noticia',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Noticias',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-noticias-index'

						],
						[
						
							'title'	=> 'Editar Noticia',
							'type'	=> 'link',
							'class'	=> 'btn-warning',
							'route'	=> 'admin-site-noticias-edit',
							'args'	=> $noticia->id

						],
						[
						
							'title'	=> 'Cadastrar Nova Noticia',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-noticias-create'

						]

					]

				];

				$body	= [

					'indice'	=> $noticia->id,
					'fields'	=> [

						[

							'type'			=> 'file-image',
							'name'			=> 'Capa da Noticia',
							'field'			=> 'image',
							'default'		=> asset('storage/noticias/' . $noticia->image),

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo',
							'field'			=> 'titulo',
							'default'		=> $noticia->titulo,

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Texto',
							'field'			=> 'texto',
							'default'		=> $noticia->texto,

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $noticia->status,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::ViewerMaker($header, $body);

				return view('automator.viewer-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert("Noticia não encontrada!", route('admin-site-noticias-index'));

			}

		}
		

		public function create() {

			$header	= [

				'title'		=> 'Noticias',
				'subtitle'	=> 'Cadastrar Nova Noticia',
				'buttons'	=> [

					[
						
						'title'	=> 'Listar Noticias',
						'type'	=> 'link',
						'class'	=> 'btn-primary',
						'route'	=> 'admin-site-noticias-index'

					]

				]

			];

			$body	= [

				'action'	=> 'admin-site-noticias-store',
				'fields'	=> [

					[

						'type'			=> 'file-upload',
						'name'			=> 'Capa da Noticia',
						'field'			=> 'imagem',
						'required'		=> false

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Titulo',
						'field'			=> 'titulo',
						'required'		=> true

					],
					[

						'type'			=> 'editor',
						'name'			=> 'Texto',
						'field'			=> 'texto',
						'required'		=> true

					],
					[

						'type'		=> 'select',
						'name'		=> 'Status',
						'field'		=> 'status',
						'required'	=> true,
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',

							]

						]

					]

				]

			];

			$data	= Automator::FormMaker($header, $body);

			return view('automator.form-maker', $data);

		}


		public function store(Request $request) {

			$campos	= [

				'titulo'		=> 'O campo "Titulo" é obrigatório',
				'texto'			=> 'O campo "Texto" é obrigatório',
				'status'		=> 'O campo "Status" é obrigatório'
			
			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$nameFile	= '';
				if($request->hasFile('imagem')) {

					$imagemname	= uniqid(date('HisYmd'));
					$extension	= $request->imagem->extension();
					$nameFile	= "{$imagemname}.{$extension}";

					$upload		= $request->imagem->storeAs('public/noticias/', $nameFile);
					if(!$upload) {

						$nameFile	= '';

					}

				}

				$nova				= new SiteNoticia;
				$nova->image		= $nameFile;
				$nova->titulo		= $request->input('titulo');
				$nova->texto		= $request->input('texto');
				$nova->status		= $request->input('status');
				$nova->created_at	= now();
				if($nova->save()) {

					echo Helper::AdminRedirectAlert('Noticia cadastrada com sucesso!', route('admin-site-noticias-index'));

				} else {

					echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o noticia não pode ser cadastrada.', route('admin-site-noticias-create'));

				}

			} else {

				echo Helper::AdminRedirectAlert($retorno, route('admin-site-noticias-index'));

			}

		}
		

		public function edit($noticia) {

			$noticia	= SiteNoticia::where('id', $noticia)->first();
			if(count($noticia) >= 1) {

				$header	= [

					'title'		=> 'Noticias',
					'subtitle'	=> 'Editar Noticia',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Noticias',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-noticias-index'

						],
						[
						
							'title'	=> 'Cadastrar Nova Noticia',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-noticias-create'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-site-noticias-update',
					'indice'	=> $noticia->id,
					'fields'	=> [

						[

							'type'			=> 'file-upload',
							'name'			=> 'Capa da Noticia',
							'field'			=> 'imagem',
							'default'		=> asset('storage/noticias/' . $noticia->image),
							'required'		=> false

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo',
							'field'			=> 'titulo',
							'default'		=> $noticia->titulo,
							'required'		=> true

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Texto',
							'field'			=> 'texto',
							'default'		=> $noticia->texto,
							'required'		=> true

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $noticia->status,
							'required'	=> true,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Noticia não encontrada!', route('admin-site-noticias-index'));

			}

		}


		public function update(Request $request) {

			$id			= $request->input('id');
			$noticia	= SiteNoticia::where('id', $id)->first();
			if(count($noticia) >= 1) {

				$campos	= [

					'titulo'		=> 'O campo "Titulo" é obrigatório',
					'texto'			=> 'O campo "Texto" é obrigatório',
					'status'		=> 'O campo "Status" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {

					$nameFile	= $noticia->image;
					if($request->hasFile('imagem')) {

						$imagemname	= uniqid(date('HisYmd'));
						$extension	= $request->imagem->extension();
						$nameFile	= "{$imagemname}.{$extension}";

						$upload		= $request->imagem->storeAs('public/noticias/', $nameFile);
						if(!$upload) {

							$nameFile	= '';

						}

					}

					$atualizar				= SiteNoticia::find($id);
					$atualizar->image		= $nameFile;
					$atualizar->titulo		= $request->input('titulo');
					$atualizar->texto		= $request->input('texto');
					$atualizar->status		= $request->input('status');
					$atualizar->updated_at	= now();
					if($atualizar->save()) {

						echo Helper::AdminRedirectAlert('Noticia editada com sucesso!', route('admin-site-noticias-edit', $id));

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e a noticia não pode ser editada.', route('admin-site-noticias-edit', $id));

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-site-noticias-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Noticia não encontrada!', route('admin-site-noticias-index'));

			}


		}


		public function destroy(Request $request) {

			$itens	= $request->input('itens');
			return Automator::ExcluirRegistros('site_noticias', $itens, route('admin-site-noticias-index'), 'Noticia(s) excluida(s) com sucesso.', 'Erro! A(s) Noticia(s) não foi/foram excluida(s).');

		}


		// Alterar status da noticia.
		public function status($noticia) {

			$noticia = SiteNoticia::where('id', $noticia)->first();
			if(count($noticia) >= 1) {

				if($noticia->status == 0) {

					$novo		= 1;
					$success	= 'Noticia ativada com sucesso!';
					$error		= 'Erro! A noticia não pode ser ativada!';

				} else {

					$novo		= 0;
					$success	= 'Noticia desativada com sucesso!';
					$error		= 'Erro! A noticia não pode ser desativada!';

				}


				$noticia				= SiteNoticia::find($noticia->id);
				$noticia->status		= $novo;
				$noticia->updated_at	= now();
				if($noticia->save()) {

					echo Helper::AdminRedirectAlert($success, route('admin-site-noticias-index'));

				} else {

					echo Helper::AdminRedirectAlert($error, route('admin-site-noticias-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Noticia não encontrada!', route('admin-site-noticias-index'));

			}

		}

	}
