<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\AppHelper;
	use App\SiteUserToken;
	use App\SiteBanner;
	use App\SiteNoticia;
	use App\SiteUser;
	use App\AppContato;
	use Response;

	class App3Controller extends Controller {

		public $appKey	= '123123123';
		public $versao	= '1.0';
		public $retorno	= [

			'result'	=> false,
			'message'	=> 'Requisição inválida'

		];


		public function __construct(Request $request) {

			// $origem = $request->input('origem');
			// if($origem != $this->appKey) {

			// 	exit;

			// }

		}

		public function versao(Request $request) {

			echo '123';
		}


		// Função de gerenciamento de tokens.
		public function tokens(Request $request) {

			$_post	= json_decode($request->input('_POST'));
			$action	= $_post->action;
			if(isset($action)) {

				if($action != '') {

					$token	= $_post->token;
					if(isset($token)) {

						if($token != '') {

							if($action == 'check') {

								// $FCMTokenID	= $_post->FCMTokenID;
								// if(isset($FCMTokenID)) {

								// 	if($FCMTokenID != '') {

								// 		$_getFCMTokenID = SiteUserToken::where('id', $FCMTokenID)->first();
								// 		if(count($_getFCMTokenID) >= 1) {

								// 			$FCMToken	= [

								// 				'user'	=> $_getFCMTokenID->user_id,
								// 				'hash'	=> $_getFCMTokenID->token

								// 			];

								// 			if($FCMToken['hash'] == $token) {

								// 				$associado	= [

								// 					'id'		=> $FCMToken['user'],
								// 					'titulo'	=> '',
								// 					'name'		=> ''

								// 				];

								// 				if($FCMToken['user'] >= 1) {

								// 					$_getAssociado = SiteUser::where('id', $FCMToken['user'])->first();
								// 					if(count($_getAssociado) >= 1) {

								// 						$associado['titulo']	= $_getAssociado->titulo;
								// 						$associado['name']		= $_getAssociado->name;

								// 					}

								// 				}

								// 				$this->retorno	= [

								// 					'result'			=> true,
								// 					'AssociadoID'		=> $associado['id'],
								// 					'AssociadoTitulo'	=> $associado['titulo'],
								// 					'AssociadoName'		=> $associado['name']

								// 				];

								// 			}

								// 		}

								// 	}

								// }


								$FCMTokenID	= $_post->FCMTokenID;
								if(isset($FCMTokenID)) {

									if($FCMTokenID != '') {

										$_getFCMTokenID = SiteUserToken::where('id', $FCMTokenID)->first();
										if(count($_getFCMTokenID) >= 1) {

											$FCMToken	= [

												'user'	=> $_getFCMTokenID->user_id,
												'hash'	=> $_getFCMTokenID->token

											];

											if($FCMToken['hash'] == $token) {

												// $associado	= [

												// 	'id'		=> $FCMToken['user'],
												// 	'titulo'	=> '',
												// 	'name'		=> ''

												// ];

												// if($FCMToken['user'] >= 1) {

												// 	$_getAssociado = SiteUser::where('id', $FCMToken['user'])->first();
												// 	if(count($_getAssociado) >= 1) {

												// 		$associado['titulo']	= $_getAssociado->titulo;
												// 		$associado['name']		= $_getAssociado->name;

												// 	}

												// }

												$this->retorno	= [

													'result'			=> true,
													// 'AssociadoID'		=> $associado['id'],
													// 'AssociadoTitulo'	=> $associado['titulo'],
													// 'AssociadoName'		=> $associado['name']

												];

											}

										}

									}

								}


							} elseif($action == 'create') {

								$_checkFCMTokenHash = SiteUserToken::where('token', $token)->first();
								if(count($_checkFCMTokenHash) >= 1) {

									$this->retorno	= [

										'result'		=> true,
										'FCMTokenID'	=> $_checkFCMTokenHash->id
									
									];

								} else {

									$novo				= new SiteUserToken;
									$novo->user_id		= 0;
									$novo->token		= $token;
									$novo->created_at	= now();
									if($novo->save()) {

										$this->retorno	= [

											'result'		=> true,
											'FCMTokenID'	=> $novo->id
										
										];

									}

								}

							}

						}

					}

				}

			}

			return response()->json($this->retorno);

		}


		// Função de login no sistema.
		public function login(Request $request) {

			$_post	= json_decode($request->input('_POST'));
			$campos	= [

				'titulo'	=> 'Por favor preencha seu "Titulo".',
				'password'	=> 'Por favor preencha sua "Senha".'

			];

			$continuar	= true;
			foreach ($campos as $campo => $mensagem) {
				
				if($continuar == true) {

					if(isset($_post->$campo)) {

						if($_post->$campo == '') {

							$continuar	= false;
							$message	= $mensagem;

						}

					} else {

						$continuar	= false;
						$message	= $mensagem;

					}

				}

			}

			if($continuar == true) {

				$_searchUser = SiteUser::where('titulo', $_post->titulo)->first();
				if(count($_searchUser) >= 1) {

					$_checkPassword	= SiteUser::where('titulo', $_post->titulo)->where('password', $_post->password)->first();
					if(count($_checkPassword) >= 1) {

						$_getFCMTokenID = SiteUserToken::where('id', $_post->FCMTokenID)->first();
						if(count($_getFCMTokenID) >= 1) {

							$_updateFCMToken 				= SiteUserToken::find($_getFCMTokenID->id);
							$_updateFCMToken->user_id		= $_searchUser->id;
							$_updateFCMToken->updated_at	= now();
							if($_updateFCMToken->save()) {

								$this->retorno	= [

									'result'			=> true,
									'FCMTokenID'		=> $_getFCMTokenID->id,
									'AssociadoID'		=> $_searchUser->id,
									'AssociadoTitulo'	=> $_searchUser->titulo,
									'AssociadoName'		=> $_searchUser->name

								];

							} else {

								$this->retorno['message'] = 'ERRO! COD:L02 Caso este erro percista por favor entre em contato conosco.';

							}

						} else {


							$this->retorno['message'] = 'ERRO! COD:L01 Caso este erro percista por favor entre em contato conosco.';

						}

					} else {

						$this->retorno['message']	= 'Dados inconsistentes!';

					}

				} else {

					$envio	= ['titulo' => $_post->titulo];
					$curl	= curl_init();

					curl_setopt_array($curl, array(

						CURLOPT_PORT			=> "8081",
						CURLOPT_URL				=> "http://138.99.201.106:8081/teste.php",
						CURLOPT_RETURNTRANSFER	=> true,
						CURLOPT_ENCODING		=> "",
						CURLOPT_MAXREDIRS		=> 10,
						CURLOPT_TIMEOUT			=> 30,
						CURLOPT_HTTP_VERSION	=> CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST	=> "POST",
						CURLOPT_POSTFIELDS		=> $envio

					));

					$response	= curl_exec($curl);
					$err		= curl_error($curl);

					curl_close($curl);

					if ($err) {

						$this->retorno['message'] = "Cadastro não encontrado!";

					} else {

						$retorno = json_decode($response);
						if($retorno->result == true) {

							$associado			= $retorno->associado;

							$novo				= new SiteUser;
							$novo->titulo		= $associado->titulo;
							$novo->name			= $associado->nome;
							$novo->passoword	= str_random(10);
							$novo->status		= $associado->status;
							$novo->created_at	= now();

							$novo->save();

							
						}

						$this->retorno['message'] = 'ERRO! COD:L02 Caso este erro percista por favor entre em contato conosco.';

					}

				}

			} else {

				$this->retorno['message'] = $message;

			}

			return response()->json($this->retorno);

		}


		// Listagem de paginação de banners para o app.
		public function banners(Request $request) {

			$banners	= SiteBanner::where('status', 1)->orderBy('created_at', 'DESC')->limit(3)->get();
			$itens		= [];
			if(count($banners) >= 1) {

				foreach ($banners as $banner) {

					$imagem	= asset('images/logo.png');
					if(isset($banner->imagem)) {

						$arquivo = asset('storage/noticias/' . $banner->id . '/' . $banner->imagem);
						if(file_exists($arquivo)) {

							$imagem = $arquivo;

						}

					}

					$itens[] = [

						'id'		=> $noticia->id,
						'imagem'	=> $imagem

					];
					
				}

			}
			
			$this->retorno	= [

				'result'	=> true,
				'total'		=> count($banners),
				'itens'		=> $itens

			];

			return response()->json($this->retorno);

		}


		// Listagem de paginação de noticias para o app.
		public function noticias(Request $request) {

			$limite		= 5;
			$pagina		= 1;
			$_pagina	= $request->input('page');
			if(isset($_pagina)) {

				if($_pagina != '') {

					$pagina = $_pagina;

				}

			}

			$numero		= ($limite * $pagina);
			$noticias	= SiteNoticia::where('status', 1)->orderBy('created_at', 'DESC')->limit($numero)->get();
			$itens		= [];
			if(count($noticias) >= 1) {

				foreach ($noticias as $noticia) {

					$imagem	= asset('images/logo.png');
					$titulo	= str_limit(strip_tags($noticia->titulo), 46);
					$resumo	= str_limit(strip_tags($noticia->texto), 147);
					if(isset($noticia->imagem)) {

						$arquivo = asset('storage/noticias/' . $noticia->id . '/' . $noticia->imagem);
						if(file_exists($arquivo)) {

							$imagem = $arquivo;

						}

					}

					$itens[] = [

						'id'		=> $noticia->id,
						'imagem'	=> $imagem,
						'titulo'	=> $titulo,
						'resumo'	=> $resumo

					];
					
				}

			}
			
			$this->retorno	= [

				'result'	=> true,
				'todas'		=> SiteNoticia::where('status', 1)->count(),
				'total'		=> count($noticias),
				'itens'		=> $itens

			];

			return response()->json($this->retorno);

		}


		// Função de exibição da noticia completa.
		public function noticia($noticia) {

			if(isset($noticia)) {

				if($noticia != '') {

					$noticia	= SiteNoticia::where('id', $noticia)->where('status', 1)->first();
					if(count($noticia) >= 1) {

						$this->retorno	= [

							'result'	=> true,
							'titulo'	=> $noticia->titulo,
							'texto'		=> $noticia->texto

						];

					} else {

						$this->retorno['message']	= 'Noticia não encontrada!';

					}

				}

			}


			return response()->json($this->retorno);

		}


		// Função do formulário de contato.
		public function contato(Request $request) {

			$_post	= json_decode($request->input('_POST'));

			$campos		= [

				'nome'		=> 'O campo "Nome" é obrigatório',
				'telefone'	=> 'O campo "Telefone" é obrigatório',
				'email'		=> 'O campo "Email" é obrigatório',
				'horario'	=> 'O campo "Melhor horário para contato" é obrigatório',
				'assunto'	=> 'O campo "O que gostaria de saber" é obrigatório',
				'mensagem'	=> 'O campo "Mensagem" é obrigatório'

			];

			$continuar	= true;
			foreach ($campos as $campo => $mensagem) {
				
				if($continuar == true) {

					if(isset($_post->$campo)) {

						if($_post->$campo == '') {

							$continuar	= false;
							$message	= $mensagem;

						}

					} else {

						$continuar	= false;
						$message	= $mensagem;

					}

				}

			}

			if($continuar == true) {

				$contato				= new AppContato;
				$contato->nome			= $_post->nome;
				$contato->telefone		= $_post->telefone;
				$contato->email			= $_post->email;
				$contato->horario		= $_post->horario;
				$contato->assunto		= $_post->assunto;
				$contato->mensagem		= $_post->mensagem;
				$contato->status		= 1;
				$contato->created_at	= now();

				if($contato->save()) {

					$this->retorno	= [

						'result'	=> true,
						'message'	=> 'Mensagem enviada com sucesso!'

					];

				} else {

					$this->retorno['message']	= 'Oops! Alguma coisa deu errado e sua mensagem não pode ser enviada.';

				}

			} else {

				$this->retorno['message']	= $message;

			}


			return response()->json($this->retorno);

		}

	}