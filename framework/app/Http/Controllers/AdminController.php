<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Helper;
	use App\AppNotificacao;
	use App\AppContato;
	use Route;
	use Auth;

	class AdminController extends Controller {
		
		/**
		 * Create a new controller instance.
		 *
		 * @return void
		*/

		public function __construct() {}


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index() {

			if (Auth::check()) {

				$mensagens	= [];
				$total		= AppContato::count();
				if($total >= 1) {

					$busca	= AppContato::orderBy('created_at', 'DESC')->limit(5)->get();
					foreach ($busca as $item) {

						$mensagens[]	=	[

							'id'		=> $item->id,
							'nome'		=> $item->nome,
							'data'		=> Helper::formatDate('view-datetime', $item->created_at)

						];

					}

				}


				$notificacoes	= [];
				$totaln			= AppNotificacao::count();
				if($totaln >= 1) {

					$buscan	= AppNotificacao::orderBy('created_at', 'DESC')->limit(5)->get();
					foreach ($buscan as $itemn) {

						$notificacoes[]	=	[

							'id'		=> $itemn->id,
							'titulo'	=> $itemn->titulo,
							'status'	=> $itemn->status,
							'data'		=> Helper::formatDate('view-datetime', $itemn->created_at)

						];

					}

				}


				$data	= [

					'contato'		=> [
						
						'mensagens'	=> $mensagens,
						'total'		=> $total

					],

					'notificacoes'	=> [

						'itens'	=> $notificacoes,
						'total'	=> $totaln

					]

				];
				
				return view('admin.dashboard', $data);

			}

		}
		

		public function myAccount() {

			if (Auth::check()) {

				return view('admin.minha-conta');

			}
		
		}

	}
