<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\AppHelper;
	use App\SiteUserToken;
	use App\SiteNoticia;
	use App\SiteUser;
	use App\AppContato;
	use Response;
	use DB;

	class App2Controller extends Controller {

		public $appKey	= '123123123';
		public $versao	= '1.0';
		public $retorno	= [

			'result'	=> false,
			'message'	=> 'Requisição inválida'

		];


		// Validação da versão atual do app.
		public function versao($origem, $versao) {

			if(isset($versao)) {

				if($versao != '') {

					if($versao <= $this->versao) {

						$this->retorno	= [

							'result'	=> true,
							'updated'	=> false,
							'message'	=> 'Uma nova versão está disponivel!'

						];

					} else {

						$this->retorno	= [

							'result'	=> true,
							'updated'	=> true,
							'message'	=> 'App atualizado'

						];

					}

				}

			}

			return response()->json($this->retorno);

		}


		// Função de cadastro de tokens do app.
		public function tokens($token) {

			$tokenID = 0;
			if(isset($token)) {

				if($token != '') {

					$busca	= SiteUserToken::where('token', $token)->first();
					if(count($busca) <= 0) {

						$novo				= new SiteUserToken;
						$novo->user_id		= 0;
						$novo->token		= $token;
						$novo->created_at	= now();
						if($novo->save()) {

							$tokenID = $novo->id;

						}

					} else {

						$tokenID = $busca->id;

					}

				}

			}

			unset($this->retorno);
			$this->retorno['tokenID'] = $tokenID;

			return response()->json($this->retorno);

		}


		// Função de cadastro de tokens do app.
		public function validar_token($associado, $token) {

			if(isset($associado)) {

				if($associado != '') {

					$associado = SiteUser::where('id', $associado)->first();
					if(count($associado) >= 1) {

						if($associado->status == 1) {

							if(isset($token)) {

								if($token != '') {

									$busca	= SiteUserToken::where('user_id', $associado->id)->first();
									if(count($busca) >= 1) {

										if($busca->token == $token) {

											$this->retorno = [

												'result'		=> true,
												'tokenID'		=> $busca->id,
												'associadoID'	=> $associado->id,
												'associadoName'	=> $associado->name

											];

										}

									}

								}

							}

						} else {

							$this->retorno['message']	= 'Titulo inativo!';

						}

					}

				}

			}

			return response()->json($this->retorno);

		}


		// Função de cadastro do app.
		public function cadastro(Request $request) {

			$campos	= [

				'nome'		=> 'O campo "Nome" é obrigatório',
				'email'		=> 'O campo "Email" é obrigatório',
				'senha'		=> 'O campo "Senha" é obrigatório'

			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$cadastro				= new SiteUser;
				$cadastro->name			= $request->input('nome');
				$cadastro->email		= $request->input('email');
				$cadastro->password		= $request->input('senha');
				$cadastro->created_at	= now();
				if($cadastro->save()) {

					AppHelper::VincularUsuario($cadastro->id);
					$this->retorno	= [

						'result'	=> true,
						'message'	=> 'Cadastro realizado com sucesso'

					];

				} else {

					$this->retorno['message']	= 'Oops! Alguma coisa deu errado e seu cadastro não pode ser realizado.';

				}


			} else {

				$this->retorno['message']	= $retorno;

			}


			return response()->json($this->retorno);

		}


		// Função de login do app.
		public function login($token, $titulo, $senha) {

			$this->retorno['token'] = $token;
			if(isset($token)) {

				$token = SiteUserToken::where('id', $token)->first();
				if(count($token) >= 1) {

					if(isset($titulo)) {

						if($titulo != '') {
							
							$busca	= SiteUser::where('titulo', $titulo)->first();
							if(count($busca) >= 1) {

								if(isset($senha)) {

									if($senha != '') {

										$valida	= SiteUser::where('titulo', $titulo)->where('password', $senha)->first();
										if(count($valida) >= 1) {

											if($valida->status == 1) {

												$atualizar				= SiteUserToken::find($token->id);
												$atualizar->user_id		= $valida->id;
												$atualizar->updated_at	= now();
												if($atualizar->save()) {

													AppHelper::CreateLog($valida->id, 'Realizou login no app.');

													$this->retorno	= [

														'result'		=> true,
														'tokenID'		=> $token->id,
														'associadoID'	=> $valida->id,
														'associadoName'	=> $valida->name

													];

												}

											} else {

												$this->retorno['message']	= 'Titulo Inativo';

											}

										} else {

											$this->retorno['message']	= 'Senha incorreta';

										}

									} else {

										$this->retorno['message']	= 'Por favor informe sua "Senha" para realizar o login.';

									}

								} else {

									$this->retorno['message']	= 'Por favor informe sua "Senha" para realizar o login.';

								}
								
							} else {

								$this->retorno['message']	= 'Titulo não encontrado';

							}

						} else {

							$this->retorno['message']	= 'Por favor informe seu "Titulo" para realizar o login.';

						}

					} else {

						$this->retorno['message']	= 'Por favor informe seu "Titulo" para realizar o login.';

					}

				}
			
			}

			return response()->json($this->retorno);

		}

		
		// Funções da conta do app.
		public function conta(Request $request) {

			$acao	= $request->input('acao');
			if($acao == 'minha-conta') {

				$titulo	= $request->input('titulo');
				if(isset($titulo)) {

					if($titulo != '') {

						$usuario	= SiteUser::where('id', $titulo)->first();
						if(count($usuario) >= 1) {

							$this->retorno	= [

								'result'	=> true,
								'usuario'	=> $usuario

							];

						} else {

							$this->retorno['message']	= 'Titulo não localizado!';

						}

					}

				}

			} elseif($acao == 'atualizar') {

				$titulo	= $request->input('titulo');
				if(isset($titulo)) {

					if($titulo != '') {

						$usuario	= SiteUser::where('id', $titulo)->first();
						if(count($usuario) >= 1) {

							$campos	= [

								'nome'		=> 'O campo "Nome" é obrigatório',
								'email'		=> 'O campo "Email" é obrigatório'

							];

							$retorno	= '';
							foreach ($campos as $campo => $mensagem) {

								if($retorno == '') {

									$valor	= $request->input($campo);
									if(isset($valor)) {

										if($valor == '') {

											$retorno	= $mensagem;

										}

									} else {

										$retorno	= $mensagem;
									}

								}
								
							}

							if($retorno == '') {

								$atualizar				= SiteUser::find($usuario->id);
								$atualizar->name		= $request->input('nome');
								$atualizar->email		= $request->input('email');
								$atualizar->updated_at	= now();
								if($atualizar->save()) {

									AppHelper::CreateLog($usuario->id, 'Atualizou os dados da conta.');
									$this->retorno	= [

										'result'	=> true,
										'message'	=> 'Dados atualizados com sucesso!'

									];

								} else {

									$this->retorno['message']	= 'Oops! Alguma coisa deu errado e seus dados não puderam ser atualizados.';

								}

							} else {

								$this->retorno['message']	= $retorno;

							}

						} else {

							$this->retorno['message']	= 'Titulo não localizado!';

						}

					}

				}

			}


			return response()->json($this->retorno);

		}

		// Função de logout do app.
		public function logout($token) {

			if(isset($token)) {

				if($token != '') {

					$busca	= SiteUserToken::where('id', $token)->first();
					if(count($busca) >= 1) {

						$atualizar				= SiteUserToken::find($busca->id);
						$atualizar->user_id		= 0;
						$atualizar->updated_at	= now();
						$atualizar->save();
						
					}

				}

			}

			return response()->json($this->retorno);

		}

		
		// Listagem de paginação de novidades para o app.
		public function novidades() {

			$users = DB::connection('sqlsrv')->getPdo();
			// $novidades	= SiteNoticia::where('status', 1)->orderBy('created_at', 'DESC')->paginate(5);
			// $itens		= [];
			// if(count($novidades) >= 1) {

			// 	foreach ($novidades as $novidade) {

			// 		$imagem	= asset('images/logo.png');
			// 		$titulo	= str_limit(strip_tags($novidade->titulo), 46);
			// 		$resumo	= str_limit(strip_tags($novidade->texto), 147);
			// 		if(isset($novidade->imagem)) {

			// 			$arquivo = asset('storage/novidades/' . $novidade->id . '/' . $novidade->imagem);
			// 			if(file_exists($arquivo)) {

			// 				$imagem = $arquivo;

			// 			}

			// 		}

			// 		$itens[] = [

			// 			'id'		=> $novidade->id,
			// 			'imagem'	=> $imagem,
			// 			'titulo'	=> $titulo,
			// 			'resumo'	=> $resumo

			// 		];
					
			// 	}

			// }
			
			// $this->retorno	= [

			// 	'result'	=> true,
			// 	'total'		=> count($novidades),
			// 	'itens'		=> $itens

			// ];

			// return response()->json($this->retorno);

		}


		// Função de exibição da noticia completa.
		public function novidade($novidade) {

			if(isset($novidade)) {

				if($novidade != '') {

					$noticia	= SiteNoticia::where('id', $novidade)->where('status', 1)->first();
					if(count($noticia) >= 1) {

						$this->retorno	= [

							'result'	=> true,
							'titulo'	=> $noticia->titulo,
							'texto'		=> $noticia->texto

						];

					} else {

						$this->retorno['message']	= 'Noticia não encontrada!';

					}

				}

			}


			return response()->json($this->retorno);

		}


		// Função do formulário de contato.
		public function contato(Request $request) {

			$formulario	= json_decode($request->input('informacoes'));

			$campos		= [

				'nome'		=> 'O campo "Nome" é obrigatório',
				'telefone'	=> 'O campo "Telefone" é obrigatório',
				'email'		=> 'O campo "Email" é obrigatório',
				'horario'	=> 'O campo "Melhor horário para contato" é obrigatório',
				'assunto'	=> 'O campo "O que gostaria de saber" é obrigatório',
				'mensagem'	=> 'O campo "Mensagem" é obrigatório'

			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $formulario->$campo;
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$contato				= new AppContato;
				$contato->nome			= $formulario->nome;
				$contato->telefone		= $formulario->telefone;
				$contato->email			= $formulario->email;
				$contato->horario		= $formulario->horario;
				$contato->assunto		= $formulario->assunto;
				$contato->mensagem		= $formulario->mensagem;
				$contato->status		= 1;
				$contato->created_at	= now();

				if($contato->save()) {

					$this->retorno	= [

						'result'	=> true,
						'message'	=> 'Mensagem enviada com sucesso!'

					];

				} else {

					$this->retorno['message']	= 'Oops! Alguma coisa deu errado e sua mensagem não pode ser enviada.';

				}

			} else {

				$this->retorno['message']	= $retorno;

			}


			return response()->json($this->retorno);

		}


	}