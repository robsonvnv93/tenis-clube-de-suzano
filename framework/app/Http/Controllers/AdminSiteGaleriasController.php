<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\SiteGaleriaItens;
	use App\SiteGaleria;
	use Response;
	use Route;

	class AdminSiteGaleriasController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function ajax(Request $request) {

			$retorno	= [

				'result'	=> false,
				'redirect'	=> false,
				'message'	=> 'Solicitação inválida!'

			];

			$action	= $request->input('action');
			if(isset($action)) {

				if($action == 'get-itens') {

					$galeria	= $request->input('galeria');
					if(isset($galeria)) {

						if($galeria >= 1) {

							$galeria	= SiteGaleria::where('id', $galeria)->first();
							if(count($galeria) >= 1) {

								$itens				= SiteGaleriaItens::where('galeria_id', $galeria->id)->orderBy('order')->get();
								$total				= count($itens);

								$retorno['result']	= true;
								$retorno['total']	= $total;
								if($total >= 1) {

									$data	= [];
									foreach ($itens as $item) {
										
										$data[]	= [
											
											'id'		=> $item->id,
											'content'	=> asset('storage/galerias/' . $galeria->id . '/' . $item->content)
											
										];
									
									}

									$retorno['itens']	= $data;

								}

							} else {

								$retorno['message']		= 'Esta galeria não existe mais!';
								$retorno['redirect']	= true;

							}

						}

					}

				}

				if($action == 'upload-file') {

					$galeria	= $request->input('galeria');
					if(isset($galeria)) {

						if($galeria >= 1) {

							$galeria	= SiteGaleria::where('id', $galeria)->first();
							if(count($galeria) >= 1) {

								$imagem	= $request->file('imagem');
								if($request->hasFile('imagem')) {

									if($request->file('imagem')->isValid()) {

										$imagemname = uniqid(date('HisYmd'));
										$extension = $request->imagem->extension();
										$nameFile = "{$imagemname}.{$extension}";

										$upload = $request->imagem->storeAs('public/galerias/' . $galeria->id, $nameFile);
										if(!$upload) {

											$retorno['message']	= 'Falha no envio do arquivo!';

										} else {

											$ordem	= SiteGaleriaItens::where('galeria_id', $galeria->id)->orderBy('order', 'DESC')->first();
											if(count($ordem) >= 1) {
												
												$ordem	= $ordem->order + 1;

											} else {

												$ordem	= 1;

											}


											$fotos	= ['jpg', 'jpeg', 'gif', 'png'];
											if(in_array($extension, $fotos)) {

												$tipo = 1;

											} else {

												$tipo = 2;

											}

											$salvar				= new SiteGaleriaItens;
											$salvar->galeria_id	= $galeria->id;
											$salvar->tipo		= $tipo;
											$salvar->title		= '';
											$salvar->content	= $nameFile;
											$salvar->status		= 1;
											$salvar->order		= $ordem;
											if($salvar->save()) {

												$retorno	= [

													'result'	=> true,
													'total'		=> SiteGaleriaItens::where('galeria_id', $galeria->id)->count(),
													'galeria'	=> $galeria->id

												];

											} else {

												$retorno['message']	= 'Falha ao salvar arquivo na galeria!';

											}

										}
									
									} else {

										$retorno['message']	= 'Arquivo inválido!';

									}

								} else {

									$retorno['message']		= 'Não foram encontrados arquivos para serem enviados a esta galeria.';

								}

							} else {

								$retorno['message']		= 'Esta galeria não existe mais!';
								$retorno['redirect']	= true;

							}

						}

					}

				}


				if($action == 'delete-file') {

					$item	= $request->input('item');
					if(isset($item)) {

						if($item >= 1) {

							$item	= SiteGaleriaItens::where('id', $item)->first();
							if(count($item) >= 1) {

								$apagar = SiteGaleriaItens::where('id', $item->id)->delete();
								if($apagar == true) {

									$retorno	= [

										'result'	=> true,
										'message'	=> 'Item apagado com sucesso!',
										'galeria'	=> $item->galeria_id

									];

								} else {

									$retorno	= [

										'result'	=> false,
										'message'	=> 'Erro! O item não pode ser excluido.',
										'galeria'	=> $item->galeria_id

									];

								}

							}

						}

					}

				}

			}

			return response()->json($retorno);

		}


		public function index(Request $request) {

			$header	= [

				'title'		=> 'Galerias',
				'subtitle'	=> 'Listar Galerias',
				'buttons'	=> [

					[
						
						'title'	=> 'Nova Galeria',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-site-galerias-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-site-galerias-destroy',
				'search'	=> 'admin-site-galerias-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'order',
					'by'	=> 'DESC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'file-image',
						'directory'	=> 'storage/galerias/',
						'name'		=> 'Capa',
						'hasID'		=> true,
						'field'		=> 'capa',
						'class'		=> 'paginator-w-25',
						'search'	=> false

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Titulo',
						'field'		=> 'titulo',
						'class'		=> 'paginator-w-40',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar Galeria',
						'route'	=> 'admin-site-galerias-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar Galeria',
						'route'	=> 'admin-site-galerias-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir Galeria',
						'route'	=> 'admin-site-galerias-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'site_galerias', $busca);


			return view('automator.paginator', $data);

		}


		public function view($galeria) {

			$galeria	= SiteGaleria::where('id', $galeria)->first();
			if(count($galeria) >= 1) {

				$header	= [

					'title'		=> 'Galerias',
					'subtitle'	=> 'Visualizar Galeria',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Galerias',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-galerias-index'

						],
						[
						
							'title'	=> 'Editar Galeria',
							'type'	=> 'link',
							'class'	=> 'btn-warning',
							'route'	=> 'admin-site-galerias-edit',
							'args'	=> $galeria->id

						],
						[
						
							'title'	=> 'Nova Galeria',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-galerias-create'

						]

					]

				];

				$body	= [

					'indice'	=> $galeria->id,
					'fields'	=> [

						[

							'type'			=> 'file-image',
							'name'			=> 'Capa',
							'field'			=> 'capa',
							'default'		=> asset('storage/galerias/' . $galeria->id . '/' . $galeria->capa),

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo',
							'field'			=> 'titulo',
							'default'		=> $galeria->titulo,

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Descrição',
							'field'			=> 'texto',
							'default'		=> $galeria->texto,

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $galeria->status,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'ID',
							'field'			=> 'id',
							'default'		=> $galeria->id,

						]

					]

				];

				$data	= Automator::ViewerMaker($header, $body);

				return view('admin.galeria', $data);

			} else {

				echo Helper::AdminRedirectAlert('Galeria não localizada!', route('admin-site-galerias-index'));

			}

		}
		

		public function create() {

			$header	= [

				'title'		=> 'Galerias',
				'subtitle'	=> 'Nova Galeria',
				'buttons'	=> [

					[
						
						'title'	=> 'Listar Galerias',
						'type'	=> 'link',
						'class'	=> 'btn-primary',
						'route'	=> 'admin-site-galerias-index'

					]

				]

			];

			$body	= [

				'action'	=> 'admin-site-galerias-store',
				'fields'	=> [

					[

						'type'			=> 'file-upload',
						'name'			=> 'Capa',
						'field'			=> 'imagem',
						'required'		=> true

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Titulo',
						'field'			=> 'titulo',
						'required'		=> true

					],
					[

						'type'			=> 'editor',
						'name'			=> 'Descrição da Galeria',
						'field'			=> 'texto',
						'required'		=> false

					],
					[

						'type'		=> 'select',
						'name'		=> 'Status',
						'field'		=> 'status',
						'required'	=> true,
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',

							]

						]

					]

				]

			];

			$data	= Automator::FormMaker($header, $body);

			return view('automator.form-maker', $data);

		}


		public function store(Request $request) {

			$campos	= [

				'titulo'	=> 'O campo "Titulo" é obrigatório',
				'status'	=> 'O campo "Status" é obrigatório'
			
			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$imagem	= $request->file('imagem');
				if($request->hasFile('imagem')) {

					$nameFile	= '';
					$imagemname	= uniqid(date('HisYmd'));
					$extension	= $request->imagem->extension();
					$nameFile	= "{$imagemname}.{$extension}";

					$nova				= new SiteGaleria;
					$nova->capa			= $nameFile;
					$nova->titulo		= $request->input('titulo');
					$nova->texto		= $request->input('texto');
					$nova->status		= $request->input('status');
					$nova->created_at	= now();
					if($nova->save()) {

						$upload		= $request->imagem->storeAs('public/galerias/' . $nova->id . '/', $nameFile);
						if(!$upload) {

							echo Helper::AdminRedirectAlert('Erro ao realizar upload da capa!', route('admin-site-galerias-index'));
						
						} else {
							
							echo Helper::AdminRedirectAlert('Galeria cadastrada com sucesso!', route('admin-site-galerias-index'));
						}

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e a galeria não pode ser cadastrada.', route('admin-site-galerias-create'));

					}

				} else {

					echo Helper::AdminRedirectAlert('O campo "Capa" é obrigatório!', route('admin-site-galerias-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert($retorno, route('admin-site-galerias-index'));

			}

		}
		

		public function edit($galeria) {

			$galeria	= SiteGaleria::where('id', $galeria)->first();
			if(count($galeria) >= 1) {

				$header	= [

					'title'		=> 'Galerias',
					'subtitle'	=> 'Editar Galeria',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Galerias',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-galerias-index'

						],
						[
						
							'title'	=> 'Nova Galeria',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-galerias-create'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-site-galerias-update',
					'indice'	=> $galeria->id,
					'fields'	=> [

						[

							'type'			=> 'file-upload',
							'name'			=> 'Capa',
							'field'			=> 'imagem',
							'default'		=> asset('storage/galerias/' . $galeria->id . '/' . $galeria->capa),
							'required'		=> false

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo',
							'field'			=> 'titulo',
							'default'		=> $galeria->titulo,
							'required'		=> true

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Descrição',
							'field'			=> 'texto',
							'default'		=> $galeria->texto,
							'required'		=> true

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $galeria->status,
							'required'	=> true,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Galeria não encontrada!', route('admin-site-galerias-index'));

			}

		}


		public function update(Request $request) {

			$id			= $request->input('id');
			$galeria	= SiteGaleria::where('id', $id)->first();
			if(count($galeria) >= 1) {

				$campos	= [

					'titulo'	=> 'O campo "Titulo" é obrigatório',
					'status'	=> 'O campo "Status" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {

					$nameFile	= $galeria->capa;
					if($request->hasFile('imagem')) {

						$imagemname	= uniqid(date('HisYmd'));
						$extension	= $request->imagem->extension();
						$nameFile	= "{$imagemname}.{$extension}";

						$upload		= $request->imagem->storeAs('public/galerias/' . $galeria->id . '/', $nameFile);
						if(!$upload) {

							$nameFile	= $galeria->capa;
							$novo		= 1;

						} else {
							
							$novo		= 2;

						}

					} else {

						$novo = 0;

					}

					$atualizar				= SiteGaleria::find($id);
					$atualizar->capa		= $nameFile;
					$atualizar->titulo		= $request->input('titulo');
					$atualizar->texto		= $request->input('texto');
					$atualizar->status		= $request->input('status');
					$atualizar->updated_at	= now();
					if($atualizar->save()) {

						if($novo >= 1) {
							
							if($novo == 2) {

								echo Helper::AdminRedirectAlert('Galeria editada com sucesso!', route('admin-site-galerias-edit', $id));

							} else {
								
								echo Helper::AdminRedirectAlert('Galeria editada com sucesso! Porem a capa não pode ser atualizada.', route('admin-site-galerias-edit', $id));

							}

						} else {

							echo Helper::AdminRedirectAlert('Galeria editada com sucesso!', route('admin-site-galerias-edit', $id));

						}

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e a galeria não pode ser editada.', route('admin-site-galerias-edit', $id));

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-site-galerias-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Galeria não encontrada!', route('admin-site-galerias-index'));

			}


		}


		public function destroy(Request $request) {

			$itens	= $request->input('itens');
			return Automator::ExcluirRegistros('site_galerias', $itens, route('admin-site-galerias-index'), 'Galeria(s) excluida(s) com sucesso', 'Erro! A(s) Galeria(s) não foi/foram excluida(s).');

		}

	}
