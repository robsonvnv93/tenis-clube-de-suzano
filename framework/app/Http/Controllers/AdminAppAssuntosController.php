<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\AppContatoAssunto;
	use Route;


	class AdminAppAssuntosController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Contato',
				'subtitle'	=> 'Listar Assuntos',
				'buttons'	=> [

					[
						
						'title'	=> 'Novo Assunto',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-app-assuntos-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-app-assuntos-destroy',
				'search'	=> 'admin-app-assuntos-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'nome',
					'by'	=> 'ASC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'Nome',
						'field'		=> 'nome',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar Assunto',
						'route'	=> 'admin-app-assuntos-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir Assunto',
						'route'	=> 'admin-app-assuntos-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'app_assuntos_contato', $busca);


			return view('automator.paginator', $data);

		}
		


		public function create() {

			$header	= [

				'title'		=> 'Contato',
				'subtitle'	=> 'Novo Assunto',
				'buttons'	=> [

					[
						
						'title'	=> 'Listar Assuntos',
						'type'	=> 'link',
						'class'	=> 'btn-primary',
						'route'	=> 'admin-app-assuntos-index'

					]

				]

			];

			$body	= [

				'action'	=> 'admin-app-assuntos-store',
				'fields'	=> [

					[

						'type'			=> 'simple-text',
						'name'			=> 'Nome',
						'field'			=> 'nome',
						'required'		=> true

					],
					[

						'type'			=> 'editor',
						'name'			=> 'Descrição',
						'field'			=> 'descricao',
						'required'		=> true

					],
					[

						'type'		=> 'select',
						'name'		=> 'Status',
						'field'		=> 'status',
						'required'	=> true,
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',

							]

						]

					]

				]

			];

			$data	= Automator::FormMaker($header, $body);

			return view('automator.form-maker', $data);

		}


		
		public function store(Request $request) {

			$campos	= [

				'nome'		=> 'O campo "Nome" é obrigatório',
				'descricao'	=> 'O campo "Descrição" é obrigatório',
				'status'	=> 'O campo "Status" é obrigatório'
			
			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$busca	= AppContatoAssunto::where('nome', $request->input('nome'))->count();
				if($busca >= 1) {

					echo Helper::AdminRedirectAlert("Este assunto já está cadastrado na lista de assuntos.", route('admin-app-assuntos-create'));

				} else {

					$nova				= new AppContatoAssunto;
					$nova->nome			= $request->input('nome');
					$nova->descricao	= $request->input('descricao');
					$nova->status		= $request->input('status');
					$nova->created_at	= now();
					if($nova->save()) {

						echo Helper::AdminRedirectAlert('Assunto cadastrado com sucesso!', route('admin-app-assuntos-index'));

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o assunto não pode ser cadastrado.', route('admin-app-assuntos-create'));

					}

				}

			} else {

				echo Helper::AdminRedirectAlert($retorno, route('admin-app-assuntos-create'));

			}

		}
		

		public function edit($assunto) {

			$assunto	= AppContatoAssunto::where('id', $assunto)->first();
			if(count($assunto) >= 1) {

				$header	= [

					'title'		=> 'Contato',
					'subtitle'	=> 'Editar Assunto',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Assuntos',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-app-assuntos-index'

						],
						[
						
							'title'	=> 'Novo Assunto',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-app-assuntos-create'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-app-assuntos-update',
					'indice'	=> $assunto->id,
					'fields'	=> [

						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'titulo',
							'default'		=> $assunto->nome,
							'required'		=> true

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Descrição',
							'field'			=> 'descricao',
							'default'		=> $assunto->descricao,
							'required'		=> true

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $assunto->status,
							'required'	=> true,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Assunto não encontrado!', route('admin-app-assuntos-index'));

			}

		}


		public function update(Request $request) {

			$id				= $request->input('id');
			$assunto	= AppContatoAssunto::where('id', $id)->first();
			if(count($assunto) >= 1) {

				$campos	= [

					'nome'		=> 'O campo "Nome" é obrigatório',
					'descricao'	=> 'O campo "Descrição" é obrigatório',
					'status'	=> 'O campo "Status" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {

					$atual	= $assunto->nome;

					if($request->input('nome') != $atual) {

						$busca	= AppContatoAssunto::where('nome', $request->input('nome'))->count();
						if($busca >= 1) {

							echo Helper::AdminRedirectAlert("Este assunto já está cadastrado na lista de assuntos.", route('admin-app-assuntos-edit', $id));

						} else {

							$atualizar				= AppContatoAssunto::find($id);
							$atualizar->nome		= $request->input('nome');
							$atualizar->descricao	= $request->input('descricao');
							$atualizar->status		= $request->input('status');
							$atualizar->updated_at	= now();
							if($atualizar->save()) {

								echo Helper::AdminRedirectAlert('Assunto editado com sucesso!', route('admin-app-assuntos-edit', $id));

							} else {

								echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o assunto não pode ser editado.', route('admin-app-assuntos-edit', $id));

							}

						}

					} else {

						$atualizar				= AppContatoAssunto::find($id);
						$atualizar->nome		= $request->input('nome');
						$atualizar->status		= $request->input('status');
						$atualizar->updated_at	= now();
						if($atualizar->save()) {

							echo Helper::AdminRedirectAlert('Assunto editado com sucesso!', route('admin-app-assuntos-edit', $id));

						} else {

							echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o assunto não pode ser editado.', route('admin-app-assuntos-edit', $id));

						}

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-app-assuntos-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Assunto não encontrado!', route('admin-app-assuntos-index'));

			}


		}


		public function destroy(Request $request) {

			$itens	= $request->input('itens');
			return Automator::ExcluirRegistros('app_assuntos_contato', $itens, route('admin-app-assuntos-index'), 'Assunto(s) excluido(s) com sucesso.', 'Erro! O(s) Assunto(s) não foi/foram excluido(s).');

		}

	}
