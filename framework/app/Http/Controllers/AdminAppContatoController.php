<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\AppContatoAssunto;
	use App\AppContato;
	use Route;


	class AdminAppContatoController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Contato',
				'subtitle'	=> 'Listar Mensagens',
				'buttons'	=> []

			];

			$body	= [

				// 'destroy'	=> 'admin-app-emails-destroy',
				'destroy'	=> '',
				'search'	=> 'admin-app-contato-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'created_at',
					'by'	=> 'DESC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'simple-text',
						'name'		=> 'ID',
						'field'		=> 'id',
						'class'		=> 'text-center',
						'search'	=> true

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Data',
						'field'		=> 'created_at',
						'class'		=> 'text-center paginator-w-10',
						'mask'		=> 'datetime2',
						'search'	=> false

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Nome',
						'field'		=> 'nome',
						'search'	=> true

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'E-mail',
						'field'		=> 'email',
						'search'	=> true

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar Mensagem',
						'route'	=> 'admin-app-contato-view',
						'class'	=> 'btn-primary'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'app_contatos', $busca);


			return view('automator.paginator', $data);

		}


		public function view($mensagem) {

			$mensagem	= AppContato::where('id', $mensagem)->first();
			if(count($mensagem) >= 1) {

				$header	= [

					'title'		=> 'Contato',
					'subtitle'	=> 'Visualizar Mensagem',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Mensagens',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-app-contato-index'

						]

					]

				];

				$assunto	= '- - -';
				$busca		= AppContatoAssunto::where('id', $mensagem->assunto_id)->first();
				if(count($busca) >= 1) {

					$assunto = $busca->nome;
					
				}

				$body	= [

					'indice'	=> $mensagem->id,
					'fields'	=> [

						[

							'type'			=> 'simple-text',
							'name'			=> 'Enviada em',
							'field'			=> 'created_at',
							'default'		=> Helper::formatDate('view-datetime', $mensagem->created_at),

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'nome',
							'default'		=> $mensagem->nome,

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Telefone',
							'field'			=> 'telefone',
							'default'		=> $mensagem->telefone,

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'E-mail',
							'field'			=> 'email',
							'default'		=> $mensagem->email,

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Melhor Horário para contato',
							'field'			=> 'horario',
							'default'		=> $mensagem->horario,

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Assunto',
							'field'			=> 'assunto_id',
							'default'		=> $assunto,

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Mensagem',
							'field'			=> 'mensagem',
							'default'		=> $mensagem->mensagem,

						],
						// [

						// 	'type'		=> 'select',
						// 	'name'		=> 'Status',
						// 	'field'		=> 'status',
						// 	'default'	=> $mensagem->status,
						// 	'options'	=> [
								
						// 		[
									
						// 			'value'	=> '1',
						// 			'text'	=> 'Fechada',
						// 			'class'	=> 'text-danger'

						// 		],
						// 		[
									
						// 			'value'	=> '2',
						// 			'text'	=> 'Aberta',
						// 			'class'	=> 'text-success'

						// 		]

						// 	]

						// ]

					]

				];

				$data	= Automator::ViewerMaker($header, $body);

				return view('automator.viewer-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert("Mensagem não encontrada!", route('admin-app-contato-index'));

			}

		}
		


		public function status(Request $request) {

			$itens	= $request->input('itens');
			// return Automator::ExcluirRegistros('app_emails', $itens, route('admin-app-emails-index'), 'Destinatário(s) excluido(s) com sucesso.', 'Erro! O(s) Destinatário(s) não foi/foram excluido(s).');

		}

	}
