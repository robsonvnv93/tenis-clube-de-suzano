<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\SiteBanner;
	use Route;

	class AdminSiteBannersController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Banners',
				'subtitle'	=> 'Listar Banners',
				'buttons'	=> [

					[
						
						'title'	=> 'Cadastrar Novo Banner',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-site-banners-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-site-banners-destroy',
				'search'	=> 'admin-site-banners-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'id',
					'by'	=> 'DESC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'file-image',
						'directory'	=> 'storage/banners/',
						'name'		=> 'Imagem',
						'field'		=> 'image',
						'class'		=> 'paginator-w-25',
						'search'	=> false

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Nome',
						'field'		=> 'name',
						'class'		=> 'paginator-w-40',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar Banner',
						'route'	=> 'admin-site-banners-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar Banner',
						'route'	=> 'admin-site-banners-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir Banner',
						'route'	=> 'admin-site-banners-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'site_banners', $busca);


			return view('automator.paginator', $data);

		}


		public function view($banner) {

			$banner	= SiteBanner::where('id', $banner)->first();
			if(count($banner) >= 1) {

				$header	= [

					'title'		=> 'Banners',
					'subtitle'	=> 'Visualizar Banner',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Banners',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-banners-index'

						],
						[
						
							'title'	=> 'Editar Banner',
							'type'	=> 'link',
							'class'	=> 'btn-warning',
							'route'	=> 'admin-site-banners-edit',
							'args'	=> $banner->id

						],
						[
						
							'title'	=> 'Cadastrar Novo Banner',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-banners-create'

						]

					]

				];

				$body	= [

					'indice'	=> $banner->id,
					'fields'	=> [

						[

							'type'			=> 'file-image',
							'name'			=> 'Imagem',
							'field'			=> 'image',
							'default'		=> asset('storage/banners/' . $banner->image),

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'name',
							'default'		=> $banner->name,

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Link',
							'field'			=> 'link',
							'default'		=> $banner->link,

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $banner->status,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::ViewerMaker($header, $body);

				return view('automator.viewer-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Banner não localizado!', route('admin-site-banners-index'));

			}

		}
		

		public function create() {

			$header	= [

				'title'		=> 'Banners',
				'subtitle'	=> 'Cadastrar Novo Banner',
				'buttons'	=> [

					[
						
						'title'	=> 'Listar Banners',
						'type'	=> 'link',
						'class'	=> 'btn-primary',
						'route'	=> 'admin-site-banners-index'

					]

				]

			];

			$body	= [

				'action'	=> 'admin-site-banners-store',
				'fields'	=> [

					[

						'type'			=> 'file-upload',
						'name'			=> 'Imagem',
						'field'			=> 'imagem',
						'required'		=> true

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Nome',
						'field'			=> 'name',
						'required'		=> true

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Link',
						'field'			=> 'link',
						'required'		=> false

					],
					[

						'type'		=> 'select',
						'name'		=> 'Status',
						'field'		=> 'status',
						'required'	=> true,
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',

							]

						]

					]

				]

			];

			$data	= Automator::FormMaker($header, $body);

			return view('automator.form-maker', $data);

		}


		public function store(Request $request) {

			$campos	= [

				'name'		=> 'O campo "Nome" é obrigatório',
				'status'	=> 'O campo "Status" é obrigatório'
			
			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$imagem	= $request->file('imagem');
				if($request->hasFile('imagem')) {

					$imagemname	= uniqid(date('HisYmd'));
					$extension	= $request->imagem->extension();
					$nameFile	= "{$imagemname}.{$extension}";

					$upload		= $request->imagem->storeAs('public/banners/', $nameFile);
					if(!$upload) {

						echo Helper::AdminRedirectAlert('Erro ao realizar upload da imagem!', route('admin-site-banners-index'));

					} else {

						$nova				= new SiteBanner;
						$nova->name			= $request->input('name');
						$nova->image		= $nameFile;
						$nova->link			= $request->input('link');
						$nova->status		= $request->input('status');
						$nova->created_at	= now();
						if($nova->save()) {

							echo Helper::AdminRedirectAlert('Banner cadastrado com sucesso!', route('admin-site-banners-index'));

						} else {

							echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o banner não pode ser cadastrado.', route('admin-site-banners-create'));

						}

					}

				} else {

					echo Helper::AdminRedirectAlert('O campo "Imagem" é obrigatório!', route('admin-site-banners-create'));

				}

			} else {

				echo Helper::AdminRedirectAlert($retorno, route('admin-site-banners-create'));

			}

		}
		

		public function edit($banner) {

			$banner	= SiteBanner::where('id', $banner)->first();
			if(count($banner) >= 1) {

				$header	= [

					'title'		=> 'Banners',
					'subtitle'	=> 'Editar Banner',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Banners',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-banners-index'

						],
						[
						
							'title'	=> 'Cadastrar Novo Banner',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-banners-create'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-site-banners-update',
					'indice'	=> $banner->id,
					'fields'	=> [

						[

							'type'			=> 'file-upload',
							'name'			=> 'Imagem',
							'field'			=> 'imagem',
							'default'		=> asset('storage/banners/' . $banner->image),
							'required'		=> false

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Nome',
							'field'			=> 'name',
							'default'		=> $banner->name,
							'required'		=> true

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Link',
							'field'			=> 'link',
							'default'		=> $banner->link,
							'required'		=> false

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $banner->status,
							'required'	=> true,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Banner não encontrado!', route('admin-site-banners-index'));

			}

		}


		public function update(Request $request) {

			$id		= $request->input('id');
			$banner	= SiteBanner::where('id', $id)->first();
			if(count($banner) >= 1) {

				$campos	= [

					'name'		=> 'O campo "Nome" é obrigatório',
					'status'	=> 'O campo "Status" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {

					$nameFile	= $banner->image;
					if($request->hasFile('imagem')) {

						$imagemname	= uniqid(date('HisYmd'));
						$extension	= $request->imagem->extension();
						$nameFile	= "{$imagemname}.{$extension}";

						$upload		= $request->imagem->storeAs('public/banners/', $nameFile);
						if(!$upload) {

							$nameFile	= $banner->image;

						}

					}

					$atualizar				= SiteBanner::find($id);
					$atualizar->name		= $request->input('name');
					$atualizar->image		= $nameFile;
					$atualizar->link		= $request->input('link');
					$atualizar->status		= $request->input('status');
					$atualizar->updated_at	= now();
					if($atualizar->save()) {

						echo Helper::AdminRedirectAlert('Banner editado com sucesso!', route('admin-site-banners-edit', $id));

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o banner não pode ser editado.', route('admin-site-banners-edit', $id));

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-site-banners-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Banner não encontrado!', route('admin-site-banners-index'));

			}


		}


		public function destroy(Request $request) {

			$itens	= $request->input('itens');
			return Automator::ExcluirRegistros('site_banners', $itens, route('admin-site-banners-index'), 'Banner(s) excluido(s) com sucesso', 'Erro! O(s) Banner(s) não foi excluido.');

		}


		// Alterar status do banner.
		public function status($banner) {

			$banner = SiteBanner::where('id', $banner)->first();
			if(count($banner) >= 1) {

				if($banner->status == 0) {

					$novo		= 1;
					$success	= 'Banner ativado com sucesso!';
					$error		= 'Erro! O banner não pode ser ativado!';

				} else {

					$novo		= 0;
					$success	= 'Banner desativado com sucesso!';
					$error		= 'Erro! o banner não pode ser desativado!';

				}


				$banner				= SiteBanner::find($banner->id);
				$banner->status		= $novo;
				$banner->updated_at	= now();
				if($banner->save()) {

					echo Helper::AdminRedirectAlert($success, route('admin-site-banners-index'));

				} else {

					echo Helper::AdminRedirectAlert($error, route('admin-site-banners-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Banner não encontrado!', route('admin-site-banners-index'));

			}

		}

	}
