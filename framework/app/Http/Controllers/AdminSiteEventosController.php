<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use App\Helpers\Automator;
	use App\Helpers\Helper;
	use App\SiteEvento;
	use Route;


	class AdminSiteEventosController extends Controller {


		/**
		 * Show the application dashboard.
		 *
		 * @return \Illuminate\Http\Response
		*/

		public function index(Request $request) {

			$header	= [

				'title'		=> 'Eventos',
				'subtitle'	=> 'Listar Eventos',
				'buttons'	=> [

					[
						
						'title'	=> 'Cadastrar Novo Evento',
						'type'	=> 'link',
						'class'	=> 'btn-success',
						'route'	=> 'admin-site-eventos-create'

					]

				]

			];

			$body	= [

				'destroy'	=> 'admin-site-eventos-destroy',
				'search'	=> 'admin-site-eventos-index',
				'limit'		=> 15,
				'order'		=> [

					'field'	=> 'data_evento',
					'by'	=> 'ASC'
				
				],
				'cols'		=> [

					[

						'type'		=> 'file-image',
						'directory'	=> 'storage/eventos/',
						'name'		=> 'Capa do Evento',
						'field'		=> 'image',
						'class'		=> 'paginator-w-15 text-center',
						'search'	=> false

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Data do Evento',
						'class'		=> 'text-center paginator-w-10',
						'field'		=> 'data_evento',
						'mask'		=> 'datamask',
						'search'	=> true

					],
					[

						'type'		=> 'simple-text',
						'name'		=> 'Titulo',
						'field'		=> 'titulo',
						'class'		=> 'paginator-w-40',
						'search'	=> true

					],
					[

						'type'		=> 'enum-text',
						'class'		=> 'text-center',
						'name'		=> 'Status',
						'field'		=> 'status',
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',
								'class'	=> 'text-danger'

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',
								'class'	=> 'text-success'

							]

						]

					]

				],
				'actions'	=> [

					[
						'icon'	=> 'eye',
						'type'	=> 'link',
						'title'	=> 'Visualizar Evento',
						'route'	=> 'admin-site-eventos-view',
						'class'	=> 'btn-info'

					],
					[
						'icon'	=> 'pencil',
						'type'	=> 'link',
						'title'	=> 'Editar Evento',
						'route'	=> 'admin-site-eventos-edit',
						'class'	=> 'btn-primary'

					],
					[
						'icon'	=> 'trash',
						'type'	=> 'link',
						'title'	=> 'Excluir Evento',
						'route'	=> 'admin-site-eventos-destroy',
						'class'	=> 'btn-danger btn-delete'

					]

				]

			];

			$busca	= $request->input('busca');
			$data	= Automator::Paginator($header, $body, 'site_eventos', $busca);


			return view('automator.paginator', $data);

		}


		public function view($evento) {

			$evento	= SiteEvento::where('id', $evento)->first();
			if(count($evento) >= 1) {

				$header	= [

					'title'		=> 'Eventos',
					'subtitle'	=> 'Visualizar Evento',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Eventos',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-eventos-index'

						],
						[
						
							'title'	=> 'Editar Evento',
							'type'	=> 'link',
							'class'	=> 'btn-warning',
							'route'	=> 'admin-site-eventos-edit',
							'args'	=> $evento->id

						],
						[
						
							'title'	=> 'Cadastrar Novo Evento',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-eventos-create'

						]

					]

				];

				$body	= [

					'indice'	=> $evento->id,
					'fields'	=> [

						[

							'type'			=> 'file-image',
							'name'			=> 'Capa do Evento',
							'field'			=> 'imagem',
							'default'		=> asset('storage/eventos/' . $evento->image),

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Data do Evento',
							'field'			=> 'data_evento',
							'default'		=> Helper::formatDate('view-date', $evento->data_evento),

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo',
							'field'			=> 'titulo',
							'default'		=> $evento->titulo,

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Descrição do Evento',
							'field'			=> 'texto',
							'default'		=> $evento->texto,

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $evento->status,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::ViewerMaker($header, $body);

				return view('automator.viewer-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert("Evento não encontrado!", route('admin-site-eventos-index'));

			}

		}
		

		public function create() {

			$header	= [

				'title'		=> 'Eventos',
				'subtitle'	=> 'Cadastrar Novo Evento',
				'buttons'	=> [

					[
						
						'title'	=> 'Listar Eventos',
						'type'	=> 'link',
						'class'	=> 'btn-primary',
						'route'	=> 'admin-site-eventos-index'

					]

				]

			];

			$body	= [

				'action'	=> 'admin-site-eventos-store',
				'fields'	=> [

					[

						'type'			=> 'file-upload',
						'name'			=> 'Capa do Evento',
						'field'			=> 'imagem',
						'required'		=> false

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Data do Evento',
						'field'			=> 'data_evento',
						'input-mask'	=> '99/99/9999',
						'required'		=> true

					],
					[

						'type'			=> 'simple-text',
						'name'			=> 'Titulo do Evento',
						'field'			=> 'titulo',
						'required'		=> true

					],
					[

						'type'			=> 'editor',
						'name'			=> 'Descrição do Evento',
						'field'			=> 'texto',
						'required'		=> true

					],
					[

						'type'		=> 'select',
						'name'		=> 'Status',
						'field'		=> 'status',
						'required'	=> true,
						'options'	=> [
							
							[
								
								'value'	=> '0',
								'text'	=> 'Inativo',

							],
							[
								
								'value'	=> '1',
								'text'	=> 'Ativo',

							]

						]

					]

				]

			];

			$data	= Automator::FormMaker($header, $body);

			return view('automator.form-maker', $data);

		}


		public function store(Request $request) {

			$campos	= [

				'data_evento'	=> 'O campo "Data do Evento" é obrigatório',
				'titulo'		=> 'O campo "Titulo do Evento" é obrigatório',
				'texto'			=> 'O campo "Descrição do Evento" é obrigatório',
				'status'		=> 'O campo "Status" é obrigatório'
			
			];

			$retorno	= '';
			foreach ($campos as $campo => $mensagem) {

				if($retorno == '') {

					$valor	= $request->input($campo);
					if(isset($valor)) {

						if($valor == '') {

							$retorno	= $mensagem;

						}

					} else {

						$retorno	= $mensagem;
					}

				}
				
			}

			if($retorno == '') {

				$nameFile	= '';
				if($request->hasFile('imagem')) {

					$imagemname	= uniqid(date('HisYmd'));
					$extension	= $request->imagem->extension();
					$nameFile	= "{$imagemname}.{$extension}";

					$upload		= $request->imagem->storeAs('public/eventos/', $nameFile);
					if(!$upload) {

						$nameFile	= '';

					}

				}

				$nova				= new SiteEvento;
				$nova->image		= $nameFile;
				$nova->data_evento	= Helper::formatDate('db-date', $request->input('data_evento'));
				$nova->titulo		= $request->input('titulo');
				$nova->texto		= $request->input('texto');
				$nova->status		= $request->input('status');
				$nova->created_at	= now();
				if($nova->save()) {

					echo Helper::AdminRedirectAlert('Evento cadastrado com sucesso!', route('admin-site-eventos-index'));

				} else {

					echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o evento não pode ser cadastrado.', route('admin-site-eventos-create'));

				}

			} else {

				echo Helper::AdminRedirectAlert($retorno, route('admin-site-eventos-index'));

			}

		}
		

		public function edit($evento) {

			$evento	= SiteEvento::where('id', $evento)->first();
			if(count($evento) >= 1) {

				$header	= [

					'title'		=> 'Eventos',
					'subtitle'	=> 'Editar Evento',
					'buttons'	=> [

						[
							
							'title'	=> 'Listar Eventos',
							'type'	=> 'link',
							'class'	=> 'btn-primary',
							'route'	=> 'admin-site-eventos-index'

						],
						[
						
							'title'	=> 'Cadastrar Novo Evento',
							'type'	=> 'link',
							'class'	=> 'btn-success',
							'route'	=> 'admin-site-eventos-create'

						]

					]

				];

				$body	= [

					'action'	=> 'admin-site-eventos-update',
					'indice'	=> $evento->id,
					'fields'	=> [

						[

							'type'			=> 'file-upload',
							'name'			=> 'Capa do Evento',
							'field'			=> 'imagem',
							'default'		=> asset('storage/eventos/' . $evento->image),
							'required'		=> false

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Data do Evento',
							'field'			=> 'data_evento',
							'default'		=> Helper::formatDate('view-date', $evento->data_evento),
							'required'		=> true

						],
						[

							'type'			=> 'simple-text',
							'name'			=> 'Titulo do Evento',
							'field'			=> 'titulo',
							'default'		=> $evento->titulo,
							'required'		=> true

						],
						[

							'type'			=> 'editor',
							'name'			=> 'Descrição do Evento',
							'field'			=> 'texto',
							'default'		=> $evento->texto,
							'required'		=> true

						],
						[

							'type'		=> 'select',
							'name'		=> 'Status',
							'field'		=> 'status',
							'default'	=> $evento->status,
							'required'	=> true,
							'options'	=> [
								
								[
									
									'value'	=> '0',
									'text'	=> 'Inativo',

								],
								[
									
									'value'	=> '1',
									'text'	=> 'Ativo',

								]

							]

						]

					]

				];

				$data	= Automator::FormMaker($header, $body);

				return view('automator.form-maker', $data);

			} else {

				echo Helper::AdminRedirectAlert('Evento não encontrado!', route('admin-site-eventos-index'));

			}

		}


		public function update(Request $request) {

			$id			= $request->input('id');
			$evento	= SiteEvento::where('id', $id)->first();
			if(count($evento) >= 1) {

				$campos	= [

					'data_evento'	=> 'O campo "Data do Evento" é obrigatório',
					'titulo'		=> 'O campo "Titulo" é obrigatório',
					'texto'			=> 'O campo "Descrição do Evento" é obrigatório',
					'status'		=> 'O campo "Status" é obrigatório'
				
				];

				$retorno	= '';
				foreach ($campos as $campo => $mensagem) {

					if($retorno == '') {

						$valor	= $request->input($campo);
						if(isset($valor)) {

							if($valor == '') {

								$retorno	= $mensagem;

							}

						} else {

							$retorno	= $mensagem;
						}

					}
					
				}

				if($retorno == '') {

					$nameFile	= $evento->image;
					if($request->hasFile('imagem')) {

						$imagemname	= uniqid(date('HisYmd'));
						$extension	= $request->imagem->extension();
						$nameFile	= "{$imagemname}.{$extension}";

						$upload		= $request->imagem->storeAs('public/eventos/', $nameFile);
						if(!$upload) {

							$nameFile	= $evento->image;

						}

					}

					$atualizar				= SiteEvento::find($id);
					$atualizar->image		= $nameFile;
					$atualizar->data_evento	= Helper::formatDate('db-date', $request->input('data_evento'));
					$atualizar->titulo		= $request->input('titulo');
					$atualizar->texto		= $request->input('texto');
					$atualizar->status		= $request->input('status');
					$atualizar->updated_at	= now();
					if($atualizar->save()) {

						echo Helper::AdminRedirectAlert('Evento editado com sucesso!', route('admin-site-eventos-edit', $id));

					} else {

						echo Helper::AdminRedirectAlert('Oops! Alguma coisa deu errada e o evento não pode ser editado.', route('admin-site-eventos-edit', $id));

					}

				} else {

					echo Helper::AdminRedirectAlert($retorno, route('admin-site-eventos-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Evento não encontrado!', route('admin-site-eventos-index'));

			}


		}


		public function destroy(Request $request) {

			$itens	= $request->input('itens');
			return Automator::ExcluirRegistros('site_eventos', $itens, route('admin-site-eventos-index'), 'Evento(s) excluido(s) com sucesso.', 'Erro! O(s) Evento(s) não foi/foram excluido(s).');

		}


		// Alterar status da evento.
		public function status($evento) {

			$evento = SiteEvento::where('id', $evento)->first();
			if(count($evento) >= 1) {

				if($evento->status == 0) {

					$novo		= 1;
					$success	= 'Evento ativado com sucesso!';
					$error		= 'Erro! O evento não pode ser ativado!';

				} else {

					$novo		= 0;
					$success	= 'Evento desativado com sucesso!';
					$error		= 'Erro! O evento não pode ser desativado!';

				}


				$evento				= SiteEvento::find($evento->id);
				$evento->status		= $novo;
				$evento->updated_at	= now();
				if($evento->save()) {

					echo Helper::AdminRedirectAlert($success, route('admin-site-eventos-index'));

				} else {

					echo Helper::AdminRedirectAlert($error, route('admin-site-eventos-index'));

				}

			} else {

				echo Helper::AdminRedirectAlert('Evento não encontrado!', route('admin-site-eventos-index'));

			}

		}

	}
