<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AppLog extends Model {

		protected $table	= 'app_logs';
		public $timestamps	= false;
		protected $fillable	= [
			
			'id',
			'ip',
			'user',
			'log',
			'created_at'

		];
	
	}
