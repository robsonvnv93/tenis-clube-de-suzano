<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteBanner extends Model {

		protected $table	= 'site_banners';
		protected $fillable	= [
			
			'id',
			'name',
			'image',
			'link',
			'status',
			'created_at',
			'updated_at'

		];
	
	}
