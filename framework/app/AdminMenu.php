<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AdminMenu extends Model {

		protected $table	= 'admin_menu';
		protected $fillable	= [
			
			'id',
			'route_id',
			'icon',
			'name',
			'parent',
			'visible',
			'status',
			'order'

		];
	
	}
