<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AppEmail extends Model {

		protected $table	= 'app_emails';
		protected $fillable	= [
			
			'id',
			'email',
			'nome',
			'status',
			'created_at',
			'updated_at',
			'deleted_at'

		];
	
	}
