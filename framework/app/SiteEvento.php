<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteEvento extends Model {

		protected $table	= 'site_eventos';
		protected $fillable	= [
			
			'id',
			'data_evento',
			'image',
			'titulo',
			'texto',
			'status',
			'order',
			'created_at',
			'updated_at'

		];
	
	}
