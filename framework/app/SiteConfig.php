<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteConfig extends Model {

		protected $table	= 'site_configs';
		protected $fillable	= [
			
			'id',
			'name',
			'value'

		];

		public static function GetConfig($config) {

			$retorno	= '';
			$query		= SiteConfig::where('name', $config)->first();
			if(count($query) >= 1) {

				$retorno = $query->value;

			}

			return $retorno;

		}
	
	}
