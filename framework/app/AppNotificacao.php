<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AppNotificacao extends Model {

		protected $table	= 'app_notificacoes';
		protected $fillable	= [
			
			'id',
			'titulo',
			'conteudo',
			'status',
			'log',
			'created_at',
			'updated_at',
			'deleted_at'

		];
	
	}
