<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteGaleria extends Model {

		protected $table	= 'site_galerias';
		protected $fillable	= [
			
			'id',
			'capa',
			'titulo',
			'texto',
			'status',
			'order',
			'created_at',
			'updated_at'

		];
	
	}
