<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SitePagina extends Model {

		protected $table	= 'app_paginas';
		protected $fillable	= [
			
			'id',
			'titulo',
			'conteudo',
			'status',
			'created_at',
			'updated_at'

		];
	
	}
