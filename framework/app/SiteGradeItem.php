<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteGradeItem extends Model {

		protected $table	= 'site_grade_itens';
		protected $fillable	= [
			
			'id',
			'grade_id',
			'dia',
			'hora',
			'status',
			'created_at',
			'updated_at'

		];
	
	}
