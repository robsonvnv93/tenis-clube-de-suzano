<?php

	namespace App;

	use Illuminate\Notifications\Notifiable;
	use Illuminate\Contracts\Auth\MustVerifyEmail;
	use Illuminate\Foundation\Auth\User as Authenticatable;

	class User extends Authenticatable {
		
		use Notifiable;

		protected $fillable	= [

			'id',
			'name',
			'email',
			'birthday',
			'description',
			'website',
			'email_verified_at',
			'status',
			'password',
			'created_at',
			'updated_at'

		];
		
		protected $hidden	= ['password', 'remember_token'];
	
	}
