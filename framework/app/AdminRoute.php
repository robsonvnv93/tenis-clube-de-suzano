<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AdminRoute extends Model {

		protected $table	= 'admin_routes';
		protected $fillable	= [
			
			'id',
			'method',
			'url',
			'controller',
			'function',
			'name'

		];
	
	}
