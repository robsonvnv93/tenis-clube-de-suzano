<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteGaleriaItens extends Model {

		protected $table	= 'site_galeria_itens';
		protected $fillable	= [
			
			'id',
			'galeria_id',
			'tipo',
			'title',
			'content',
			'status',
			'order',
			'created_at',
			'updated_at'

		];
	
	}
