<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteUserToken extends Model {

		protected $table	= 'site_users_tokens';
		protected $fillable = [
			
			'id',
			'user_id',
			'token',
			'status',
			'created_at',
			'updated_at'

		];
	
	}
