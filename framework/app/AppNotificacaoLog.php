<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AppNotificacaoLog extends Model {

		public $timestamps	= false;
		protected $table	= 'app_notificacoes_log';
		protected $fillable	= [
			
			'id',
			'sender_id',
			'user_token_id',
			'status',
			'opened_at',
			'created_at'

		];
	
	}
