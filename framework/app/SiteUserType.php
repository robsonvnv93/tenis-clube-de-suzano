<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteUserType extends Model {

		protected $table	= 'site_user_types';
		protected $fillable	= ['id', 'user_id', 'user_type_id'];
		public $timestamps	= false;
	
	}
