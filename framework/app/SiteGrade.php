<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteGrade extends Model {

		protected $table	= 'site_grades';
		protected $fillable	= [
			
			'id',
			'categoria_id',
			'name',
			'description',
			'status',
			'order',
			'created_at',
			'updated_at'

		];
	
	}
