<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class UserTypePermission extends Model {

		protected $table	= 'user_type_permissions';
		protected $fillable	= ['id', 'user_type_id', 'menu_id'];
		public $timestamps	= false;
	
	}
