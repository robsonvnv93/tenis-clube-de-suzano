<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteNoticia extends Model {

		protected $table	= 'site_noticias';
		protected $fillable	= [
			
			'id',
			'image',
			'titulo',
			'texto',
			'status',
			'created_at',
			'updated_at'

		];
	
	}
