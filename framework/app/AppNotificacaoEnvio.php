<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AppNotificacaoEnvio extends Model {

		public $timestamps	= false;
		protected $table	= 'app_notificacao_envios';
		protected $fillable	= [
			
			'id',
			'notificacao_id',
			'user_id',
			'created_at'

		];
	
	}
