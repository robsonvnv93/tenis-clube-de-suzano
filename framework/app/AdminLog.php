<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AdminLog extends Model {

		protected $table	= 'admin_logs';
		public $timestamps	= false;
		protected $fillable	= [
			
			'id',
			'ip',
			'url',
			'log',
			'created_at'

		];
	
	}
