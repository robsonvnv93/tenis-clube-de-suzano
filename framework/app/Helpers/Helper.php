<?php

	namespace App\Helpers;


	use Illuminate\Support\Facades\Schema;
	use Illuminate\Http\Request;
	use App\UserTypePermission;
	use App\SiteConfig;
	use App\AdminRoute;
	use App\AdminMenu;
	use App\UsersType;
	use App\UserType;
	use App\AdminLog;
	use App\User;
	use Route;
	use Auth;
	use File;


	/*
		
		Resumo de funções desta classe:
			
			- Funções do site:

				- S01 - Buscar configurações salvas de acordo com o nome.
				- S02 - Buscar arquivos no diretorio do theme selecionado no sistema.

			- Funções do admin:
				
				- A01 - Registrar os logs do painel.
				- A02 - Busca o nome da rota registrada no sistema.
				- A03 - Registrar rotas cadastradas na base.
				- A04 - Exibir alerta e redirecionar usuario.
				- A05 - Buscar screenshot do template.

			- Funções do usuário:
	 			
	 			- UO1: Buscar informação do usuário do painel.
	 			- U02: Buscar foto de perfil do usuário.
	 			- U03: Buscar permissões de acesso do usuário.
	 			- U04: Buscar tipos de usuário vinculados a conta do usuário.
	 			- U05: Verificar todas as rotas que o usuário pode acessar.
	 			- U06: Gerar o menu do painel de acordo com o usuário.
	 			- U07: Gerar botões de navegação do painel de acordo com permissões de acesso do usuário.
	 			- U08: Verificar se o usuário pode acessar uma rota especifica.

	 */


	class Helper {
		
		// S01 - (Nome da Configuração, Valor Padrão)
		public static function GetSiteConfig($config, $default = null) {

			$GetConfig	= SiteConfig::where('name', $config)->first();
			if(count($GetConfig) >= 1) {

				$default = $GetConfig->value;

			}

			return $default;

		}

		// S02 - (Nome do Theme)
		public static function GetSiteThemeDirectory($theme) {

			$directory		= '';
			$themes			= [];
			$path			= resource_path('views/themes/');
			$diretorio		= dir($path);
			while($pasta = $diretorio -> read()){
				
				if($pasta != '.') {

					if($pasta != '..') {

						$tema			= resource_path('views/themes/' . $pasta . '/');
						$temadir		= dir($tema);
						$index			= file_exists($tema . 'index.blade.php');
						$layout			= file_exists($tema . 'layout.blade.php');
						$css			= file_exists($tema . 'style.css');
						if($index == 1 && $layout == 1 && $css == 1) {

							while($arquivo = $temadir -> read()){
								
								if($arquivo == 'style.css') {

									$content			= File::get(resource_path('views/themes/' . $pasta . '/style.css'));
									$cabecalhoStart		= strpos($content, '/*');
									$cabecalhoEnd		= strpos($content, '*/');
									$cabecalho			= substr($content, ($cabecalhoStart + 2), ($cabecalhoEnd - 2));
									$quebrar			= explode(';', $cabecalho);

									$basico['pasta']	= $pasta;

									$data				= [];
									foreach ($quebrar as $linha) {

										$linha				= trim($linha);
										$posicao			= strpos($linha, ':');
										$variavel			= strtolower(substr($linha, 0, $posicao));
										$valor				= substr($linha, ($posicao + 2));
										if($variavel != '') {

											if($variavel == 'theme') {

												$data[$variavel]	= str_slug($valor);
											
											}

										}

									}

									$themes[]	= array_merge($basico, $data);

								}

							}

						}

					}

				}

			}

			if(count($themes) >= 1) {

				$indice = '';
				foreach ($themes as $a => $item) {
					
					if($indice == '') {

						if($item['theme'] == $theme) {

							$indice = $a;

						}

					}
				
				}

				if($indice != '') {

					$active		= $themes[$indice];
					$directory	= 'views.themes.' . $active['pasta'];
					
				}

			}


			return $directory;

		}

		// A01 - (Descritivo da ação)
		public static function RegisterAdminLog($log) {

			$register				= new AdminLog;
			$register->ip			= $_SERVER["REMOTE_ADDR"];
			$register->url			= Route::currentRouteName();
			$register->log			= $log;
			$register->created_at	= now();

			$register->save();

		}


		// A02 - (ID da rota)
		public static function GetAdminRouteName($route) {

			$name	= '';
			$route	= AdminRoute::where('id', $route)->first();
			if(count($route) >= 1) {

				$name = $route->name;

			}

			return $name;

		}


		// A03
		public static function AdminRoutes() {

			if(Schema::hasTable('admin_routes')) {

				Route::middleware('painel')->group(function () {

					Route::get('/access-adenied/', function () { return view('admin.access-adenied'); })->name('admin-access-adenied');
					Route::get('/', 'AdminController@index')->name('admin-dashboard');
					Route::get('/minha-conta/', 'AdminController@myAccount')->name('admin-minha-conta');
					$rotas	= AdminRoute::get();
					foreach ($rotas as $rota) {


						Route::match(json_decode($rota['method']), $rota['url'], $rota['controller'] . '@' . $rota['function'])->name($rota['name']);

					}

				});

			}

		}


		// A04 - (Texto de alerta, URL de redirecionamento)
		public static function AdminRedirectAlert($txt, $url) {

			return '<script type="text/javascript"> alert("' . $txt . '"); window.location.href = "' . $url . '"; </script>';

		}



		// =====> AÇÕES DO USUÁRIO <=====



		// UO1 - (ID do usuario, Coluna do Banco)
		public static function GetUserData($user, $data) {

			$retorno	= '';
			$busca		= User::where('id', $user)->first();
			if(count($busca) >= 1) {

				$retorno	= $busca->$data;

			}

			return $retorno;

		}


		// U02 - (ID do usuário, Classe da imagem)
		public static function GetUserProfilePicture($user, $classe = '') {

			if($classe == '') {

				$picture = '<img src="' . asset('images/img.jpg') . '" alt="" />';

			} else {

				$picture = '<img src="' . asset('images/img.jpg') . '" alt="" class="' . $classe . '" />';

			}
			return $picture;

		}


		// U03 - (Tipos do usuário)
		public static function GetUserPermissions($types) {

			$itens = [];
			foreach ($types as $type) {
				
				$permissions = UserTypePermission::where('user_type_id', $type['id'])->get();
				foreach ($permissions as $permission) {
					
					if(!in_array($permission->menu_id, $itens)) {

						$itens[] = $permission->menu_id;

					}

				}

			}

			return $itens;

		}


		// U04 - (ID do usuário)
		public static function GetUserTypes($user) {

			$types = [];
			$search		= UserType::where('user_id', $user)->get();
			foreach ($search as $item) {
				
				$userType	= UsersType::where('id', $item->user_type_id)->where('status', 1)->first(); 
				if(count($userType) >= 1) {

					$types[]	= [
						
						'id'	=> $userType->id,
						'name'	=> $userType->name

					];

				}

			}

			return $types;

		}

		// U05 - (ID do usuário)
		public static function UserCanAccess($user) {

			$retorno		= false;
			$types			= Helper::GetUserTypes($user);
			$permissions	= Helper::GetUserPermissions($types);
			$routes 		= AdminMenu::select('route_id')->whereIn('id', $permissions)->where('status', 1)->get();
			$avalible		= [];
			foreach ($routes as $route) {
				
				$avalible[] = Helper::GetAdminRouteName($route->route_id);
				
			}

			$current	= Route::currentRouteName();
			if(in_array($current, $avalible)) {

				$retorno = true;

			}

			return $retorno;

		}

		// U06 - (ID do usuário)		
		public static function UserAdminMenu($user) {

			$retorno		= '';
			$itens			= [];
			$types			= Helper::GetUserTypes($user);
			$permissions	= Helper::GetUserPermissions($types);
			$topItens 		= AdminMenu::whereIn('id', $permissions)->where('parent', 0)->where('status', 1)->where('visible', 1)->orderBy('order')->get();
			$avalible		= [];
			foreach ($topItens as $topItem) {
				
				if(!in_array($topItem->id, $avalible)) {

					$avalible[] = $topItem->id;

				}
				
			}

			if(count($avalible) >= 1) {

				foreach ($avalible as $parentItem) {
					
					$childs		= [];
					$menu		= AdminMenu::where('id', $parentItem)->first();

					$hasChild	= AdminMenu::whereIn('id', $permissions)->where('parent', $parentItem)->where('status', 1)->where('visible', 1)->orderBy('order')->get();
					if(count($hasChild) >= 1) {

						foreach ($hasChild as $childItem) {
							
							$childs[] = [
								
								'id'	=> $childItem->id,
								'name'	=> $childItem->name,
								'route'	=> Helper::GetAdminRouteName($childItem->route_id)

							];

						}

					}


					$itens[]	= [

						'id'		=> $menu->id,
						'icon'		=> $menu->icon,
						'name'		=> $menu->name,
						'route'		=> Helper::GetAdminRouteName($menu->route_id),
						'childs'	=> $childs

					];

				}

			}


			if(count($itens) >= 1) {

				$retorno	 = '<ul class="nav side-menu">';

					$retorno	.= '<li><a href="' . route('admin-dashboard') . '">Dashboard</a></li>';
					foreach ($itens as $item) {

						$retorno	.= '<li>';
							
							if(count($item['childs']) >= 1) {

								$retorno	.= '<a><i class="fa fa-' . $item['icon'] . '"></i> ' . $item['name'] . ' <span class="fa fa-chevron-down"></span></a>';
								$retorno	.= '<ul class="nav child_menu">';

									foreach ($item['childs'] as $childItem) {

										if(Route::has($childItem['route'])) {

											$retorno	.= '<li><a href="' . route($childItem['route']) .'">' . $childItem['name'] . '</a></li>';

										} else {
											
											$retorno	.= '<li class="disabled"><a href="#" onclick="return false;">' . $childItem['name'] . '</a></li>';

										}
										
									}

								$retorno	.= '</ul>';

							} else {

								$retorno	.= '<a><i class="fa fa-' . $item['icon'] . '"></i> ' . $item['name'] . '</a>';

							}

						$retorno	.= '</li>';

					}

				$retorno	.= '</ul>';

			}

			return $retorno;

		}


		// U07 - (ID do usuário, Informações do Botão, ID de registro [se existir])		
		public static function UserAdminButton($user, $button, $id = null) {

			$retorno		= '';
			if(isset($button['route'])) {

				$access			= false;
				$types			= Helper::GetUserTypes($user);
				$permissions	= Helper::GetUserPermissions($types);
				$routes 		= AdminMenu::select('route_id')->whereIn('id', $permissions)->where('status', 1)->get();
				$avalible		= [];
				foreach ($routes as $route) {
					
					$avalible[] = Helper::GetAdminRouteName($route->route_id);
					
				}

				$route	= $button['route'];
				if(in_array($route, $avalible)) {

					$access = true;

				}

				if($access == true) {

					$btnTitle	= '';
					$btnClass	= '';

					if(isset($button['title'])) {

						$btnTitle = $button['title'];
					
					}


					if(isset($button['class'])) {

						$btnClass = $button['class'];
					
					}


					if($button['type'] == 'link') {

						if(isset($button['args'])) {

							if(isset($id)) {

								$btnLink = route($button['route'], [$id, $button['args']]);

							} else {

								$btnLink = route($button['route'], $button['args']);

							}
						
						} else {
							
							if(isset($id)) {

								$btnLink = route($button['route'], $id);

							} else {
								
								$btnLink = route($button['route'], $id);

							}

						}

						if(isset($button['icon'])) {

							$retorno	= '<a href="' . $btnLink . '" class="btn btn-automator btn-sm btn-icon ' . $btnClass . '" data-title="' . $btnTitle . '"><i class="fa fa-' . $button['icon'] . '"></i></a>';

						} else {
							
							$retorno	= '<a href="' . $btnLink . '" class="btn btn-automator btn-sm ' . $btnClass . ' m-2">' . $btnTitle . '</a>';

						}

					}

				}

			}

			return $retorno;

		}


		// U08 - (ID do usuário, Nome da Rota)
		public static function UserCanAccessRoute($user, $target) {

			$retorno		= false;
			$types			= Helper::GetUserTypes($user);
			$permissions	= Helper::GetUserPermissions($types);
			$routes 		= AdminMenu::select('route_id')->whereIn('id', $permissions)->where('status', 1)->get();
			$avalible		= [];
			foreach ($routes as $route) {
				
				$avalible[] = Helper::GetAdminRouteName($route->route_id);
				
			}

			if(in_array($target, $avalible)) {

				$retorno = true;

			}

			return $retorno;

		}


		// Formatar data
		public static function formatDate($tipo, $data) {

			$retorno	= '';
			if(isset($data)) {

				if($tipo == 'db-date') {

					$explode	= explode('/', $data);
					$retorno	= $explode[2] . '-' . $explode[1] . '-' . $explode[0];

				} elseif($tipo == 'view-datetime') {

					$quebra		= explode(' ', $data);
					$hora		= $quebra[1];
					$data		= $quebra[0];
					$explode	= explode('-', $data);
					$retorno	= $explode[2] . '/' . $explode[1] . '/' . $explode[0] . ' - ' . $hora;

				} elseif($tipo == 'view-date') {

					$explode	= explode('-', $data);
					$retorno	= $explode[2] . '/' . $explode[1] . '/' . $explode[0];

				}

			}

			return $retorno;

		}
		
	}