<?php

	namespace App\Helpers;
	use Illuminate\Support\Facades\Schema;

	use DB;

	class Automator {


		public static function Paginator($header, $body, $tabela, $busca) {

			// Define o numero máximo de registros por página.
			$limit	= 15;
			if(isset($body['limit'])) {

				if($body['limit'] != '') {

					$limit = $body['limit'];

				}

			}


			$order	= [
				
				'field'	=> 'id',
				'by'	=> 'DESC'

			];

			if(isset($body['order'])) {

				if($body['order'] != '') {

					$order = $body['order'];

				}

			}



			if($busca != '') {

				$cols	= [];
				foreach ($body['cols'] as $col) {
					
					if(isset($col['search'])) {

						if($col['search'] == true) {

							if(!in_array($col['field'], $cols)) {

								$cols[]	= $col['field'];
							
							}

						}

					}

				}


				$data = [];
				if(count($cols) >= 1) {

					$itens = [];
					foreach ($cols as $col) {
						
						$query = DB::table($tabela)->select('id')->where($col, 'LIKE', '%' . $busca . '%')->get();
						if(count($query) >= 1) {

							foreach ($query as $item) {
								
								if(!in_array($item->id, $itens)) {

									$itens[]	= $item->id;

								}

							}

						}

					}

					if(count($itens) >= 1) {

						$data	= DB::table($tabela)->whereIn('id',$itens)->orderBy($order['field'], $order['by'])->paginate($limit);

					}

				}

			} else {

				$data	= DB::table($tabela)->orderBy($order['field'], $order['by'])->paginate($limit);

			}


			$search	= '';
			if(isset($body['search'])) {

				if($body['search'] != '') {

					$search = route($body['search']);
				
				}

			}

			$retorno	= [

				'header'	=> $header,
				'search'	=> $search,
				'busca'		=> $busca,
				'body'		=> $body,
				'data'		=> $data

			];

			return $retorno;

		}


		public static function ViewerMaker($header, $body) {

			$retorno	= [

				'header'	=> $header,
				'body'		=> $body,

			];

			return $retorno;

		}


		public static function FormMaker($header, $body) {

			$body['action']	= route($body['action']);

			$retorno	= [

				'header'	=> $header,
				'body'		=> $body,

			];

			return $retorno;

		}


		public static function RelatedValue($tabela, $field, $indice) {

			$retorno	= '- - -';
			$consulta	= DB::table($tabela)->select($field)->where('id', $indice)->first();
			if(count($consulta) >= 1) {

				$retorno	= $consulta->$field;

			}

			return $retorno;

		}



		public static function RelatedField($tabela, $field, $where = '') {

			$retorno	= [];
			if($where != '') {

				if(count($where) >= 1) {

					$consulta	= DB::table($tabela)->select(['id', $field])->where($where)->get();

				} else {

					$consulta	= DB::table($tabela)->select(['id', $field])->get();

				}

			} else {
				
				$consulta	= DB::table($tabela)->select(['id', $field])->get();

			}

			if(count($consulta) >= 1) {

				foreach ($consulta as $item) {
					
					$retorno[]	= [

						'value'	=> $item->id,
						'text'	=> $item->$field

					];

				}

			}

			return $retorno;

		}


		public static function RelatedValues($tabela, $indice, $valor, $field) {

			$retorno	= [];
			$consulta	= DB::table($tabela)->select($field)->where($indice, $valor)->distinct()->get();
			if(count($consulta) >= 1) {

				foreach ($consulta as $item) {
					
					$retorno[]	= $item->$field;

				}

			}

			return $retorno;

		}
		
		public static function ExcluirRegistros($tabela, $itens, $callback, $success = 'Registro(s) excluido(s) com sucesso!', $fail = 'Erro! O registro selecionado não pode ser excluido.', $retorno = 'json') {

			$result	= false;
			$txt	= 'Solicitação inválida, nenhum item foi selecionado para ser excluido!';
			if(isset($itens)) {

				if(is_array($itens)) {

					$retorno	= 'redirect';
					$total		= count($itens);
					if($total >= 1) {

						$a = 0;
						foreach ($itens as $item) {

							$busca	= DB::table($tabela)->where('id', $item)->count();
							if($busca >= 1) {

								if(Schema::hasColumn($tabela, 'deleted_at')){

									$delete = DB::table($tabela)->where('id', $item)->update(['deleted_at' => now()]);
									if($delete == true) { $a++; }

								} else {

									$delete = DB::table($tabela)->where('id', $item)->delete();
									if($delete == true) { $a++; }

								}

							} else {

								$a++;

							}
							
						}


						if($a >= $total) {

							$txt	= $success;
							$result	= true;

						} else {

							$txt	= $fail;

						}

					}

				} else {

					if($itens != '') {

						$busca	= DB::table($tabela)->where('id', $itens)->count();
						if($busca >= 1) {

							if(Schema::hasColumn($tabela, 'deleted_at')){

								$excluir	= DB::table($tabela)->where('id', $itens)->update(['deleted_at' => now()]);
								if($excluir == true) {

									$txt	= $success;
									$result	= true;

								} else {

									$txt	= $fail;

								}

							} else {

								$excluir	= DB::table($tabela)->where('id', $itens)->delete();
								if($excluir == true) {

									$result	= true;
									$txt	= $success;

								} else {

									$txt	= $fail;

								}

							}

						} else {

							$txt	= 'O registro selecionado não foi encontrado!';

						}

					}

				}

			}

			if($retorno == 'redirect') {

				echo '<script type="text/javascript"> alert("' . $txt . '"); window.location.href = "' . $callback . '"; </script>';

			} else {

				$response	= [

					'result'	=> $result,
					'message'	=> $txt

				];

				return json_encode($response);
			
			}

		}

	}