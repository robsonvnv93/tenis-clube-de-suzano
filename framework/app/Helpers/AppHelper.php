<?php

	namespace App\Helpers;

	use App\Http\Controllers\PHPMailer;
	use App\AppNotificacaoLog;
	use App\AppContatoAssunto;
	use App\AppNotificacaoEnvio;
	use App\AppContato;
	use App\AppEmail;
	use App\UserType;
	use App\AppLog;


	class AppHelper {


		// Vincular usuário ao tipo no sistema.
		public static function VincularUsuario($user) {

			$create					= new UserType;
			$create->user_id		= $user;
			$create->user_type_id	= 2;

			$create->save();

		}


		// Criar log de ações realizadas por usuários do app.
		public static function CreateLog($user, $log) {

			$create				= new AppLog;
			$create->ip 		= $_SERVER['REMOTE_ADDR'];
			$create->user		= $user;
			$create->log		= $log;
			$create->created_at	= now();

			$create->save();

		}


		// Formatar data
		public static function formatDate($tipo, $data) {

			$retorno	= '';
			if(isset($data)) {

				if($tipo == 'db-date') {

					$explode	= explode('/', $data);
					$retorno	= $explode[2] . '-' . $explode[1] . '-' . $explode[0];

				} elseif($tipo == 'view-datetime') {

					$quebra		= explode(' ', $data);
					$hora		= $quebra[1];
					$data		= $quebra[0];
					$explode	= explode('-', $data);
					$retorno	= $explode[2] . '/' . $explode[1] . '/' . $explode[0] . ' - ' . $hora;

				} elseif($tipo == 'view-date') {

					$explode	= explode('-', $data);
					$retorno	= $explode[2] . '/' . $explode[1] . '/' . $explode[0];

				}

			}

			return $retorno;

		}


		public static function enviarEmailSMTP($servidor, $usuario, $senha, $porta, $para, $de, $de_nome, $assunto, $corpo) { 
									
			global $error;
			$mail	= new PHPMailer();
			$mail->IsSMTP();
			$mail->SMTPDebug	= 1;
			$mail->SMTPAuth		= true;
			$mail->Host			= $servidor;
			$mail->Port			= $porta;  		// A porta 587 deverá estar aberta em seu servidor
			$mail->Username		= $usuario;
			$mail->Password		= $senha;
			$mail->SetFrom($de, $de_nome);
			$mail->Subject		= $assunto;
			$mail->isHTML(true);
			$mail->Body			= $corpo;
			$mail->AddAddress($para);
			if(!$mail->Send()) {

				$error = 'Mail error: '.$mail->ErrorInfo;
				return false;

			} else {

				$error = 'Mensagem enviada!';
				return true;

			}

		}



		public static function enviarContato($contato) {

			$contato	= AppContato::where('id', $contato)->first();
			if(count($contato) >= 1) {

				$assunto	= '- - -';
				$busca		= AppContatoAssunto::where('id', $contato->assunto_id)->first();
				if(count($busca) >= 1) {

					$assunto = $busca->nome;
					
				}

				$servidor	= 'mail.imoveisaltotiete.com.br';
				$usuario	= 'contato@imoveisaltotiete.com.br';
				$senha		= 'q1w2e3r4';
				$porta		= 587;

				$message	= utf8_decode("<h1 style='color: red;'>CONTATO REALIZADO PELO APP</h1><hr /><b>Nome:</b> " . $contato->nome . "<br /><br /><b>E-mail: </b>" . $contato->email . "<br /><br /><b>Telefone:</b> " . $contato->telefone . "<br /><br /><b>Assunto:</b> " . $assunto . "<br /><br /><b>Data de Contato:</b> " . (AppHelper::formatDate('view-datetime', $contato->created_at)) . "<br /><br /><b>Mensagem:</b><br />" . $contato->mensagem);

				$destinatarios	= AppEmail::where('status', 1)->get();
				if(count($destinatarios) >= 1) {

					foreach ($destinatarios as $destinatario) {
						
						AppHelper::enviarEmailSMTP($servidor, $usuario, $senha, $porta, $destinatario->email, $contato->email, $contato->nome, utf8_decode('CONTATO PELO APP - ' . $assunto), $message);

					}

				}

			}

		}



		public static function enviarPush($sender, $notificacao, $tokens) {

			$apykey		= 'AAAA2V1GOU0:APA91bGX2Oht2wSGM-JVbY-Sj1EbTljjtaosBT3tEdafT5XvgcsGfcSZLEZEr0NIgtw7fAXm_fqKYGfWiWRltmNTUXpFV40hXuL-i-VCVgXu3E4zs3ksk_T1JH3bvsisTkHWR_J2-Xmf';
			$success	= 0;
			$fail		= 0;

			$headers[]	= 'Authorization: key=' . $apykey;
			$headers[]	= 'Content-Type: application/json';

			$fcmMsg["sound"]				= "default";
			$fcmMsg["color"]				= "#25387F";
			$fcmMsg["icon"]					= "http://app.tenisclubedesuzano.com.br/images/favicon.ico";
			$fcmMsg["click_action"]			= "FCM_PLUGIN_ACTIVITY";
			
			$fcmMsg['title']				= $notificacao->titulo;
			$fcmMsg['body']					= $notificacao->conteudo;


			$tipo	= $notificacao->tipo;

			if($tipo == 2) {

				$data			= array(

					"opcao"		=> "NOTICIA",
					"item_id"	=> $notificacao->item_id

				);

			} elseif($tipo == 3) {

				$data = array(

					"opcao"		=> "EVENTO",
					"item_id"	=> $notificacao->item_id

				);

			} else {

				$mensagem		= strip_tags($notificacao->conteudo);
				if(strlen($mensagem) > 150) {

					$mensagem	= str_limit($mensagem, 147) . '...';

				}

				$fcmMsg['body']	= $mensagem;

				$data = array(

					"opcao"		=> "NOTIFICACAO",
					"item_id"	=> $notificacao->id

				);

			}

			$fcmFields['notification']		= $fcmMsg;
			$fcmFields["priority"]			= "high";
			$fcmFields["token"]				= $apykey;

			$fcmFields['data']				= $data;


			foreach ($tokens as $token) {
				
				$fcmFields['registration_ids']	= array($token['FCM']);

				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
				curl_setopt($ch,CURLOPT_POST, true);
				curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fcmFields));
				
				$result		= curl_exec($ch);
				$response	= json_decode($result);

				if($response->success >= 1) {
						
					$status	= 1;
					$success++;

				} else {
					
					$status	= 0;
					$fail++;

				}

				$log					= new AppNotificacaoLog;
				$log->sender_id			= $sender;
				$log->user_token_id		= $token['id'];
				$log->status			= $status;
				$log->created_at		= now();
				$log->save();
				
				curl_close($ch);

			}


			$retorno	= [

				'success'	=> $success,
				'fail'		=> $fail

			];


			return $retorno;

		}

	}