<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class UserType extends Model {

		protected $table	= 'user_types';
		protected $fillable	= ['id', 'user_id', 'user_type_id'];
		public $timestamps	= false;
	
	}
