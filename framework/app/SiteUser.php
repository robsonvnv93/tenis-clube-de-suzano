<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteUser extends Model {

		protected $table	= 'site_users';
		protected $fillable = [
			
			'id',
			'titulo',
			'name',
			'password',
			'status',
			'created_at',
			'updatet_at'

		];
	
	}
