<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AppVersion extends Model {

		protected $table	= 'app_versions';
		protected $fillable	= [
			
			'id',
			'arquivo',
			'nome',
			'comentarios',
			'status',
			'created_at',
			'updated_at',
			'deleted_at'

		];
	
	}
