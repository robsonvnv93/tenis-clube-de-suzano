<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AppContatoAssunto extends Model {

		protected $table	= 'app_assuntos_contato';
		protected $fillable	= [
			
			'id',
			'nome',
			'descricao',
			'status',
			'order',
			'created_at',
			'updated_at'

		];
	
	}
