<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteUsersType extends Model {

		protected $table	= 'site_users_types';
		protected $fillable	= ['id', 'name', 'description', 'status'];
	
	}
