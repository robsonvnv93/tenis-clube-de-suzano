<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteHorario extends Model {

		protected $table	= 'site_horarios';
		protected $fillable	= [
			
			'id',
			'image',
			'name',
			'description',
			'status',
			'order',
			'created_at',
			'updated_at'

		];
	
	}
