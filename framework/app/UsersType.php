<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class UsersType extends Model {

		protected $table	= 'users_types';
		protected $fillable	= [
			
			'id',
			'name',
			'description',
			'status',
			'created_at',
			'updated_at'

		];
	
	}
