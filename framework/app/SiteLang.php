<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteLang extends Model {

		protected $table	= 'site_langs';
		protected $fillable	= [
			
			'id',
			'name',
			'status'

		];
	
	}
