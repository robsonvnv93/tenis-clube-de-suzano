<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class SiteCategoriaGrade extends Model {

		protected $table	= 'site_categorias_grade';
		protected $fillable	= [
			
			'id',
			'name',
			'status',
			'order',
			'created_at',
			'updated_at'

		];
	
	}
