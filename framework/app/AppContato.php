<?php

	namespace App;

	use Illuminate\Database\Eloquent\Model;

	class AppContato extends Model {

		protected $table	= 'app_contatos';
		protected $fillable	= [
			
			'id',
			'nome',
			'telefone',
			'email',
			'horario',
			'assunto_id',
			'mensagem',
			'status',
			'created_at',
			'updated_at'

		];
	
	}
