<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateUserGalleryTable extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('user_gallery', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('user_id');
				$table->string('picture');
				$table->integer('profile')->default(0);
				$table->integer('order')->default(0);
				$table->timestamps();

				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			
			});
		
		}


		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('user_gallery');
		
		}
	
	}
