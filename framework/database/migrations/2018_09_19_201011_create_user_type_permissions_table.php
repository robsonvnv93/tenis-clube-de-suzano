<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateUserTypePermissionsTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('user_type_permissions', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('user_type_id');
				$table->unsignedInteger('menu_id');

				$table->foreign('user_type_id')->references('id')->on('users_types')->onDelete('cascade');
				$table->foreign('menu_id')->references('id')->on('admin_menu')->onDelete('cascade');
			
			});
		
		}


		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('user_type_permissions');
		
		}

	}
