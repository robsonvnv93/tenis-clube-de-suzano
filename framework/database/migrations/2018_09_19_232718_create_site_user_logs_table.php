<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateSiteUserLogsTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('site_user_logs', function (Blueprint $table) {
				
				$table->increments('id');
				$table->string('ip');
				$table->unsignedInteger('user_id');
				$table->string('route');
				$table->longtext('log')->nullable();
				$table->dateTime('created_at');

				$table->foreign('user_id')->references('id')->on('site_users')->onDelete('cascade');
			
			});
		
		}

		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('site_user_logs');
		
		}
	
	}
