<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateSiteMenuItensTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('site_menu_itens', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('type_id');
				$table->unsignedInteger('menu_id');
				$table->string('name');
				$table->integer('status')->default(1);
				$table->integer('order');
				$table->timestamps();

				$table->foreign('type_id')->references('id')->on('site_menu_item_types')->onDelete('cascade');
				$table->foreign('menu_id')->references('id')->on('site_menu')->onDelete('cascade');
			
			});
		
		}

		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('site_menu_itens');
		
		}
	
	}
