<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateSiteUserTypePermissionsTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('site_user_type_permissions', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('user_type_id');
				$table->unsignedInteger('menu_id');

				$table->foreign('user_type_id')->references('id')->on('site_users_types')->onDelete('cascade');
				$table->foreign('menu_id')->references('id')->on('site_menu_itens')->onDelete('cascade');
			
			});
		
		}

		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('site_user_type_permissions');
		
		}
	
	}
