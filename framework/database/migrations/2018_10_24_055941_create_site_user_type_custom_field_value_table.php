<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateSiteUserTypeCustomFieldValueTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('site_user_type_custom_field_value', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('custom_field_id');
				$table->unsignedInteger('user_id');
				$table->string('field_value')->nullable();
				$table->timestamps();

				$table->foreign('custom_field_id')->references('id')->on('site_users_type_custom_fields')->onDelete('cascade');
				$table->foreign('user_id')->references('id')->on('site_users')->onDelete('cascade');
			
			});
		
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('site_user_type_custom_field_value');
		
		}
	
	}
