<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateAdminLangWordsTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('admin_lang_words', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('lang_id');
				$table->string('lang_word');
				$table->string('lang_value');
				$table->timestamps();

				$table->foreign('lang_id')->references('id')->on('admin_langs')->onDelete('cascade');
			
			});
		
		}

		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('admin_lang_words');
		
		}
	
	}
