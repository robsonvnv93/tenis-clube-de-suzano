<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateSiteUserTypesTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('site_user_types', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('user_id');
				$table->unsignedInteger('user_type_id');

				$table->foreign('user_type_id')->references('id')->on('site_users_types')->onDelete('cascade');
				$table->foreign('user_id')->references('id')->on('site_users')->onDelete('cascade');
			
			});
		
		}

		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('site_user_types');
		
		}
	
	}
