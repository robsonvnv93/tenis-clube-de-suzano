<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;


	class CreateSitePagesTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('site_pages', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('lang_id');
				$table->string('title');
				$table->string('slug')->unique();
				$table->longtext('content')->nullable();
				$table->integer('parent')->default(0);
				$table->integer('visible')->default(1);
				$table->integer('status')->default(1);
				$table->timestamps();

				$table->foreign('lang_id')->references('id')->on('site_langs')->onDelete('cascade');
			
			});
		
		}

		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('site_pages');
		
		}
	
	}
