<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateAdminMenuTable extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {

			Schema::create('admin_menu', function (Blueprint $table) {

				$table->increments('id');
				$table->unsignedInteger('route_id');
				$table->string('icon')->nullable();
				$table->string('name');
				$table->integer('parent')->default(0);
				$table->integer('visible')->default(1);
				$table->integer('status')->default(1);
				$table->integer('order');
				$table->timestamps();

				$table->foreign('route_id')->references('id')->on('admin_routes')->onDelete('cascade');

			});
		
		}


		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('admin_menu');
		
		}
	
	}
