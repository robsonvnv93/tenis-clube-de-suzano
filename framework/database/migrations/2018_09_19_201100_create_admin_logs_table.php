<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateAdminLogsTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('admin_logs', function (Blueprint $table) {
				
				$table->increments('id');
				$table->string('ip');
				$table->string('url');
				$table->longtext('log')->nullable();
				$table->dateTime('created_at');
			
			});
		
		}

		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('admin_logs');
		
		}
	
	}