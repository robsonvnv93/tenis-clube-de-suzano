<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateSiteUsersTypeCustomFieldsTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('site_users_type_custom_fields', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('user_type_id');
				$table->string('type');
				$table->string('name');
				$table->string('default_value')->nullable();
				$table->longtext('description')->nullable();
				$table->string('placeholder')->nullable();
				$table->integer('required_field')->default(0);
				$table->integer('order')->default(0);
				$table->timestamps();

				$table->foreign('user_type_id')->references('id')->on('site_users_types')->onDelete('cascade');

			});
		
		}

		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('site_users_type_custom_fields');

		}
	
	}