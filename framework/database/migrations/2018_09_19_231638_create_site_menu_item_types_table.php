<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateSiteMenuItemTypesTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('site_menu_item_types', function (Blueprint $table) {
				
				$table->increments('id');
				$table->string('slug')->unique();
				$table->string('name');
				$table->string('class');
				$table->string('function');
				$table->string('args')->nullable();
				$table->integer('status')->default(1);
				$table->timestamps();
			
			});
		
		}

		
		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('site_menu_item_types');
		
		}
	
	}
