<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateUserNotificationsTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('user_notifications', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('user_id');
				$table->string('url')->nullable();
				$table->string('text');
				$table->integer('status')->default(0);
				$table->timestamps();

				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			
			});
		
		}


		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('user_notifications');
		
		}
	
	}
