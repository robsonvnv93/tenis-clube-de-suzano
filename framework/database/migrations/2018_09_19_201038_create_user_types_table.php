<?php

	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;

	class CreateUserTypesTable extends Migration {
		
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {
			
			Schema::create('user_types', function (Blueprint $table) {
				
				$table->increments('id');
				$table->unsignedInteger('user_id');
				$table->unsignedInteger('user_type_id');

				$table->foreign('user_type_id')->references('id')->on('users_types')->onDelete('cascade');
				$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			
			});
		
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {
			
			Schema::dropIfExists('user_types');
		
		}
	
	}
