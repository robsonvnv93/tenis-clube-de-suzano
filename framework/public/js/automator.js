// Função para validar emails
function validaEmail(email) {

	var regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	return regex.test(email);

}


function AutomatorFormMakerComboBox(input, opts) {

	var started	= $("#" + input + '-input').attr('data-started');
	if(started == 'false') {

		$("#" + input + '-input').attr('data-started', 'true');
		$("#" + input + '-input').autocomplete({

			source: opts,
			select: function(event, ui) {

				event.preventDefault();
				$('#' + input + '-input').val(ui.item.label);
				$('#' + input).val(ui.item.value);
			
			}

		});

	}

}


function AutomatorFormMakerComboBoxAjax(input, url, extra) {

	var started	= $("#" + input + '-input').attr('data-started');
	if(started == 'false') {

		$("#" + input + '-input').attr('data-started', 'true');
		var token	= $('meta[name="csrf-token"]').attr('content');
		
		$("#" + input + '-input').autocomplete({

			source: function(request, response) {

				$.ajax({

					url:		url,
					type:		"POST",
					dataType:	"json",
					data:		{ '_token': token, extra: extra, valor: request.term },
					success:	function(retorno) {

						if(retorno.result == true) {

							if(retorno.total >= 1) {

								$("#" + input + '-input').removeClass('is-valid');
								$("#" + input + '-input').removeClass('is-invalid');
								response(retorno.itens);

							} else {

								$("#" + input + '-input').addClass('is-invalid');

							}

						} else {

							console.log(retorno.message);

						}

					}

				});

			},
			select: function(event, ui) {

				event.preventDefault();
				$('#' + input + '-input').val(ui.item.label);
				$('#' + input).val(ui.item.value);
				$("#" + input + '-input').removeClass('is-invalid');
				$("#" + input + '-input').addClass('is-valid');
			
			}

		});

	}

}


$(function() {

	$('.maskMoney').maskMoney();

	$('.maskNumber').keyup(function () {

		this.value = this.value.replace(/[^0-9\.]/g,'');

	});

	$('.maskDate').setMask('99/99/9999');
	$('.maskCPF').setMask('999.999.999-99');
	$('.maskPhone').setMask('(99) 9999 - 9999');
	$('.maskCellPhone').setMask('(99) 99999 - 9999');

	// Função para mostrar ou ocultar a sidebar.
	$('.sidebar-btn').click(function() {

		var botao	= $(this);
		var sidebar	= $('#sidebar');
		var content	= $('#content');

		var id		= botao.attr('id');
		if(id == 'sidebar-open-btn') {

			var outro = $('#sidebar-close-btn');

		} else {
			
			var outro = $('#sidebar-open-btn');

		}

			
		if(sidebar.hasClass('sidebar-opened')) {

			var anterior	= 'sidebar-opened';
			var nova		= 'sidebar-closed';
			var sessao		= 'closed';
			var conteudo	= 'col-12';

		} else {
			
			var anterior	= 'sidebar-closed';
			var nova		= 'sidebar-opened';
			var sessao		= 'opened';
			var conteudo	= 'col-12 col-sm-8 offset-sm-4 col-md-9 offset-md-3 col-lg-10 offset-lg-2';

		}


		$('#sidebar').switchClass(anterior, nova, 700, function () {

			botao.removeClass('sidebar-btn-show');
			botao.addClass('sidebar-btn-hidden');

			outro.removeClass('sidebar-btn-hidden');
			outro.addClass('sidebar-btn-show');

			content.attr('class', conteudo);

			var base	= $('meta[name="base-url"]').attr('content');
			var token	= $('meta[name="csrf-token"]').attr('content');
			$.ajax({

				url: base + "/automator/sidebar/",
				type: "POST",
				data: { '_token': token, 'sidebar': sessao },
				success: function(res) {},
				error: function(e) {}

			});

		});

	});


	// Função para mostrar ou ocultar algum submenu dos itens do menu
	$('.sidebar-nav-item-submenu').click(function() {

		var item	= $(this);
		if(!item.hasClass('sidebar-nav-item-submenu-clicked')) {

			item.addClass('sidebar-nav-item-submenu-clicked');
			var link	= item.find('a.sidebar-nav-item-link');
			var submenu	= item.find('div.sidebar-nav-item-submenu-container');

			var arrow	= link.find('div.sidebar-nav-item-link-arrow');
			var seta	= arrow.find('span.fa');

			if(seta.hasClass('fa-chevron-down')) {

				submenu.slideDown(500, function() {

					seta.attr('class', 'fa fa-chevron-up');
					item.removeClass('sidebar-nav-item-submenu-clicked');

				});

			} else {

				submenu.slideUp(500, function() {
					
					seta.attr('class', 'fa fa-chevron-down');
					item.removeClass('sidebar-nav-item-submenu-clicked');

				});

			}

		}

	});


	// Ação de atualizar informações do usuário
	$('#account-form').submit(function(){

		var nome	= $('#account-form-name');
		var email	= $('#account-form-email');
		if(nome.val() == '') {

			alert('Por favor preencha seu "Nome".');
			nome.focus();

		} else {

			if(email.val() == '') {

				alert('Por favor preencha seu "Email".');
				email.focus();

			} else {

				if (validaEmail(email.val())) {

					var base	= $('meta[name="base-url"]').attr('content');
					var token	= $('meta[name="csrf-token"]').attr('content');

					$.ajax({

						url: base + "/automator/user/",
						type: "POST",
						data: { '_token': token, 'action': 'update-data', 'nome': nome.val(), 'email': email.val() },
						success: function(res) {

							alert(res.message);
							if(res.result == true) {

								location.reload();

							}

						},
						error: function(e) {

							alert("Erro! Seus dados não foram alterados, por favor atualize esta página e tente realizar a alteração de seus dados novamente.");

						}

					});

				} else {

					alert('"Email" inválido.');
					email.focus();

				}

			}

		}

		return false;

	});


	// Ação de atualizar a senha do usuário
	$('#account-form-password').submit(function(){

		var nova		= $('#account-form-password-nova');
		var confirmar	= $('#account-form-password-confirmar');
		var atual		= $('#account-form-password-atual');

		if(nova.val() == '') {

			alert('Por favor preencha sua "Nova Senha".');
			nova.focus();

		} else {

			if(nova.val().length <= 7) {

				alert('Sua "Nova Senha" precisa ter ao menos 8 caracteres.');
				nova.focus();

			} else {

				if(confirmar.val() == '') {

					alert('Por favor confirme sua "Nova Senha".');
					confirmar.focus();

				} else {

					if(confirmar.val() != nova.val()) {

						alert('A confirmação de sua "Nova Senha" está incorreta.');
						confirmar.focus();

					} else {

						if(atual.val() == '') {

							alert('Por favor preencha sua "Senha Atual".');
							atual.focus();

						} else {

							var base	= $('meta[name="base-url"]').attr('content');
							var token	= $('meta[name="csrf-token"]').attr('content');

							$.ajax({

								url: base + "/automator/user/",
								type: "POST",
								data: { '_token': token, 'action': 'update-password', 'atual': atual.val(), 'nova': nova.val() },
								success: function(res) {

									alert(res.message);
									if(res.result == true) {

										location.reload();

									}

								},
								error: function(e) {

									alert("Erro! Sua senha não foi alterada, por favor atualize esta página e tente realizar a alteração de sua senha novamente.");

								}

							});

						}

					}

				}

			}

		}

		return false;
	});


	// Função para habilitar os tooltips nos botões do sistema.
	$('.btn-icon').tooltip({'trigger': 'hover'});


	// Função para excluir algum item da paginação de registros.
	$('.btn-paginator-delete').click(function() {

		if(confirm("Tem certeza de que deseja excluir este registro? Esta ação não poderá ser desfeita")) {

			var rota	= $('#paginator-list-form').attr('action');
			var token	= $('meta[name="csrf-token"]').attr('content');
			var item	= $(this).attr('data-id');
			$.ajax({

				url: rota,
				type: "POST",
				data: { '_token': token, 'itens': item },
				success: function(res) {

					console.log(res);
					alert(res.message);
					if(res.result == true) {

						location.reload();

					}

				},
				error: function(e) {

					alert("Erro! O registro não pode ser excluido, por favor atualize esta página e tente realizar a exclusão deste registro novamente.");

				}

			});

		}

	});


	$('#paginator-btn-delete').click(function() {

		var livre = $(this).attr('disabled');
		if(livre == false || livre == undefined) {

			if(confirm("Tem certeza de que deseja excluir este registro? Esta ação não poderá ser desfeita")) {

				$('#paginator-list-form').submit();

			}

		}

	});


	$("#paginator-checkall").click(function() {

		var status = $(this).prop('checked');
		$(".paginator-list-item").each(function() {

			$(this).prop('checked', status);

		});

		if(status == true) {

			$('#paginator-btn-delete').attr('disabled', false);

		} else {
			
			$('#paginator-btn-delete').attr('disabled', true);

		}

	});


	$(".paginator-list-item").click(function() {

		var status = $(this).prop('checked');
		if (status == true) {

			var total = 1;

		} else {
			
			var total = 0;

		}

		$(".paginator-list-item").each(function() {

			item = $(this).prop('checked');
			if (item == true) {

				total++;

			}

		});

		if (total >= 1) {

			$('#paginator-btn-delete').attr('disabled', false);

		} else {

			$('#paginator-btn-delete').attr('disabled', true);
			$("#paginator-checkall").prop('checked', false);

		}

	});


	$('#paginator-search-form').submit(function() {

		var busca = $('#paginator-search-input').val();
		if(busca == '') {

			alert("Por favor digite algo para realizar a busca!");
			$('#paginator-search-input').focus();
			return false;

		}

	});

	$('.routes-url-insert').val('/');

	$('.routes-url').keyup(function() {

		var valor 		= $(this).val();
		var primeiro	= valor.substring(0, 1);
		if(primeiro != '/') {

			var novo = '/' + valor;
			$(this).val(novo);

		}

	}).blur(function() {

		var valor		= $(this).val();
		var ultimo		= valor.length;
		var penultimo	= ultimo - 1;
		var final		= valor.substring(penultimo, ultimo);
		if(final != '/') {

			$(this).val(valor + '/');
			
		}

	});

});