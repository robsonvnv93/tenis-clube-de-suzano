function atualizarStatus(url, notificacao) {
	
	var token	= $('meta[name="csrf-token"]').attr('content');

	$.ajax({

		url: url,
		type: "POST",
		dataType: 'json',
		data: { '_token': token, action: 'get-notification-status', notificacao: notificacao },
		success: function(response) {

			if(response.result == true) {

				$('#notificacao-' + notificacao).attr('data-original-title', response.titulo).attr('data-title', response.titulo).attr('title', response.titulo).parent().find('.tooltip-inner').html(response.titulo);
				$('#notificacao-' + notificacao).removeClass('btn-dark').removeClass('btn-send-push-cursor').addClass(response.classe).attr('data-status', response.status).html('<i class="fa fa-' + response.icone + '"></i>');

			} else {

				$('#notificacao-' + notificacao).attr('data-original-title', 'ERRO').attr('data-title', 'ERRO').attr('title', 'ERRO').parent().find('.tooltip-inner').html('ERRO');
				$('#notificacao-' + notificacao).removeClass('btn-dark').addClass('btn-danger').attr('data-status', '-1').html('<i class="fa fa-close"></i>');

			}

		},
		error: function(e) {

			$('#notificacao-' + notificacao).attr('data-original-title', 'ERRO').attr('data-title', 'ERRO').attr('title', 'ERRO').parent().find('.tooltip-inner').html('ERRO');
			$('#notificacao-' + notificacao).removeClass('btn-dark').addClass('btn-danger').attr('data-status', '-1').html('<i class="fa fa-close"></i>');

		}

	});

}



function atualizarStatusNotificacao(url, acao, item) {
	
	var token	= $('meta[name="csrf-token"]').attr('content');

	$.ajax({

		url: url,
		type: "POST",
		dataType: 'json',
		data: { '_token': token, action: acao, item: item },
		success: function(response) {

			if(response.result == true) {

				$('#botao-notificacao-' + item).attr('data-original-title', response.titulo).attr('data-title', response.titulo).attr('title', response.titulo).parent().find('.tooltip-inner').html(response.titulo);
				$('#botao-notificacao-' + item).removeClass('btn-dark').removeClass('btn-send-push-cursor').addClass(response.classe).attr('data-status', response.status).attr('data-notificacao', response.notificacao).attr('data-tipo', response.tipo).attr('data-item', item).html('<i class="fa fa-' + response.icone + '"></i>');

			} else {

				$('#botao-notificacao-' + item).attr('data-original-title', 'ERRO').attr('data-title', 'ERRO').attr('title', 'ERRO').parent().find('.tooltip-inner').html('ERRO');
				$('#botao-notificacao-' + item).removeClass('btn-dark').addClass('btn-danger').attr('data-status', '-1').html('<i class="fa fa-close"></i>');

			}

		},
		error: function(e) {

			$('#botao-notificacao-' + item).attr('data-original-title', 'ERRO').attr('data-title', 'ERRO').attr('title', 'ERRO').parent().find('.tooltip-inner').html('ERRO');
			$('#botao-notificacao-' + item).removeClass('btn-dark').addClass('btn-danger').attr('data-status', '-1').html('<i class="fa fa-close"></i>');

		}

	});

}




function fecharBoxPush() {

	location.reload();

}



function visualizarDetalhesEnvio(id) {

	var status = $('#detalhes-envio-' + id).attr('data-display');

	if(status == 0) {

		$('#detalhes-envio-botao-' + id).text('Ocultar Detalhes');
		$('#detalhes-envio-' + id).attr('data-display', 1);
		$('#detalhes-envio-' + id).slideDown(500);

	} else {

		$('#detalhes-envio-botao-' + id).text('Visualizar Detalhes');
		$('#detalhes-envio-' + id).attr('data-display', 0);
		$('#detalhes-envio-' + id).slideUp(500);

	}

}





$(function() {


	// Botões send push - página listar notificações.
	var botoes	= $('.btn-send-push').length;

	if(botoes >= 1) {

		$('.btn-send-push').each(function() {

			var botao		= $(this);
			var link		= botao.attr('href');
			var quebra		= link.split('?');
			var url			= quebra[0];
			var notificacao	= quebra[1];

			botao.attr('id', 'notificacao-' + notificacao);
			atualizarStatus(url, notificacao);

		});


		$('.btn-send-push').click(function() {

			var status		= $(this).attr('data-status');
			if(status >= 0) {

				var continuar	= false;
				if(status == 1) {

					if(confirm("Esta notificação já foi enviada anteriormente. Tem certeza de que deseja realizar o envio desta mensagem novamente?")) {

						continuar = true;

					}

				} else {

					continuar = true;

				}

				if(continuar == true) {

					var link		= $(this).attr('href');
					var quebra		= link.split('?');
					var url			= quebra[0];
					var notificacao	= quebra[1];
					$('#page-loader').fadeIn(500, function() {

						var token	= $('meta[name="csrf-token"]').attr('content');

						$.ajax({

							url: url,
							type: "POST",
							dataType: 'json',
							data: { '_token': token, action: 'send-push', notificacao: notificacao },
							success: function(response) {

								if(response.result == true) {

									$("#page-loader").find('table').html('<tr><td style="padding-left: 10px; padding-right: 10px;"><div style="margin: 10px auto; position: relative; display: table; width: 100%; max-width: 400px; background-color: #FFFFFF; border-radius: 4px;"><div style="width: 100%; position: relative; display: table; border-bottom: 1px solid #000000; text-align: center; font-size: 21px; color: #000000; height: 40px; margin-top: 14px;">RELATÓRIO DE ENVIO</div><div style="width: 100%; position: relative; display: table;"><div style="position: relative; display: table; width: 50%; float: left; padding: 10px; font-size: 16px; margin-top: 10px; margin-bottom: 10px;" class="text-success text-center"><span style="font-size: 20px;">' + response.success + '</span><br />Enviadas</div><div style="position: relative; display: table; width: 50%; padding: 10px; float: left; font-size: 16px; margin-top: 10px; margin-bottom: 10px;" class="text-danger text-center"><span style="font-size: 20px;">' + response.fail + '</span><br />Falharam</div></div><div style="position: relative; display: table; width: 100%; cursor: pointer; background-color: red; border-radius: 4px; padding: 10px; font-size: 15px; color: #FFFFFF;" onClick="fecharBoxPush();"><div style="position: relative; display: table; width: 100%; margin-top: 10px; margin-bottom: 10px; text-align: center;">FECHAR</div></div></div></td><tr>');

								} else {

									alert(response.message);
									$('#page-loader').fadeOut(500, function() {

										$('#page-loader').remove();

									});

								}

							},
							error: function(e) {

								console.log(e);
								alert('Oops! Alguma coisa deu errado, tente atualizar a página, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.');
								$('#page-loader').fadeOut(500);

							}

						});

					});

				}

			}

			return false;

		});

	}



	// Botões send push - página noticias.
	var notificacoes	= $('.btn-send-push-notificacoes').length;

	if(notificacoes >= 1) {

		var modalHTML = '';
		modalHTML += '<div class="modal fade" id="enviar-notificacao-modal" tabindex="-1" role="dialog" aria-labelledby="enviar-notificacao-modal">';
			
			modalHTML += '<div class="modal-dialog" role="document">';

				modalHTML += '<div class="modal-content modal-md">';

					modalHTML += '<div class="modal-header">';

						modalHTML += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
						modalHTML += '<h4 class="modal-title text-center">ENVIAR NOTIFICAÇÃO</h4>';

					modalHTML += '</div>';
					modalHTML += '<div class="modal-body">';
						
						modalHTML += '<form id="enviar-notificacao-modal-form">';
							
							modalHTML += '<input type="hidden" id="enviar-notificacao-modal-form-tipo" value="" />';
							modalHTML += '<input type="hidden" id="enviar-notificacao-modal-form-item" value="" />';
							modalHTML += '<div class="row">';
								
								modalHTML += '<div class="col-xs-12" style="margin-bottom: 20px;">';

									modalHTML += '<label for="enviar-notificacao-modal-form-titulo">Titulo <span class="text-danger">*</span></label>';
									modalHTML += '<input type="text" id="enviar-notificacao-modal-form-titulo" name="enviar-notificacao-modal-form-titulo" max-length="191" class="form-control" />';

								modalHTML += '</div>';

							modalHTML += '</div>';
							modalHTML += '<div class="row">';
								
								modalHTML += '<div class="col-xs-12" style="margin-bottom: 10px;">';

									modalHTML += '<label for="enviar-notificacao-modal-form-conteudo">Conteudo <span class="text-danger">*</span></label>';
									modalHTML += '<input type="text" id="enviar-notificacao-modal-form-conteudo" name="enviar-notificacao-modal-form-conteudo" max-length="191" class="form-control" />';

								modalHTML += '</div>';

							modalHTML += '</div>';
							modalHTML += '<input type="submit" value="enviar" style="display: none;" />';

						modalHTML += '</form>';

					modalHTML += '</div>';
					modalHTML += '<div class="modal-footer">';

						modalHTML += '<button type="button" class="btn btn-danger" data-dismiss="modal">CANCELAR</button>';
						modalHTML += '<button id="enviar-notificacao-modal-form-submit" type="button" class="btn btn-success">ENVIAR</button>';

					modalHTML += '</div>';

				modalHTML += '</div>';

			modalHTML += '</div>';

		modalHTML += '</div>';

		$('body').append(modalHTML);

		$('.btn-send-push-notificacoes').each(function() {

			var botao	= $(this);
			var link	= botao.attr('href');
			var quebra	= link.split('?');
			var url		= quebra[0];
			var item	= quebra[1];

			if(botao.hasClass('btn-send-push-noticia')) {

				var acao = 'get-noticia-status';

			} else {
				
				var acao = 'get-evento-status';

			}

			botao.attr('id', 'botao-notificacao-' + item);
			atualizarStatusNotificacao(url, acao, item);

		});


		$('.btn-send-push-notificacoes').click(function() {

			var botao		= $(this);
			var status		= botao.attr('data-status');
			var tipo		= botao.attr('data-tipo');

			if(status == 0) {

				if(tipo == 2) {

					alert("Para enviar uma notificação aos usuários do aplicativo esta notícia precisa estar ativa primeira.");

				} else {

					alert("Para enviar uma notificação aos usuários do aplicativo este evento precisa estar ativo primeiro.");

				}

			} else {

				$('#page-loader').fadeIn(500, function() {

					var item	= botao.attr('data-item');
					var titulo	= botao.attr('data-notificacao');
					var link	= botao.attr('href');
					var quebra	= link.split('?');
					var url		= quebra[0];
					$('#enviar-notificacao-modal-form').attr('action', url);
					$('#enviar-notificacao-modal-form-tipo').val(tipo);
					$('#enviar-notificacao-modal-form-item').val(item);
					$('#enviar-notificacao-modal-form-titulo').val(titulo);
					$('#enviar-notificacao-modal-form-conteudo').val("");
					$('#page-loader').fadeOut(500, function() {

						$('#enviar-notificacao-modal').modal("show");

					});

				});

			}

			return false;

		});


		$('#enviar-notificacao-modal-form-submit').click(function() {

			$('#enviar-notificacao-modal-form').submit();
		
		});



		$('#enviar-notificacao-modal-form').submit(function() {

			var titulo		= $('#enviar-notificacao-modal-form-titulo').val();
			var conteudo	= $('#enviar-notificacao-modal-form-conteudo').val();
			if(titulo == "") {

				alert("O Titulo da notificação é obrigatória.");
				$('#enviar-notificacao-modal-form-titulo').focus();

			} else {

				if(conteudo == "") {

					alert("O Conteudo da notificação é obrigatório.");
					$('#enviar-notificacao-modal-form-conteudo').focus();

				} else {

					$("#page-loader").fadeIn(500, function() {

						var url		= $('#enviar-notificacao-modal-form').attr('action');
						var tipo	= $('#enviar-notificacao-modal-form-tipo').val();
						var item	= $('#enviar-notificacao-modal-form-item').val();
						var token	= $('meta[name="csrf-token"]').attr('content');

						$.ajax({

							url: url,
							type: "POST",
							dataType: 'json',
							data: { '_token': token, action: 'create-push', tipo: tipo, item: item, titulo: titulo, conteudo: conteudo },
							success: function(response) {

								alert(response.message);
								if(response.result == true) {

									$('#enviar-notificacao-modal').modal('hide');
									$('#enviar-notificacao-modal-form-tipo').val("");
									$('#enviar-notificacao-modal-form-item').val("");
									$('#enviar-notificacao-modal-form-titulo').val("");
									$('#enviar-notificacao-modal-form-conteudo').val("");
									if(confirm("Deseja realizar o envio desta notificação agora?")) {

										$.ajax({

											url: url,
											type: "POST",
											dataType: 'json',
											data: { '_token': token, action: 'send-push', notificacao: response.notificacao },
											success: function(response1) {

												if(response1.result == true) {

													$("#page-loader").find('table').html('<tr><td style="padding-left: 10px; padding-right: 10px;"><div style="margin: 10px auto; position: relative; display: table; width: 100%; max-width: 400px; background-color: #FFFFFF; border-radius: 4px;"><div style="width: 100%; position: relative; display: table; border-bottom: 1px solid #000000; text-align: center; font-size: 21px; color: #000000; height: 40px; margin-top: 14px;">RELATÓRIO DE ENVIO</div><div style="width: 100%; position: relative; display: table;"><div style="position: relative; display: table; width: 50%; float: left; padding: 10px; font-size: 16px; margin-top: 10px; margin-bottom: 10px;" class="text-success text-center"><span style="font-size: 20px;">' + response1.success + '</span><br />Enviadas</div><div style="position: relative; display: table; width: 50%; padding: 10px; float: left; font-size: 16px; margin-top: 10px; margin-bottom: 10px;" class="text-danger text-center"><span style="font-size: 20px;">' + response1.fail + '</span><br />Falharam</div></div><div style="position: relative; display: table; width: 100%; cursor: pointer; background-color: red; border-radius: 4px; padding: 10px; font-size: 15px; color: #FFFFFF;" onClick="fecharBoxPush();"><div style="position: relative; display: table; width: 100%; margin-top: 10px; margin-bottom: 10px; text-align: center;">FECHAR</div></div></div></td><tr>');

												} else {

													alert(response1.message);
													$('#page-loader').fadeOut(500, function() {

														$('#page-loader').remove();

													});

												}

											},
											error: function(e) {

												console.log(e);
												alert('Oops! Alguma coisa deu errado, tente atualizar a página, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.');
												$('#page-loader').fadeOut(500);

											}

										});

									} else {

										$("#page-loader").fadeOut(500);

									}

								} else {

									$("#page-loader").fadeOut(500);


								}

							},
							error: function(e) {

								console.log(e);
								alert('Oops! Alguma coisa deu errado, tente atualizar a página, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.');
								$('#page-loader').fadeOut(500);

							}

						});

					});


				}

			}


			return false;

		});

	}


	$(".datepicker-time").daterangepicker({

		singleDatePicker: !0,
		timePicker: !0,
		locale: {

			"format":			"DD/MM/YYYY - hh:mm:ss",
			"separator":		" - ",
			"applyLabel":		"Aplicar",
			"cancelLabel":		"Cancelar",
			"fromLabel":		"De",
			"toLabel":			"Até",
			"customRangeLabel":	"Custom",
			"daysOfWeek":		["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"],
			"monthNames":		["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
			"firstDay":			0

		}

	});

});