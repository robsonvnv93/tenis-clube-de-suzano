<?php

	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
	// error_reporting(E_ALL);
	ini_set('display_errors', 1);

	/*
	|--------------------------------------------------------------------------
	| Web Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register web routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| contains the "web" middleware group. Now create something great!
	|
	*/


	Route::get('/', 'SiteController@index')->name('home');


	Route::prefix('admin')->group(function () {
		
		Auth::routes();
		Helper::AdminRoutes();

	});

	

	// Roras de API do app.
	Route::prefix('api')->group(function () {

		// Função de validação de versão do app.
		Route::any('/_version/check/',			'AppController@CheckVersion')->name('app-version-check');		// Retorna versão atual do app.
		Route::any('/_version/update/',			'AppController@UpdateAppVersion')->name('app-version-update');	// Retorna o apk pra download da versão nova do app.


		
		// Funções do FCMToken do app.
		Route::any('/_token/check/',			'AppController@CheckToken')->name('app-token-check');			// Validar FCMTokenID enviado pelo dispositivo.
		Route::any('/_token/create/',			'AppController@CreateToken')->name('app-token-create');			// Cria novo FCMTokenID para uma nova hash enviada pelo dispositivo.
		
		
		
		// Listagem de informações do sistema para o app.
		Route::any('/_get/banners/',			'AppController@GetBanners')->name('app-get-banners');			// Listagem de banners.
		Route::any('/_get/noticias/',			'AppController@GetNoticias')->name('app-get-noticias');			// Listagem de noticias.
		Route::any('/_get/eventos/',			'AppController@GetEventos')->name('app-get-eventos');			// Listagem de eventos.
		Route::any('/_get/horarios/',			'AppController@GetHorarios')->name('app-get-horarios');			// Listagem de horários de funcionamento de atrações do clube.
		Route::any('/_get/grades/',				'AppController@GetGrades')->name('app-get-grades');				// Listagem de grades.
		Route::any('/_get/galerias/',			'AppController@GetGalerias')->name('app-get-galerias');			// Listagem de galerias.
		Route::any('/_get/notificacoes/',		'AppController@GetNotificacoes')->name('app-get-notificacoes');	// Listagem de notificações.
		
		
		
		// Exibição de informações de itens selecionados dentro do app cadastradas no sistema.
		Route::any('/_show/noticia/',			'AppController@ShowNoticia')->name('app-show-noticia');			// Exibe detalhes de uma noticia.
		Route::any('/_show/evento/',			'AppController@ShowEvento')->name('app-show-evento');			// Exibe detalhes de um evento.
		Route::any('/_show/grade/',				'AppController@ShowGrade')->name('app-show-grade');				// Exibe detalhes de uma grade.
		Route::any('/_show/galeria/',			'AppController@ShowGaleria')->name('app-show-galeria');			// Exibe detalhes de uma galeria.
		Route::any('/_show/pagina/',			'AppController@ShowPagina')->name('app-show-pagina');			// Exibe detalhes de uma galeria.
		Route::any('/_show/notificacao/',		'AppController@ShowNotificacao')->name('app-show-notificacao');	// Exibe detalhes de uma notificação enviada.
		

		Route::any('/_change/notificacao/',		'AppController@ChangeNotificacaoStatus')->name('app-status-notificacao');	// Altera o status do log de envio de uma notificação enviada.
		

		
		// Funções relacionadas as ações de contato do app.
		Route::any('/_contato/getassuntos/',	'AppController@ContatoAssuntos')->name('app-contato-assuntos');	// Realiza listagem de assuntos disponiveis para seleção no formulário de contato do app.
		Route::any('/_contato/enviar/',			'AppController@Contato')->name('app-contato');					// Realiza ação de envio das informações inseridas do formulario de contato do app.
		

	});
		
