-- phpMyAdmin SQL Dump
-- version 4.3.7
-- http://www.phpmyadmin.net
--
-- Host: mysql04-farm76.kinghost.net
-- Tempo de geração: 25/02/2019 às 04:57
-- Versão do servidor: 5.6.38-log
-- Versão do PHP: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `tenisclubedesu03`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_configs`
--

CREATE TABLE IF NOT EXISTS `admin_configs` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `admin_configs`
--

INSERT INTO `admin_configs` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'lang', '1', '2018-10-30 05:07:58', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_langs`
--

CREATE TABLE IF NOT EXISTS `admin_langs` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `admin_langs`
--

INSERT INTO `admin_langs` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Português Brasil', 1, '2018-10-30 05:07:58', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_lang_words`
--

CREATE TABLE IF NOT EXISTS `admin_lang_words` (
  `id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned NOT NULL,
  `lang_word` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_logs`
--

CREATE TABLE IF NOT EXISTS `admin_logs` (
  `id` int(10) unsigned NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `log` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `admin_logs`
--

INSERT INTO `admin_logs` (`id`, `ip`, `url`, `log`, `created_at`) VALUES
(1, '127.0.0.1', 'login', 'Acessou a Página', '2018-10-30 02:08:13'),
(2, '179.154.193.11', 'login', 'Acessou a Página', '2018-10-30 04:53:54'),
(3, '179.154.193.11', 'login', 'Acessou a Página', '2018-10-30 05:10:17'),
(4, '179.154.193.11', 'login', 'Acessou a Página', '2018-10-30 05:10:42'),
(5, '179.154.193.11', 'login', 'Acessou a Página', '2018-10-30 07:22:20'),
(6, '179.154.193.11', 'login', 'Acessou a Página', '2018-10-30 07:25:05'),
(7, '187.10.193.157', 'login', 'Acessou a Página', '2018-10-30 10:57:56'),
(8, '179.154.193.11', 'login', 'Acessou a Página', '2018-10-31 19:26:08'),
(9, '179.154.193.11', 'login', 'Acessou a Página', '2018-11-09 07:08:34'),
(10, '179.154.193.11', 'login', 'Acessou a Página', '2018-11-09 07:09:04'),
(11, '179.154.193.11', 'login', 'Acessou a Página', '2018-11-16 18:16:59'),
(12, '179.154.193.11', 'login', 'Acessou a Página', '2018-11-16 18:17:05'),
(13, '138.99.201.106', 'login', 'Acessou a Página', '2018-11-16 18:17:13'),
(14, '179.154.193.11', 'login', 'Acessou a Página', '2018-11-16 18:38:53'),
(15, '179.154.193.11', 'login', 'Acessou a Página', '2018-11-20 19:20:53'),
(16, '179.154.193.11', 'login', 'Acessou a Página', '2018-11-21 13:12:43'),
(17, '179.154.193.11', 'login', 'Acessou a Página', '2018-11-26 21:39:43'),
(18, '177.143.35.243', 'login', 'Acessou a Página', '2018-12-05 03:13:05'),
(19, '177.143.35.243', 'login', 'Acessou a Página', '2018-12-05 03:13:05'),
(20, '177.143.35.243', 'login', 'Acessou a Página', '2018-12-06 03:03:10'),
(21, '177.143.35.243', 'login', 'Acessou a Página', '2018-12-06 03:04:28'),
(22, '177.143.35.243', 'login', 'Acessou a Página', '2018-12-08 03:54:05'),
(23, '177.143.35.243', 'login', 'Acessou a Página', '2018-12-08 13:10:49'),
(24, '177.143.35.243', 'login', 'Acessou a Página', '2018-12-19 17:26:41'),
(25, '177.143.35.243', 'login', 'Acessou a Página', '2018-12-21 21:46:22'),
(26, '177.143.78.223', 'login', 'Acessou a Página', '2019-01-03 13:40:43'),
(27, '177.143.78.223', 'login', 'Acessou a Página', '2019-01-03 13:40:49'),
(28, '177.143.78.223', 'login', 'Acessou a Página', '2019-01-03 13:41:18'),
(29, '177.143.78.223', 'login', 'Acessou a Página', '2019-01-03 17:13:51'),
(30, '177.143.43.40', 'login', 'Acessou a Página', '2019-01-04 13:55:14'),
(31, '177.143.43.40', 'login', 'Acessou a Página', '2019-01-04 13:55:14'),
(32, '177.143.43.40', 'login', 'Acessou a Página', '2019-01-07 14:47:38'),
(33, '179.152.202.242', 'login', 'Acessou a Página', '2019-01-30 02:37:52'),
(34, '179.152.202.242', 'login', 'Acessou a Página', '2019-01-30 14:40:04'),
(35, '179.152.202.242', 'login', 'Acessou a Página', '2019-01-30 20:19:23'),
(36, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-01 05:06:46'),
(37, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-01 05:47:51'),
(38, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-01 05:48:02'),
(39, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-01 05:49:36'),
(40, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-01 05:50:16'),
(41, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-01 05:50:18'),
(42, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-01 05:50:24'),
(43, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-03 05:45:54'),
(44, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-03 05:47:31'),
(45, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-03 13:50:26'),
(46, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-04 02:48:34'),
(47, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-04 02:49:49'),
(48, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-05 14:14:36'),
(49, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-05 14:14:36'),
(50, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 15:47:07'),
(51, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 15:47:11'),
(52, '177.79.9.56', 'login', 'Acessou a Página', '2019-02-09 15:48:22'),
(53, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 15:48:24'),
(54, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 15:48:36'),
(55, '177.79.16.72', 'login', 'Acessou a Página', '2019-02-09 15:48:46'),
(56, '177.79.9.56', 'login', 'Acessou a Página', '2019-02-09 15:48:54'),
(57, '177.79.16.72', 'login', 'Acessou a Página', '2019-02-09 15:49:07'),
(58, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 15:49:22'),
(59, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 15:49:33'),
(60, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-09 18:34:22'),
(61, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 18:49:34'),
(62, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 18:50:12'),
(63, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-09 18:50:27'),
(64, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 18:50:37'),
(65, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 19:03:05'),
(66, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 19:08:58'),
(67, '191.17.4.120', 'login', 'Acessou a Página', '2019-02-09 19:10:02'),
(68, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-10 23:08:10'),
(69, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-10 23:08:10'),
(70, '187.34.97.211', 'login', 'Acessou a Página', '2019-02-11 21:47:21'),
(71, '187.34.97.211', 'login', 'Acessou a Página', '2019-02-12 01:21:28'),
(72, '138.99.201.106', 'login', 'Acessou a Página', '2019-02-12 11:43:04'),
(73, '138.99.201.106', 'login', 'Acessou a Página', '2019-02-12 11:44:11'),
(74, '177.35.181.72', 'login', 'Acessou a Página', '2019-02-14 18:12:06'),
(75, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-21 00:30:50'),
(76, '152.250.123.153', 'login', 'Acessou a Página', '2019-02-21 00:30:58'),
(77, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-25 05:39:45'),
(78, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-25 05:40:47'),
(79, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-25 05:43:09'),
(80, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-25 05:43:14'),
(81, '179.152.202.242', 'login', 'Acessou a Página', '2019-02-25 05:56:39');

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_menu`
--

CREATE TABLE IF NOT EXISTS `admin_menu` (
  `id` int(10) unsigned NOT NULL,
  `route_id` int(10) unsigned NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `visible` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `route_id`, `icon`, `name`, `parent`, `visible`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 15, 'group', 'Associados', 0, 1, 0, 1, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(2, 16, '', 'Configurações', 1, 1, 0, 2, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(3, 19, '', 'Tipos de Usuário', 1, 1, 0, 3, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(4, 21, '', 'Novo Tipo de Usuário', 1, 0, 0, 4, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(5, 22, '', 'Salvar Novo Tipo de Usuário', 1, 0, 0, 5, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(6, 23, '', 'Editar Tipo de Usuário', 1, 0, 0, 6, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(7, 24, '', 'Atualizar Tipo de Usuário', 1, 0, 0, 7, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(8, 25, '', 'Excluir Tipo de Usuário', 1, 0, 0, 8, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(9, 26, '', 'Status do Tipo de Usuário', 1, 0, 0, 9, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(10, 28, '', 'Usuários', 1, 1, 1, 10, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(11, 30, '', 'Novo Usuário', 1, 0, 1, 11, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(12, 31, '', 'Salvar Novo Usuário', 1, 0, 1, 12, '2018-10-30 07:35:36', '2019-02-25 06:18:27'),
(13, 32, '', 'Editar Usuário', 1, 0, 1, 13, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(14, 33, '', 'Atualizar Usuário', 1, 0, 1, 14, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(15, 34, '', 'Excluir Usuário', 1, 0, 1, 15, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(16, 35, '', 'Status do Usuário', 1, 0, 1, 16, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(17, 36, 'newspaper-o', 'Noticias', 0, 1, 1, 55, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(18, 37, NULL, 'Listar Noticias', 17, 1, 1, 56, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(19, 39, NULL, 'Cadastrar Nova Noticia', 17, 1, 1, 58, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(20, 40, NULL, 'Salvar Nova Noticia', 17, 0, 1, 59, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(21, 41, NULL, 'Editar Noticia', 17, 0, 1, 60, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(22, 42, NULL, 'Atualizar Noticia', 17, 0, 1, 61, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(23, 38, NULL, 'Visualizar Noticia', 17, 0, 1, 57, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(24, 43, NULL, 'Excluir Noticia', 17, 0, 1, 62, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(25, 44, NULL, 'Status da Noticia', 17, 0, 1, 63, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(26, 1, 'user', 'Páginas', 0, 1, 0, 92, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(27, 2, '', 'Todas as Páginas', 26, 1, 0, 93, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(28, 4, '', 'Nova Página', 26, 1, 0, 94, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(29, 5, '', 'Salvar Nova Página', 26, 0, 0, 95, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(30, 6, '', 'Editar Página', 26, 0, 0, 96, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(31, 7, '', 'Atualizar Página', 26, 0, 0, 97, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(32, 8, '', 'Excluir Página', 26, 0, 0, 98, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(33, 9, '', 'Status da Página', 26, 0, 0, 99, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(34, 10, 'paint-brush', 'Aparência', 0, 1, 0, 100, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(35, 11, '', 'Temas', 34, 1, 0, 101, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(36, 12, '', 'Personalizar', 34, 1, 0, 102, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(37, 14, '', 'Editor', 34, 1, 0, 103, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(38, 45, 'user', 'Usuarios', 0, 1, 1, 104, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(39, 46, '', 'Tipos de Usuário', 38, 1, 1, 105, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(40, 48, '', 'Novo Tipo de Usuário', 38, 0, 1, 106, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(41, 49, '', 'Salvar Novo Tipo de Usuário', 38, 0, 1, 107, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(42, 50, '', 'Editar Tipo de Usuário', 38, 0, 1, 108, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(43, 51, '', 'Atualizar Tipo de Usuário', 38, 0, 1, 109, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(44, 52, '', 'Excluir Tipo de Usuário', 38, 0, 1, 110, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(45, 53, '', 'Status do Tipo de Usuário', 38, 0, 1, 111, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(46, 54, '', 'Usuários', 38, 1, 1, 112, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(47, 56, '', 'Novo Usuário', 38, 0, 1, 113, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(48, 57, '', 'Salvar Novo Usuário', 38, 0, 1, 114, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(49, 58, '', 'Editar Usuário', 38, 0, 1, 115, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(50, 59, '', 'Atualizar Usuário', 38, 0, 1, 116, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(51, 60, '', 'Excluir Usuário', 38, 0, 1, 117, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(52, 61, '', 'Status do Usuário', 38, 0, 1, 118, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(53, 62, 'cog', 'Administrador', 0, 1, 1, 128, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(54, 63, '', 'Geral', 53, 1, 1, 129, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(55, 66, 'image', 'Gerenciar Rotas', 53, 1, 1, 130, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(56, 67, '', 'Nova Rota', 53, 0, 0, 131, '2018-10-30 07:35:36', '2019-02-25 06:18:28'),
(57, 68, '', 'Salvar Nova Rota', 53, 0, 0, 132, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(58, 69, '', 'Editar Rota', 53, 0, 0, 133, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(59, 70, '', 'Atualizar Rota', 53, 0, 0, 134, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(60, 71, '', 'Excluir Rota', 53, 0, 0, 135, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(61, 72, '', 'Gerenciar Menu', 53, 1, 1, 136, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(62, 73, '', 'Novo Menu', 53, 0, 1, 137, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(63, 74, '', 'Salvar Novo Menu', 53, 0, 1, 138, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(64, 75, '', 'Editar Menu', 53, 0, 1, 139, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(65, 76, '', 'Atualizar Menu', 53, 0, 1, 140, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(66, 77, '', 'Excluir Menu', 53, 0, 1, 141, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(67, 78, '', 'Status do Menu', 53, 0, 1, 142, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(68, 79, '', 'Ordem do Menu', 53, 0, 1, 143, '2018-10-30 07:35:36', '2019-02-25 06:18:29'),
(69, 84, 'picture-o', 'Banners', 0, 1, 1, 47, '2018-12-05 06:40:40', '2019-02-25 06:18:28'),
(70, 84, NULL, 'Listar Banners', 69, 1, 1, 48, '2018-12-05 06:44:43', '2019-02-25 06:18:28'),
(71, 86, NULL, 'Cadastrar Novo Banner', 69, 1, 1, 49, '2018-12-05 06:45:30', '2019-02-25 06:18:28'),
(72, 87, NULL, 'Salvar Novo Banner', 69, 0, 1, 50, '2018-12-05 06:48:08', '2019-02-25 06:18:28'),
(73, 88, NULL, 'Editar Banner', 69, 0, 1, 51, '2018-12-05 06:48:59', '2019-02-25 06:18:28'),
(74, 89, NULL, 'Atualizar Banner', 69, 0, 1, 52, '2018-12-05 06:49:31', '2019-02-25 06:18:28'),
(75, 90, NULL, 'Excluir Banner', 69, 0, 1, 53, '2018-12-05 06:49:53', '2019-02-25 06:18:28'),
(76, 91, NULL, 'Status do Banner', 69, 0, 1, 54, '2018-12-05 06:50:08', '2019-02-25 06:18:28'),
(77, 92, 'star-o', 'Expediente', 0, 1, 1, 72, '2019-01-30 22:48:40', '2019-02-25 06:18:28'),
(78, 93, NULL, 'Listar Atrações', 77, 1, 1, 73, '2019-01-30 22:49:25', '2019-02-25 06:18:28'),
(79, 95, NULL, 'Cadastrar Nova Atração', 77, 1, 1, 74, '2019-01-30 22:50:20', '2019-02-25 06:18:28'),
(80, 109, 'book', 'Grades', 0, 1, 1, 79, '2019-01-30 23:00:49', '2019-02-25 06:18:28'),
(81, 108, NULL, 'Listar Categorias', 80, 1, 1, 80, '2019-01-30 23:01:48', '2019-02-25 06:18:28'),
(82, 107, NULL, 'Visualizar Grades da Categoria', 80, 0, 1, 81, '2019-01-30 23:02:57', '2019-02-25 06:18:28'),
(88, 96, NULL, 'Salvar Novo Horario', 77, 0, 1, 75, '2019-01-30 23:29:07', '2019-02-25 06:18:28'),
(89, 97, NULL, 'Editar Horario', 77, 0, 1, 76, '2019-01-30 23:31:11', '2019-02-25 06:18:28'),
(90, 98, NULL, 'Atualizar Horario', 77, 0, 1, 77, '2019-01-30 23:34:25', '2019-02-25 06:18:28'),
(91, 99, NULL, 'Excluir Horario', 77, 0, 1, 78, '2019-01-30 23:35:16', '2019-02-25 06:18:28'),
(92, 119, 'camera-retro', 'Galerias', 0, 1, 1, 83, '2019-01-30 23:41:04', '2019-02-25 06:18:28'),
(93, 118, NULL, 'Ajax Galerias', 92, 0, 1, 84, '2019-01-30 23:41:52', '2019-02-25 06:18:28'),
(94, 117, NULL, 'Listar Galerias', 92, 1, 1, 85, '2019-01-30 23:42:19', '2019-02-25 06:18:28'),
(95, 116, NULL, 'Visualizar Galeria', 92, 0, 1, 86, '2019-01-30 23:42:54', '2019-02-25 06:18:28'),
(96, 115, NULL, 'Nova Galeria', 92, 1, 1, 87, '2019-01-30 23:43:44', '2019-02-25 06:18:28'),
(97, 114, NULL, 'Salvar Nova Galeria', 92, 0, 1, 88, '2019-01-30 23:44:06', '2019-02-25 06:18:28'),
(98, 113, NULL, 'Editar Galeria', 92, 0, 1, 89, '2019-01-30 23:44:52', '2019-02-25 06:18:28'),
(99, 112, NULL, 'Atualizar Galeria', 92, 0, 1, 90, '2019-01-30 23:45:23', '2019-02-25 06:18:28'),
(100, 111, NULL, 'Excluir Galeria', 92, 0, 1, 91, '2019-01-30 23:45:48', '2019-02-25 06:18:28'),
(101, 120, 'calendar', 'Eventos', 0, 1, 1, 64, '2019-02-04 07:35:20', '2019-02-25 06:18:28'),
(102, 121, NULL, 'Listar Eventos', 101, 1, 1, 65, '2019-02-04 07:36:02', '2019-02-25 06:18:28'),
(103, 122, NULL, 'Visualizar Evento', 101, 0, 1, 66, '2019-02-04 07:36:31', '2019-02-25 06:18:28'),
(104, 123, NULL, 'Cadastrar Novo Evento', 101, 1, 1, 67, '2019-02-04 07:37:14', '2019-02-25 06:18:28'),
(105, 124, NULL, 'Salvar Novo Evento', 101, 0, 1, 68, '2019-02-04 07:37:43', '2019-02-25 06:18:28'),
(106, 125, NULL, 'Editar Evento', 101, 0, 1, 69, '2019-02-04 07:38:10', '2019-02-25 06:18:28'),
(107, 126, NULL, 'Atualizar Evento', 101, 0, 1, 70, '2019-02-04 07:38:37', '2019-02-25 06:18:28'),
(108, 127, NULL, 'Excluir Evento', 101, 0, 1, 71, '2019-02-04 07:38:59', '2019-02-25 06:18:28'),
(109, 129, 'mobile', 'Versões', 0, 1, 1, 119, '2019-02-11 19:08:41', '2019-02-25 06:18:28'),
(110, 130, NULL, 'Listar Versões', 109, 1, 1, 120, '2019-02-11 19:09:09', '2019-02-25 06:18:28'),
(111, 131, NULL, 'Visualizar Versão', 109, 0, 1, 121, '2019-02-11 19:09:44', '2019-02-25 06:18:28'),
(112, 132, NULL, 'Cadastrar Nova Versão', 109, 1, 1, 122, '2019-02-11 19:10:07', '2019-02-25 06:18:28'),
(113, 133, NULL, 'Salvar Nova Versão', 109, 0, 1, 123, '2019-02-11 19:10:30', '2019-02-25 06:18:28'),
(114, 134, NULL, 'Editar Versão', 109, 0, 1, 124, '2019-02-11 19:10:51', '2019-02-25 06:18:28'),
(115, 135, NULL, 'Atualizar Versão', 109, 0, 1, 125, '2019-02-11 19:11:10', '2019-02-25 06:18:28'),
(116, 136, NULL, 'Excluir Versão', 109, 0, 1, 126, '2019-02-11 19:11:33', '2019-02-25 06:18:28'),
(117, 137, NULL, 'Status da Versão', 109, 0, 1, 127, '2019-02-11 19:11:49', '2019-02-25 06:18:28'),
(118, 138, NULL, 'Ajax Grades', 80, 0, 1, 82, '2019-02-11 19:59:26', '2019-02-25 06:18:28'),
(119, 143, 'file-text-o', 'App', 0, 1, 1, 17, '2019-02-20 06:30:40', '2019-02-25 06:18:28'),
(120, 142, NULL, 'Listar Páginas', 119, 1, 1, 18, '2019-02-20 06:31:08', '2019-02-25 06:18:28'),
(121, 141, NULL, 'Visualizar Página', 119, 0, 1, 19, '2019-02-20 06:31:29', '2019-02-25 06:18:28'),
(122, 140, NULL, 'Editar Página', 119, 0, 1, 20, '2019-02-20 06:31:55', '2019-02-25 06:18:28'),
(123, 139, NULL, 'Atualizar Página', 119, 0, 1, 21, '2019-02-20 06:32:18', '2019-02-25 06:18:28'),
(124, 151, NULL, 'Listar Notificações', 119, 1, 1, 22, '2019-02-20 06:52:57', '2019-02-25 06:18:28'),
(125, 150, NULL, 'Visualizar Notificação', 119, 0, 1, 24, '2019-02-20 06:53:26', '2019-02-25 06:18:28'),
(126, 149, NULL, 'Nova Notificação', 119, 1, 1, 25, '2019-02-20 06:53:59', '2019-02-25 06:18:28'),
(127, 148, NULL, 'Salvar Nova Notificação', 119, 0, 1, 26, '2019-02-20 06:54:23', '2019-02-25 06:18:28'),
(128, 147, NULL, 'Editar Notificação', 119, 0, 1, 27, '2019-02-20 06:54:57', '2019-02-25 06:18:28'),
(129, 146, NULL, 'Atualizar Notificação', 119, 0, 1, 28, '2019-02-20 06:55:24', '2019-02-25 06:18:28'),
(130, 145, NULL, 'Excluir Notificação', 119, 0, 1, 29, '2019-02-20 06:55:47', '2019-02-25 06:18:28'),
(131, 144, NULL, 'Ajax Notificações', 119, 0, 1, 23, '2019-02-20 06:56:12', '2019-02-25 06:18:28'),
(132, 159, 'envelope', 'Contato', 0, 1, 1, 30, '2019-02-21 03:06:02', '2019-02-25 06:18:28'),
(133, 158, NULL, 'Listar Destinatarios', 132, 1, 1, 37, '2019-02-21 03:06:29', '2019-02-25 06:18:28'),
(134, 157, NULL, 'Novo Destinatário', 132, 1, 1, 38, '2019-02-21 03:06:51', '2019-02-25 06:18:28'),
(135, 156, NULL, 'Salvar Novo Destinatário', 132, 0, 1, 39, '2019-02-21 03:07:14', '2019-02-25 06:18:28'),
(136, 155, NULL, 'Editar Destinatário', 132, 0, 1, 40, '2019-02-21 03:07:34', '2019-02-25 06:18:28'),
(137, 154, NULL, 'Atualizar Destinatário', 132, 0, 1, 41, '2019-02-21 03:08:00', '2019-02-25 06:18:28'),
(138, 153, NULL, 'Excluir Destinatário', 132, 0, 1, 42, '2019-02-21 03:08:20', '2019-02-25 06:18:28'),
(139, 152, NULL, 'Status do Destinatário', 132, 0, 1, 43, '2019-02-21 03:08:41', '2019-02-25 06:18:28'),
(140, 162, NULL, 'Listar Mensagens', 132, 1, 1, 44, '2019-02-21 03:35:50', '2019-02-25 06:18:28'),
(141, 161, NULL, 'Visualizar Mensagem', 132, 0, 1, 45, '2019-02-21 03:36:14', '2019-02-25 06:18:28'),
(142, 160, NULL, 'Status da Mensagem', 132, 0, 1, 46, '2019-02-21 03:36:38', '2019-02-25 06:18:28'),
(143, 168, NULL, 'Listar Assuntos', 132, 1, 1, 31, '2019-02-25 06:14:50', '2019-02-25 06:18:28'),
(144, 167, NULL, 'Novo Assunto', 132, 1, 1, 32, '2019-02-25 06:15:19', '2019-02-25 06:18:28'),
(145, 166, NULL, 'Salvar Novo Assunto', 132, 0, 1, 33, '2019-02-25 06:15:38', '2019-02-25 06:18:28'),
(146, 165, NULL, 'Editar Assunto', 132, 0, 1, 34, '2019-02-25 06:15:57', '2019-02-25 06:18:28'),
(147, 164, NULL, 'Atualizar Assunto', 132, 0, 1, 35, '2019-02-25 06:16:20', '2019-02-25 06:18:28'),
(148, 163, NULL, 'Excluir Assunto', 132, 0, 1, 36, '2019-02-25 06:16:38', '2019-02-25 06:18:28');

-- --------------------------------------------------------

--
-- Estrutura para tabela `admin_routes`
--

CREATE TABLE IF NOT EXISTS `admin_routes` (
  `id` int(10) unsigned NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `function` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `admin_routes`
--

INSERT INTO `admin_routes` (`id`, `method`, `url`, `controller`, `function`, `name`, `created_at`, `updated_at`) VALUES
(1, '["get"]', '/paginas/', 'AdminController', 'pages', 'admin-site-pages', '2018-10-30 07:27:00', NULL),
(2, '["get"]', '/paginas/index/', 'SitePagesController', 'index', 'admin-site-pages-index', '2018-10-30 07:27:00', NULL),
(3, '["get"]', '/paginas/view/', 'SitePagesController', 'view', 'admin-site-pages-view', '2018-10-30 07:27:00', NULL),
(4, '["get"]', '/paginas/create/', 'SitePagesController', 'create', 'admin-site-pages-create', '2018-10-30 07:27:00', NULL),
(5, '["post"]', '/paginas/store/', 'SitePagesController', 'store', 'admin-site-pages-store', '2018-10-30 07:27:00', NULL),
(6, '["get"]', '/paginas/edit/{id}/', 'SitePagesController', 'edit', 'admin-site-pages-edit', '2018-10-30 07:27:00', NULL),
(7, '["post"]', '/paginas/update/', 'SitePagesController', 'update', 'admin-site-pages-update', '2018-10-30 07:27:00', NULL),
(8, '["post"]', '/paginas/destroy/', 'SitePagesController', 'destroy', 'admin-site-pages-destroy', '2018-10-30 07:27:00', NULL),
(9, '["get"]', '/paginas/status/{id}/', 'SitePagesController', 'status', 'admin-site-pages-status', '2018-10-30 07:27:00', NULL),
(10, '["get"]', '/aparencia/', 'AdminController', 'front', 'admin-site-front', '2018-10-30 07:27:00', NULL),
(11, '["get", "post"]', '/aparencia/temas/', 'SiteFrontController', 'themes', 'admin-site-front-themes', '2018-10-30 07:27:00', NULL),
(12, '["get", "post"]', '/aparencia/custom/', 'SiteFrontController', 'custom', 'admin-site-front-custom', '2018-10-30 07:27:00', NULL),
(13, '["get", "post"]', '/aparencia/menus/', 'SiteFrontController', 'menus', 'admin-site-front-menus', '2018-10-30 07:27:00', NULL),
(14, '["get", "post"]', '/aparencia/editor/', 'SiteFrontController', 'editor', 'admin-site-front-editor', '2018-10-30 07:27:00', NULL),
(15, '["get"]', '/site/', 'AdminController', 'site', 'admin-site', '2018-10-30 07:27:00', NULL),
(16, '["get"]', '/site/configs/', 'AdminSiteController', 'configs', 'admin-site-configs', '2018-10-30 07:27:00', NULL),
(17, '["post"]', '/site/configs/langs/', 'AdminSiteController', 'langs', 'admin-site-configs-langs', '2018-10-30 07:27:00', NULL),
(18, '["post"]', '/site/configs/langs/words/', 'AdminSiteController', 'langs_words', 'admin-site-configs-langs-words', '2018-10-30 07:27:00', NULL),
(19, '["get"]', '/site/tipos-de-usuarios/', 'AdminSiteUsersTypesController', 'index', 'admin-site-users-types', '2018-10-30 07:27:00', NULL),
(20, '["get"]', '/site/tipos-de-usuarios/view/', 'AdminSiteUsersTypesController', 'view', 'admin-site-users-types-view', '2018-10-30 07:27:00', NULL),
(21, '["get"]', '/site/tipos-de-usuarios/create/', 'AdminSiteUsersTypesController', 'create', 'admin-site-users-types-create', '2018-10-30 07:27:00', NULL),
(22, '["post"]', '/site/tipos-de-usuarios/store/', 'AdminSiteUsersTypesController', 'store', 'admin-site-users-types-store', '2018-10-30 07:27:00', NULL),
(23, '["get"]', '/site/tipos-de-usuarios/edit/{id}/', 'AdminSiteUsersTypesController', 'edit', 'admin-site-users-types-edit', '2018-10-30 07:27:00', NULL),
(24, '["post"]', '/site/tipos-de-usuarios/update/', 'AdminSiteUsersTypesController', 'update', 'admin-site-users-types-update', '2018-10-30 07:27:00', NULL),
(25, '["post"]', '/site/tipos-de-usuarios/destroy/', 'AdminSiteUsersTypesController', 'destroy', 'admin-site-users-types-destroy', '2018-10-30 07:27:00', NULL),
(26, '["get"]', '/site/tipos-de-usuarios/status/{id}/', 'AdminSiteUsersTypesController', 'status', 'admin-site-users-types-status', '2018-10-30 07:27:00', NULL),
(27, '["post"]', '/site/tipos-de-usuarios/custom-fields/', 'AdminSiteUsersTypesController', 'custom_fields', 'admin-site-users-types-custom-fields', '2018-10-30 07:27:00', NULL),
(28, '["get"]', '/site/usuarios/', 'AdminSiteUsersController', 'index', 'admin-site-users-index', '2018-10-30 07:27:00', NULL),
(29, '["get"]', '/site/usuarios/view/', 'AdminSiteUsersController', 'view', 'admin-site-users-view', '2018-10-30 07:27:00', NULL),
(30, '["get"]', '/site/usuarios/create/', 'AdminSiteUsersController', 'create', 'admin-site-users-create', '2018-10-30 07:27:00', NULL),
(31, '["post"]', '/site/usuarios/store/', 'AdminSiteUsersController', 'store', 'admin-site-users-store', '2018-10-30 07:27:00', NULL),
(32, '["get"]', '/site/usuarios/edit/{id}/', 'AdminSiteUsersController', 'edit', 'admin-site-users-edit', '2018-10-30 07:27:00', NULL),
(33, '["post"]', '/site/usuarios/update/', 'AdminSiteUsersController', 'update', 'admin-site-users-update', '2018-10-30 07:27:00', NULL),
(34, '["post"]', '/site/usuarios/destroy/', 'AdminSiteUsersController', 'destroy', 'admin-site-users-destroy', '2018-10-30 07:27:00', NULL),
(35, '["get"]', '/site/usuarios/status/{id}/', 'AdminSiteUsersController', 'status', 'admin-site-users-status', '2018-10-30 07:27:00', NULL),
(36, '["get"]', '/site/noticias/', 'AdminController', 'noticias', 'admin-site-noticias', '2018-10-30 07:27:00', NULL),
(37, '["get"]', '/site/noticias/index/', 'AdminSiteNoticiasController', 'index', 'admin-site-noticias-index', '2018-10-30 07:27:00', NULL),
(38, '["get"]', '/site/noticias/view/{id}/', 'AdminSiteNoticiasController', 'view', 'admin-site-noticias-view', '2018-10-30 07:27:00', NULL),
(39, '["get"]', '/site/noticias/create/', 'AdminSiteNoticiasController', 'create', 'admin-site-noticias-create', '2018-10-30 07:27:00', NULL),
(40, '["post"]', '/site/noticias/store/', 'AdminSiteNoticiasController', 'store', 'admin-site-noticias-store', '2018-10-30 07:27:00', NULL),
(41, '["get"]', '/site/noticias/edit/{id}/', 'AdminSiteNoticiasController', 'edit', 'admin-site-noticias-edit', '2018-10-30 07:27:00', NULL),
(42, '["post"]', '/site/noticias/update/', 'AdminSiteNoticiasController', 'update', 'admin-site-noticias-update', '2018-10-30 07:27:00', NULL),
(43, '["post"]', '/site/noticias/destroy/', 'AdminSiteNoticiasController', 'destroy', 'admin-site-noticias-destroy', '2018-10-30 07:27:00', NULL),
(44, '["get"]', '/site/noticias/status/{id}/', 'AdminSiteNoticiasController', 'status', 'admin-site-noticias-status', '2018-10-30 07:27:00', NULL),
(45, '["get"]', '/usuarios/', 'AdminController', 'users', 'admin-users', '2018-10-30 07:27:00', NULL),
(46, '["get"]', '/tipos-de-usuarios/index/', 'UsersTypesController', 'index', 'admin-users-types-index', '2018-10-30 07:27:00', NULL),
(47, '["get"]', '/tipos-de-usuarios/view/', 'UsersTypesController', 'view', 'admin-users-types-view', '2018-10-30 07:27:00', NULL),
(48, '["get"]', '/tipos-de-usuarios/create/', 'UsersTypesController', 'create', 'admin-users-types-create', '2018-10-30 07:27:00', NULL),
(49, '["post"]', '/tipos-de-usuarios/store/', 'UsersTypesController', 'store', 'admin-users-types-store', '2018-10-30 07:27:00', NULL),
(50, '["get"]', '/tipos-de-usuarios/edit/{id}/', 'UsersTypesController', 'edit', 'admin-users-types-edit', '2018-10-30 07:27:00', NULL),
(51, '["post"]', '/tipos-de-usuarios/update/', 'UsersTypesController', 'update', 'admin-users-types-update', '2018-10-30 07:27:00', NULL),
(52, '["post"]', '/tipos-de-usuarios/destroy/', 'UsersTypesController', 'destroy', 'admin-users-types-destroy', '2018-10-30 07:27:00', NULL),
(53, '["get"]', '/tipos-de-usuarios/status/{id}/', 'UsersTypesController', 'status', 'admin-users-types-status', '2018-10-30 07:27:00', NULL),
(54, '["get"]', '/usuarios/index/', 'UsersController', 'index', 'admin-users-index', '2018-10-30 07:27:00', NULL),
(55, '["get"]', '/usuarios/view/', 'UsersController', 'view', 'admin-users-view', '2018-10-30 07:27:00', NULL),
(56, '["get"]', '/usuarios/create/', 'UsersController', 'create', 'admin-users-create', '2018-10-30 07:27:00', NULL),
(57, '["post"]', '/usuarios/store/', 'UsersController', 'store', 'admin-users-store', '2018-10-30 07:27:00', NULL),
(58, '["get"]', '/usuarios/edit/{id}/', 'UsersController', 'edit', 'admin-users-edit', '2018-10-30 07:27:00', NULL),
(59, '["post"]', '/usuarios/update/', 'UsersController', 'update', 'admin-users-update', '2018-10-30 07:27:00', NULL),
(60, '["post"]', '/usuarios/destroy/', 'UsersController', 'destroy', 'admin-users-destroy', '2018-10-30 07:27:00', NULL),
(61, '["get"]', '/usuarios/status/{id}/', 'UsersController', 'status', 'admin-users-status', '2018-10-30 07:27:00', NULL),
(62, '["get"]', '/config/', 'AdminController', 'config', 'admin-config', '2018-10-30 07:27:00', NULL),
(63, '["get", "post"]', '/config/index/', 'AdminConfigController', 'index', 'admin-config-index', '2018-10-30 07:27:00', NULL),
(64, '["get", "post"]', '/config/langs/index/', 'AdminLangsController', 'index', 'admin-config-langs-index', '2018-10-30 07:27:00', NULL),
(65, '["get", "post"]', '/config/langs/words/', 'AdminLangsController', 'words', 'admin-config-langs-words', '2018-10-30 07:27:00', NULL),
(66, '["get"]', '/config/routes/index/', 'AdminRoutesController', 'index', 'admin-config-routes-index', '2018-10-30 07:27:00', NULL),
(67, '["get"]', '/config/routes/create/', 'AdminRoutesController', 'create', 'admin-config-routes-create', '2018-10-30 07:27:00', NULL),
(68, '["post"]', '/config/routes/store/', 'AdminRoutesController', 'store', 'admin-config-routes-store', '2018-10-30 07:27:00', NULL),
(69, '["get"]', '/config/routes/edit/{id}/', 'AdminRoutesController', 'edit', 'admin-config-routes-edit', '2018-10-30 07:27:00', NULL),
(70, '["post"]', '/config/routes/update/', 'AdminRoutesController', 'update', 'admin-config-routes-update', '2018-10-30 07:27:00', NULL),
(71, '["post"]', '/config/routes/destroy/', 'AdminRoutesController', 'destroy', 'admin-config-routes-destroy', '2018-10-30 07:27:00', NULL),
(72, '["get"]', '/config/menu/index/', 'AdminMenuController', 'index', 'admin-config-menu-index', '2018-10-30 07:27:00', NULL),
(73, '["get"]', '/config/menu/create/', 'AdminMenuController', 'create', 'admin-config-menu-create', '2018-10-30 07:27:00', NULL),
(74, '["post"]', '/config/menu/store/', 'AdminMenuController', 'store', 'admin-config-menu-store', '2018-10-30 07:27:00', NULL),
(75, '["post"]', '/config/menu/edit/', 'AdminMenuController', 'edit', 'admin-config-menu-edit', '2018-10-30 07:27:00', NULL),
(76, '["post"]', '/config/menu/update/', 'AdminMenuController', 'update', 'admin-config-menu-update', '2018-10-30 07:27:00', NULL),
(77, '["post"]', '/config/menu/destroy/', 'AdminMenuController', 'destroy', 'admin-config-menu-destroy', '2018-10-30 07:27:00', NULL),
(78, '["get"]', '/config/menu/status/{id}/', 'AdminMenuController', 'status', 'admin-config-menu-status', '2018-10-30 07:27:00', NULL),
(79, '["post"]', '/config/menu/order/', 'AdminMenuController', 'order', 'admin-config-menu-order', '2018-10-30 07:27:00', NULL),
(80, '["get"]', '/config/extensoes/index/', 'AdminConfigController', 'extensoes', 'admin-config-extensoes-index', '2018-10-30 07:27:00', NULL),
(81, '["post"]', '/config/extensoes/upload/', 'AdminConfigController', 'extensoesUpload', 'admin-config-extensoes-upload', '2018-10-30 07:27:00', NULL),
(82, '["get"]', '/config/extensoes/status/{id}/', 'AdminConfigController', 'extensoesStatus', 'admin-config-extensoes-status', '2018-10-30 07:27:00', NULL),
(83, '["get"]', '/config/extensoes/remove/{id}/', 'AdminConfigController', 'extensoesRemove', 'admin-config-extensoes-remove', '2018-10-30 07:27:00', NULL),
(84, '["get"]', '/site/banners/index/', 'AdminSiteBannersController', 'index', 'admin-site-banners-index', '2018-10-30 07:27:00', NULL),
(85, '["get"]', '/site/banners/view/{id}/', 'AdminSiteBannersController', 'view', 'admin-site-banners-view', '2018-10-30 07:27:00', NULL),
(86, '["get"]', '/site/banners/create/', 'AdminSiteBannersController', 'create', 'admin-site-banners-create', '2018-10-30 07:27:00', NULL),
(87, '["post"]', '/site/banners/store/', 'AdminSiteBannersController', 'store', 'admin-site-banners-store', '2018-10-30 07:27:00', NULL),
(88, '["get"]', '/site/banners/edit/{id}/', 'AdminSiteBannersController', 'edit', 'admin-site-banners-edit', '2018-10-30 07:27:00', NULL),
(89, '["post"]', '/site/banners/update/', 'AdminSiteBannersController', 'update', 'admin-site-banners-update', '2018-10-30 07:27:00', NULL),
(90, '["post"]', '/site/banners/destroy/', 'AdminSiteBannersController', 'destroy', 'admin-site-banners-destroy', '2018-10-30 07:27:00', NULL),
(91, '["get"]', '/site/banners/status/{id}/', 'AdminSiteBannersController', 'status', 'admin-site-banners-status', '2018-10-30 07:27:00', NULL),
(92, '["get"]', '/site/horarios/', 'AdminController', 'horarios', 'admin-site-horarios', '2018-10-30 10:27:00', NULL),
(93, '["get"]', '/site/horarios/index/', 'AdminSiteHorariosController', 'index', 'admin-site-horarios-index', '2018-10-30 10:27:00', NULL),
(94, '["get"]', '/site/horarios/view/{id}/', 'AdminSiteHorariosController', 'view', 'admin-site-horarios-view', '2018-10-30 10:27:00', NULL),
(95, '["get"]', '/site/horarios/create/', 'AdminSiteHorariosController', 'create', 'admin-site-horarios-create', '2018-10-30 10:27:00', NULL),
(96, '["post"]', '/site/horarios/store/', 'AdminSiteHorariosController', 'store', 'admin-site-horarios-store', '2018-10-30 10:27:00', NULL),
(97, '["get"]', '/site/horarios/edit/{id}/', 'AdminSiteHorariosController', 'edit', 'admin-site-horarios-edit', '2018-10-30 10:27:00', NULL),
(98, '["post"]', '/site/horarios/update/', 'AdminSiteHorariosController', 'update', 'admin-site-horarios-update', '2018-10-30 10:27:00', NULL),
(99, '["post"]', '/site/horarios/destroy/', 'AdminSiteHorariosController', 'destroy', 'admin-site-horarios-destroy', '2018-10-30 10:27:00', NULL),
(100, '["get"]', '/site/horarios/status/{id}/', 'AdminSiteHorariosController', 'status', 'admin-site-horarios-status', '2018-10-30 10:27:00', NULL),
(107, '["get"]', '/site/grades/view/{id}/', 'AdminSiteGradesController', 'view', 'admin-site-grades-view', '2018-10-30 13:27:00', NULL),
(108, '["get"]', '/site/grades/index/', 'AdminSiteGradesController', 'index', 'admin-site-grades-index', '2018-10-30 13:27:00', NULL),
(109, '["get"]', '/site/grades/', 'AdminController', 'grades', 'admin-site-grades', '2018-10-30 13:27:00', NULL),
(110, '["get"]', '/site/galerias/status/{id}/', 'AdminSiteGaleriasController', 'status', 'admin-site-galerias-status', '2018-10-30 13:27:00', NULL),
(111, '["post"]', '/site/galerias/destroy/', 'AdminSiteGaleriasController', 'destroy', 'admin-site-galerias-destroy', '2018-10-30 13:27:00', NULL),
(112, '["post"]', '/site/galerias/update/', 'AdminSiteGaleriasController', 'update', 'admin-site-galerias-update', '2018-10-30 13:27:00', NULL),
(113, '["get"]', '/site/galerias/edit/{id}/', 'AdminSiteGaleriasController', 'edit', 'admin-site-galerias-edit', '2018-10-30 13:27:00', NULL),
(114, '["post"]', '/site/galerias/store/', 'AdminSiteGaleriasController', 'store', 'admin-site-galerias-store', '2018-10-30 13:27:00', NULL),
(115, '["get"]', '/site/galerias/create/', 'AdminSiteGaleriasController', 'create', 'admin-site-galerias-create', '2018-10-30 13:27:00', NULL),
(116, '["get"]', '/site/galerias/view/{id}/', 'AdminSiteGaleriasController', 'view', 'admin-site-galerias-view', '2018-10-30 13:27:00', NULL),
(117, '["get"]', '/site/galerias/index/', 'AdminSiteGaleriasController', 'index', 'admin-site-galerias-index', '2018-10-30 13:27:00', NULL),
(118, '["post"]', '/site/galerias/ajax/', 'AdminSiteGaleriasController', 'ajax', 'admin-site-galerias-ajax', '2018-10-30 13:27:00', NULL),
(119, '["get"]', '/site/galerias/', 'AdminController', 'galerias', 'admin-site-galerias', '2018-10-30 13:27:00', NULL),
(120, '["get"]', '/site/eventos/', 'AdminController', 'eventos', 'admin-site-eventos', '2018-10-30 10:27:00', NULL),
(121, '["get"]', '/site/eventos/index/', 'AdminSiteEventosController', 'index', 'admin-site-eventos-index', '2018-10-30 10:27:00', NULL),
(122, '["get"]', '/site/eventos/view/{id}/', 'AdminSiteEventosController', 'view', 'admin-site-eventos-view', '2018-10-30 10:27:00', NULL),
(123, '["get"]', '/site/eventos/create/', 'AdminSiteEventosController', 'create', 'admin-site-eventos-create', '2018-10-30 10:27:00', NULL),
(124, '["post"]', '/site/eventos/store/', 'AdminSiteEventosController', 'store', 'admin-site-eventos-store', '2018-10-30 10:27:00', NULL),
(125, '["get"]', '/site/eventos/edit/{id}/', 'AdminSiteEventosController', 'edit', 'admin-site-eventos-edit', '2018-10-30 10:27:00', NULL),
(126, '["post"]', '/site/eventos/update/', 'AdminSiteEventosController', 'update', 'admin-site-eventos-update', '2018-10-30 10:27:00', NULL),
(127, '["post"]', '/site/eventos/destroy/', 'AdminSiteEventosController', 'destroy', 'admin-site-eventos-destroy', '2018-10-30 10:27:00', NULL),
(128, '["get"]', '/site/eventos/status/{id}/', 'AdminSiteEventosController', 'status', 'admin-site-eventos-status', '2018-10-30 10:27:00', NULL),
(129, '["get"]', '/site/versoes/', 'AdminController', 'versoes', 'admin-site-versoes', '2018-10-30 13:27:00', NULL),
(130, '["get"]', '/site/versoes/index/', 'AdminSiteVersoesController', 'index', 'admin-site-versoes-index', '2018-10-30 13:27:00', NULL),
(131, '["get"]', '/site/versoes/view/{id}/', 'AdminSiteVersoesController', 'view', 'admin-site-versoes-view', '2018-10-30 13:27:00', NULL),
(132, '["get"]', '/site/versoes/create/', 'AdminSiteVersoesController', 'create', 'admin-site-versoes-create', '2018-10-30 13:27:00', NULL),
(133, '["post"]', '/site/versoes/store/', 'AdminSiteVersoesController', 'store', 'admin-site-versoes-store', '2018-10-30 13:27:00', NULL),
(134, '["get"]', '/site/versoes/edit/{id}/', 'AdminSiteVersoesController', 'edit', 'admin-site-versoes-edit', '2018-10-30 13:27:00', NULL),
(135, '["post"]', '/site/versoes/update/', 'AdminSiteVersoesController', 'update', 'admin-site-versoes-update', '2018-10-30 13:27:00', NULL),
(136, '["post"]', '/site/versoes/destroy/', 'AdminSiteVersoesController', 'destroy', 'admin-site-versoes-destroy', '2018-10-30 13:27:00', NULL),
(137, '["get"]', '/site/versoes/status/{id}/', 'AdminSiteVersoesController', 'status', 'admin-site-versoes-status', '2018-10-30 13:27:00', NULL),
(138, '["post"]', '/site/grades/ajax/', 'AdminSiteGradesController', 'ajax', 'admin-site-grades-ajax', '2018-10-30 13:27:00', NULL),
(139, '["post"]', '/app/paginas/update/', 'AdminAppPaginasController', 'update', 'admin-app-paginas-update', '2018-10-30 16:27:00', NULL),
(140, '["get"]', '/app/paginas/edit/{id}/', 'AdminAppPaginasController', 'edit', 'admin-app-paginas-edit', '2018-10-30 16:27:00', NULL),
(141, '["get"]', '/app/paginas/view/{id}/', 'AdminAppPaginasController', 'view', 'admin-app-paginas-view', '2018-10-30 16:27:00', NULL),
(142, '["get"]', '/app/paginas/index/', 'AdminAppPaginasController', 'index', 'admin-app-paginas-index', '2018-10-30 16:27:00', NULL),
(143, '["get"]', '/app/paginas/', 'AdminController', 'AppPaginas', 'admin-app-paginas', '2018-10-30 16:27:00', NULL),
(144, '["post"]', '/app/notificacoes/ajax/', 'AdminAppNotificacoesController', 'ajax', 'admin-app-notificacoes-ajax', '2018-10-30 16:27:00', NULL),
(145, '["post"]', '/app/notificacoes/destroy/', 'AdminAppNotificacoesController', 'destroy', 'admin-app-notificacoes-destroy', '2018-10-30 16:27:00', NULL),
(146, '["post"]', '/app/notificacoes/update/', 'AdminAppNotificacoesController', 'update', 'admin-app-notificacoes-update', '2018-10-30 16:27:00', NULL),
(147, '["get"]', '/app/notificacoes/edit/{id}/', 'AdminAppNotificacoesController', 'edit', 'admin-app-notificacoes-edit', '2018-10-30 16:27:00', NULL),
(148, '["post"]', '/app/notificacoes/store/', 'AdminAppNotificacoesController', 'store', 'admin-app-notificacoes-store', '2018-10-30 16:27:00', NULL),
(149, '["get"]', '/app/notificacoes/create/', 'AdminAppNotificacoesController', 'create', 'admin-app-notificacoes-create', '2018-10-30 16:27:00', NULL),
(150, '["get"]', '/app/notificacoes/view/{id}/', 'AdminAppNotificacoesController', 'view', 'admin-app-notificacoes-view', '2018-10-30 16:27:00', NULL),
(151, '["get"]', '/app/notificacoes/index/', 'AdminAppNotificacoesController', 'index', 'admin-app-notificacoes-index', '2018-10-30 16:27:00', NULL),
(152, '["get"]', '/app/contato/emails/status/{id}/', 'AdminAppEmailsController', 'status', 'admin-app-emails-status', '2018-10-30 10:27:00', NULL),
(153, '["post"]', '/app/contato/emails/destroy/', 'AdminAppEmailsController', 'destroy', 'admin-app-emails-destroy', '2018-10-30 10:27:00', NULL),
(154, '["post"]', '/app/contato/emails/update/', 'AdminAppEmailsController', 'update', 'admin-app-emails-update', '2018-10-30 10:27:00', NULL),
(155, '["get"]', '/app/contato/emails/edit/{id}/', 'AdminAppEmailsController', 'edit', 'admin-app-emails-edit', '2018-10-30 10:27:00', NULL),
(156, '["post"]', '/app/contato/emails/store/', 'AdminAppEmailsController', 'store', 'admin-app-emails-store', '2018-10-30 10:27:00', NULL),
(157, '["get"]', '/app/contato/emails/create/', 'AdminAppEmailsController', 'create', 'admin-app-emails-create', '2018-10-30 10:27:00', NULL),
(158, '["get"]', '/app/contato/emails/index/', 'AdminAppEmailsController', 'index', 'admin-app-emails-index', '2018-10-30 10:27:00', NULL),
(159, '["get"]', '/app/contato/', 'AdminController', 'AppContato', 'admin-app-emails', '2018-10-30 19:27:00', NULL),
(160, '["post"]', '/app/contato/mensagens/status/', 'AdminAppContatoController', 'status', 'admin-app-contato-status', '2018-10-30 10:27:00', NULL),
(161, '["get"]', '/app/contato/mensagens/view/{id}/', 'AdminAppContatoController', 'view', 'admin-app-contato-view', '2018-10-30 10:27:00', NULL),
(162, '["get"]', '/app/contato/mensagens/index/', 'AdminAppContatoController', 'index', 'admin-app-contato-index', '2018-10-30 10:27:00', NULL),
(163, '["post"]', '/app/contato/assuntos/destroy/', 'AdminAppAssuntosController', 'destroy', 'admin-app-assuntos-destroy', '2018-10-30 10:27:00', NULL),
(164, '["post"]', '/app/contato/assuntos/update/', 'AdminAppAssuntosController', 'update', 'admin-app-assuntos-update', '2018-10-30 10:27:00', NULL),
(165, '["get"]', '/app/contato/assuntos/edit/{id}/', 'AdminAppAssuntosController', 'edit', 'admin-app-assuntos-edit', '2018-10-30 10:27:00', NULL),
(166, '["post"]', '/app/contato/assuntos/store/', 'AdminAppAssuntosController', 'store', 'admin-app-assuntos-store', '2018-10-30 10:27:00', NULL),
(167, '["get"]', '/app/contato/assuntos/create/', 'AdminAppAssuntosController', 'create', 'admin-app-assuntos-create', '2018-10-30 10:27:00', NULL),
(168, '["get"]', '/app/contato/assuntos/index/', 'AdminAppAssuntosController', 'index', 'admin-app-assuntos-index', '2018-10-30 10:27:00', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `app_assuntos_contato`
--

CREATE TABLE IF NOT EXISTS `app_assuntos_contato` (
  `id` int(10) NOT NULL,
  `nome` varchar(191) NOT NULL,
  `descricao` longtext,
  `status` int(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `app_assuntos_contato`
--

INSERT INTO `app_assuntos_contato` (`id`, `nome`, `descricao`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 'Como se tornar sócio', NULL, 1, 1, '2019-02-08 15:26:36', NULL),
(2, 'Academia', NULL, 1, 2, '2019-02-08 15:26:47', NULL),
(3, 'Sobre o clube', NULL, 1, 3, '2019-02-08 15:26:58', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `app_contatos`
--

CREATE TABLE IF NOT EXISTS `app_contatos` (
  `id` int(10) NOT NULL,
  `nome` varchar(191) NOT NULL,
  `telefone` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `horario` varchar(191) NOT NULL,
  `assunto_id` int(10) NOT NULL,
  `mensagem` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `app_emails`
--

CREATE TABLE IF NOT EXISTS `app_emails` (
  `id` int(10) NOT NULL,
  `email` varchar(191) NOT NULL,
  `nome` varchar(191) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `app_logs`
--

CREATE TABLE IF NOT EXISTS `app_logs` (
  `id` int(10) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `user` int(10) NOT NULL,
  `log` longtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `app_logs`
--

INSERT INTO `app_logs` (`id`, `ip`, `user`, `log`, `created_at`) VALUES
(1, '179.154.193.11', 1, 'Realizou login no app.', '2018-11-17 11:48:06'),
(2, '187.35.60.75', 1, 'Realizou login no app.', '2018-11-17 14:38:12'),
(3, '187.35.60.75', 1, 'Realizou login no app.', '2018-11-17 14:42:24');

-- --------------------------------------------------------

--
-- Estrutura para tabela `app_notificacao_envios`
--

CREATE TABLE IF NOT EXISTS `app_notificacao_envios` (
  `id` int(10) NOT NULL,
  `notificacao_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `app_notificacoes`
--

CREATE TABLE IF NOT EXISTS `app_notificacoes` (
  `id` int(10) NOT NULL,
  `titulo` varchar(191) NOT NULL,
  `conteudo` varchar(191) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '0',
  `log` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `app_notificacoes_log`
--

CREATE TABLE IF NOT EXISTS `app_notificacoes_log` (
  `id` int(10) NOT NULL,
  `sender_id` int(10) NOT NULL,
  `user_token_id` int(10) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `app_paginas`
--

CREATE TABLE IF NOT EXISTS `app_paginas` (
  `id` int(10) NOT NULL,
  `titulo` varchar(191) NOT NULL,
  `conteudo` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `app_paginas`
--

INSERT INTO `app_paginas` (`id`, `titulo`, `conteudo`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cupons', 'Teste2', 1, '2019-02-17 20:48:05', '2019-02-20 03:43:05'),
(2, 'Ajuda', 'Texto Ajuda', 1, '2019-02-19 16:28:40', NULL),
(3, 'Sobre Nós', 'Texto Sobre nos', 1, '2019-02-19 16:28:40', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `app_versions`
--

CREATE TABLE IF NOT EXISTS `app_versions` (
  `id` int(10) NOT NULL,
  `arquivo` varchar(191) NOT NULL,
  `nome` varchar(191) NOT NULL,
  `comentarios` longtext,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `app_versions`
--

INSERT INTO `app_versions` (`id`, `arquivo`, `nome`, `comentarios`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'dsadsa.apk', '1.0', 'Nada', 1, '2019-02-11 17:45:48', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_09_19_200935_create_admin_configs_table', 1),
(2, '2018_09_19_200937_create_admin_routes_table', 1),
(3, '2018_09_19_200945_create_admin_menu_table', 1),
(4, '2018_09_19_201001_create_users_types_table', 1),
(5, '2018_09_19_201011_create_user_type_permissions_table', 1),
(6, '2018_09_19_201021_create_users_table', 1),
(7, '2018_09_19_201028_create_user_gallery_table', 1),
(8, '2018_09_19_201038_create_user_types_table', 1),
(9, '2018_09_19_201050_create_password_resets_table', 1),
(10, '2018_09_19_201100_create_admin_logs_table', 1),
(11, '2018_09_19_230840_create_site_menu_table', 1),
(12, '2018_09_19_231638_create_site_menu_item_types_table', 1),
(13, '2018_09_19_231643_create_site_menu_itens_table', 1),
(14, '2018_09_19_232008_create_site_users_types_table', 1),
(15, '2018_09_19_232150_create_site_user_type_permissions_table', 1),
(16, '2018_09_19_232346_create_site_users_table', 1),
(17, '2018_09_19_232501_create_site_user_gallery_table', 1),
(18, '2018_09_19_232640_create_site_user_types_table', 1),
(19, '2018_09_19_232658_create_site_password_resets_table', 1),
(20, '2018_09_19_232718_create_site_user_logs_table', 1),
(21, '2018_09_19_233353_create_site_configs_table', 1),
(22, '2018_09_21_232158_create_user_notifications_table', 1),
(23, '2018_10_05_061906_create_site_langs_table', 1),
(24, '2018_10_24_055211_create_site_users_type_custom_fields_table', 1),
(25, '2018_10_24_055941_create_site_user_type_custom_field_value_table', 1),
(26, '2018_10_24_063355_create_admin_langs_table', 1),
(27, '2018_10_24_063429_create_admin_lang_words_table', 1),
(28, '2018_10_24_063454_create_site_lang_words_table', 1),
(29, '2018_10_24_071533_create_site_pages_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_banners`
--

CREATE TABLE IF NOT EXISTS `site_banners` (
  `id` int(10) NOT NULL,
  `name` varchar(191) NOT NULL,
  `image` varchar(191) NOT NULL,
  `link` longtext,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `site_banners`
--

INSERT INTO `site_banners` (`id`, `name`, `image`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Seu verão é aqui', '143100201902055c599e24d375d.png', NULL, 1, '2019-02-05 14:31:01', '2019-02-14 18:12:40'),
(2, 'Banner de teste', '191808201902095c5f27701af5c.png', NULL, 1, '2019-02-09 19:18:08', '2019-02-09 19:18:08');

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_categorias_grade`
--

CREATE TABLE IF NOT EXISTS `site_categorias_grade` (
  `id` int(10) NOT NULL,
  `name` varchar(191) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `site_categorias_grade`
--

INSERT INTO `site_categorias_grade` (`id`, `name`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 'Natação', 1, 1, '2019-01-30 18:30:14', NULL),
(2, 'Academia', 1, 2, '2019-01-30 18:30:14', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_configs`
--

CREATE TABLE IF NOT EXISTS `site_configs` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `site_configs`
--

INSERT INTO `site_configs` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'lang', '1', '2018-10-30 05:07:58', NULL),
(2, 'theme', 'default', '2018-10-30 05:07:58', NULL),
(3, 'register', '1', '2018-10-30 05:07:58', NULL),
(4, 'title', 'Exemplo 1', '2018-10-30 05:07:58', NULL),
(5, 'description', 'Descrição', '2018-10-30 05:07:58', NULL),
(6, 'maintenance', '0', '2018-10-30 05:07:58', NULL),
(7, 'index-follow', '1', '2018-10-30 05:07:58', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_eventos`
--

CREATE TABLE IF NOT EXISTS `site_eventos` (
  `id` int(10) NOT NULL,
  `data_evento` date NOT NULL,
  `image` varchar(191) NOT NULL,
  `titulo` varchar(191) NOT NULL,
  `texto` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `site_eventos`
--

INSERT INTO `site_eventos` (`id`, `data_evento`, `image`, `titulo`, `texto`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, '2019-02-22', '', 'teste de evento', 'sdfsdfsdfsdfsdfsdfsdf', 1, 1, '2019-02-09 19:32:53', '2019-02-09 19:32:53'),
(2, '2019-02-22', '', 'teste de evento 2', 'sdfsdfsdfsdfsdfsdfsdf', 1, 1, '2019-02-09 19:32:53', '2019-02-09 19:32:53'),
(3, '2019-02-22', '', 'teste de evento 3', 'sdfsdfsdfsdfsdfsdfsdf', 1, 1, '2019-02-09 19:32:53', '2019-02-09 19:32:53'),
(4, '2019-02-22', '', 'teste de evento 4', 'sdfsdfsdfsdfsdfsdfsdf', 1, 1, '2019-02-09 19:32:53', '2019-02-09 19:32:53'),
(5, '2019-02-22', '', 'teste de evento 5', 'sdfsdfsdfsdfsdfsdfsdf', 1, 1, '2019-02-09 19:32:53', '2019-02-09 19:32:53'),
(6, '2019-02-22', '', 'teste de evento 6', 'sdfsdfsdfsdfsdfsdfsdf', 1, 1, '2019-02-09 19:32:53', '2019-02-09 19:32:53');

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_galerias`
--

CREATE TABLE IF NOT EXISTS `site_galerias` (
  `id` int(10) NOT NULL,
  `capa` varchar(191) DEFAULT NULL,
  `titulo` varchar(191) NOT NULL,
  `texto` longtext,
  `status` int(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `site_galerias`
--

INSERT INTO `site_galerias` (`id`, `capa`, `titulo`, `texto`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, '154634201902055c59afda46090.jpeg', 'Noite do Halloween 2017', '<span style="color: rgb(29, 33, 41); font-family: system-ui, -apple-system, system-ui, ".SFNSText-Regular", sans-serif; font-size: 14px; text-align: center;">A nossa Noite do Halloween foi um sucesso e reuniu muita gente bonita, que se divertiu e caprichou nas fantasias.</span>', 1, 1, '2019-02-05 15:46:34', '2019-02-05 15:46:34');

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_galeria_itens`
--

CREATE TABLE IF NOT EXISTS `site_galeria_itens` (
  `id` int(10) NOT NULL,
  `galeria_id` int(10) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT '1',
  `title` varchar(191) DEFAULT NULL,
  `content` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `site_galeria_itens`
--

INSERT INTO `site_galeria_itens` (`id`, `galeria_id`, `tipo`, `title`, `content`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '', '155137201902055c59b109111c8.jpeg', 1, 1, '2019-02-05 15:51:37', '2019-02-05 15:51:37'),
(2, 1, 1, '', '160323201902055c59b3cb80b3b.jpeg', 1, 2, '2019-02-05 16:03:23', '2019-02-05 16:03:23'),
(3, 1, 1, '', '160336201902055c59b3d8240b9.jpeg', 1, 3, '2019-02-05 16:03:36', '2019-02-05 16:03:36'),
(4, 1, 1, '', '160341201902055c59b3dda87e5.jpeg', 1, 4, '2019-02-05 16:03:41', '2019-02-05 16:03:41');

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_grades`
--

CREATE TABLE IF NOT EXISTS `site_grades` (
  `id` int(10) NOT NULL,
  `categoria_id` int(10) NOT NULL,
  `name` varchar(191) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `site_grades`
--

INSERT INTO `site_grades` (`id`, `categoria_id`, `name`, `description`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 2, 'Alongamento', 'Sabe aquele incômodo nas costas, quando você sente um estralo ali ou uma dorzinha? Existem várias causas para isso como: má postura, estresse, insônia, atividade física e… espere, atividade física? Sim! O exercício de alongamento deve ser feito de forma correta e, de preferência, acompanhado por um profissional. Venha conhecer nossa academia e nossa equipe!', 1, 1, '2019-02-16 07:36:58', '2019-02-16 07:36:58'),
(2, 2, 'Capoeira', 'Além de ser uma expressão cultural brasileira que combina artes marciais, música, acrobacias e dança na realização golpes e movimentos rápidos, complexos e únicos, que requerem uma grande quantidade de força e flexibilidade corporal. Quem pratica a capoeira geralmente apresenta uma excelente forma física e bem-estar, pois as acrobacias e movimentos não estimulam apenas o corpo, mas a personalidade e o estado mental. Gostou? Venha conferir aqui no Tênis Clube!', 1, 1, '2019-02-16 07:38:16', '2019-02-16 07:38:16'),
(3, 2, 'Fitdance', 'Das modalidades mais populares e procuradas da academia. Mas por quê, você sabe? Três em um. Para quem gosta de dançar as músicas do momento e fazer atividade física ao mesmo tempo e ainda ter gasto calórico é super elevado. Vem conhecer o fitdance aqui do Tênis Clube!', 1, 1, '2019-02-16 07:39:07', '2019-02-16 07:39:07'),
(4, 2, 'Cross Hit', 'Feito para você que busca resultados eficientes e fuga da rotina. Essa modalidade se baseia em uma série de exercícios físicos intensos e alternativos à musculação. Trabalha todo o corpoe e oferece bons resultados em pouco tempo. Prepara-se para quebrar a rotina se exercitando com o uso de pneus, cordas e realizar treinos inusitados! Vem conferir na academia do Tênis Clube!', 1, 1, '2019-02-16 07:39:39', '2019-02-16 07:39:39'),
(5, 2, 'Jump', 'Queridinho nas academias e popular no mundo todo. Feito para emagrecer, tonificar, combater celulite, estresse ou cardiovascular, além de desenvolver o sistema cardiorrespiratório. É, de fato, bem mais que dar uns pulinhos na cama elástica, não é mesmo? Vem dar um up, ou um “jump”, e fazer parte da nossas turmas!', 1, 1, '2019-02-16 07:40:04', '2019-02-16 07:40:04'),
(6, 2, 'Pilates', 'Queridinho nas academias e popular no mundo todo. Feito para emagrecer, tonificar, combater celulite, estresse ou cardiovascular, além de desenvolver o sistema cardiorrespiratório. É, de fato, bem mais que dar uns pulinhos na cama elástica, não é mesmo? Vem dar um up, ou um “jump”, e fazer parte da nossas turmas!', 1, 1, '2019-02-16 07:40:29', '2019-02-16 07:40:29'),
(7, 2, 'Pumpmax', 'Um dos principais benefícios do Pumpmax é o gasto calórico. A aula de em média 1 hora pode proporcionar a queima de até 600 calorias. Ideal para quem busca diminuir gordura corporal e uma alternativa para fugir dos tradicionais exercícios aeróbicos. Faça parte das nossas turmas aqui no Tênis Clube!', 1, 1, '2019-02-16 07:40:54', '2019-02-16 07:40:54'),
(8, 2, 'Spinning', 'Pedalar só por pedalar? Não mesmo! Conhecido como o “detonador de calorias”, o spinning é uma das aulas mais disputadas das academias. E não é por menos. Ele queima gorduras, fortalece os músculos e, de quebra, aumenta a resistência cardiovascular e respiratória, além de contribuir para a redução do colesterol, da pressão arterial e ajudar no controle do diabetes. Venha cuidar da sua saúde na academia do Tênis Clube.', 1, 1, '2019-02-16 07:41:23', '2019-02-16 07:41:23'),
(9, 2, 'Sertanejo', 'Que tal perder peso, tonificar o corpo, reduzir o estresse e a ansiedade, e ainda fazer novos amigos ao som de um dos ritmos mais populares atualmente? A dança pode ser praticada por qualquer pessoa, de qualquer idade. Vem fazer parte da “comitiva” do Tênis Clube!', 1, 1, '2019-02-16 07:41:51', '2019-02-16 07:41:51'),
(10, 2, 'Judô', 'Disciplina, respeito, valores, condicionamento físico, defesa pessoal e capacidade analítica para agir no momento certo  são algumas das definições da arte marcial de origem japonesa que tem como principal objetivo derrubar o adversário e conseguir a vitória utilizando o próprio corpo e os movimentos do oponente para que esta meta seja atingida. Pegue o kimono e venha para o tamate do Tênis Clube.', 1, 1, '2019-02-16 07:42:37', '2019-02-16 07:42:37'),
(11, 2, 'Jiu Jitsu', 'Luta com foco principal na defesa pessoal. Indicada para todas as idades, entre homens e muito procurada principalmente para mulheres, pois exige mais técnica do que força. A capacidade de se superar, de pensar sobre pressão, de respeitar o próximo e ter persistência para conquistar a vitória são valores passados pelo Jiu Jitsu. Se idenfitica? Venha se desenvolver aqui no nosso tamate!', 1, 1, '2019-02-16 07:43:38', '2019-02-16 07:43:38'),
(12, 2, 'Musculação', 'Além do corpo definido, a musculação feita com o acompanhamento certo e seguro, realizado por profissionas capacidatos proporciona o ganho de massa magra, perda de peso, definição, reabilitação e potência muscular que garantem mais disposição física, ou seja, faz o corpo funcionar de maneira mais eficiente. Contamos com aparelhos bem cuidados e frequentemente inspecionados para que você alcance o máximo de desempenho. Venha conferir nossa academia!', 1, 1, '2019-02-16 07:44:12', '2019-02-16 07:44:12'),
(13, 2, 'Abd Power', 'A aula de Power Abs é composta por exercícios focados em ativar a musculatura abdominal profunda, sem necessariamente produzir movimento na coluna. Com isso, proporciona ótimos resultados dirigidos ao fortalecimento abdominal e lombar, utlizando técnicas de aprofundamento, tonificação e pouquíssimas repetições. Quer saber mais? Venha conferir e fazer parte da academia do Tênis Clube.', 1, 1, '2019-02-16 07:45:03', '2019-02-16 07:45:03'),
(14, 2, 'Ballet', 'Para quem já fez aulas e para quem deseja começar agora, o ballet é um excelente exercício para cuidar da saúde, da postura, trabalhar todos os músculos do corpo, adquirir agilidade e coordenação motora. Além de proporcionar bem-estar, é uma dança belíssima para o corpo e para os olhos de quem vê. A atividade é paga separadamente de nossa academia. Venha conferir e se encantar.', 1, 1, '2019-02-16 07:45:36', '2019-02-16 07:45:36'),
(15, 2, 'Zumba', 'Uma mistura de ginástica aeróbica, de dança latina com ritmo intenso! Perfeito para quem quer emagrecer e tonificar os músculos, além de poder ser praticada por pessoas de todas as idades. Vem fazer parte das turmas da nossa academia!', 1, 1, '2019-02-16 07:46:02', '2019-02-16 07:46:02'),
(16, 2, 'Natação', 'A natação proporciona desde a perda calórica até no auxílio na recuperação de lesões. A modalidade é indicada para pessoas de todas as idades e biotipos, e ainda é um esporte que não afeta as articulações, promove o relaxamento dos músculos, ajuda a aliviar a tensão muscular, melhora a postura, a capacidade aeróbica, além de promover a circulação e a flexibilidade muscular. Venha conferir nossa estrutura e os horários das turmas!', 1, 1, '2019-02-16 07:47:14', '2019-02-16 07:47:14'),
(17, 2, 'Step', 'O step simula subida e descida em degraus ao ritmo de dança composto de animação dos professores. Nesta modalidade a aula é planejada para trabalhar principalmente a resistência muscular, a capacidade cardiorrespiratória e a coordenação motora. Vem conferir as aulas e outras modalidades na academia do Tênis Clube!', 1, 1, '2019-02-16 07:47:39', '2019-02-16 07:47:39'),
(18, 2, 'Hidroginástica', 'Exercício que pode ser feito por pessoas de todas as idades, pois possui baixo impacto e conta ainda com a refrescância da água para os dias mais quentes. Esses são alguns dos principais motivos para você se sentir sempre animado para se exercitar na piscina. Excelente para quem quer e precisa cuidar do sistema circulatório, obter força, ter controle de peso e melhoria nas articulações. Tudo isso, pertinho de você. Venha conhecer a modalidade aqui no Tênis Clube. Esperamos por você!', 1, 1, '2019-02-16 07:48:03', '2019-02-16 07:48:03'),
(19, 2, 'Muay Thai', 'Mais uma das muitas modalidades da nossa academia, no muay thay você conhece atingir objetivo de queima de calorias, condicionamento físico e defesa pessoal, baseados em uma das artes marciais que mais movimenta partes diferentes do corpo. Punhos, cotovelos, joelhos, canelas e os pés, são empregados em golpes, numa modalidade intensa e com treinos muito puxados. É hora de você colocar toda sua energia em uso!', 1, 1, '2019-02-16 07:49:03', '2019-02-16 07:49:03'),
(20, 2, 'Treino Funcional', 'Alcance resultados de curto e longo prazo para o seu corpo, com um treinamento pensado em exercícios funcionais. O treinamento se adapta as suas necessidades, melhorando sua saúde e condicionamento físico, coordenação motora, melhor na postura e equilíbrio muscular, percepção dos movimentos e muito mais. Treine movimentos e não apenas o corpo. Comece agora e comprove os resultados.', 1, 1, '2019-02-16 07:49:30', '2019-02-16 07:49:30'),
(21, 2, 'Gap', 'Ideal para as mulheres que querem tonificar a parte inferior ou o abdômen. Os treinos trabalham glúteos, abdominais e pernas em atividades físicas eficazes para o corpo, servindo para fortalecer e tonificar os músculos. Além disso, os exercícios localizados melhoram a força dos glúteos, adutores e quadríceps. Também melhoram a circulação sanguínea e aumentam a resistência.', 1, 1, '2019-02-16 07:49:54', '2019-02-16 07:49:54'),
(22, 2, 'Baby Ballet', 'A dança estimula o desenvolvimento da mente e do corpo da criança, principalmente nas primeiras fases da infância. Apresentada de maneira lúdica e afetiva, o ballet baby é o ballet clássico apresentado as crianças que querem aprender a dançar e fazer os primeiros movimentos com as sapatilhas.', 1, 1, '2019-02-16 07:50:18', '2019-02-16 07:50:18'),
(23, 1, 'NATAÇÃO 5 a 7 anos', 'Natação para pessoas de 5 a 7 anos de idade.', 1, 1, '2019-02-17 14:54:37', '2019-02-17 14:54:37'),
(24, 1, 'CRIANÇAS ESPECIAIS 8 a 11 anos', 'Natação para crianças especiais de 8 a 11 anos.', 1, 1, '2019-02-17 14:58:36', '2019-02-17 14:58:36'),
(25, 1, 'NATAÇÃO 8 a 11 anos', 'Natação para crianças de 8 a 11 anos de idade.', 1, 1, '2019-02-17 14:59:59', '2019-02-17 14:59:59'),
(26, 1, 'NATAÇÃO ADULTO', 'Natação para adultos.', 1, 1, '2019-02-17 15:20:29', '2019-02-17 15:20:29'),
(27, 1, 'HIDRO', 'Hidroginastica.', 1, 1, '2019-02-17 15:27:51', '2019-02-17 15:27:51'),
(28, 1, 'NATAÇÃO BB1/BB2', 'Crianças 06 meses a 2 anos.\nCrianças 3 a 4 anos.', 1, 1, '2019-02-17 15:34:30', '2019-02-17 15:34:30');

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_grade_itens`
--

CREATE TABLE IF NOT EXISTS `site_grade_itens` (
  `id` int(10) NOT NULL,
  `grade_id` int(10) NOT NULL,
  `dia` int(1) NOT NULL,
  `hora` time NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `site_grade_itens`
--

INSERT INTO `site_grade_itens` (`id`, `grade_id`, `dia`, `hora`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '07:00:00', 1, '2019-02-16 07:51:09', '2019-02-16 07:51:09'),
(2, 1, 2, '18:00:00', 1, '2019-02-16 07:51:43', '2019-02-16 07:51:43'),
(3, 2, 1, '19:00:00', 1, '2019-02-16 07:58:59', '2019-02-16 07:58:59'),
(4, 7, 1, '07:30:00', 1, '2019-02-16 07:59:33', '2019-02-16 07:59:33'),
(5, 5, 1, '08:15:00', 1, '2019-02-16 07:59:56', '2019-02-16 07:59:56'),
(6, 15, 1, '09:00:00', 1, '2019-02-16 08:00:22', '2019-02-16 08:00:22'),
(7, 3, 1, '18:00:00', 1, '2019-02-16 08:00:54', '2019-02-16 08:00:54'),
(8, 13, 1, '18:45:00', 1, '2019-02-16 08:01:28', '2019-02-16 08:01:28'),
(9, 17, 1, '19:00:00', 1, '2019-02-16 08:02:18', '2019-02-16 08:02:18'),
(10, 4, 1, '19:45:00', 1, '2019-02-16 08:03:32', '2019-02-16 08:03:32'),
(11, 6, 1, '20:30:00', 1, '2019-02-16 08:03:53', '2019-02-16 08:03:53'),
(12, 3, 2, '07:00:00', 1, '2019-02-16 08:04:47', '2019-02-16 08:04:47'),
(13, 21, 2, '07:45:00', 1, '2019-02-16 08:05:29', '2019-02-16 08:05:29'),
(14, 5, 3, '07:30:00', 1, '2019-02-16 08:05:51', '2019-02-16 08:05:51'),
(15, 15, 4, '07:30:00', 1, '2019-02-16 08:06:24', '2019-02-16 08:06:24'),
(16, 17, 5, '07:30:00', 1, '2019-02-16 08:07:06', '2019-02-16 08:07:06'),
(17, 8, 2, '08:00:00', 1, '2019-02-16 08:08:56', '2019-02-16 08:08:56'),
(18, 8, 4, '08:00:00', 1, '2019-02-16 08:09:22', '2019-02-16 08:09:22'),
(19, 4, 3, '08:15:00', 1, '2019-02-16 08:10:06', '2019-02-16 08:10:06'),
(20, 4, 5, '08:15:00', 1, '2019-02-16 08:10:25', '2019-02-16 08:10:25'),
(21, 6, 4, '08:15:00', 1, '2019-02-16 08:10:52', '2019-02-16 08:10:52'),
(22, 20, 2, '08:30:00', 1, '2019-02-16 08:11:43', '2019-02-16 08:11:43'),
(23, 6, 3, '09:00:00', 1, '2019-02-16 08:12:11', '2019-02-16 08:12:11'),
(24, 7, 4, '09:00:00', 1, '2019-02-16 08:12:46', '2019-02-16 08:12:46'),
(25, 13, 5, '09:00:00', 1, '2019-02-16 08:13:29', '2019-02-16 08:13:29'),
(26, 15, 5, '09:30:00', 1, '2019-02-16 08:13:52', '2019-02-16 08:13:52'),
(27, 21, 5, '18:15:00', 1, '2019-02-16 08:14:22', '2019-02-16 08:14:22'),
(28, 5, 5, '18:45:00', 1, '2019-02-16 08:14:41', '2019-02-16 08:14:41'),
(29, 4, 5, '19:30:00', 1, '2019-02-16 08:15:04', '2019-02-16 08:15:04'),
(30, 11, 5, '20:00:00', 1, '2019-02-16 08:15:28', '2019-02-16 08:15:28'),
(31, 15, 5, '20:15:00', 1, '2019-02-16 08:15:50', '2019-02-16 08:15:50'),
(32, 20, 2, '18:30:00', 1, '2019-02-17 14:24:16', '2019-02-17 14:24:16'),
(33, 10, 2, '18:30:00', 1, '2019-02-17 14:27:32', '2019-02-17 14:27:32'),
(34, 5, 2, '19:00:00', 1, '2019-02-17 14:28:13', '2019-02-17 14:28:13'),
(35, 8, 2, '19:00:00', 1, '2019-02-17 14:32:00', '2019-02-17 14:32:00'),
(36, 10, 2, '19:30:00', 1, '2019-02-17 14:32:47', '2019-02-17 14:32:47'),
(37, 7, 2, '19:45:00', 1, '2019-02-17 14:33:14', '2019-02-17 14:33:14'),
(38, 15, 2, '20:30:00', 1, '2019-02-17 14:33:36', '2019-02-17 14:33:36'),
(39, 13, 3, '18:00:00', 1, '2019-02-17 14:36:11', '2019-02-17 14:36:11'),
(40, 15, 3, '18:15:00', 1, '2019-02-17 14:36:50', '2019-02-17 14:36:50'),
(41, 9, 3, '19:00:00', 1, '2019-02-17 14:37:08', '2019-02-17 14:37:08'),
(42, 8, 3, '19:15:00', 1, '2019-02-17 14:37:45', '2019-02-17 14:37:45'),
(43, 4, 3, '20:00:00', 1, '2019-02-17 14:38:32', '2019-02-17 14:38:32'),
(44, 11, 3, '20:00:00', 1, '2019-02-17 14:38:48', '2019-02-17 14:38:48'),
(45, 4, 4, '18:15:00', 1, '2019-02-17 14:39:18', '2019-02-17 14:39:18'),
(46, 10, 4, '18:30:00', 1, '2019-02-17 14:40:26', '2019-02-17 14:40:26'),
(47, 3, 4, '19:00:00', 1, '2019-02-17 14:46:03', '2019-02-17 14:46:03'),
(48, 10, 4, '19:30:00', 1, '2019-02-17 14:46:38', '2019-02-17 14:46:38'),
(49, 17, 4, '19:45:00', 1, '2019-02-17 14:47:09', '2019-02-17 14:47:09'),
(50, 8, 4, '19:50:00', 1, '2019-02-17 14:47:35', '2019-02-17 14:47:35'),
(51, 6, 4, '20:30:00', 1, '2019-02-17 14:48:03', '2019-02-17 14:48:03'),
(52, 22, 6, '09:00:00', 1, '2019-02-17 14:48:42', '2019-02-17 14:48:42'),
(53, 14, 6, '10:00:00', 1, '2019-02-17 14:49:11', '2019-02-17 14:49:11'),
(54, 5, 6, '10:00:00', 1, '2019-02-17 14:49:32', '2019-02-17 14:49:32'),
(55, 20, 6, '10:45:00', 1, '2019-02-17 14:50:03', '2019-02-17 14:50:03'),
(56, 2, 6, '11:00:00', 1, '2019-02-17 14:50:37', '2019-02-17 14:50:37'),
(57, 9, 6, '11:30:00', 1, '2019-02-17 14:51:06', '2019-02-17 14:51:06'),
(58, 19, 6, '14:00:00', 1, '2019-02-17 14:51:30', '2019-02-17 14:51:30'),
(59, 23, 1, '18:00:00', 1, '2019-02-17 14:54:56', '2019-02-17 14:54:56'),
(60, 23, 3, '18:00:00', 1, '2019-02-17 14:55:14', '2019-02-17 14:55:14'),
(61, 23, 5, '18:00:00', 1, '2019-02-17 14:55:30', '2019-02-17 14:55:30'),
(62, 23, 2, '08:20:00', 1, '2019-02-17 14:55:59', '2019-02-17 14:55:59'),
(63, 23, 4, '08:20:00', 1, '2019-02-17 14:56:18', '2019-02-17 14:56:18'),
(64, 23, 3, '08:20:00', 1, '2019-02-17 14:56:58', '2019-02-17 14:56:58'),
(65, 24, 1, '18:40:00', 1, '2019-02-17 14:59:03', '2019-02-17 14:59:03'),
(66, 25, 1, '19:20:00', 1, '2019-02-17 15:00:42', '2019-02-17 15:00:42'),
(67, 25, 2, '09:00:00', 1, '2019-02-17 15:16:55', '2019-02-17 15:16:55'),
(68, 25, 4, '09:00:00', 1, '2019-02-17 15:17:12', '2019-02-17 15:17:12'),
(69, 25, 5, '09:00:00', 1, '2019-02-17 15:17:27', '2019-02-17 15:17:27'),
(70, 25, 5, '17:15:00', 1, '2019-02-17 15:17:54', '2019-02-17 15:17:54'),
(71, 25, 5, '19:20:00', 1, '2019-02-17 15:18:22', '2019-02-17 15:18:22'),
(72, 25, 3, '17:15:00', 1, '2019-02-17 15:18:41', '2019-02-17 15:18:41'),
(73, 25, 3, '19:20:00', 1, '2019-02-17 15:18:56', '2019-02-17 15:18:56'),
(74, 25, 6, '08:40:00', 1, '2019-02-17 15:19:46', '2019-02-17 15:19:46'),
(75, 26, 1, '20:00:00', 1, '2019-02-17 15:20:51', '2019-02-17 15:20:51'),
(76, 26, 2, '07:00:00', 1, '2019-02-17 15:21:08', '2019-02-17 15:21:08'),
(77, 26, 2, '09:40:00', 1, '2019-02-17 15:21:25', '2019-02-17 15:21:25'),
(78, 26, 2, '18:00:00', 1, '2019-02-17 15:21:44', '2019-02-17 15:21:44'),
(79, 26, 2, '18:40:00', 1, '2019-02-17 15:21:59', '2019-02-17 15:21:59'),
(80, 26, 2, '20:00:00', 1, '2019-02-17 15:22:15', '2019-02-17 15:22:15'),
(81, 26, 2, '20:40:00', 1, '2019-02-17 15:22:31', '2019-02-17 15:22:31'),
(82, 26, 3, '07:00:00', 1, '2019-02-17 15:22:47', '2019-02-17 15:22:47'),
(83, 26, 3, '09:00:00', 1, '2019-02-17 15:23:02', '2019-02-17 15:23:02'),
(84, 26, 3, '18:40:00', 1, '2019-02-17 15:23:19', '2019-02-17 15:23:19'),
(85, 26, 3, '20:00:00', 1, '2019-02-17 15:23:33', '2019-02-17 15:23:33'),
(86, 26, 4, '07:00:00', 1, '2019-02-17 15:23:51', '2019-02-17 15:23:51'),
(87, 26, 4, '09:40:00', 1, '2019-02-17 15:24:19', '2019-02-17 15:24:19'),
(88, 26, 4, '18:00:00', 1, '2019-02-17 15:24:35', '2019-02-17 15:24:35'),
(89, 26, 4, '18:40:00', 1, '2019-02-17 15:24:49', '2019-02-17 15:24:49'),
(90, 26, 4, '20:00:00', 1, '2019-02-17 15:25:04', '2019-02-17 15:25:04'),
(91, 26, 4, '20:40:00', 1, '2019-02-17 15:25:24', '2019-02-17 15:25:24'),
(92, 26, 5, '07:00:00', 1, '2019-02-17 15:25:41', '2019-02-17 15:25:41'),
(93, 26, 5, '08:20:00', 1, '2019-02-17 15:25:57', '2019-02-17 15:25:57'),
(94, 26, 5, '09:40:00', 1, '2019-02-17 15:26:24', '2019-02-17 15:26:24'),
(95, 26, 5, '18:40:00', 1, '2019-02-17 15:26:39', '2019-02-17 15:26:39'),
(96, 26, 5, '20:00:00', 1, '2019-02-17 15:26:56', '2019-02-17 15:26:56'),
(97, 26, 6, '08:00:00', 1, '2019-02-17 15:27:15', '2019-02-17 15:27:15'),
(98, 27, 2, '07:40:00', 1, '2019-02-17 15:28:12', '2019-02-17 15:28:12'),
(99, 27, 2, '10:20:00', 1, '2019-02-17 15:28:26', '2019-02-17 15:28:26'),
(100, 27, 2, '19:20:00', 1, '2019-02-17 15:28:43', '2019-02-17 15:28:43'),
(101, 27, 3, '07:40:00', 1, '2019-02-17 15:29:00', '2019-02-17 15:29:00'),
(102, 27, 3, '10:20:00', 1, '2019-02-17 15:29:20', '2019-02-17 15:29:20'),
(103, 27, 3, '20:40:00', 1, '2019-02-17 15:29:34', '2019-02-17 15:29:34'),
(104, 27, 4, '07:40:00', 1, '2019-02-17 15:29:52', '2019-02-17 15:29:52'),
(105, 27, 4, '10:20:00', 1, '2019-02-17 15:30:06', '2019-02-17 15:30:06'),
(106, 27, 4, '19:20:00', 1, '2019-02-17 15:30:22', '2019-02-17 15:30:22'),
(107, 27, 5, '07:40:00', 1, '2019-02-17 15:31:10', '2019-02-17 15:31:10'),
(108, 27, 5, '10:20:00', 1, '2019-02-17 15:31:29', '2019-02-17 15:31:29'),
(109, 27, 5, '20:40:00', 1, '2019-02-17 15:32:45', '2019-02-17 15:32:45'),
(110, 27, 6, '10:00:00', 1, '2019-02-17 15:33:03', '2019-02-17 15:33:03'),
(111, 28, 3, '09:40:00', 1, '2019-02-17 15:34:46', '2019-02-17 15:34:46'),
(112, 28, 6, '09:20:00', 1, '2019-02-17 15:35:03', '2019-02-17 15:35:03');

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_horarios`
--

CREATE TABLE IF NOT EXISTS `site_horarios` (
  `id` int(10) NOT NULL,
  `image` varchar(191) NOT NULL,
  `name` varchar(191) NOT NULL,
  `description` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `site_horarios`
--

INSERT INTO `site_horarios` (`id`, `image`, `name`, `description`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, '145212201902055c59a31cd7d0c.jpeg', 'Academia', '<p class="color-pink-2 font-medium" style="margin-bottom: 5px; color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px; line-height: 1.4;">Segunda a Sexta: <span class="color-pink-2 font-bold font-size-18" style="font-weight: bold; font-size: 18px;">6h - 12h </span><span style="font-size: 15.2px; font-weight: bold;">e das 14h às 22h</span></p><p class="color-pink-2 font-medium" style="margin-bottom: 5px; color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px; line-height: 1.4;">Sábado: <span class="font-bold" style="font-weight: bold;">8h às 16h</span><br>Domingo: <span class="font-bold" style="font-weight: bold;">10h às 14h</span><br>Feriado: <span class="font-bold" style="font-weight: bold;">fechado</span></p>', 1, 1, '2019-02-05 14:52:13', '2019-02-05 14:52:13'),
(2, '145350201902055c59a37ea264d.jpeg', 'Churrasqueira', '<span style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;">Terça a Domingo: </span><span class="color-pink-2 font-bold font-size-18" style="color: rgb(112, 112, 112); font-family: Raleway; font-weight: bold; font-size: 18px;">9h - 17h</span>', 1, 1, '2019-02-05 14:53:50', '2019-02-05 14:53:50'),
(3, '145553201902055c59a3f9ec0d5.jpeg', 'Toboagua', '<span style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;">Alta temporada:</span><br style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;"><span class="color-pink-2 font-bold" style="color: rgb(112, 112, 112); font-family: Raleway; font-weight: bold; font-size: 15.2px;">10h às 12h e das 14h às 17h</span>', 1, 1, '2019-02-05 14:55:54', '2019-02-05 14:55:54'),
(4, '145845201902055c59a4a5ebeb2.jpeg', 'Piscina', '<p class="color-pink-2 font-bold" style="margin-bottom: 5px; color: rgb(112, 112, 112); font-family: Raleway; font-weight: bold; font-size: 15.2px; line-height: 1.4;">Horário de Verão<br><span class="color-pink-2 font-medium" style="font-weight: 500;">Terça à Domingo:</span> <span class="color-pink-2 font-bold">9h às 17h30</span></p><br style="color: rgb(33, 37, 41); font-family: -apple-system, system-ui, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"; font-size: 15.2px;"><p class="color-pink-2 font-bold font-size-15" style="margin-bottom: 5px; color: rgb(112, 112, 112); font-family: Raleway; font-weight: bold; font-size: 15px; line-height: 1.4;">Após o término do horário de verão:<br><span class="color-pink-2 font-medium" style="font-weight: 500;">Terça a sexta: </span><span class="color-pink-2 font-bold">9h às 17h</span><br><span class="color-pink-2 font-medium" style="font-weight: 500;">Sábado, domingo e feriados: </span><span class="color-pink-2 font-bold">9h às 17h30</span></p>', 1, 1, '2019-02-05 14:58:46', '2019-02-05 14:58:46'),
(5, '150020201902055c59a504e1d4f.jpeg', 'Sauna', '<p class="color-pink-2 font-medium" style="margin-bottom: 5px; color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px; line-height: 1.4;">Homens<br>Terça e quinta:<br><span class="color-pink-2 font-bold" style="font-weight: bold;">10h às 11h30 / 18h30 às 20h</span><br>sábados, domingos e feriados:<br><span class="color-pink-2 font-bold" style="font-weight: bold;">14h às 15h30</span><br></p><br style="color: rgb(33, 37, 41); font-family: -apple-system, system-ui, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol"; font-size: 15.2px;"><p class="color-pink-2 font-medium" style="margin-bottom: 5px; color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px; line-height: 1.4;">Mulheres<br>Quarta e sexta:<br><span class="color-pink-2 font-bold" style="font-weight: bold;">10h às 11h30 / 18h30 às 20h</span><br>sábados, domingos e feriados:<br><span class="color-pink-2 font-bold" style="font-weight: bold;">16h às 17h30</span></p>', 1, 1, '2019-02-05 15:00:21', '2019-02-05 15:00:21'),
(6, '150203201902055c59a56b4024b.jpeg', 'Espaço criança', '<span style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;">Sábados, domingos e feriados:</span><br style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;"><span class="color-pink-2 font-bold" style="color: rgb(112, 112, 112); font-family: Raleway; font-weight: bold; font-size: 15.2px;">10h às 12h e das 14h às 16h</span>', 1, 1, '2019-02-05 15:02:03', '2019-02-05 15:02:03'),
(7, '150753201902055c59a6c9096bc.jpeg', 'Avaliação da piscina', '<span style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;">Alta temporada</span><br style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;"><span class="color-pink-2 font-bold" style="color: rgb(112, 112, 112); font-family: Raleway; font-weight: bold; font-size: 15.2px;">Terça a sexta:</span><br style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;"><span style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;">9h às 12h </span><span class="color-pink-2 font-bold" style="color: rgb(112, 112, 112); font-family: Raleway; font-weight: bold; font-size: 15.2px;">sábados, domingos e feriados </span><span style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;">9h às 14h</span>', 1, 1, '2019-02-05 15:07:53', '2019-02-05 15:07:53'),
(8, '150832201902055c59a6f099f43.jpeg', 'Estacionamento', '<span style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;">Alta temporada</span><br style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;"><span style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;">Terça a domingo: </span><span class="color-pink-2 font-bold" style="color: rgb(112, 112, 112); font-family: Raleway; font-weight: bold; font-size: 15.2px;">9h às 18h</span><br style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;"><br style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;"><span style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;">Baixa temporada:</span><br style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;"><span style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;">sábados, domingos e feriados:</span><br style="color: rgb(112, 112, 112); font-family: Raleway; font-size: 15.2px;"><span class="color-pink-2 font-bold" style="color: rgb(112, 112, 112); font-family: Raleway; font-weight: bold; font-size: 15.2px;">9h às 18h</span>', 1, 1, '2019-02-05 15:08:32', '2019-02-05 15:08:32');

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_langs`
--

CREATE TABLE IF NOT EXISTS `site_langs` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `site_langs`
--

INSERT INTO `site_langs` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Português Brasil', 1, '2018-10-30 05:07:58', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_lang_words`
--

CREATE TABLE IF NOT EXISTS `site_lang_words` (
  `id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned NOT NULL,
  `lang_word` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_menu`
--

CREATE TABLE IF NOT EXISTS `site_menu` (
  `id` int(10) unsigned NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_menu_item_types`
--

CREATE TABLE IF NOT EXISTS `site_menu_item_types` (
  `id` int(10) unsigned NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `function` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `args` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_menu_itens`
--

CREATE TABLE IF NOT EXISTS `site_menu_itens` (
  `id` int(10) unsigned NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_noticias`
--

CREATE TABLE IF NOT EXISTS `site_noticias` (
  `id` int(10) NOT NULL,
  `image` varchar(191) DEFAULT NULL,
  `titulo` varchar(131) NOT NULL,
  `texto` longtext NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `site_noticias`
--

INSERT INTO `site_noticias` (`id`, `image`, `titulo`, `texto`, `status`, `created_at`, `updated_at`) VALUES
(1, '142144201902055c599bf893dd2.jpeg', 'Vantagens da academia do Clube', '<span style="color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Aqui no Tênis Clube a academia traz muito mais vantagens para você. Além de muito completa, com 21 atividades diferentes, ela fica aberta todos os dias. De segunda a sexta-feira, das 06h às 12h | das 14h às 22h, sábado das 8h às 16h e domingo das 10h às 14h. E aos domingos a musculação é grátis para sócios, sendo necessário apenas ter atestado médico em dia e carteirinha da academia. Aproveite!</span>', 1, '2019-02-05 14:21:44', '2019-02-05 14:21:44'),
(2, '142355201902055c599c7b57917.jpeg', 'Academia dentro do Clube', '<span style="color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Com programas de bem-estar e mais 18 modalidades, a academia do Tênis Clube de Suzano está preparada para você. Se o seu foco for perder peso, temos fitdance, cross hit, zumba, spinning entre outros; se o foco for ganhar massa temos musculação. E também temos opções como jiu-jitsu, muay thay, hidro e muito mais. Aqui você encontra 21 atividades diferentes para manter sua saúde e bem-estar em dia. Aproveite e vem pro nosso Clube!</span>', 1, '2019-02-05 14:23:55', '2019-02-05 14:23:55'),
(3, '', 'Pra que esquentar a cabeça nesse verão?', '<p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Você e a sua família curtindo todos os dias do verão da melhor forma em um espaço completo para você.</p><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">O Clube está esperando você neste verão com diversos serviços e opções: parque aquático, academia, playground, espaço Kids e muito mais. E o melhor: o plano familiar a partir R$99,90 confira condições.</p><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Seja sócio e confira no vídeo abaixo tudo que você pode aproveitar nesse verão!</p>', 1, '2019-02-05 14:24:56', '2019-02-05 14:24:56'),
(4, '142631201902055c599d173564c.png', 'Melhorando o Clube', '<p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Confira o que já está concluído:</p><ul style="margin-bottom: 1rem; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><li><span style="font-weight: bolder;">Reforma da sauna</span></li></ul><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Já funcionando e esperando você, consulte os horários disponíveis e aproveite.</p><ul style="margin-bottom: 1rem; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><li><span style="font-weight: bolder;">Paisagismo</span></li></ul><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Um ambiente bonito e bem cuidado promove bem-estar e relaxamento. Por isso nossa área verde um carinho especial, com novas plantas e cuidados.</p><hr style="overflow: visible; margin-top: 1rem; margin-bottom: 1rem; border-top-color: rgba(0, 0, 0, 0.1); color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><h2 style="margin-top: 24px; margin-bottom: 15px; font-family: Raleway; font-weight: 600; line-height: 1.25; color: rgb(36, 41, 46); font-size: 24px; clear: both; padding-bottom: 0.3em;"><span style="font-weight: bolder;">Novidades – parte 1 de 2</span></h2><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">A lista é grande, então vamos a elas sem demora.</p><ul style="margin-bottom: 1rem; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><li><span style="font-weight: bolder;">Aulas especiais na piscina, aos finais de semana, durante o verão.</span></li></ul><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Hidrão e dança, em 20 minutos de atividade oferecido pelo professor da academia.</p><ul style="margin-bottom: 1rem; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><li><span style="font-weight: bolder;">Splash, parquinho molhado</span></li></ul><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Uma fonte seca para crianças e adultos se refrescarem e se divertirem muuuuito.</p><ul style="margin-bottom: 1rem; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><li><span style="font-weight: bolder;">Half tênis, brinquedo radical</span></li></ul><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">São 11 metros de altura por 18 metros de comprimento de muita diversão. Adolescente e adultos, que curtem sentir adrenalina já estão convidados.</p><hr style="overflow: visible; margin-top: 1rem; margin-bottom: 1rem; border-top-color: rgba(0, 0, 0, 0.1); color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><h2 style="margin-top: 24px; margin-bottom: 15px; font-family: Raleway; font-weight: 600; line-height: 1.25; color: rgb(36, 41, 46); font-size: 24px; clear: both; padding-bottom: 0.3em;"><span style="font-weight: bolder;">Novidades – parte 2 de 2</span></h2><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><span style="font-weight: bolder;">Novidades na área da piscina:</span></p><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">– Banheiros masculinos e femininos;</p><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">– Novo local de guarda volumes;</p><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">– Ampliação da área de alimentação ao redor da piscina.</p><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><span style="font-weight: bolder;">Lista de espera – natação:</span></p><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Nos dias 30 e 31 de cada mês, a recepção divulgará as vagas para matrículas. Entre os dias 1 e 7, do mês seguinte, o sócio poderá procurar a recepção e efetuar sua matrícula conforme vagas disponíveis. Necessário: atestado clínico (apto para atividade física) em mãos e exame dermatológico em dia.</p>', 1, '2019-02-05 14:26:31', '2019-02-05 14:26:31'),
(5, '143225201902055c599e790cd04.jpeg', 'Muay Thai em 9 benefícios para você e seu corpo', '<ol style="margin-bottom: 1rem; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><li>Melhora o condicionamento físico;</li><li>Acelera o metabolismo;</li><li>Eleva a autoestima;</li><li>Queima muitas calorias;</li><li>Define os músculos;</li><li>Melhora a força e a agilidade;</li><li>Aumenta a flexibilidade;</li><li>Protege o sistema imunológico;</li><li>Exercita a coordenação motora.</li></ol><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Maior flexibilidade, músculos mais definidos e menos gordurinhas, tudo isso em um treino muito agitado. Mas, sem dúvida, tanto esforço vale a pena pela saúde e o bem-estar.</p><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;"><span style="font-weight: bolder;">Muay Thai e a queima de calorias</span></p><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Aliado a uma alimentação saudável, a prática dessa arte marcial pode te ajudar a emagrecer, muito devido a intensidade dos treinos e do preparo físico. O gasto calórico pode ser de até 1.500 calorias por aula, em aulas bem puxadas; para iniciantes pode chegar a 750 calorias/aula.</p><p style="margin-bottom: 1.5em; color: rgb(77, 77, 77); font-family: Raleway; font-size: 15.2px;">Como o muay thai exige muito da musculatura do corpo, ele ajuda a definir os músculos, melhorando o contorno corporal e ajudando a combater a retenção de líquidos e a celulite.</p>', 1, '2019-02-05 14:32:25', '2019-02-05 14:32:25'),
(6, '', 'Teste de notícia', 'testando o sistema de cadastramento de notícia', 1, '2019-02-09 19:48:05', '2019-02-09 19:48:05');

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_pages`
--

CREATE TABLE IF NOT EXISTS `site_pages` (
  `id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `parent` int(11) NOT NULL DEFAULT '0',
  `visible` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `site_pages`
--

INSERT INTO `site_pages` (`id`, `lang_id`, `title`, `slug`, `content`, `parent`, `visible`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Home', '/', '', 0, 1, 1, '2018-10-30 05:07:58', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_password_resets`
--

CREATE TABLE IF NOT EXISTS `site_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_users`
--

CREATE TABLE IF NOT EXISTS `site_users` (
  `id` int(10) unsigned NOT NULL,
  `titulo` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `site_users`
--

INSERT INTO `site_users` (`id`, `titulo`, `name`, `password`, `status`, `created_at`, `updated_at`) VALUES
(1, '0000000001', 'Robson Vinicius Nogueira Vieira', '123', 1, '2018-11-16 20:44:39', '2018-12-08 15:11:10');

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_users_tokens`
--

CREATE TABLE IF NOT EXISTS `site_users_tokens` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `token` longtext NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `site_users_tokens`
--

INSERT INTO `site_users_tokens` (`id`, `user_id`, `token`, `created_at`, `updated_at`) VALUES
(1, 0, 'dB9K27kKKLg:APA91bHJo4ekK5_InGx5Co5KQvd5Sp-OUr3HX-h_k01h7Xytn23_Vza3fPtf92OocpGZqth31VDHS8QEXDEB8Cg1P6xP2G_W5vL52XxigEBNUI5DCUw6suBYroG1EyUrXZYKyOFVh8_X', '2019-02-23 00:33:33', '2019-02-23 00:33:33');

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_users_types`
--

CREATE TABLE IF NOT EXISTS `site_users_types` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `site_users_types`
--

INSERT INTO `site_users_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Visitante', 'Usuário sem identificação, não possui cadastro no site.', 1, '2018-10-30 04:49:39', NULL),
(2, 'Associado', 'Associado cadastrado no sistema.', 1, '2018-10-30 04:49:39', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_users_type_custom_fields`
--

CREATE TABLE IF NOT EXISTS `site_users_type_custom_fields` (
  `id` int(10) unsigned NOT NULL,
  `user_type_id` int(10) unsigned NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `placeholder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `required_field` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_user_gallery`
--

CREATE TABLE IF NOT EXISTS `site_user_gallery` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_user_logs`
--

CREATE TABLE IF NOT EXISTS `site_user_logs` (
  `id` int(10) unsigned NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `log` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_user_types`
--

CREATE TABLE IF NOT EXISTS `site_user_types` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_user_type_custom_field_value`
--

CREATE TABLE IF NOT EXISTS `site_user_type_custom_field_value` (
  `id` int(10) unsigned NOT NULL,
  `custom_field_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `field_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `site_user_type_permissions`
--

CREATE TABLE IF NOT EXISTS `site_user_type_permissions` (
  `id` int(10) unsigned NOT NULL,
  `user_type_id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `birthday`, `description`, `website`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'admin@admin.com', NULL, NULL, NULL, NULL, '$2y$10$lIVrwduadhQ97eDs6TeH7OtspInDddJUIV.JhEYWox27r0zLKf5Sm', 1, 'XV3QmQgSp8ysvbBeowzjEcXXQetaEfTLV3fqdFYF6fvDQfrZLG20FGawVnM1', '2018-10-30 05:07:59', NULL),
(2, 'Desenvolvedor', 'dev@superteia.com.br', NULL, NULL, NULL, NULL, '$2y$10$lIVrwduadhQ97eDs6TeH7OtspInDddJUIV.JhEYWox27r0zLKf5Sm', 1, 'fmUhYmtUrpr3qttxRQhS7SRIOdI0JrqnE3WWWFaoc94SUpWs4OmMib0l3fJV', '2019-02-25 05:38:34', NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `users_types`
--

CREATE TABLE IF NOT EXISTS `users_types` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `users_types`
--

INSERT INTO `users_types` (`id`, `name`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Desenvolvedor', 'Usuário responsável pelo desenvolvimento das funcionalidades do sistema.', 1, NULL, NULL),
(2, 'Administrador', 'Usuário responsável pelas tarefas administrativas do sistema.', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `user_gallery`
--

CREATE TABLE IF NOT EXISTS `user_gallery` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `picture` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `user_notifications`
--

CREATE TABLE IF NOT EXISTS `user_notifications` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura para tabela `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `user_types`
--

INSERT INTO `user_types` (`id`, `user_id`, `user_type_id`) VALUES
(1, 1, 2),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `user_type_permissions`
--

CREATE TABLE IF NOT EXISTS `user_type_permissions` (
  `id` int(10) unsigned NOT NULL,
  `user_type_id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=318 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Fazendo dump de dados para tabela `user_type_permissions`
--

INSERT INTO `user_type_permissions` (`id`, `user_type_id`, `menu_id`) VALUES
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(26, 1, 26),
(27, 1, 27),
(28, 1, 28),
(29, 1, 29),
(30, 1, 30),
(31, 1, 31),
(32, 1, 32),
(33, 1, 33),
(34, 1, 34),
(35, 1, 35),
(36, 1, 36),
(37, 1, 37),
(38, 1, 38),
(39, 1, 39),
(40, 1, 40),
(41, 1, 41),
(42, 1, 42),
(43, 1, 43),
(44, 1, 44),
(45, 1, 45),
(46, 1, 46),
(47, 1, 47),
(48, 1, 48),
(49, 1, 49),
(50, 1, 50),
(51, 1, 51),
(52, 1, 52),
(53, 1, 53),
(54, 1, 54),
(56, 1, 56),
(57, 1, 57),
(58, 1, 58),
(59, 1, 59),
(60, 1, 60),
(61, 1, 61),
(62, 1, 62),
(63, 1, 63),
(64, 1, 64),
(65, 1, 65),
(66, 1, 66),
(67, 1, 67),
(68, 1, 68),
(79, 2, 72),
(80, 1, 72),
(81, 2, 73),
(82, 1, 73),
(83, 2, 74),
(84, 1, 74),
(85, 2, 75),
(86, 1, 75),
(87, 2, 76),
(88, 1, 76),
(92, 2, 69),
(93, 1, 69),
(94, 1, 1),
(95, 1, 55),
(110, 2, 80),
(111, 1, 80),
(116, 2, 83),
(117, 1, 83),
(118, 2, 84),
(119, 1, 84),
(120, 2, 85),
(121, 1, 85),
(122, 2, 86),
(123, 1, 86),
(124, 2, 87),
(125, 1, 87),
(128, 2, 89),
(129, 1, 89),
(130, 2, 90),
(131, 1, 90),
(132, 2, 91),
(133, 1, 91),
(136, 2, 93),
(137, 1, 93),
(138, 2, 94),
(139, 1, 94),
(142, 2, 96),
(143, 1, 96),
(144, 2, 97),
(145, 1, 97),
(146, 2, 98),
(147, 1, 98),
(148, 2, 99),
(149, 1, 99),
(150, 2, 100),
(151, 1, 100),
(152, 2, 92),
(153, 1, 92),
(185, 2, 88),
(186, 1, 88),
(187, 2, 95),
(188, 1, 95),
(193, 2, 71),
(194, 1, 71),
(195, 2, 70),
(196, 1, 70),
(197, 2, 78),
(198, 1, 78),
(201, 2, 17),
(202, 1, 17),
(203, 2, 18),
(204, 1, 18),
(205, 2, 19),
(206, 1, 19),
(207, 2, 20),
(208, 1, 20),
(209, 2, 21),
(210, 1, 21),
(211, 2, 22),
(212, 1, 22),
(213, 2, 23),
(214, 1, 23),
(215, 2, 24),
(216, 1, 24),
(217, 2, 25),
(218, 1, 25),
(219, 2, 79),
(220, 1, 79),
(221, 2, 101),
(222, 1, 101),
(223, 2, 102),
(224, 1, 102),
(225, 2, 103),
(226, 1, 103),
(227, 2, 104),
(228, 1, 104),
(229, 2, 105),
(230, 1, 105),
(231, 2, 106),
(232, 1, 106),
(233, 2, 107),
(234, 1, 107),
(235, 2, 108),
(236, 1, 108),
(237, 2, 77),
(238, 1, 77),
(239, 1, 109),
(240, 1, 110),
(241, 1, 111),
(242, 1, 112),
(243, 1, 113),
(244, 1, 114),
(245, 1, 115),
(246, 1, 116),
(247, 1, 117),
(252, 2, 81),
(253, 1, 81),
(254, 2, 82),
(255, 1, 82),
(256, 2, 118),
(257, 1, 118),
(258, 2, 119),
(259, 1, 119),
(260, 2, 120),
(261, 1, 120),
(262, 2, 121),
(263, 1, 121),
(264, 2, 122),
(265, 1, 122),
(266, 2, 123),
(267, 1, 123),
(268, 2, 124),
(269, 1, 124),
(270, 2, 125),
(271, 1, 125),
(272, 2, 126),
(273, 1, 126),
(274, 2, 127),
(275, 1, 127),
(276, 2, 128),
(277, 1, 128),
(278, 2, 129),
(279, 1, 129),
(280, 2, 130),
(281, 1, 130),
(282, 2, 131),
(283, 1, 131),
(284, 2, 132),
(285, 1, 132),
(286, 2, 133),
(287, 1, 133),
(288, 2, 134),
(289, 1, 134),
(290, 2, 135),
(291, 1, 135),
(292, 2, 136),
(293, 1, 136),
(294, 2, 137),
(295, 1, 137),
(296, 2, 138),
(297, 1, 138),
(298, 2, 139),
(299, 1, 139),
(300, 2, 140),
(301, 1, 140),
(302, 2, 141),
(303, 1, 141),
(304, 2, 142),
(305, 1, 142),
(306, 2, 143),
(307, 1, 143),
(308, 2, 144),
(309, 1, 144),
(310, 2, 145),
(311, 1, 145),
(312, 2, 146),
(313, 1, 146),
(314, 2, 147),
(315, 1, 147),
(316, 2, 148),
(317, 1, 148);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `admin_configs`
--
ALTER TABLE `admin_configs`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `admin_langs`
--
ALTER TABLE `admin_langs`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `admin_lang_words`
--
ALTER TABLE `admin_lang_words`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `admin_logs`
--
ALTER TABLE `admin_logs`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `admin_routes`
--
ALTER TABLE `admin_routes`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `admin_routes_url_unique` (`url`), ADD UNIQUE KEY `admin_routes_name_unique` (`name`);

--
-- Índices de tabela `app_assuntos_contato`
--
ALTER TABLE `app_assuntos_contato`
  ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Índices de tabela `app_contatos`
--
ALTER TABLE `app_contatos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `app_emails`
--
ALTER TABLE `app_emails`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- Índices de tabela `app_logs`
--
ALTER TABLE `app_logs`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `app_notificacao_envios`
--
ALTER TABLE `app_notificacao_envios`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `app_notificacoes`
--
ALTER TABLE `app_notificacoes`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `app_notificacoes_log`
--
ALTER TABLE `app_notificacoes_log`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `app_paginas`
--
ALTER TABLE `app_paginas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `app_versions`
--
ALTER TABLE `app_versions`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_banners`
--
ALTER TABLE `site_banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_categorias_grade`
--
ALTER TABLE `site_categorias_grade`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_configs`
--
ALTER TABLE `site_configs`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_eventos`
--
ALTER TABLE `site_eventos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_galerias`
--
ALTER TABLE `site_galerias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_galeria_itens`
--
ALTER TABLE `site_galeria_itens`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_grades`
--
ALTER TABLE `site_grades`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_grade_itens`
--
ALTER TABLE `site_grade_itens`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_horarios`
--
ALTER TABLE `site_horarios`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_langs`
--
ALTER TABLE `site_langs`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_lang_words`
--
ALTER TABLE `site_lang_words`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_menu`
--
ALTER TABLE `site_menu`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_menu_item_types`
--
ALTER TABLE `site_menu_item_types`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_menu_itens`
--
ALTER TABLE `site_menu_itens`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_noticias`
--
ALTER TABLE `site_noticias`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_pages`
--
ALTER TABLE `site_pages`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_users`
--
ALTER TABLE `site_users`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_users_tokens`
--
ALTER TABLE `site_users_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_users_types`
--
ALTER TABLE `site_users_types`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_users_type_custom_fields`
--
ALTER TABLE `site_users_type_custom_fields`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_user_gallery`
--
ALTER TABLE `site_user_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_user_logs`
--
ALTER TABLE `site_user_logs`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_user_types`
--
ALTER TABLE `site_user_types`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_user_type_custom_field_value`
--
ALTER TABLE `site_user_type_custom_field_value`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `site_user_type_permissions`
--
ALTER TABLE `site_user_type_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `users_types`
--
ALTER TABLE `users_types`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `user_gallery`
--
ALTER TABLE `user_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`), ADD KEY `user_types_user_type_id_foreign` (`user_type_id`), ADD KEY `user_types_user_id_foreign` (`user_id`);

--
-- Índices de tabela `user_type_permissions`
--
ALTER TABLE `user_type_permissions`
  ADD PRIMARY KEY (`id`), ADD KEY `user_type_permissions_user_type_id_foreign` (`user_type_id`), ADD KEY `user_type_permissions_menu_id_foreign` (`menu_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `admin_configs`
--
ALTER TABLE `admin_configs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `admin_langs`
--
ALTER TABLE `admin_langs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `admin_lang_words`
--
ALTER TABLE `admin_lang_words`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `admin_logs`
--
ALTER TABLE `admin_logs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT de tabela `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=149;
--
-- AUTO_INCREMENT de tabela `admin_routes`
--
ALTER TABLE `admin_routes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=169;
--
-- AUTO_INCREMENT de tabela `app_assuntos_contato`
--
ALTER TABLE `app_assuntos_contato`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `app_contatos`
--
ALTER TABLE `app_contatos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `app_emails`
--
ALTER TABLE `app_emails`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `app_logs`
--
ALTER TABLE `app_logs`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `app_notificacao_envios`
--
ALTER TABLE `app_notificacao_envios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `app_notificacoes`
--
ALTER TABLE `app_notificacoes`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `app_notificacoes_log`
--
ALTER TABLE `app_notificacoes_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `app_paginas`
--
ALTER TABLE `app_paginas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de tabela `app_versions`
--
ALTER TABLE `app_versions`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de tabela `site_banners`
--
ALTER TABLE `site_banners`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `site_categorias_grade`
--
ALTER TABLE `site_categorias_grade`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `site_configs`
--
ALTER TABLE `site_configs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de tabela `site_eventos`
--
ALTER TABLE `site_eventos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de tabela `site_galerias`
--
ALTER TABLE `site_galerias`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `site_galeria_itens`
--
ALTER TABLE `site_galeria_itens`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de tabela `site_grades`
--
ALTER TABLE `site_grades`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de tabela `site_grade_itens`
--
ALTER TABLE `site_grade_itens`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT de tabela `site_horarios`
--
ALTER TABLE `site_horarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de tabela `site_langs`
--
ALTER TABLE `site_langs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `site_lang_words`
--
ALTER TABLE `site_lang_words`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `site_menu`
--
ALTER TABLE `site_menu`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `site_menu_item_types`
--
ALTER TABLE `site_menu_item_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `site_menu_itens`
--
ALTER TABLE `site_menu_itens`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `site_noticias`
--
ALTER TABLE `site_noticias`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de tabela `site_pages`
--
ALTER TABLE `site_pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `site_users`
--
ALTER TABLE `site_users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `site_users_tokens`
--
ALTER TABLE `site_users_tokens`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de tabela `site_users_types`
--
ALTER TABLE `site_users_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `site_users_type_custom_fields`
--
ALTER TABLE `site_users_type_custom_fields`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `site_user_gallery`
--
ALTER TABLE `site_user_gallery`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `site_user_logs`
--
ALTER TABLE `site_user_logs`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `site_user_types`
--
ALTER TABLE `site_user_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `site_user_type_custom_field_value`
--
ALTER TABLE `site_user_type_custom_field_value`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `site_user_type_permissions`
--
ALTER TABLE `site_user_type_permissions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `users_types`
--
ALTER TABLE `users_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `user_gallery`
--
ALTER TABLE `user_gallery`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de tabela `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de tabela `user_type_permissions`
--
ALTER TABLE `user_type_permissions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=318;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
