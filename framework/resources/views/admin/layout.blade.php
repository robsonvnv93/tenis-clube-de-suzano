<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

	<head>
		
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="icon" href="{!! asset('images/favicon.ico') !!}" type="image/ico" />
		 
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		 
		<title>Tenis Clube de Suzano</title>

		 
		<link rel="stylesheet" href="{{ asset('vendors/google-code-prettify/src/prettify.css') }}" />
		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('fonts/css/font-awesome.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendors/nprogress/nprogress.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendors/dropzone/dist/min/dropzone.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendors/iCheck/skins/flat/green.css')}}" />
		<link rel="stylesheet" href="{{ asset('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendors/jqvmap/dist/jqvmap.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendors/pnotify/dist/pnotify.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendors/pnotify/dist/pnotify.buttons.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendors/pnotify/dist/pnotify.nonblock.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendors/fullcalendar/dist/fullcalendar.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('vendors/fullcalendar/dist/fullcalendar.print.css') }}" media="print" />
		<link rel="stylesheet" href="{{ asset('vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
		<link rel="stylesheet" href="{{ asset('css/automator/paginator.css') }}" />
		<link rel="stylesheet" href="{{ asset('css/automator/form-maker-dropzone.css') }}" />
		

		<!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito" />
		
		 
		<!--[if lt IE 9]>
			
			<script src="../assets/js/ie8-responsive-file-warning.js"></script>

		<![endif]-->
		 
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

		<![endif]-->
		<!-- ######################## -->
		@stack('css')

	</head>
	<body class="nav-md">

		@guest
		@else

			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">

				@csrf

			</form>
			<div class="container body">
				
				<div class="main_container">

					<div class="col-md-3 left_col">
						
						<div class="left_col scroll-view">

							<div class="navbar nav_title" style="border: 0; padding-bottom: 10px; height: 77px;">

								<a href="{{ route('admin-dashboard') }}" class="site_title" style="line-height: 30px; margin-top: 15px;"><span>Tenis Clube de Suzano</span></a>

							</div>
							<div class="clearfix"></div>

							<!-- /menu profile quick info -->
							<br />


							<!-- sidebar menu -->
							<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

								<div class="menu_section">

									<h3>Navegação</h3>
									{!! Helper::UserAdminMenu(Auth::user()->id) !!}

								</div>

							</div>


							<!-- /sidebar menu -->


							<!-- /menu footer buttons -->
							<div class="sidebar-footer hidden-small">

								<a href="{!! route('admin-app-contato-index') !!}" data-toggle="tooltip" data-placement="top" title="Contato">

									<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>

								</a>
								<a href="{{ route('admin-dashboard') }}" data-toggle="tooltip" data-placement="top" title="Dashboard">

									<span class="glyphicon glyphicon-home" aria-hidden="true"></span>

								</a>
								<a href="{{ route('admin-minha-conta') }}" data-toggle="tooltip" data-placement="top" title="Minha Conta">

									<span class="glyphicon glyphicon-user" aria-hidden="true"></span>

								</a>
								<a data-toggle="tooltip" data-placement="top" title="Sair" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">

									<span class="glyphicon glyphicon-off" aria-hidden="true"></span>

								</a>

							</div>
							<!-- /menu footer buttons -->

						</div>

					</div>


					 <!-- top navigation -->
					<div class="top_nav">

						<div class="nav_menu">

							<nav>

								<div class="nav toggle">

									<a id="menu_toggle"><i class="fa fa-bars"></i></a>

								</div>
								<ul class="nav navbar-nav navbar-right">

									<li class="">

										<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

											{!! Helper::GetUserProfilePicture(Auth::user()->id) !!} {!! Helper::GetUserData(Auth::user()->id, 'name') !!}
											<span class=" fa fa-angle-down"></span>

										</a>
										<ul class="dropdown-menu dropdown-usermenu pull-right">

											<li><a href="{{ route('admin-minha-conta') }}"> Minha Conta</a></li>
											<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i> Sair</a></li>

										</ul>

									</li>
									<!-- <li role="presentation" class="dropdown"> -->

										<!-- <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false"> -->

											<!-- <i class="fa fa-envelope-o"></i> -->
											<!-- <span class="badge bg-green">6</span> -->

										<!-- </a> -->
										<!-- <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">

											<li>

												<a>

													<span class="image"><img src="images/img.jpg" alt="Profile Image" /></span>
													<span>

														<span>John Smith</span>
														<span class="time">3 mins ago</span>

													</span>
													<span class="message">

														Film festivals used to be do-or-die moments for movie makers. They were where...

													</span>

												</a>

											</li>
										
										</ul> -->

									<!-- </li> -->

								</ul>

							</nav>

						</div>

					</div>


					<!-- page content -->
					<div class="right_col" role="main">

						@yield('content')

					</div>
				
				</div>

			</div>

			
			<!-- jQuery -->
			<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
			

			<!-- Bootstrap -->
			<script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
			

			<!-- FastClick -->
			<script src="{{ asset('vendors/fastclick/lib/fastclick.js') }}"></script>
			

			<!-- NProgress -->
			<script src="{{ asset('vendors/nprogress/nprogress.js') }}"></script>
			

			<!-- Chart.js -->
			<script src="{{ asset('vendors/Chart.js/dist/Chart.min.js') }}"></script>
			

			<!-- gauge.js -->
			<script src="{{ asset('vendors/gauge.js/dist/gauge.min.js') }}"></script>
			

			<!-- bootstrap-progressbar -->
			<script src="{{ asset('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
			
			<!-- iCheck -->
			<script src="{{ asset('vendors/iCheck/icheck.min.js') }}"></script>
			
			<!-- Skycons -->
			<script src="{{ asset('vendors/skycons/skycons.js') }}"></script>
			
			<!-- Flot -->
			<script src="{{ asset('vendors/Flot/jquery.flot.js') }}"></script>
			<script src="{{ asset('vendors/Flot/jquery.flot.pie.js') }}"></script>
			<script src="{{ asset('vendors/Flot/jquery.flot.time.js') }}"></script>
			<script src="{{ asset('vendors/Flot/jquery.flot.stack.js') }}"></script>
			<script src="{{ asset('vendors/Flot/jquery.flot.resize.js') }}"></script>
			

			<!-- Flot plugins -->
			<script src="{{ asset('vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
			<script src="{{ asset('vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
			<script src="{{ asset('vendors/flot.curvedlines/curvedLines.js') }}"></script>
			
			<!-- DateJS -->
			<script src="{{ asset('vendors/DateJS/build/date.js') }}"></script>
			
			<!-- JQVMap -->
			<script src="{{ asset('vendors/jqvmap/dist/jquery.vmap.js') }}"></script>
			<script src="{{ asset('vendors/jqvmap/dist/maps/jquery.vmap.world.js') }}"></script>
			<script src="{{ asset('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') }}"></script>
			
			
			<!-- PNotify -->
			<script src="{{ asset('vendors/pnotify/dist/pnotify.js') }}"></script>
			<script src="{{ asset('vendors/pnotify/dist/pnotify.buttons.js') }}"></script>
			<script src="{{ asset('vendors/pnotify/dist/pnotify.nonblock.js') }}"></script>
			
			<!-- bootstrap-daterangepicker -->
			<script src="{{ asset('vendors/moment/min/moment.min.js') }}"></script>
			<script src="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

			<!-- bootstrap-datetimepicker -->
			<script src="{{ asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>

			<!-- bootstrap-wysiwyg -->
			<script src="{{ asset('vendors/jquery.hotkeys/jquery.hotkeys.js') }}"></script>
			<script src="{{ asset('vendors/google-code-prettify/src/prettify.js') }}"></script>
			<script src="{{ asset('vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') }}"></script>

			
			<!-- bootstrap-dropzone -->
			<script src="{{ asset('vendors/dropzone/dist/min/dropzone.min.js') }}"></script>

			<!-- FullCalendar -->
			<script src="{{ asset('vendors/fullcalendar/dist/fullcalendar.min.js') }}"></script>

			 <!-- Bootstrap Colorpicker -->
			 <script src="{{ asset('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>

			<!-- Custom Theme Scripts -->
			<script src="{{ asset('js/custom.js') }}"></script>
			<script src="{{ asset('js/functions.js') }}"></script>
			@stack('scripts')

		@endguest

	</body>

</html>