@extends('admin.layout')
@section('content')
	
	<div class="row">

		<div class="col-md-12">

			<div class="page-title">

				<div class="title-left">
					
					<h3>Dashboard</h3>

				</div>
				<hr size="1" />

			</div>
			<br />
			
		</div>
		<div class="col-md-12">

			<h4>Seja bem-vindo, <b>{!! Auth::user()->name !!}</b>.</h4>
			<br />

		</div>
		<div class="col-md-12">

			<div class="row">

				<div class="col-xs-12 col-md-6">

					<div class="panel panel-primary">

						<!-- Default panel contents -->
						<div class="panel-heading">Mensagens de contato - ( <small>{!! $contato['total'] !!}</small> )</div>
						<div class="panel-body">
							
							<p>Acesse rapidamente as ultimas mensagens enviadas por pessoas utilizando o formulário de contato do aplicado.</p>

						</div>
						<!-- Table -->
						<table class="table">

							<tbody>

								@if($contato['total'] >= 1)

									@foreach($contato['mensagens'] as $mensagem)
									<tr>

										<td class="text-center" style="width: 160px;">{!! $mensagem['data'] !!}</td>
										<td>{!! $mensagem['nome'] !!}</td>
										<td style="width: 35px;" class="text-center"><a href="{!! route('admin-app-contato-view', $mensagem['id']) !!}"><i class="fa fa-eye"></i></a></td>

									</tr>
									@endforeach

								@else

									<tr>

										<td class="text-center">Não existem mensagens para exibir.</td>

									</tr>

								@endif

							</tbody>

						</table>

					</div>

				</div>
				<div class="col-xs-12 col-md-6">

					<div class="panel panel-danger">

						<!-- Default panel contents -->
						<div class="panel-heading">Notificações - ( <small>{!! $notificacoes['total'] !!}</small> )</div>
						<div class="panel-body">
							
							<p>Visualize abaixo as ultimas notificações que foram enviadas aos usuários do app.</p>

						</div>


						<!-- Table -->
						<table class="table">

							<tbody>

								@if($notificacoes['total'] >= 1)

									@foreach($notificacoes['itens'] as $notificacao)
									<tr>

										<td class="text-center" style="width: 160px;">{!! $notificacao['data'] !!}</td>
										<td style="width: 37%;">{!! $notificacao['titulo'] !!}</td>
										<td>
											@if($notificacao['status'] == 0)
												
												<span class="text-danger">envio pendente</span>

											@else
												
												<span class="text-success">envio realizado</span>

											@endif
										</td>
										<td style="width: 35px;" class="text-center"><a href="{!! route('admin-app-notificacoes-view', $notificacao['id']) !!}"><i class="fa fa-eye"></i></a></td>

									</tr>
									@endforeach

								@else

									<tr>

										<td class="text-center">Não existem notificações para exibir.</td>

									</tr>

								@endif

							</tbody>

						</table>

					</div>

				</div>

			</div>
			
		</div>

	</div>

@endsection