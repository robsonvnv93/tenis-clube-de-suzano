@extends('admin.layout')

@push('css')
	
	<style type="text/css">

		.thumbnail-active .caption,
		.thumbnail-active { color: #31708f; background-color: #d9edf7; border-color: #bce8f1 }
		.thumbnail { height: 400px; }
		.thumbnail .image { height: 276px; }
		.thumbnail .image .mask { height: 276px; }
		.thumbnail .image .mask .tools { margin-top: 190px; }
		.thumbnail .image .mask p { font-size: 18px !important; }
		
	</style>

@endpush

@section('content')
	
	<div class="row">

		<div class="col-md-12">

			<div class="page-title">

				<div class="title-left">
					<h3>Aparência - <small>Temas</small></h3>
				</div>

			</div>
			
		</div>
		<div class="col-md-12">

			<hr size="1" />
			<br />
			
		</div>

	</div>
	@if(count($themes) >= 1)
		
		<div class="row">

			@foreach($themes as $theme)

				<div class="col-md-4 col-sm-6 col-xs-12">

					@if(str_slug($theme['theme']) == $active)
						
						<div class="thumbnail thumbnail-active">
							
					@else
						
						<div class="thumbnail">

					@endif

						<div class="image view view-first">

							{!! $theme['screenshot'] !!}
							<div class="mask">

								<p>{!! $theme['theme'] !!} - {!! $theme['version'] !!}</p>
								<div class="tools tools-bottom">
									
									<a href="#"><i class="fa fa-eye"></i></a>
									@if(str_slug($theme['theme']) != $active)
										
										<a href="#"><i class="fa fa-check"></i></a>

									@else

										@if(Helper::UserCanAccessRoute(Auth::user()->id, 'admin-site-front-editor'))

											<a href="{{ route('admin-site-front-editor') }}"><i class="fa fa-pencil"></i></a>

										@endif

									@endif

								</div>

							</div>

						</div>
						<div class="caption">
							
							<b class="text-center" style="width: 100%; display: table;">{!! $theme['theme'] !!}</b>
							<br />
							{!! $theme['description'] !!}
							<br />
							<b>Dev:</b> {!! $theme['developer'] !!}

						</div>

					</div>

				</div>

			@endforeach

		</div>

	@else

		<h4 class="text-center">Nenhum tema localizado!</h4>

	@endif

@endsection