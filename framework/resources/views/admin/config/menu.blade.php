@extends('admin.layout')
@section('content')
	
	@push('css')

		<style type="text/css">

			#page-loader {

				background-color: rgba(0, 0, 0, 0.6);
				position: fixed;
				z-index: 9999;
				display: none;
				height: 100%;
				width: 100%;
				left: 0px;
				top: 0px;

			}


			#page-loader table {

				text-align: center;
				font-size: 30px;
				position: relative;
				display: table;
				height: 100%;
				width: 100%;
				color: #FFFFFF;

			}


			#fa-form-menu-icon {
				
				font-size: 28px;
				display: none;
				margin: 0px auto 10px auto;

			}

			.dd3-content i.fa {

				margin-right: 5px;
				margin-top: 2px;
				cursor: pointer;

			}
			
			.dd3-content span.traco { margin: -2px 5px 0px; }

			.cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
			* html .cf { zoom: 1; }
			*:first-child+html .cf { zoom: 1; }
			/**
			 * Nestable
			**/

			.dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }
			.dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
			.dd-list .dd-list { padding-left: 30px; }
			.dd-collapsed .dd-list { display: none; }

			.dd-item,
			.dd-empty,
			.dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }
			.dd-handle {

				display: block;
				height: 30px;
				margin: 5px 0;
				padding: 5px 10px;
				color: #333;
				text-decoration: none;
				font-weight: bold;
				border: 1px solid #ccc;
				background: #fafafa;
				background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
				background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
				background:         linear-gradient(top, #fafafa 0%, #eee 100%);
				-webkit-border-radius: 3px;
				border-radius: 3px;
				box-sizing: border-box;
				-moz-box-sizing: border-box;

			}

			.dd-handle:hover { color: #2ea8e5; background: #fff; }
			.dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
			.dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
			.dd-item > button[data-action="collapse"]:before { content: '-'; }

			.dd-placeholder,
			.dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
			.dd-empty {

				border: 1px dashed #bbb;
				min-height: 100px;
				background-color: #e5e5e5;
				background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff), -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
				background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff), -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
				background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff), linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
				background-size: 60px 60px;
				background-position: 0 0, 30px 30px;

			}

			.dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
			.dd-dragel > .dd-item .dd-handle { margin-top: 0; }
			.dd-dragel .dd-handle {

				-webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
				box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);

			}


			@media only screen and (min-width: 700px) {

				.dd { float: left; width: 48%; }
				.dd + .dd { margin-left: 2%; }

			}

			.dd-hover > .dd-handle { background: #2ea8e5 !important; }


			/**
			 * Nestable Draggable Handles
			**/

			.dd3-content {

				display: block;
				height: 30px;
				margin: 5px 0;
				padding: 5px 10px 5px 40px;
				color: #333;
				text-decoration: none;
				font-weight: bold;
				border: 1px solid #ccc;
				background: #fafafa;
				background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
				background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
				background:         linear-gradient(top, #fafafa 0%, #eee 100%);
				-webkit-border-radius: 3px;
				border-radius: 3px;
				box-sizing: border-box;
				-moz-box-sizing: border-box;
			
			}

			.dd3-content:hover { color: #2ea8e5; background: #fff; }
			.dd-dragel > .dd3-item > .dd3-content { margin: 0; }
			.dd3-item > button { margin-left: 30px; }
			.dd3-handle {

				position: absolute;
				margin: 0;
				left: 0;
				top: 0;
				cursor: pointer;
				width: 30px;
				text-indent: 100%;
				white-space: nowrap;
				overflow: hidden;
				border: 1px solid #aaa;
				background: #ddd;
				background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
				background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
				background:         linear-gradient(top, #ddd 0%, #bbb 100%);
				border-top-right-radius: 0;
				border-bottom-right-radius: 0;

			}

			.dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
			.dd3-handle:hover { background: #ddd; }


			/**
			 * Socialite
			**/

			.socialite { display: block; float: left; height: 35px; }

			.dd-item-status-0,
			.dd-item-status-0 > .dd3-content { color: #d9534f !important; }

			.dd-item-visible-0 { opacity: .65; }

		</style>

	@endpush
	@push('scripts')

		<script src="{{ asset('js/jquery.nestable.js') }}"></script>
		<script type="text/javascript">

			function excluirMenu(menu) {

				if(confirm("Tem certeza de que deseja excluir este item?")) {

					$('#page-loader').fadeIn(500, function() {

						$('#form-menu')[0].reset();
						
						$.ajax({
						
							url: "{{ route('admin-config-menu-destroy') }}",
							type: "POST",
							data: { '_token': '{{ csrf_token() }}', menu: menu },
							success: function(response){

								if(response.result == true) {

									$('#menu-' + menu).fadeOut(500, function() {

										$(this).remove();

									});

								} else {

									alert("Oops! Alguma coisa deu errado por favor entre em contato com seu desenvolvedor.");
									
								}

								$('#page-loader').fadeOut(500);

							},
							error: function(e) {

								console.log(e);
								alert("Oops! Alguma coisa deu errado por favor entre em contato com seu desenvolvedor.");
								$('#page-loader').fadeOut(500);

							}
						
						});

					});

				}

			}


			// Acão de click no botão de edição do menu.
			function editarMenu(menu) {

				$('#page-loader').fadeIn(500, function() {

					$('#form-menu-route > option').removeAttr('selected');
					$('#form-menu-visible > option').removeAttr('selected');
					$('#form-menu-status > option').removeAttr('selected');
					$('#form-menu')[0].reset();
					$.ajax({
						
						url: "{{ route('admin-config-menu-edit') }}",
						type: "POST",
						data: { '_token': '{{ csrf_token() }}', menu: menu },
						success: function(response){

							if(response.result == true) {

								var dados = response.menu;
								$("#form-menu-id").val(dados['id']);
								if(dados['icon'] != '' && dados['icon'] != 'null' && dados['icon'] != null) {

									$('#fa-form-menu-icon').removeAttr('class').attr('style', 'display: none');
									$('#fa-form-menu-icon').addClass('fa fa-' + dados['icon']).attr('style', 'display: table');
									$("#form-menu-icon").val(dados['icon']);

								} else {
									
									$('#fa-form-menu-icon').removeAttr('class').attr('style', 'display: none');
									$("#form-menu-icon").val("");

								}

								$('#form-menu-route > option').removeAttr('selected');
								$("#form-menu-route > option[value='" + dados['route'] + "']").attr('selected', 'selected');
								$("#form-menu-name").val(dados['name']);
								$("#form-menu-visible > option[value='" + dados['visible'] + "']").attr('selected', 'selected');
								$("#form-menu-status > option[value='" + dados['status'] + "']").attr('selected', 'selected');

								$('.permissoes').removeAttr('checked');

								var permissoes = response.permissions;
								for(a = 0; a < permissoes.length; a++) {

									$('#permissao-' + permissoes[a]).prop('checked', true);

								}

								var topo = $('#form-menu').scrollTop();
								$('html, body').animate({scrollTop: topo }, 1000, function() {

									$('#form-menu-name').focus();
									$('#page-loader').fadeOut(500);
									
								});

							} else {

								$('#page-loader').fadeOut(500, function() {
									
									alert(response.message);

								});

							}

						},
						error: function(e) {

							alert("Oops! Alguma coisa deu errado, por favor entre em contato com seu desenvolvedor.");
							console.log(e);

						}
					
					});

				});

			}

			$(document).ready(function() {

				$('#form-menu-icon').keyup(function() {

					var valor = $(this).val();
					$('#fa-form-menu-icon').attr('class', 'fa fa-' + valor).css('display', 'table');

				});

				// Cancelar edição ou inserção de menu.
				$('#add-menu-cancel').click(function() {

					$('#page-loader').fadeIn(500, function() {

						$('#form-menu-route > option').removeAttr('selected');
						$('#form-menu-visible > option').removeAttr('selected');
						$('#form-menu-status > option').removeAttr('selected');
						$('#form-menu')[0].reset();
						$('#fa-form-menu-icon').removeAttr('class').attr('style', 'display: none');
						$('#page-loader').fadeOut(500);

					});

				});


				// Ação de submit no formulário de edição ou inserção de menu.
				$('#form-menu').submit(function() {

					var id			= $('#form-menu-id').val();
					var icone		= $('#form-menu-icon').val();
					var nome		= $('#form-menu-name').val();
					var route		= $('#form-menu-route').val();
					var visible		= $('#form-menu-visible').val();
					var status		= $('#form-menu-status').val();
					var permissoes	= [];
					var count		= 0;

					if(nome == "") {

						alert("Por favor digite o Nome do menu!");
						$('#form-menu-name').focus();

					} else {

						if(route == "") {

							alert("Por favor selecione a Rota do menu!");
							$('#form-menu-route').focus();

						} else {

							if(visible == "") {

								alert("Por favor selecione a Visibilidade do menu!");
								$('#form-menu-visible').focus();

							} else {

								if(status == "") {

									alert("Por favor selecione o Status do menu!");
									$('#form-menu-status').focus();
									
								} else {

									$('#page-loader').fadeIn(500, function() {

										$('.permissoes').each(function(index, valor) {

										if($(this).is(':checked')) {

											permissoes[count] = $(this).val();
											count++;

										}

										}).promise().done(function() {

											var dados	= {};
											if(id == "") {

												var acao	= "{{ route('admin-config-menu-store') }}";
												var novo	= true;

											} else {

												var acao	= "{{ route('admin-config-menu-update') }}";
												var novo	= false;
												dados['id']	= id;

											}

											dados['icon']		= icone;
											dados['name']		= nome;
											dados['route']		= route;
											dados['visible']	= visible;
											dados['status']		= status;
											dados['permissoes']	= permissoes;

											$.ajax({
										
												url: acao,
												type: "POST",
												data: { '_token': '{{ csrf_token() }}', dados: dados },
												success: function(response){

													alert(response.message);
													$('#page-loader').fadeOut(500, function() {

														location.reload();

													});

												},
												error: function(e) {

													console.log(e);
													alert("Oops! Alguma coisa deu errado por favor entre em contato com seu desenvolvedor.");
													$('#page-loader').fadeOut(500);

												}
											
											});

										});

									});

								}

							}

						}

					}

					return false;

				});


				var updateMenu = function(e) {

					$('#page-loader').fadeIn(500, function() {

						var list	= e.length ? e : $(e.target),
							output	= list.data('output');
						
						if (window.JSON) {

							var itens = (window.JSON.stringify(list.nestable('serialize')));
							$.ajax({
						
								url: "{{ route('admin-config-menu-order') }}",
								type: "POST",
								data: { '_token': '{{ csrf_token() }}', itens: itens },
								success: function(response){

									if(response.result == true) {

										$('#page-loader').fadeOut(500, function() {

											if($(this).attr('data-pnotify') != 'false') {
												
												$(this).attr('data-pnotify', 'false');
												new PNotify({

													title: 'Atenção',
													text: 'Para visualizar a alteração da ordem dos itens do menu realizadas será necessário que esta pagina seja atualizada.',
													type: 'info',
													nonblock: { nonblock: true },
													hide: false,
													styling: 'bootstrap3'

												});
												
											}


										});

									} else {

										alert("Oops! Alguma coisa deu errado por favor entre em contato com seu desenvolvedor.");
										location.reload();
										
									}

								},
								error: function(e) {

									console.log(e);
									alert("Oops! Alguma coisa deu errado por favor entre em contato com seu desenvolvedor.");
									$('#page-loader').fadeOut(500);

								}
							
							});
						
						} else {

							console.log('JSON browser support required for this demo.');

						}

					});

				};
				
				$('#nestable3').nestable({ maxDepth: 2 }).on('change', updateMenu);

			});
			
		</script>

	@endpush
	
	<div id="page-loader">

		<table>
			
			<tr>

				<td><i class="fa fa-spin fa-refresh"></i></td>

			</tr>

		</table>
		
	</div>
	<div>

		<!-- Page Title -->
		<div class="page-title">
			
			<div class="title_left">
				
				<h3>Administrador - <small>Gerenciar menu</small></h3>
			
			</div>

		</div>
		<div class="clearfix"></div>
		<div class="row">
			
			<div class="col-xs-12">

				<hr size="1" style="margin: 0px 0px 10px 0px;" />

			</div>

		</div>
		<div class="row">

			<div class="col-xs-12 col-md-3">

				<h5>Informações do menu</h5>
				<hr />
				<form id="form-menu">

					<input type="hidden" id="form-menu-id" value="" />
					<i class="fa" id="fa-form-menu-icon"></i>
					<input type="text" id="form-menu-icon" class="form-control" placeholder="Icone" />
					<br />
					<input type="text" id="form-menu-name" class="form-control" placeholder="Nome" required />
					<br />
					<select id="form-menu-route" class="form-control" required>
						
						<option value="" selected>Rota</option>
						@if(isset($rotas))

							@if(count($rotas) >= 1)

								@foreach($rotas as $rota)

									<option value="{{ $rota['id'] }}">{!! $rota['name'] !!}</option>

								@endforeach

							@endif

						@endif

					</select>
					<br />
					<select id="form-menu-visible" class="form-control" required>
						
						<option value="" selected>Visibilidade</option>
						<option value="1">Visivel</option>
						<option value="0">Oculto</option>

					</select>
					<br />
					<select id="form-menu-status" class="form-control" required>
						
						<option value="" selected>Status</option>
						<option value="1">Ativo</option>
						<option value="0">Inativo</option>

					</select>
					<br />
					@if(count($types) >= 1)

						<b style="font-size: 14px; width: 100%; display: table; margin-bottom: 10px;">Permissões de acesso:</b>
						@foreach($types as $type)
							
							<input class="permissoes" type="checkbox" name="permissoes[]" id="permissao-{{ $type->id }}" value="{{ $type->id }}" /> <label for="permissao-{{ $type->id }}" style="cursor: pointer; font-weight: normal;">{!! $type->name !!}</label>
							<br />

						@endforeach

					@endif
					<br />
					<div class="row">

						<div class="col-xs-12 col-md-6">

							<button type="reset" id="add-menu-cancel" class="btn btn-danger btn-block">Cancelar</button>

						</div>
						<div class="col-xs-12 col-md-6">

							<button type="submit" id="add-menu" class="btn btn-success btn-block">Salvar</button>

						</div>

					</div>

				</form>

			</div>
			<div class="col-xs-12 col-md-9">

				@if(count($menu) >= 1)

					<div id="nestable3" class="dd">

						<ol id="menu-lista" class="dd-list">

							@foreach($menu as $parent)

								<li id="menu-{{ $parent['id'] }}" class="dd-item dd3-item dd-item-status-{{ $parent['status'] }} dd-item-visible-{{ $parent['visible'] }}" data-id="{{ $parent['id'] }}">

									<div class="dd-handle dd3-handle">&nbsp;</div>
									<div class="dd3-content">

										<span id="menu-{{ $parent['id'] }}-name" class="name">{!! $parent['name'] !!}</span>
										<i onclick="excluirMenu({{ $parent['id'] }})" class="fa fa-trash pull-right"></i>
										<span class="traco pull-right">-</span>
										<i onclick="editarMenu({{ $parent['id'] }})" class="fa fa-pencil pull-right"></i>

									</div>
									@if(count($parent['childs']) >= 1)

										<ol class="dd-list">

											@foreach($parent['childs'] as $child)

												<li id="menu-{{ $child['id'] }}" class="dd-item dd3-item dd-item-status-{{ $child['status'] }} dd-item-visible-{{ $child['visible'] }}" data-id="{{ $child['id'] }}">

													<div class="dd-handle dd3-handle">&nbsp;</div>
													<div class="dd3-content">
														
														<span id="menu-{{ $child['id'] }}-name" class="name">{!! $child['name'] !!}</span>
														<i onclick="excluirMenu({{ $child['id'] }})" class="fa fa-trash pull-right"></i>
														<span class="traco pull-right">-</span>
														<i onclick="editarMenu({{ $child['id'] }})" class="fa fa-pencil pull-right"></i>

													</div>

												</li>

											@endforeach

										</ol>

									@endif

								</li>

							@endforeach

						</ol>

					</div>
					
				@else

					<h3 class="text-center"></h3>

				@endif

			</div>

		</div>

	</div>

@endsection