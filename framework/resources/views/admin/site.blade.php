@extends('admin.layout')
@section('content')
	
	<div class="row">

		<div class="col-md-12">

			<div class="page-title">

				<div class="title-left">
					<h3>Site - <small>Configurações</small></h3>
				</div>

			</div>
			
		</div>
		<div class="col-md-12">

			<hr size="1" />
			<br />
			
		</div>
		<div class="col-md-12">

			<form method="POST" class="form-horizontal form-label-left">

				@csrf
				<input type="hidden" name="action" value="update" />
				<div class="form-group">
					
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Modo de Manutenção <span class="required">*</span></label>
						
					<div class="col-md-6 col-sm-6 col-xs-12">
						
						<div id="maintenance" class="btn-group" data-toggle="buttons">
							
							<label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-primary">
								
								<input type="radio" name="maintenance" value="1" data-parsley-multiple="maintenance"> &nbsp; Sim &nbsp;
							
							</label>
							<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">

								<input type="radio" name="maintenance" value="0" data-parsley-multiple="maintenance" /> Não
							
							</label>
						
						</div>
					
					</div>
				
				</div>
				<div class="form-group">
					
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Evitar mecanismos de busca <span class="required">*</span></label>
						
					<div class="col-md-6 col-sm-6 col-xs-12">
						
						<div id="index-follow" class="btn-group" data-toggle="buttons">
							
							<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
								
								<input type="radio" name="index-follow" value="1" data-parsley-multiple="index-follow"> &nbsp; Sim &nbsp;
							
							</label>
							<label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">

								<input type="radio" name="index-follow" value="0" data-parsley-multiple="index-follow" /> Não
							
							</label>
						
						</div>
					
					</div>
				
				</div>
				<div class="form-group">
					
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Titulo <span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">

						<input type="text" id="title" name="title" value="{!! $title !!}" required="required" class="form-control col-md-7 col-xs-12" />

					</div>
				
				</div>
				<div class="form-group">
					
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">Descrição <span class="required">*</span></label>
					<div class="col-md-6 col-sm-6 col-xs-12">

						<input type="text" id="description" name="description" value="{!! $description !!}" required="required" class="form-control col-md-7 col-xs-12" />

					</div>
				
				</div>
				<div class="control-group">
					
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Keywords</label>
						
					<div class="col-md-6 col-sm-9 col-xs-12">
						
						<input id="keywords" type="text" class="tags form-control" value="{!! $keywords !!}" data-tagsinput-init="true" style="display: none;" />

						<div id="keywords_tagsinput" class="tagsinput" style="width: auto; min-height: 100px; height: 100px;">

							<span class="tag"><span>social&nbsp;&nbsp;</span><a href="#" title="Removing tag">x</a></span>
							<span class="tag"><span>adverts&nbsp;&nbsp;</span><a href="#" title="Removing tag">x</a></span>
							<span class="tag"><span>sales&nbsp;&nbsp;</span><a href="#" title="Removing tag">x</a></span>
							<div id="keywords_addTag">

								<input id="keywords_tag" value="" data-default="add a tag" placeholder="add a tag" style="color: rgb(102, 102, 102); width: 72px;" />

							</div>
							<div class="tags_clear"></div>

						</div>
						<div id="suggestions-container" style="position: relative; float: left; width: 250px; margin: 10px;"></div>
					
					</div>

				</div>
				<div style="clear: both;"></div>
				<div class="form-group">
					
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Idioma <span class="required">*</span></label>
						
					<div class="col-md-2 col-sm-9 col-xs-12">
						
						<select class="form-control">
							
							<option value="" disabled>- Selecione -</option>
							@if(count($langs) >= 1)

								@foreach($langs as $lang)

									<option value="{{ $lang->id }}">{!! $lang->name !!}</option>

								@endforeach

							@endif
						
						</select>
					
					</div>
				
				</div>
				<div class="form-group">
					
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Permitir cadastros <span class="required">*</span></label>
						
					<div class="col-md-6 col-sm-6 col-xs-12">
						
						<div id="register" class="btn-group" data-toggle="buttons">
							
							<label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
								
								<input type="radio" name="register" value="1" data-parsley-multiple="register"> &nbsp; Sim &nbsp;
							
							</label>
							<label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">

								<input type="radio" name="register" value="0" data-parsley-multiple="register" /> Não
							
							</label>
						
						</div>
					
					</div>
				
				</div>
				<div class="form-group">
					
					<label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de usuário padrão para novo cadastro <span class="required">*</span></label>
						
					<div class="col-md-3 col-sm-9 col-xs-12">
						
						<select class="form-control">
							
							<option value="" disabled>- Selecione -</option>
							@if(count($users_types) >= 1)

								@foreach($users_types as $user_type)

									<option value="{{ $user_type->id }}">{!! $user_type->name !!}</option>

								@endforeach

							@endif
						
						</select>
					
					</div>
				
				</div>
				<div class="form-group">
					
					<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
						
						<button type="button" class="btn btn-danger">Cancelar</button>
						<button type="submit" class="btn btn-success">Salvar</button>

					</div>

				</div>

			</form>

		</div>

	</div>

@endsection