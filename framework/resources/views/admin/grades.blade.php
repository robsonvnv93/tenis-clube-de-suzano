@extends('admin.layout')
@section('content')
	
	@push('css')

		<style type="text/css">

			
			/* Page Loader */
			#page-loader {

				background-color:	rgba(0, 0, 0, 0.6);
				position:			fixed;
				z-index:			9999;
				display:			none;
				height:				100%;
				width:				100%;
				left:				0px;
				top:				0px;

			}


			#page-loader table {

				text-align:	center;
				font-size:	30px;
				position:	relative;
				display:	table;
				height:		100%;
				width:		100%;
				color:		#FFFFFF;

			}

			#grade-informacoes { padding-bottom: 20px; }

			/* Atividades na categoria */
			#grade-itens {
				
				background-color:	#FFFFFF;
				padding-bottom:		20px;
				padding-right:		5px;
				padding-left:		5px;
				padding-top:		20px;
				margin-top:			20px;
				overflow-y:			auto;

			}


			.grade-itens-lista-item {

				border-radius:	4px;
				margin-bottom:	20px;
				padding:		10px 15px;
				display:		table;
				height:			auto;
				width:			100%;

			}

			.grade-itens-lista-item-acoes { padding-right: 5px; }
			.grade-itens-lista-item-acoes > .fa {

				border-top-right-radius:	4px;
				border-top-left-radius:		4px;
				background-color:			#ECECEC;
				margin-bottom:				1px;
				margin-left:				13px;
				padding:					5px 7px;
				cursor:						pointer;
				float:						right;

			}



			/* Atividades na grade da categoria. */
			.conteudo-grade-item {

				background-color:	#337AB7;
				margin-bottom:		15px;
				border-radius:		5px;
				text-align:			center;
				position:			relative;
				font-size:			11px;
				padding:			8px;
				cursor:				pointer;
				width:				100%;
				color:				#FFFFFF;

			}


		</style>
	
	@endpush

	@push('scripts')

		<script type="text/javascript" src="{{ asset('js/jquery.meio.mask.js') }}"></script>
		<script type="text/javascript">


			// Ordenar atividades da grade da categoria.
			function sortByKey(array, key) {

				return array.sort(function(a, b) {

					var x = a[key];
					var y = b[key];


					if (typeof x == "string") {

						x = (""+x).toLowerCase(); 

					}


					if (typeof y == "string") {

						y = (""+y).toLowerCase();

					}


					return ((x < y) ? -1 : ((x > y) ? 1 : 0));

				});

			}



			// Função para carregar os itens da grade.
			function carregar_grade(id) {

				$('#grade-itens').fadeOut(500);
				$('#conteudo-grade').fadeOut(500, function() {

					$(this).html('<h5 class="text-center">Carregando ...</h5>').fadeIn(500, function() {

						$.ajax({

							url: "{!! route('admin-site-grades-ajax') !!}",
							type: "POST",
							dataType: 'json',
							data: { '_token': '{{ csrf_token() }}', action: 'get-grade-itens', categoria: id },
							success: function(response) {

								if(response.result == true) {

									if(response.atividades['total'] >= 1) {
										
										var $_grade		= {

											segunda: [],
											terca: [],
											quarta: [],
											quinta: [],
											sexta: [],
											sabado: [],
											domingo: []

										};

										var count	= 0;
										var itens	= response.atividades['itens'];
										var lista	= '';
										itens.forEach(function(indice, item) {

											lista += '<li id="item-' + indice['id'] + '" class="col-xs-12 mr-0 pb-4">';
												
												lista += '<div class="d-block w-100 grade-itens-lista-item-acoes">';
													
													lista += '<i onclick="addAtividade(' + indice['id'] + ')" class="fa fa-plus btn-atividade-acao" data-toggle="tooltip" data-placement="top" title="Adicionar Atividade a Grade"></i>';
													lista += '<i onclick="editAtividade(' + indice['id'] + ')" class="fa fa-pencil btn-atividade-acao" data-toggle="tooltip" data-placement="top" title="Editar Atividade"></i>';
													lista += '<i onclick="excluirAtividade(' + indice['id'] + ')" class="fa fa-trash btn-atividade-acao" data-toggle="tooltip" data-placement="top" title="Excluir Atividade"></i>';

												lista += '</div>';
												if(indice['status'] == '1') {

													lista += '<div class="bg-success d-block w-100 grade-itens-lista-item">';

												} else {
													
													lista += '<div class="bg-danger d-block w-100 grade-itens-lista-item">';

												}

													lista += '<span>' + indice['name'] + '</span>';
												
												lista += '</div>';

											lista += '</li>';

											count++;
											if(count >= response.atividades['total']) {

												var texto	= '<ul id="grade-itens-lista" class="list-inline w-100" style="display: table; width: 100%;">';
												texto	+= lista;
												texto	+= '</ul>';
												$('#grade-itens').fadeOut(500, function() {
													
													$('#grade-itens').html(texto).fadeIn(500, function() {

														if(response.atividades['grades']['total'] >= 1) {

															var conta		= 0;
															var grade		= '';
															var segunda		= 0;
															var terca		= 0;
															var quarta		= 0;
															var quinta		= 0;
															var sexta		= 0;
															var sabado		= 0;
															var domingo		= 0;

															var atividades	= response.atividades['grades']['itens'];
															atividades.forEach(function(indicex, itemx) {

																grade = '<div class="conteudo-grade-item" onclick="deleteAtividade(' + indicex['id'] + ')"><b>' + indicex['atividade'] + '</b><br />' + indicex['hora'].slice(0, 5) + '</div>';

																if(indicex['dia'] == 1) {


																	$_grade['segunda'][segunda] = { grade: grade, hora: indicex['hora'] };

																	segunda++;
																	
																} else if(indicex['dia'] == 2) {

																	$_grade['terca'][terca] = { grade: grade, hora: indicex['hora'] };

																	terca++;

																} else if(indicex['dia'] == 3) {

																	$_grade['quarta'][quarta] = { grade: grade, hora: indicex['hora'] };

																	quarta++;

																} else if(indicex['dia'] == 4) {

																	$_grade['quinta'][quinta] = { grade: grade, hora: indicex['hora'] };

																	quinta++;

																} else if(indicex['dia'] == 5) {

																	$_grade['sexta'][sexta] = { grade: grade, hora: indicex['hora'] };

																	sexta++;

																} else if(indicex['dia'] == 6) {

																	$_grade['sabado'][sabado] = { grade: grade, hora: indicex['hora'] };
																	sabado++;

																} else if(indicex['dia'] == 7) {

																	$_grade['domingo'][domingo] = { grade: grade, hora: indicex['hora'] };
																	domingo++;

																}

																conta++;
																if(conta >= response.atividades['grades']['total']) {

																	// Segunda
																	var textSegunda		= '';
																	var itensSegunda	= $_grade['segunda'];
																	if((itensSegunda.length) >= 1) {

																		itensSegunda	= sortByKey(itensSegunda, 'hora');

																		itensSegunda.forEach(function(indiceSegunda, itemSegunda) {

																			textSegunda += indiceSegunda['grade'];

																		});

																	}


																	// Terça
																	var textTerca		= '';
																	var itensTerca		= $_grade['terca'];
																	if((itensTerca.length) >= 1) {

																		itensTerca	= sortByKey(itensTerca, 'hora');
																		itensTerca.forEach(function(indiceTerca, itemTerca) {

																			textTerca += indiceTerca['grade'];

																		});

																	}



																	// Quarta
																	var textQuarta		= '';
																	var itensQuarta		= $_grade['quarta'];
																	if((itensQuarta.length) >= 1) {

																		itensQuarta	= sortByKey(itensQuarta, 'hora');
																		itensQuarta.forEach(function(indiceQuarta, itemQuarta) {

																			textQuarta += indiceQuarta['grade'];

																		});

																	}


																	// Quinta
																	var textQuinta		= '';
																	var itensQuinta		= $_grade['quinta'];
																	if((itensQuinta.length) >= 1) {

																		itensQuinta	= sortByKey(itensQuinta, 'hora');
																		itensQuinta.forEach(function(indiceQuinta, itemQuinta) {

																			textQuinta += indiceQuinta['grade'];

																		});

																	}

																	
																	// Sexta
																	var textSexta		= '';
																	var itensSexta		= $_grade['sexta'];
																	if((itensSexta.length) >= 1) {

																		itensSexta	= sortByKey(itensSexta, 'hora');
																		itensSexta.forEach(function(indiceSexta, itemSexta) {

																			textSexta += indiceSexta['grade'];

																		});

																	}

																	
																	// Sabado
																	var textSabado		= '';
																	var itensSabado		= $_grade['sabado'];
																	if((itensSabado.length) >= 1) {

																		itensSabado	= sortByKey(itensSabado, 'hora');
																		itensSabado.forEach(function(indiceSabado, itemSabado) {

																			textSabado += indiceSabado['grade'];

																		});

																	}


																	// Domingo
																	var textDomingo		= '';
																	var itensDomingo	= $_grade['domingo'];
																	if((itensDomingo.length) >= 1) {

																		itensDomingo	= sortByKey(itensDomingo, 'hora');
																		itensDomingo.forEach(function(indiceDomingo, itemDomingo) {

																			textDomingo += indiceDomingo['grade'];

																		});

																	}
																	
																	var tabela	 = '<div class="table-responsive">';
																			
																		tabela	+= '<table class="table table-bordered table-hover table-striped">';
																			
																			tabela	+= '<thead>';
																				
																				tabela	+= '<tr>';
																					
																					tabela	+= '<th style="width: 14%;" scope="col" class="text-center">Segunda</th>';
																					tabela	+= '<th style="width: 14%;" scope="col" class="text-center">Terça</th>';
																					tabela	+= '<th style="width: 14%;" scope="col" class="text-center">Quarta</th>';
																					tabela	+= '<th style="width: 14%;" scope="col" class="text-center">Quinta</th>';
																					tabela	+= '<th style="width: 14%;" scope="col" class="text-center">Sexta</th>';
																					tabela	+= '<th style="width: 14%;" scope="col" class="text-center">Sábado</th>';
																					// tabela	+= '<th style="width: 14%;" scope="col" class="text-center">Domingo</th>';

																				tabela	+= '</tr>';

																			tabela	+= '</thead>';
																			tabela	+= '<tbody>';
																				
																				tabela	+= '<tr>';
																				
																					tabela	+= '<td scope="col">' + textSegunda + '</td>';
																					tabela	+= '<td scope="col">' + textTerca + '</td>';
																					tabela	+= '<td scope="col">' + textQuarta + '</td>';
																					tabela	+= '<td scope="col">' + textQuinta + '</td>';
																					tabela	+= '<td scope="col">' + textSexta + '</td>';
																					tabela	+= '<td scope="col">' + textSabado + '</td>';
																					// tabela	+= '<td scope="col">' + textDomingo + '</td>';

																				tabela	+= '</tr>';

																			tabela	+= '</tbody>';

																		tabela	+= '</table>';

																	tabela	+= '</div>';
																	tabela	+= '<div class="row"><div class="col-xs-12"><i>* Para remover o horário de uma atividade da grade desta categoria basta clicar sobre ela.</i></div></div>';

																	$('#conteudo-grade').fadeOut(500, function() {
													
																		$('#conteudo-grade').html(tabela).fadeIn(500, function() {

																			var altura = $('#conteudo-grade').height();
																			$('#grade-itens').css('max-height', (altura - 100));
																			$('#page-loader').fadeOut(500);

																		});

																	});

																}


															});

														} else {

															$('#conteudo-grade').fadeOut(500, function() {
													
																$('#conteudo-grade').html('<h5 class="text-center">Esta categoria não possui nenhuma atividade inserida em sua grade.</h5>').fadeIn(500, function() {

																	$('#page-loader').fadeOut(500);

																});

															});

														}

													});

												});

											}

										});

									} else {

										$('#grade-itens, #conteudo-grade').fadeOut(500, function() {

											$('#grade-itens').html('<h5 class="text-center">Sem atividades cadastradas para esta categoria.</h5>');
											$('#conteudo-grade').html('<h5 class="text-center">Esta categoria não possui nenhuma atividade inserida em sua grade.</h5>');
											$('#grade-itens, #conteudo-grade').fadeIn(500, function() {

												$('#page-loader').fadeOut(500);

											});

										});

									}

								} else {

									alert(response.message);
									$('#page-loader').fadeOut(500, function() {

										window.location.href = "{!! route('admin-site-grades-index') !!}";

									});

								}

							},
							error: function(e) {

								console.log(e);
								alert('Oops! Alguma coisa deu errado, tente atualizar a página, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.');
								$('#page-loader').fadeOut(500);

							}

						});

					});

				});

			}


			
			// Função para incluir a atividade na grade.
			function addAtividade(id) {

				$('#page-loader').fadeIn(500, function() {

					$.ajax({

						url: "{!! route('admin-site-grades-ajax') !!}",
						type: "POST",
						dataType: 'json',
						data: { '_token': '{{ csrf_token() }}', action: 'get-grade-item', item: id },
						success: function(response) {

							if(response.result == true) {

								$('#modal-add-grade-content-form-categoria').val(response.item['categoria']);
								$('#modal-add-grade-content-form-id').val(response.item['id']);
								$('#modal-add-grade-content-form-hora').val("");
								$('#modal-add-grade-content-form-dia').val("").focus();
								$('#modal-add-grade-content-form-nome').html(response.item['name']);
								$('#page-loader').fadeOut(500, function() {
									
									$('#modal-add-grade').modal('show');

								});

							} else {

								alert(response.message);
								$('#page-loader').fadeOut(500, function() {

									if(response.redirect == true) {

										window.location.href = "{!! route('admin-site-grades-index') !!}";

									}

								});

							}

						},
						error: function(e) {

							console.log(e);
							alert("Erro! Por favor tente recarregar esta pagina e repetir este procedimento, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.");
							$('#page-loader').fadeOut(500);

						}

					});

				});

			}

			
			
			// Funcão para editar as informações de alguma atividade cadastrada.
			function editAtividade(id) {

				$('#page-loader').fadeIn(500, function() {

					$.ajax({

						url: "{!! route('admin-site-grades-ajax') !!}",
						type: "POST",
						dataType: 'json',
						data: { '_token': '{{ csrf_token() }}', action: 'get-grade-item', item: id },
						success: function(response) {

							if(response.result == true) {

								$('#modal-form-grade-item-titulo').html("EDITAR ATIVIDADE");
								$('#modal-form-grade-item-form-id').val(response.item['id']);
								$('#modal-form-grade-item-form-nome').val(response.item['name']);
								$('#modal-form-grade-item-form-status').val(response.item['status']);
								$('#modal-form-grade-item-form-descricao').val(response.item['descricao']);
								$('#modal-form-grade-item-salvar').attr('data-acao', 'edit');
								$('#page-loader').fadeOut(500, function() {
									
									$('#modal-form-grade-item').modal('show');

								});

							} else {

								alert(response.message);
								$('#page-loader').fadeOut(500, function() {

									if(response.redirect == true) {

										window.location.href = "{!! route('admin-site-grades-index') !!}";

									}

								});

							}

						},
						error: function(e) {

							console.log(e);
							alert("Erro! Por favor tente recarregar esta pagina e repetir este procedimento, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.");
							$('#page-loader').fadeOut(500);

						}

					});

				});

			}

			

			// Função para realizar a excluisão de alguma atividade da categoria.
			function excluirAtividade(item) {

				if(confirm('Tem certeza de qe deseja excluir essa atividade? Suas grades também serão excluidas dessa categoria ao fazer isso.')) {

					$('#page-loader').fadeIn(500, function() {
					});

				}

			}


			function deleteAtividade(grade_id) {

				if(confirm("Tem certeza de que deseja remover esta atividade da grade desta categoria?")) {

					$('#page-loader').fadeIn(500, function() {

						var categoria = $('#grade-informacoes').attr('data-categoria');
						$.ajax({

							url: "{!! route('admin-site-grades-ajax') !!}",
							type: "POST",
							dataType: 'json',
							data: { '_token': '{{ csrf_token() }}', action: 'delete-grade-item', item: grade_id },
							success: function(response) {

								alert(response.message);
								if(response.result == true) {

									carregar_grade(categoria);

								} else {

									$('#page-loader').fadeOut(500);

								}

							},
							error: function(e) {

								console.log(e);
								alert("Erro! Por favor tente recarregar esta pagina e repetir este procedimento, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.");
								$('#page-loader').fadeOut(500);

							}

						});

					});

				}

			}



			$(function() {

				// Inicia a função de tooltip nos botões de ação das atividades cadastradas na catogoria.
				$('.btn-atividade-acao').tooltip();


				// Cria mascara no campo hora do forumário de inserir atividade na grade
				$('#modal-add-grade-content-form-hora').setMask('99:99');



				// Faz o primeiro carregamento da grade da categoria aberta.
				$('#page-loader').fadeIn(500, function() {
					
					carregar_grade({!! ($body['fields'][2]['default']) !!});

				});



				// Ação de click no botão de inserir novas atividades na categoria.
				$('#conteudo-grade-cadastrar').click(function() {

					$('#modal-form-grade-item-titulo').html("CADASTRAR ATIVIDADE");
					$('#modal-form-grade-item-form-id').val("");
					$('#modal-form-grade-item-form-nome').val("");
					$('#modal-form-grade-item-form-status').val("");
					$('#modal-form-grade-item-form-descricao').val("");
					$('#modal-form-grade-item-salvar').attr('data-acao', 'create');
					$('#modal-form-grade-item').modal('show');

				});



				// Ação de click no botão de inserir/editar atividade na categoria.
				$('#modal-form-grade-item-salvar').click(function() {

					$('#modal-form-grade-item-form').submit();

				});



				// Ação de submit do formulário de inclusão/edição de atividade na categoria.
				$('#modal-form-grade-item-form').submit(function() {

					var acao		= $('#modal-form-grade-item-salvar').attr('data-acao');

					var nome		= $('#modal-form-grade-item-form-nome').val();
					var status		= $('#modal-form-grade-item-form-status').val();
					var descricao	= $('#modal-form-grade-item-form-descricao').val();

					if(nome == '' || nome == 0 || nome == '0' || nome == null || nome == 'null' || nome == undefined || nome == 'undefined') {

						if(acao == 'edit'){

							alert("Por favor digite o nome da atividade para atualizar seus dados.");

						} else {

							alert("Por favor digite o nome da atividade para cadastra-la na categoria.");

						}

						$('#modal-form-grade-item-form-nome').focus();

					} else {

						if(status == '' || status == null || status == 'null' || status == undefined || status == 'undefined') {

							if(acao == 'edit'){

								alert("Por favor selecione o status da atividade para atualizar seus dados.");

							} else {

								alert("Por favor selecione o status da atividade para cadastra-la na categoria.");

							}
							
							$('#modal-form-grade-item-form-status').focus();

						} else {

							if(descricao == '' || descricao == 0 || descricao == '0' || descricao == null || descricao == 'null' || descricao == undefined || descricao == 'undefined') {

								if(acao == 'edit'){

									alert("Por favor digite a descrição da atividade para atualizar seus dados.");

								} else {

									alert("Por favor digite a descrição da atividade para cadastra-la na categoria.");

								}
								
								$('#modal-form-grade-item-form-descricao').focus();

							} else {

								var id			= $('#modal-form-grade-item-form-id').val();
								var categoria	= $('#grade-informacoes').attr('data-categoria');

								$('#page-loader').fadeIn(500, function() {

									$.ajax({

										url: "{!! route('admin-site-grades-ajax') !!}",
										type: "POST",
										dataType: 'json',
										data: { '_token': '{{ csrf_token() }}', action: acao + '-grade-item', categoria: categoria, item: id, nome: nome, status: status, descricao: descricao },
										success: function(response) {

											alert(response.message);
											$('#modal-form-grade-item').modal('hide');
											if(response.result == true) {

												carregar_grade(categoria);

											} else {

												$('#page-loader').fadeOut(500);

											}

										},
										error: function(e) {

											console.log(e);
											alert("Erro! Por favor tente recarregar esta pagina e repetir este procedimento, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.");
											$('#page-loader').fadeOut(500);

										}

									});

								});

							}

						}

					}

					return false;

				});



				// Ação de click no botão de inserir atividade na grade da categoria.
				$('#modal-add-grade-inserir').click(function() {

					$('#modal-add-grade-content-form').submit();

				});



				// Ação de submit do formulário de inclusão de atividade na categoria da grade.
				$('#modal-add-grade-content-form').submit(function() {

					var categoria	= $('#modal-add-grade-content-form-categoria').val();
					var id			= $('#modal-add-grade-content-form-id').val();
					var dia			= $('#modal-add-grade-content-form-dia').val();
					var hora		= $('#modal-add-grade-content-form-hora').val();
					if(dia == '' || dia == 0 || dia == '0' || dia == null || dia == 'null' || dia == undefined || dia == 'undefined') {

						alert("Por favor selecione o dia da semana que deseja inserir esta atividade nesta grade.");
						$('#modal-add-grade-content-form-dia').focus();

					} else {

						if(hora == '' || hora == null || hora == 'null' || hora == undefined || hora == 'undefined') {

							alert("Por favor digite a hora do dia que deseja inserir esta atividade nesta grade.");
							$('#modal-add-grade-content-form-hora').focus();

						} else {

							$('#page-loader').fadeIn(500, function() {

								$.ajax({

									url: "{!! route('admin-site-grades-ajax') !!}",
									type: "POST",
									dataType: 'json',
									data: { '_token': '{{ csrf_token() }}', action: 'insert-grade-item', categoria: categoria, item: id, dia: dia, hora: hora },
									success: function(response) {

										alert(response.message);
										$('#modal-add-grade').modal('hide');
										if(response.result == true) {

											carregar_grade(response.categoria);

										} else {

											$('#page-loader').fadeOut(500);

										}

									},
									error: function(e) {

										console.log(e);
										alert("Erro! Por favor tente recarregar esta pagina e repetir este procedimento, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.");
										$('#page-loader').fadeOut(500);

									}

								});

							});

						}

					}

					return false;

				});


			});


		</script>

	@endpush

	<!-- Modal pra adicionar/editar um item.  -->
	<div id="modal-form-grade-item" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">

		<div class="modal-dialog modal-md">

			<div class="modal-content">

				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title text-center" id="modal-form-grade-item-titulo"></h4>

				</div>
				<div class="modal-body">

					<form id="modal-form-grade-item-form">

						<input type="hidden" id="modal-form-grade-item-form-id" name="modal-add-grade-content-form-id" value="" />
						<div class="row">

							<div class="col-xs-12 col-sm-6 mb-4" style="margin-bottom: 20px;">

								<label for="modal-form-grade-item-form-nome">Nome:</label>
								<input type="text" id="modal-form-grade-item-form-nome" name="modal-form-grade-item-form-nome" class="form-control" />
								

							</div>
							<div class="col-xs-12 col-sm-6" style="margin-bottom: 20px;">

								<label for="modal-form-grade-item-form-status">Status:</label>
								<select id="modal-form-grade-item-form-status" name="modal-form-grade-item-form-status" class="form-control">
									
									<option value="" disabled>- Selecione -</option>
									<option value="1">Ativo</option>
									<option value="0">Inativo</option>

								</select>

							</div>

						</div>
						<div class="row">

							<div class="col-xs-12 mb-4">

								<label for="modal-form-grade-item-form-descricao">Descrição:</label>
								<textarea id="modal-form-grade-item-form-descricao" name="modal-form-grade-item-form-descricao" class="form-control" style="min-height: 160px;"></textarea>
								

							</div>

						</div>

					</form>

				</div>
				<div class="modal-footer">

					<button type="button" class="btn btn-danger" data-dismiss="modal">CANCELAR</button>
					<button id="modal-form-grade-item-salvar" type="button" class="btn btn-success">SALVAR</button>

				</div>

			</div>

		</div>

	</div>



	<!-- Modal pra adicionar algum item na grade da categoria -->
	<div id="modal-add-grade" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">

		<div class="modal-dialog modal-sm">

			<div class="modal-content">

				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
					<h5 class="modal-title text-center">INSERIR ATIVIDADE NA GRADE</h5>

				</div>
				<div id="modal-add-grade-content" class="modal-body">

					<form id="modal-add-grade-content-form">

						<input type="hidden" id="modal-add-grade-content-form-categoria" name="modal-add-grade-content-form-categoria" value="" />
						<input type="hidden" id="modal-add-grade-content-form-id" name="modal-add-grade-content-form-id" value="" />
						<div class="row">

							<div class="col-xs-12 text-center">

								<h4 id="modal-add-grade-content-form-nome"></h4>

							</div>

						</div>
						<div class="row">

							<div class="col-xs-12 col-sm-6">

								<label for="modal-add-grade-content-form-dia">Dia:</label>
								<select id="modal-add-grade-content-form-dia" name="modal-add-grade-content-form-dia" class="form-control">
									
									<option value="" disabled>- Selecione -</option>
									<option value="1">Segunda-Feira</option>
									<option value="2">Terça-Feira</option>
									<option value="3">Quarta-Feira</option>
									<option value="4">Quinta-Feira</option>
									<option value="5">Sexta-Feira</option>
									<option value="6">Sábado</option>
									<!-- <option value="7">Domingo</option> -->

								</select>

							</div>
							<div class="col-xs-12 col-sm-6">

								<label for="modal-add-grade-content-form-hora">Hora:</label>
								<input type="text" id="modal-add-grade-content-form-hora" name="modal-add-grade-content-form-hora" class="form-control" />

							</div>

						</div>

					</form>

				</div>
				<div class="modal-footer">

					<button type="button" class="btn btn-danger" data-dismiss="modal">CANCELAR</button>
					<button id="modal-add-grade-inserir" type="button" class="btn btn-success">INSERIR</button>

				</div>

			</div>

		</div>

	</div>



	<div id="page-loader">

		<table>
			
			<tr>

				<td><i class="fa fa-spin fa-refresh"></i></td>

			</tr>

		</table>
		
	</div>
	<div>

		<!-- Page Title -->
		<div class="page-title">
			
			<div class="title_left">
				
				<h3>{!! $header['title'] !!} - <small>{!! $header['subtitle'] !!}</small></h3>
			
			</div>

		</div>
		<div class="clearfix"></div>
		<div class="row">
			
			<div class="col-xs-12">

				<hr size="1" style="margin: 0px 0px 10px 0px;" />

			</div>
			@if(count($header['buttons']) >= 1)
				
				<div class="col-xs-12" style="margin-bottom: 10px;">

					@foreach($header['buttons'] as $headerButton)

						{!! Helper::UserAdminButton(Auth::user()->id, $headerButton) !!}

					@endforeach
					
				</div>

			@endif

		</div>
		<div id="grade-informacoes" data-categoria="{!! ($body['fields'][2]['default']) !!}" class="row">

			<div class="col-xs-12 col-md-3" style="margin-bottom: 30px;">

				<h5>Informações da Categoria</h5>
				<hr size="1" />
				<h4 class="text-center">{!! ($body['fields'][0]['default']) !!}</h4>
				@if(($body['fields'][1]['default']) >= 1)
					
					<div class="text-center text-success">Ativo</div>

				@else

					<div class="text-center text-danger">Inativo</div>

				@endif
				<div id="grade-itens"></div>

			</div>
			<div class="col-xs-12 col-md-9">

				<h5 style="display: inline-block;">Grade da Categoria</h5>
				<button id="conteudo-grade-cadastrar" type="button" class="btn btn-primary pull-right btn-sm">Nova Atividade</button>
				<hr size="1" />
				<div id="conteudo-grade"></div>

			</div>

		</div>

	</div>


@endsection