@extends('admin.layout')
@section('content')
	
	@push('css')

		<style type="text/css">

			#page-loader {

				background-color: rgba(0, 0, 0, 0.6);
				position: fixed;
				z-index: 9999;
				display: none;
				height: 100%;
				width: 100%;
				left: 0px;
				top: 0px;

			}


			#page-loader table {

				text-align: center;
				font-size: 30px;
				position: relative;
				display: table;
				height: 100%;
				width: 100%;
				color: #FFFFFF;

			}

		</style>
	
	@endpush

	@push('scripts')

		<script type="text/javascript">

			// Função para carregar os itens da galeria.
			function carregar_galeria(id) {

				$.ajax({

					url: "{!! route('admin-site-galerias-ajax') !!}",
					type: "POST",
					dataType: 'json',
					data: { '_token': '{{ csrf_token() }}', action: 'get-itens', galeria: id },
					success: function(response) {

						if(response.result == true) {

							if(response.total >= 1) {

								var count	= 0;
								var itens	= response.itens;
								var lista	= '';
								itens.forEach(function(indice, item) {

									lista += '<li id="' + indice['id'] + '" class="text-center imagem-' + indice['id'] + ' ui-sortable-handle list-inline-item col-6 col-md-4 col-lg-3 mr-0 pb-4">';
										
										lista += '<img src="' + indice['content'] + '" class="img-responsive img-thumbnail rounded" />';
										lista += '<button type="button" class="btn btn-action btn-danger align-middle" style="margin-top: 10px;" data-content="Excluir item" onclick="excluirItem(' + indice['id'] + ')"><i class="fa fa-trash"></i></button>';

									lista += '</li>';
									count++;
									if(count >= response.total) {

										var texto 	 = '<div class="gallery">';
											texto	+= '<ul id="galeria" class="reorder-gallery list-inline w-100" style="display: table;">';
											texto	+= lista;
											texto	+= '</ul">';
											texto	+= '</div">';

										$('#conteudo-galeria').html(texto).fadeIn(500, function() {

											$('#page-loader').fadeOut(500);

										});
									}

								});

							} else {

								$('#conteudo-galeria').html('<h5 class="text-center" style="font-weight: 300;">Esta galeria está vazia!</h5>').css('display', 'block');
								$('#page-loader').fadeOut(500);

							}

						} else {

							alert(response.message);
							$('#page-loader').fadeOut(500, function() {

								if(response.redirect == true) {

									window.location.href = "{!! route('admin-site-galerias-index') !!}";

								}

							});

						}

					},
					error: function(e) {

						console.log(e);
						alert('Oops! Alguma coisa deu errado, tente atualizar a página, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.');
						$('#page-loader').fadeOut(500);

					}

				});

			}


			function updateOrder() {
				
				var item_order = new Array();
				$('ul.reorder-gallery li').each(function() {
					
					item_order.push($(this).attr("id"));
				
				});
				
				var order_string = item_order;
				$.ajax({
					
					url: "{{ route('admin-site-galerias-ajax') }}",
					type: "POST",
					data: { '_token': '{{ csrf_token() }}', action: 'mudar-ordem', galeria: "{!! ($body['fields'][3]['default']) !!}", itens: order_string },
					success: function(response){

						if(response.result == false) {

							alert("Erro! A ordem dos itens não pode ser alterada!");

						}

					}
				
				});
			
			}


			function excluirItem(item) {

				$('#page-loader').fadeIn(500, function() {

					$.ajax({
						
						url: "{{ route('admin-site-galerias-ajax') }}",
						type: "POST",
						data: { '_token': '{{ csrf_token() }}', action: 'delete-file', item: item },
						success: function(response){

							alert(response.message);
							if(response.result == true) {

								carregar_galeria(response.galeria);

							} else {

								$('#page-loader').fadeOut(500);

							}

						}
					
					});

				});

			}
			
			$(function() {

				// Faz o primeiro carregamento dos itens da galeria quando a página é carregada.
				$('#page-loader').fadeIn(500, function() {
					
					carregar_galeria({!! ($body['fields'][3]['default']) !!});

				});

				
				// Ação de click no botão para upload de novo item da galeria.
				$('#enviar').click(function() {

					$('#enviar-arquivo-input').click();

				});


				// Inicio do evento de upload de novos itens para a galeria.
				$("#enviar-arquivo-input").change(function() {

					var foto = $(this).val();
					if(foto != '') {

						$('#enviar-arquivo').submit();

					}

				});


				// Submit do formulario de upload de nova foto
				$("#enviar-arquivo").on('submit',(

					function(e) {
					
						e.preventDefault();
							
						$.ajax({
							
							url: "{{ route('admin-site-galerias-ajax') }}",
							type: "POST",
							data:  new FormData(this),
							contentType: false,
							cache: false,
							processData:false,
							beforeSend : function() {

								$('#page-loader').fadeIn(500);

							},
							success: function(response) {

								console.log(response);
								if(response.result == true) {

									carregar_galeria(response.galeria);

								} else {

									alert(response.message);
									$('#page-loader').fadeOut(500, function() {

										if(response.redirect == true) {

											window.location.href = "{!! route('admin-site-galerias-index') !!}";

										}

									});

								}
								

							},
							error: function(e) {

								console.log(e);
								alert("Oops! O envio do arquivo não pode ser realizado, por favor tente recarregar esta pagine e repetir este procedimento, caso esta mensagem volte a aparecer por favor entre em contato com seu desenvolvedor.");
								$('#page-loader').fadeOut(500);

							}

						});

					}

				));

			});

		</script>

	@endpush

	<div id="page-loader">

		<table>
			
			<tr>

				<td><i class="fa fa-spin fa-refresh"></i></td>

			</tr>

		</table>
		
	</div>
	<div>

		<!-- Page Title -->
		<div class="page-title">
			
			<div class="title_left">
				
				<h3>{!! $header['title'] !!} - <small>{!! $header['subtitle'] !!}</small></h3>
			
			</div>

		</div>
		<div class="clearfix"></div>
		<div class="row">
			
			<div class="col-xs-12">

				<hr size="1" style="margin: 0px 0px 10px 0px;" />

			</div>
			@if(count($header['buttons']) >= 1)
				
				<div class="col-xs-12" style="margin-bottom: 10px;">

					@foreach($header['buttons'] as $headerButton)

						{!! Helper::UserAdminButton(Auth::user()->id, $headerButton) !!}

					@endforeach
					
				</div>

			@endif

		</div>
		<div class="row">

			<div class="col-xs-12 col-md-3" style="margin-bottom: 30px;">

				<h5>Informações da Galeria</h5>
				<hr size="1" />
				<img src="{!! ($body['fields'][0]['default']) !!}" class="img-responsive" />
				<h4 class="text-center">{!! ($body['fields'][1]['default']) !!}</h4>
				{!! ($body['fields'][2]['default']) !!}
				@if(($body['fields'][3]['default']) >= 1)
					
					<div class="text-center text-success">Ativo</div>

				@else

					<div class="text-center text-danger">Inativo</div>

				@endif

			</div>
			<div class="col-xs-12 col-md-9">

				<h5 style="display: inline-block;">Conteudo da Galeria</h5>
				<form id="enviar-arquivo" method="POST" enctype="multipart/form-data" style="display: none;">

					@csrf
					<input type="hidden" id="galeria" name="galeria" value="{!! ($body['fields'][3]['default']) !!}" />
					<input type="hidden" id="action" name="action" value="upload-file" />
					<input type="file" id="enviar-arquivo-input" name="imagem" />
					
				</form>
				<button id="enviar" type="button" class="btn btn-primary pull-right btn-sm">Enviar Foto ou Video</button>
				<hr size="1" />
				<div id="conteudo-galeria" style="display: none;"></div>

			</div>

		</div>

	</div>


@endsection