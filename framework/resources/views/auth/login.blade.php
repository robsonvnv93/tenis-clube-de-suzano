<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

	<head>
		
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		 
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		 
		<title>{{ config('app.name', 'Laravel') }}</title>


		<!-- Bootstrap core CSS -->
		 
		<link rel="stylesheet" href="{{ asset('css/app.css') }}" />
		<link rel="stylesheet" href="{{ asset('fonts/css/font-awesome.min.css') }}" />
		<link rel="stylesheet" href="{{ asset('css/animate.min.css') }}" />
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
		

		<!-- Fonts -->
		<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito" />
		
		 
		<!--[if lt IE 9]>
			
			<script src="../assets/js/ie8-responsive-file-warning.js"></script>

		<![endif]-->
		 
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

		<![endif]-->
		<!-- ######################## -->


		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}" defer></script>

	</head>
	<body class="login">
		
		{{ Helper::RegisterAdminLog('Acessou a Página') }}
		<div id="app">

			<a class="hiddenanchor" id="signup"></a>
			<a class="hiddenanchor" id="signin"></a>
			<div class="login_wrapper">

				<div class="animate form login_form">

					<section class="login_content">

						<form method="POST" action="{{ route('login') }}">

							@csrf
							<h1>{{ config('app.name', 'Laravel') }}</h1>

							<div>

								@if ($errors->has('email'))
									
									<span class="invalid-feedback" role="alert">
										
										<strong>{{ $errors->first('email') }}</strong>
									
									</span>

								@endif
								<input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Usuário" value="{{ old('email') }}" autofocus required />

							</div>
							<div>

								@if ($errors->has('password'))

									<span class="invalid-feedback" role="alert">

										<strong>{{ $errors->first('password') }}</strong>

									</span>

								@endif
								<input type="password" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Senha" required />

							</div>
							<div class="text-left mb-4">

								<input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} />
								<label for="remember">Manter-me conectado</label>

							</div>
							<div>

								<button type="submit" class="btn btn-default submit">Login</button>
								<a class="reset_pass" href="#">Esqueci minha senha</a>

							</div>
							<div class="clearfix"></div>

						</form>

					</section>

				</div>

			</div>

		</div>

	</body>

</html>