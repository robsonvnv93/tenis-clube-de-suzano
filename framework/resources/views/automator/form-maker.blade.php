@extends('admin.layout')
@section('content')
	
	@push('css')

		<style type="text/css">

			label { display: table; width: 100%; }
			.checkbox-label { display: inline; width: 100%; margin-left: 5px; font-weight: normal; }
			input[type="text"],
			select,
			textarea { margin-bottom: 15px; }

		</style>

	@endpush
	<div>

		<!-- Page Title -->
		<div class="page-title">
			
			<div class="title_left">
				
				<h3>{!! $header['title'] !!} - <small>{!! $header['subtitle'] !!}</small></h3>
			
			</div>

		</div>
		<div class="clearfix"></div>
		<div class="row">
			
			<div class="col-xs-12">

				<hr size="1" style="margin: 0px 0px 10px 0px;" />

			</div>
			@if(count($header['buttons']) >= 1)
				
				<div class="col-xs-12" style="margin-bottom: 10px;">

					@foreach($header['buttons'] as $headerButton)

						{!! Helper::UserAdminButton(Auth::user()->id, $headerButton) !!}

					@endforeach
					
				</div>

			@endif

		</div>
		<div class="row">

			<div class="col-xs-12">

				<form method="POST" action="{{ $body['action'] }}" enctype="multipart/form-data">

					@csrf
					@if(isset($body['indice']))
						
						<input type="hidden" name="id" value="{!! $body['indice'] !!}" />

					@endif

					@foreach($body['fields'] as $indice => $item)

						<label for="campo-{{ $indice }}">{!! $item['name'] !!} @if($item['required'] == true) * @endif</label>
						@if($item['type'] == 'simple-text')

							@if($item['required'] == true)
								
								@if(isset($item['default']))
									
									@if(isset($item['input-mask']))
									
										@if(isset($item['maxlength']))
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" data-inputmask="'mask' : '{!! $item['input-mask'] !!}'" class="form-control" required value="{!! $item['default'] !!}" maxlength="{!! $item['maxlength'] !!}" />
										
										@else
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" data-inputmask="'mask' : '{!! $item['input-mask'] !!}'" class="form-control" required value="{!! $item['default'] !!}" />
										
										@endif

									@else
										
										@if(isset($item['maxlength']))
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" required value="{!! $item['default'] !!}" maxlength="{!! $item['maxlength'] !!}" />
										
										@else
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" required value="{!! $item['default'] !!}" />
										
										@endif

									@endif

								@else
									
									@if(isset($item['input-mask']))
										
										@if(isset($item['maxlength']))
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" maxlength="{{ $item['maxlength'] }}" data-inputmask="'mask' : '{!! $item['input-mask'] !!}'" class="form-control" required />

										@else
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" data-inputmask="'mask' : '{!! $item['input-mask'] !!}'" class="form-control" required />
										
										@endif
									
									@else
										
										@if(isset($item['maxlength']))
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" maxlength="{{ $item['maxlength'] }}" class="form-control" required />
										
										@else
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" required />

										@endif

									@endif

								@endif

							@else
								
								@if(isset($item['default']))
									
									@if(isset($item['input-mask']))

										@if(isset($item['maxlength']))
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" data-inputmask="'mask' : '{!! $item['input-mask'] !!}'" class="form-control" value="{!! $item['default'] !!}" maxlength="{!! $item['maxlength'] !!}" />
										
										@else
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" data-inputmask="'mask' : '{!! $item['input-mask'] !!}'" class="form-control" value="{!! $item['default'] !!}" />
										
										@endif
										
									@else
										
										@if(isset($item['maxlength']))
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" value="{!! $item['default'] !!}" maxlength="{!! $item['maxlength'] !!}" />

										@else
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" value="{!! $item['default'] !!}" />

										@endif

									@endif

								@else

									@if(isset($item['input-mask']))
										
										@if(isset($item['maxlength']))
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" maxlength="{{ $item['maxlength'] }}" data-inputmask="'mask' : '{!! $item['input-mask'] !!}'" class="form-control" />

										@else
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" data-inputmask="'mask' : '{!! $item['input-mask'] !!}'" class="form-control" />

										@endif
										
									@else
										
										@if(isset($item['maxlength']))
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" maxlength="{{ $item['maxlength'] }}" class="form-control" />

										@else
											
											<input type="text" id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" />

										@endif

									@endif

								@endif

							@endif

						@elseif($item['type'] == 'checkbox')

							@if(count($item['options']) >= 1)

								<div class="row">
									
									@foreach($item['options'] as $opt => $option)

										<div class="col-xs-12 col-md-6">

											@if(isset($item['default']))

												@if(in_array($option['value'], $item['default']))

													<input type="checkbox" id="{!! $item['field'] !!}-{{ $opt }}" name="{!! $item['field'] !!}[]" value="{!! $option['value'] !!}" checked /> <label class="checkbox-label" for="{!! $item['field'] !!}-{{ $opt }}">{!! $option['text'] !!}</label>

												@else

													<input type="checkbox" id="{!! $item['field'] !!}-{{ $opt }}" name="{!! $item['field'] !!}[]" value="{!! $option['value'] !!}" /> <label class="checkbox-label" for="{!! $item['field'] !!}-{{ $opt }}">{!! $option['text'] !!}</label>

												@endif

											@else

												<input type="checkbox" id="{!! $item['field'] !!}-{{ $opt }}" name="{!! $item['field'] !!}[]" value="{!! $option['value'] !!}" /> <label class="checkbox-label" for="{!! $item['field'] !!}-{{ $opt }}">{!! $option['text'] !!}</label>
												
											@endif

										</div>

									@endforeach

								</div>

							@else

								<div class="row">
									
									<div class="col-xs-12">
										
										Nenhuma opção disponivel
								
									</div>

								</div>
								
							@endif
							<br />
							
						@elseif($item['type'] == 'select')

							@if($item['required'] == true)
								
								<select id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" required>

									@if(!isset($item['default']))
										
										<option value="" disabled selected>- Selecione -</option>

									@else
										
										<option value="" disabled>- Selecione -</option>

									@endif

							@else
								
								<select id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control">

									<option value="">- Selecione -</option>

							@endif

								@foreach($item['options'] as $option)

									@if(isset($item['default']))
										
										@if($option['value'] == $item['default'])
											
											<option value="{!! $option['value'] !!}" selected>{!! $option['text'] !!}</option>
											
										@else
											
											<option value="{!! $option['value'] !!}">{!! $option['text'] !!}</option>

										@endif
										
									@else
										
										<option value="{!! $option['value'] !!}">{!! $option['text'] !!}</option>

									@endif

								@endforeach
								
							</select>

						@elseif($item['type'] == 'textarea')

							@if($item['required'] == true)
								
								@if(isset($item['default']))
								
									<textarea id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" required>{!! $item['default'] !!}</textarea>

								@else
									
									<textarea id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" required></textarea>

								@endif

							@else
								
								@if(isset($item['default']))
									
									<textarea id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control">{!! $item['default'] !!}</textarea>
									
								@else
									
									<textarea id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control"></textarea>
								
								@endif

							@endif

						@elseif($item['type'] == 'file-upload')

							@if($item['required'] == true)
								
								@if(isset($item['default']))
								
									<img src="{!! $item['default'] !!}" class="img-responsive" />
									<br />
									<input type="file" id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" required />

								@else
									
									<input type="file" id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" required />

								@endif

							@else
								
								@if(isset($item['default']))
									
									<img src="{!! $item['default'] !!}" class="img-responsive" />
									<br />
									<input type="file" id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" />

								@else

									<input type="file" id="campo-{{ $indice }}" name="{{ $item['field'] }}" class="form-control" />

								@endif

							@endif
							<br />

						@elseif($item['type'] == 'editor')

							<!-- AUTOMATOR: EDITOR -->
							<div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#campo-{{ $indice }}-editor">

								<div class="btn-group">

									<a class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height"></i>&nbsp;<b class="caret"></b></a>
									<ul class="dropdown-menu">
										
										<li><a data-edit="fontSize 5" class="fs-Five">Huge</a></li>
										<li><a data-edit="fontSize 3" class="fs-Three">Normal</a></li>
										<li><a data-edit="fontSize 1" class="fs-One">Small</a></li>
									
									</ul>
								
								</div>
								<div class="btn-group">
									
									<a class="btn btn-default" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
									<a class="btn btn-default" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
									<a class="btn btn-default" data-edit="strikethrough" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
									<a class="btn btn-default" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
								
								</div>
								<div class="btn-group">
									
									<a class="btn btn-default" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
									<a class="btn btn-default" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
									<a class="btn btn-default" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-outdent"></i></a>
									<a class="btn btn-default" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
								
								</div>
								<div class="btn-group">
									
									<a class="btn btn-default" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
									<a class="btn btn-default" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
									<a class="btn btn-default" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
									<a class="btn btn-default" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
								
								</div>
								<div class="btn-group">

									<a class="btn btn-default dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="fa fa-link"></i></a>
									<div class="dropdown-menu input-append">

										<input placeholder="URL" type="text" data-edit="createLink" />
										<button class="btn" type="button">Add</button>

									</div>

								</div>
								<div class="btn-group">
									
									<a class="btn btn-default" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
									<a class="btn btn-default" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
								
								</div>
							
							</div>
							@if(isset($item['default']))

								<div id="campo-{{ $indice }}-editor" class="editor-wrapper">{!! $item['default'] !!}</div>
								<textarea style="display: none;" name="{{ $item['field'] }}" id="campo-{{ $indice }}">{!! $item['default'] !!}</textarea>

							@else

								<div id="campo-{{ $indice }}-editor" class="editor-wrapper"></div>
								<textarea style="display: none;" name="{{ $item['field'] }}" id="campo-{{ $indice }}"></textarea>

							@endif
							<br />
							@push('scripts')

								<script type="text/javascript">

									$('#campo-{{ $indice }}-editor').wysiwyg().on('change', function() {

										$('#campo-{{ $indice }}').html($(this).cleanHtml());

									});


									$(".dropdown-menu > input").click(function (e) {

										e.stopPropagation();

									});
									
								</script>

							@endpush

						@endif

					@endforeach
					<br />
					<input type="submit" class="btn btn-success center-block" value="Salvar" />

				</form>

			</div>

		</div>

	</div>

@endsection