@extends('admin.layout')
@section('content')
	
	<div>

		<!-- Page Title -->
		<div class="page-title">
			
			<div class="title_left">
				
				<h3>{!! $header['title'] !!} - <small>{!! $header['subtitle'] !!}</small></h3>
			
			</div>

		</div>
		<div class="clearfix"></div>
		<div class="row">
			
			<div class="col-xs-12">

				<hr size="1" style="margin: 0px 0px 10px 0px;" />

			</div>
			@if(count($header['buttons']) >= 1)
				
				<div class="col-xs-12" style="margin-bottom: 10px;">

					@foreach($header['buttons'] as $headerButton)

						{!! Helper::UserAdminButton(Auth::user()->id, $headerButton) !!}

					@endforeach
					
				</div>

			@endif

		</div>
		<div class="row">

			<div class="col-xs-12">

				@if(isset($body['fields']))

					@foreach($body['fields'] as $indice => $item)

						<label for="campo-{!! $indice !!}">{!! $item['name'] !!}</label>
						<br />
						@if($item['type'] == 'simple-text' || $item['type'] == 'textarea' || $item['type'] == 'editor')

							@if($item['default'] != '')
								
								{!! $item['default'] !!}
							
							@else

								- - -
							
							@endif
							<br />
							<br />

						@elseif($item['type'] == 'file-image')

							@if($item['default'] != '')
								
								<img src="{!! $item['default'] !!}" class="img-responsive" />
							
							@else

								- - -
							
							@endif
							<br />
							<br />

						@elseif($item['type'] == 'checkbox')

							<div class="row">
								
								@if(isset($item['default']))

									@if(count($item['default']) >= 1)

										@foreach($item['default'] as $valor)

											@foreach($item['options'] as $option)

												@if($option['value'] == $valor)

													<div class="col-xs-12 col-md-6">{!! $option['text'] !!}</div>

												@endif

											@endforeach
										@endforeach

									@else
										
										<div class="col-xs-12">- - -</div>

									@endif

								@else

									<div class="col-xs-12">- - -</div>

								@endif

							</div>
							<br />
							
						@elseif($item['type'] == 'select')

							<div class="row">
								<div class="col-xs-12">

									@if($item['default'] != '')
										
										@foreach($item['options'] as $option)

											@if($option['value'] == $item['default'])

												@if(isset($option['class']))
													
													<span class="{!! $option['class'] !!}">{!! $option['text'] !!}</span>

												@else
													
													{!! $option['text'] !!}

												@endif

											@endif

										@endforeach
									
									@else

										- - -
									
									@endif

								</div>

							</div>
							<br />
							
						@endif

					@endforeach

				@endif

			</div>

		</div>

	</div>


@endsection