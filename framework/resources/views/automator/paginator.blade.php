@extends('admin.layout')
@section('content')
	
	@push('scripts')

		<script type="text/javascript">

			$(function() {

				$('.btn-icon').tooltip({'trigger': 'hover'});

				$('.btn-delete').click(function() {

					if(confirm("Tem certeza de que deseja excluir este registro? Esta ação não poderá ser desfeita")) {

						var link	= $(this).attr('href');
						var quebra	= link.split('?');
						var url		= quebra[0];
						var item	= quebra[1];

						$('#page-loader').fadeIn(500, function() {

							$.ajax({
							
								url: url,
								type: "POST",
								dataType:	"json",
								data: { '_token': '{{ csrf_token() }}', itens: item },
								success: function(response){

									console.log(response);
									alert(response.message);
									if(response.result == true) {

										location.reload();

									} else {

										$('#page-loader').fadeOut(500);
										
									}


								},
								error: function(e) {

									alert("Oops! Alguma coisa deu errado por favor entre em contato com seu desenvolvedor.");
									$('#page-loader').fadeOut(500);

								}
							
							});

						});

					}

					return false;

				});


				$('#paginator-btn-delete').click(function() {

					var livre = $(this).attr('disabled');
					console.log(livre);
					if(livre == false || livre == undefined) {

						$('#paginator-list-form').submit();

					}

				});


				$("#paginator-checkall").click(function() {

					var status = $(this).prop('checked');
					$(".paginator-list-item").each(function() {

						$(this).prop('checked', status);

					});

					if(status == true) {

						$('#paginator-btn-delete').attr('disabled', false);

					} else {
						
						$('#paginator-btn-delete').attr('disabled', true);

					}

				});


				$(".paginator-list-item").click(function() {

					var status = $(this).prop('checked');
					if (status == true) {

						var total = 1;

					} else {
						
						var total = 0;

					}

					$(".paginator-list-item").each(function() {

						item = $(this).prop('checked');
						if (item == true) {

							total++;

						}

					});

					if (total >= 1) {

						$('#paginator-btn-delete').attr('disabled', false);

					} else {

						$('#paginator-btn-delete').attr('disabled', true);
						$("#paginator-checkall").prop('checked', false);

					}

				});

			});
			
		</script>

	@endpush

	@push('css')

		<style type="text/css">

			.btn-automator { margin: 10px; }
			.table-paginator thead > tr > th,
			.table-paginator tbody > tr > td { vertical-align: middle; }
			#paginator-search-cancel { border-radius: 100%; margin-top: 6px; margin-left: 10px; }
			#page-loader {

				background-color: rgba(0, 0, 0, 0.6);
				position: fixed;
				z-index: 9999;
				display: none;
				height: 100%;
				width: 100%;
				left: 0px;
				top: 0px;

			}


			#page-loader table {

				text-align: center;
				font-size: 30px;
				position: relative;
				display: table;
				height: 100%;
				width: 100%;
				color: #FFFFFF;

			}
			
		</style>

	@endpush

	<div id="page-loader">

		<table>
			
			<tr>

				<td><i class="fa fa-spin fa-refresh"></i></td>

			</tr>

		</table>
		
	</div>
	<div class>

		<!-- Page Title -->
		<div class="page-title">
			
			<div class="title_left">
				
				<h3>{!! $header['title'] !!} - <small>{!! $header['subtitle'] !!}</small></h3>
			
			</div>


			@if(isset($search))

				@if($search != '')

					@push('scripts')

						<script type="text/javascript">

							$('#paginator-search-form').submit(function() {

								var busca = $('#paginator-search-input').val();
								if(busca == '') {

									alert("Por favor digite algo para realizar a busca!");
									$('#paginator-search-input').focus();
									return false;

								}

							});
							
						</script>

					@endpush

					<div class="title_right">
						
						<div class="col-md-6 col-sm-5 col-xs-12 form-group pull-right top_search">
							
							@if(isset($busca))

								@if($busca != '')

									<a id="paginator-search-cancel" href="{{ $search }}" data-title="Cancelar Busca" data-placement="left" class="btn btn-icon btn-xs btn-danger pull-right"><i class="fa fa-times"></i></a>

								@endif

							@endif
							<form id="paginator-search-form" action="{{ $search }}" method="GET">
								
								<div class="input-group">
								
									<input id="paginator-search-input" type="text" name="busca" class="form-control" value="{{ $busca }}" placeholder="Buscar..." />
									<span class="input-group-btn">
										
										<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
									
									</span>

								</div>

							</form>
						
						</div>
					
					</div>

				@endif

			@endif

		</div>
		<div class="clearfix"></div>
		<div class="row">
			
			<div class="col-xs-12">

				<hr size="1" style="margin: 0px 0px 10px 0px;" />

			</div>
			@if(count($header['buttons']) >= 1)
				
				<div class="col-xs-12" style="margin-bottom: 10px;">

					@foreach($header['buttons'] as $headerButton)

						{!! Helper::UserAdminButton(Auth::user()->id, $headerButton) !!}

					@endforeach
					@if(isset($body['destroy']))

						@if(Helper::UserCanAccessRoute(Auth::user()->id, $body['destroy']))

							<button type="button" id="paginator-btn-delete" class="btn btn-automator btn-danger btn-sm pull-right" disabled="">Excluir Selecionado(s)</button>

						@endif
						
					@endif
					
				</div>

			@else

				@if(isset($body['destroy']))

					@if(Helper::UserCanAccessRoute(Auth::user()->id, $body['destroy']))

						<div class="col-xs-12" style="margin-bottom: 10px;">
							
							<button type="button" id="paginator-btn-delete" class="btn btn-danger btn-sm pull-right" disabled="">Excluir Selecionado(s)</button>

						</div>

					@endif
					
				@endif

			@endif
			<div class="col-xs-12">

				@if(isset($body))

					@if(isset($body['destroy']))

						@if(Helper::UserCanAccessRoute(Auth::user()->id, $body['destroy']))

							<form id="paginator-list-form" method="POST" action="{!! route($body['destroy']) !!}">

								@csrf

						@endif

					@endif
					<div class="table-responsive">

						<table class="table table-paginator table-striped table-hover table-bordered">
							
							<thead>

								<tr>

									@if(isset($body['destroy']))

										@if(Helper::UserCanAccessRoute(Auth::user()->id, $body['destroy']))

											<th class="text-center"><input type="checkbox" id="paginator-checkall" /></th>

										@endif
										
									@endif
									@foreach($body['cols'] as $col)

										@if(isset($col['class']))
													
											@if($col['class'] != '')
												
												<th class="{{ $col['class'] }}">
													
											@else
												
												<th>

											@endif
												
										@else
											
											<th>

										@endif

											{!! $col['name'] !!}

										</th>

									@endforeach
									@if(isset($body['actions']))

										@if(count($body['actions']) >= 1)

											<th class="text-center" width="20%">Ações</th>

										@endif

									@endif

								</tr>
								
							</thead>
							<tbody>

								@if(count($data) >= 1)

									@foreach($data as $item)

										<tr>

											@if(isset($body['destroy']))

												@if(Helper::UserCanAccessRoute(Auth::user()->id, $body['destroy']))

													<td class="text-center paginator-w-5"><input type="checkbox" name="itens[]" class="paginator-list-item" value="{{ $item->id }}" /></td>

												@endif
												
											@endif
											@foreach($body['cols'] as $col)

												@if(isset($col['class']))
													
													@if($col['class'] != '')
														
														<td class="{{ $col['class'] }}">
															
													@else
														
														<td>

													@endif
														
												@else
													
													<td>

												@endif
													
													@if($col['type'] == 'simple-text')
													
														@if(data_get($item, $col['field']) != '')
															
															@if(isset($col['mask']))
																
																@if($col['mask'] == 'datamask')

																	{!! date('d/m/Y', strtotime(data_get($item, $col['field']))) !!}

																@endif
																
																@if($col['mask'] == 'datetime')

																	{!! date('d/m/Y - H:i:s', strtotime(data_get($item, $col['field']))) !!}

																@endif
																
																@if($col['mask'] == 'datetime2')
																	
																	{!! date('d/m/Y H:i:s', strtotime(data_get($item, $col['field']))) !!}

																@endif
																
															@else

																{!! data_get($item, $col['field']) !!}

															@endif

														@else

															- - -

														@endif

													@elseif($col['type'] == 'related-text')

														@if(data_get($item, $col['field']) != '')
														
															{!! Automator::RelatedValue($col['table'], $col['text'], data_get($item, $col['field'])) !!}

														@else

															- - -

														@endif

													@elseif($col['type'] == 'file-image')

														@if(data_get($item, $col['field']) != '')
															
															@if(isset($col['directory']))
																
																@if(isset($col['hasID']))

																	<img src="{!! asset($col['directory'] . $item->id . '/' . (data_get($item, $col['field']))) !!}" class="img-responsive" />

																@else
																	
																	<img src="{!! asset($col['directory'] . (data_get($item, $col['field']))) !!}" class="img-responsive" />

																@endif

															@else

																<img src="{!! asset(data_get($item, $col['field'])) !!}" class="img-responsive" />

															@endif

														@else

															- - -

														@endif

													@elseif($col['type'] == 'enum-text')

														@if(data_get($item, $col['field']) != '')
															
															@if(isset($col['options']))
																
																@if(count($col['options']) >= 1)
																	
																	@foreach($col['options'] as $opt)

																		@if(isset($opt['value']))

																			@if($opt['value'] == (data_get($item, $col['field'])))

																				@if(isset($opt['text']))

																					@if($opt['text'] != '')

																						@if(isset($opt['class']))

																							@if($opt['class'] != '')

																								<span class="{{ $opt['class'] }}">{!! $opt['text'] !!}</span>

																							@else
																								
																								{!! $opt['text'] !!}

																							@endif

																						@else

																							{!! $opt['text'] !!}

																						@endif

																					@else
																						
																						- - -

																					@endif

																				@else

																					- - -

																				@endif

																			@endif

																		@endif

																	@endforeach

																@else
																	
																	- - -

																@endif

															@else

																- - -

															@endif

														@else

															- - -

														@endif

													@endif

												</td>

											@endforeach
											@if(isset($body['actions']))

												@if(count($body['actions']) >= 1)

													<td class="text-center">

														@foreach($body['actions'] as $act)

															{!! Helper::UserAdminButton(Auth::user()->id, $act, $item->id) !!}

														@endforeach

													</td>

												@endif

											@endif

										</tr>

									@endforeach

								@else

									@if(isset($body['destroy']))

										@if(Helper::UserCanAccessRoute(Auth::user()->id, $body['destroy']))

											@if(isset($body['actions']))

												@if(count($body['actions']) >= 1)

													<tr>

														<td class="text-center" colspan="{{ (count($body['cols']) + 2) }}">Nenhum registro encontrado!</td>
														
													</tr>

												@else

													<tr>

														<td class="text-center" colspan="{{ (count($body['cols']) + 1) }}">Nenhum registro encontrado!</td>
														
													</tr>

												@endif

											@else

												<tr>

													<td class="text-center" colspan="{{ (count($body['cols']) + 1) }}">Nenhum registro encontrado!</td>
													
												</tr>

											@endif

										@else

											@if(isset($body['actions']))

												@if(count($body['actions']) >= 1)

													<tr>

														<td class="text-center" colspan="{{ (count($body['cols']) + 1) }}">Nenhum registro encontrado!</td>
														
													</tr>

												@else

													<tr>

														<td class="text-center" colspan="{{ count($body['cols']) }}">Nenhum registro encontrado!</td>
														
													</tr>

												@endif

											@else

												<tr>

													<td class="text-center" colspan="{{ count($body['cols']) }}">Nenhum registro encontrado!</td>
													
												</tr>

											@endif

										@endif

									@else

										@if(isset($body['actions']))

											@if(count($body['actions']) >= 1)

												<tr>

													<td class="text-center" colspan="{{ (count($body['cols']) + 1) }}">Nenhum registro encontrado!</td>
													
												</tr>

											@else

												<tr>

													<td class="text-center" colspan="{{ count($body['cols']) }}">Nenhum registro encontrado!</td>
													
												</tr>

											@endif

										@else

											<tr>

												<td class="text-center" colspan="{{ count($body['cols']) }}">Nenhum registro encontrado!</td>
												
											</tr>

										@endif

									@endif

								@endif
								
							</tbody>

						</table>
						
					</div>
					@if(isset($body['destroy']))

						@if(Helper::UserCanAccessRoute(Auth::user()->id, $body['destroy']))

							</form>

						@endif

					@endif

					@if(count($data) >= 1)

						<div class="text-center">

							@if(isset($busca))
								
								@if($busca != '')
									
									{!! $data->appends(['busca' => $busca])->links() !!}

								@else
									
									{!! $data->links() !!}

								@endif

							@else

								{!! $data->links() !!}
								
							@endif

						</div>

					@endif

				@else

					<h3 class="text-center">Erro nas configurações do Paginator.</h3>

				@endif

			</div>

		</div>

	</div>

@endsection